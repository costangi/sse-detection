import os
from os import listdir
from os.path import isfile, join

import matplotlib.colors as mcolors
import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.basemap import Basemap

from sse_detector.sse_detector import SSEdetector
from sse_generator.artificial_noise_cascadia import n_surrogates_stack_pca_lib
from sse_generator.okada import forward as okada85
from sse_generator.synthetic_sse_cascadia import sigmoidal_rise_time
from utils import cascadia_filtered_stations, _preliminary_operations, tremor_catalogue, read_from_slab2

np.random.seed(42)

plt.rc('font', family='Helvetica')

SMALL_SIZE = 8 + 2 + 0
LEGEND_SIZE = 8 + 2 + 0
MEDIUM_SIZE = 10 + 2 + 0
BIGGER_SIZE = 12 + 2 + 0

plt.rc('font', size=SMALL_SIZE)  # controls default text sizes
plt.rc('axes', titlesize=SMALL_SIZE)  # fontsize of the axes title
plt.rc('axes', labelsize=MEDIUM_SIZE)  # fontsize of the x and y labels
plt.rc('xtick', labelsize=SMALL_SIZE)  # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)  # fontsize of the tick labels
plt.rc('legend', fontsize=LEGEND_SIZE)  # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title


def _custom_synthetic_displacement_stations_cascadia(i, depth_list, strike_list, dip_list, station_coordinates,
                                                     epi_lat=48.5, epi_lon=-125,
                                                     **params):
    """Generates one set of synthetic displacements by applying Okada model (Okada, 1985) on an event placed on
    the slab (slab2) of the Cascadia subduction, registered at a given set of stations. The slab information
    is passed in input as {depth_list}, {strike_list} and {dip_list}"""
    # valid_depth_idx = np.logical_and(depth_list[..., 2] > -30, depth_list[..., 2] < -20)
    # depth_list = depth_list[valid_depth_idx]
    # strike_list = strike_list[valid_depth_idx]
    # dip_list = dip_list[valid_depth_idx]

    # random_idx_slab = np.random.randint(low=0, high=depth_list.shape[0])
    idx_slab = np.logical_and(depth_list[:, 0] == epi_lon, depth_list[:, 1] == epi_lat)
    hypo_depth = - depth_list[idx_slab, 2]  # - depth_list[random_idx_slab, 2]
    depth_variability = 0  # -10 + 20 * params['uniform_vector'][i, 0]
    if hypo_depth > 14.6:
        hypo_depth = hypo_depth + depth_variability
    if hypo_depth < 0:
        raise Exception('Negative depth')
    strike = strike_list[idx_slab, 2]  # strike_list[random_idx_slab, 2]
    dip = dip_list[idx_slab, 2]  # dip_list[random_idx_slab, 2]
    rake = 90  # 75 + 25 * params['uniform_vector'][i, 1]  # rake from 75 to 100 deg
    min_mw, max_mw = 5, 7
    if 'magnitude_range' in params:
        min_mw, max_mw = params['magnitude_range']
    Mw = min_mw + (max_mw - min_mw) * params['uniform_vector'][i, 2]
    Mo = 10 ** (1.5 * Mw + 9.1)
    stress_drop = 5000  # params['lognormal_vector'][i]
    R = (7 / 16 * Mo / stress_drop) ** (1 / 3)
    u = 16 / (7 * np.pi) * stress_drop / 30e09 * R * 10 ** 3  # converted in mm in order to have displacement in mm
    L = np.sqrt(2 * np.pi) * R  # meters
    print('dislocation length', L * 10 ** (-3))
    W = L / 2
    L = L * 10 ** (-3)  # conversion in km and then in lat, lon (suppose 1 degree ~ 100 km) for okada
    W = W * 10 ** (-3)
    displacement = okada85((station_coordinates[:, 1] - epi_lon) * 111.3194,
                           (station_coordinates[:, 0] - epi_lat) * 111.3194, 0, 0,
                           hypo_depth + W / 2 * np.sin(np.deg2rad(dip)), L, W, u, 0, strike, dip, rake)
    return displacement, epi_lat, epi_lon, hypo_depth, Mw, strike, dip, rake, u, stress_drop


def load_station_dat(station_name):
    components = ['ew', 'ns']
    data = []
    for component in components:
        data_file = np.genfromtxt(f'geo_data/itoh/obs_smooth{station_name}_{component}.dat')
        t = data_file[:, 0][24::48]  # subsample
        disp = data_file[:, 2][24::48] * 10.  # subsample, convert to mm
        data.append(disp)
    data = np.array(data).T
    t = np.array(t)
    return t, data


def get_station_name_from_filename(filename):
    return filename[10:14]


def plot_static_displacement(disp, coords):
    cascadia_box = [40 - 1.5, 51.8 - 0.2, -128.3 - 2.5, -121 + 2]  # min/max_latitude, min/max_longitude

    # fig, _ = plt.subplots(1, 1, figsize=(7.8, 7.8), dpi=100)
    fig = plt.figure(figsize=(7.8, 7.8), dpi=100)
    cascadia_map = Basemap(llcrnrlon=cascadia_box[2], llcrnrlat=cascadia_box[0],
                           urcrnrlon=cascadia_box[3], urcrnrlat=cascadia_box[1],
                           lat_0=cascadia_box[0], lon_0=cascadia_box[2],
                           resolution='i', projection='lcc')  # , ax=ax9)

    cascadia_map.drawcoastlines(linewidth=0.5)
    cascadia_map.fillcontinents(color='bisque', lake_color='lightcyan')  # burlywood
    cascadia_map.drawmapboundary(fill_color='lightcyan')
    cascadia_map.drawmapscale(-122.5, 51.05, -122.5, 51.05, 300, barstyle='fancy', zorder=10)
    cascadia_map.drawparallels(np.arange(cascadia_box[0], cascadia_box[1], (cascadia_box[1] - cascadia_box[0]) / 4),
                               labels=[0, 1, 0, 0], linewidth=0.1)
    cascadia_map.drawmeridians(np.arange(cascadia_box[2], cascadia_box[3], (cascadia_box[3] - cascadia_box[2]) / 4),
                               labels=[0, 0, 0, 1], linewidth=0.1)
    cascadia_map.readshapefile('geo_data/tectonicplates-master/PB2002_boundaries', '', linewidth=0.3)

    cascadia_map.scatter(coords[:, 1], coords[:, 0], marker='^', s=15,
                         color='C3',
                         latlon=True, edgecolors='black', linewidth=0.7)

    max_length = np.max(np.sqrt(disp[:, 0] ** 2 + disp[:, 1] ** 2))
    # concatenate unit arrow
    unit_arrow_x, unit_arrow_y = -127., 39.5
    x = np.concatenate((coords[:, 1], [unit_arrow_x]))
    y = np.concatenate((coords[:, 0], [unit_arrow_y]))
    disp_x = np.concatenate((disp[:, 0], [-max_length]))
    disp_y = np.concatenate((disp[:, 1], [0.]))

    cascadia_map.quiver(x, y, disp_x, disp_y, color='black', latlon=True, width=0.0035)

    label_pos_x, label_pos_y = cascadia_map(unit_arrow_x - 1.45, unit_arrow_y + 0.25)

    plt.annotate(f'{np.around(max_length, decimals=1)}mm', xy=(unit_arrow_x, unit_arrow_y), xycoords='data',
                 xytext=(label_pos_x, label_pos_y), color='black')

    ax = fig.gca()
    for axis in ['top', 'bottom', 'left', 'right']:
        ax.spines[axis].set_linewidth(0.5)

    plt.show()


def draw_static_displacement(axis, disp, coords, cut_map=True, draw_tremors=True):
    uniform_scaling_factor = 1
    disp = disp * uniform_scaling_factor
    if not cut_map:
        cascadia_box = [40 - 1.5, 51.8 - 0.2, -128.3 - 2.5, -121 + 2]  # min/max_latitude, min/max_longitude
    else:
        cascadia_box = [46.5 - 1.5, 51.8 - 0.2, -128.3 - 2.5, -121 + 2]  # min/max_latitude, min/max_longitude

    cascadia_map = Basemap(llcrnrlon=cascadia_box[2], llcrnrlat=cascadia_box[0],
                           urcrnrlon=cascadia_box[3], urcrnrlat=cascadia_box[1],
                           lat_0=cascadia_box[0], lon_0=cascadia_box[2],
                           resolution='i', projection='lcc', ax=axis)

    cascadia_map.drawcoastlines(linewidth=0.5)
    cascadia_map.fillcontinents(color='bisque', lake_color='lightcyan')  # burlywood
    cascadia_map.drawmapboundary(fill_color='lightcyan')
    cascadia_map.drawmapscale(-122.5, 51.05, -122.5, 51.05, 300, barstyle='fancy', zorder=10)
    cascadia_map.drawparallels(np.arange(cascadia_box[0], cascadia_box[1], (cascadia_box[1] - cascadia_box[0]) / 4),
                               labels=[1, 0, 0, 0], linewidth=0.1)
    cascadia_map.drawmeridians(np.arange(cascadia_box[2], cascadia_box[3], (cascadia_box[3] - cascadia_box[2]) / 4),
                               labels=[0, 0, 0, 1], linewidth=0.1)
    cascadia_map.readshapefile('geo_data/tectonicplates-master/PB2002_boundaries', '', linewidth=0.3)
    if draw_tremors:
        tremors = tremor_catalogue(north=False)
        tremors_idx = np.logical_and(tremors[:, 3] > 2017.1636, tremors[:, 3] < 2017.3279)
        lat_filter = tremors[tremors_idx][:, 0] > 47.
        print(lat_filter)
        print(tremors[tremors_idx][0])
        valid_tremors = tremors[tremors_idx][lat_filter]
        # ax_data.scatter(tremors[tremors_idx][lat_filter][:, 1], tremors[tremors_idx][lat_filter][:, 1])
        sc = cascadia_map.scatter(valid_tremors[:, 1], valid_tremors[:, 0], latlon=True, c=valid_tremors[:, 3], s=5,
                                  cmap='turbo', alpha=1., edgecolors=None)
        cbar_ax = plt.colorbar(sc, ax=axis, orientation='horizontal', fraction=0.018, pad=0.065, shrink=0.6,
                               format='%1.2f')

    cascadia_map.scatter(coords[:, 1], coords[:, 0], marker='^', s=15,
                         color='C3',
                         latlon=True, edgecolors='black', linewidth=0.7)

    max_length = np.max(np.sqrt(disp[:, 0] ** 2 + disp[:, 1] ** 2))
    # concatenate unit arrow
    if not cut_map:
        unit_arrow_x, unit_arrow_y = -127., 39.5
    else:
        unit_arrow_x, unit_arrow_y = -124.9, 46
    x = np.concatenate((coords[:, 1], [unit_arrow_x]))
    y = np.concatenate((coords[:, 0], [unit_arrow_y]))
    disp_x = np.concatenate((disp[:, 0], [-max_length]))
    disp_y = np.concatenate((disp[:, 1], [0.]))

    cascadia_map.quiver(x, y, disp_x, disp_y, color='black', latlon=True, width=0.0035, scale=20)

    label_pos_x, label_pos_y = cascadia_map(unit_arrow_x - 1.45, unit_arrow_y + 0.25)

    axis.annotate(f'{np.around(max_length / uniform_scaling_factor, decimals=1)} mm', xy=(unit_arrow_x, unit_arrow_y),
                  xycoords='data',
                  xytext=(label_pos_x, label_pos_y), color='black')

    for ax in ['top', 'bottom', 'left', 'right']:
        axis.spines[ax].set_linewidth(0.5)


def predict_runinng_window(data, weight_code='01Aug2022-144844', name='realgaps_ext_v3_0.001_6-7_135',
                           window_length=60):
    params = {'n_stations': 135,
              'window_length': 60,
              'n_directions': 2,
              'batch_size': None,
              'n_epochs': None,
              'learning_rate': None,
              'embedding_regularizer': None,
              'kernel_regularizer': None,
              'bias_regularizer': None,
              'embeddings_regularizer': None}

    detector = SSEdetector(**params)
    detector.build()
    # detector.summary()

    model = detector.get_model()

    detector.load_weights(os.path.join('weights', 'SSEdetector', f'best_cascadia_{weight_code}_{name}.h5'))

    '''prediction_proba = np.zeros(data.shape[1])  # probability for each time step
    for i in range(window_length // 2, data.shape[1] - window_length // 2):
        data_window = data[:, i - window_length // 2:i + window_length // 2, :]
        window_prediction_proba = model.predict(data_window[np.newaxis, :])
        prediction_proba[i] = window_prediction_proba'''

    test_data = []
    for i in range(window_length // 2, data.shape[1] - window_length // 2):
        data_window = data[:, i - window_length // 2:i + window_length // 2, :]
        test_data.append(data_window)

    test_data = np.array(test_data)
    print(test_data.shape)

    output_proba = model.predict(test_data).squeeze()
    prediction_proba = np.zeros((data.shape[1],))
    prediction_proba.fill(np.nan)
    prediction_proba[30:-30] = output_proba

    return prediction_proba


def itoh_figure_old(synthetic_noise, weight_code='01Aug2022-144844', name='realgaps_ext_v3_0.001_6-7_135'):
    path = 'geo_data/itoh'
    filenames = [f for f in listdir(path) if isfile(join(path, f))]

    itoh_stations = []
    for filename in sorted(filenames):
        if filename.startswith('obs') and 'ew' in filename:
            station_name = get_station_name_from_filename(filename)
            itoh_stations.append(station_name)

    itoh_data = []
    itoh_valid_stations = []
    itoh_duration = 29
    for code in station_codes_subset.tolist():
        if code in itoh_stations:
            t, data = load_station_dat(code)
            itoh_valid_stations.append(code)
            itoh_data.append(data)
        else:
            itoh_data.append(np.zeros((itoh_duration, 2)))

    itoh_data = np.array(itoh_data)

    moment_rate_itoh = np.loadtxt('geo_data/itoh/moment_rate_history_30GPa', usecols=(1))
    moment_rate_itoh = moment_rate_itoh[24::48]

    # plt.plot(itoh_data[0, :, 0])
    # plt.show()

    '''plt.matshow(itoh_data[latsort, :, 0], aspect='auto', vmin=-5, vmax=3, cmap='RdBu_r')
    plt.colorbar()
    plt.show()'''

    # static displacement
    itoh_static_displacement = itoh_data[:, -1, :] - itoh_data[:, 0, :]

    # plot_static_displacement(itoh_static_displacement, station_coordinates_subset)

    # building of the synthetic time series
    # we add 30 days before and after to be able to fully scan the itoh modeled time series
    # we also add 14 days before and after to be able to see the evolution of the proba curve

    delta_win = 30  # days
    window_length = 60

    rnd_idx = np.random.randint(synthetic_noise.shape[1] - window_length)
    print('random index:', rnd_idx)
    cut_noise = synthetic_noise[:,
                rnd_idx - window_length // 2 - delta_win:rnd_idx + itoh_duration + window_length // 2 + delta_win, :]
    modeled_time_series = np.zeros(cut_noise.shape)
    modeled_time_series[:, window_length // 2 + delta_win:-window_length // 2 - delta_win, :] = itoh_data
    for i in range(n_directions):
        modeled_time_series[:, -window_length // 2 - delta_win:, i] = itoh_data[:, -1, i][..., np.newaxis] * np.ones(
            (n_selected_stations, window_length // 2 + delta_win))

    synthetic_time_series = cut_noise + modeled_time_series

    time_array = selected_time_array[
                 rnd_idx - window_length // 2 - delta_win:rnd_idx + itoh_duration + window_length // 2 + delta_win]

    time_array = np.arange(synthetic_time_series.shape[1]) - delta_win - window_length // 2

    print(synthetic_time_series.shape)

    '''fig, axes = plt.subplots(1, 3)
    axes[0].matshow(cut_noise[latsort, :, 0], aspect='auto', vmin=-5, vmax=3, cmap='RdBu_r')
    axes[1].matshow(modeled_time_series[latsort, :, 0], aspect='auto', vmin=-5, vmax=3, cmap='RdBu_r')
    axes[2].matshow(synthetic_time_series[latsort, :, 0], aspect='auto', vmin=-5, vmax=3, cmap='RdBu_r')
    plt.show()'''
    proba = predict_runinng_window(synthetic_time_series)

    synth_matrix_limit = station_coordinates_subset[latsort, 0] > 45.

    modeled_time_series[modeled_time_series == 0] = np.nan

    # final figure
    figure = plt.figure(figsize=(14.5, 8.5), dpi=100)
    ax_map = figure.add_subplot(1, 2, 1)
    ax_synth_data = figure.add_subplot(5, 2, 2)
    ax_data = figure.add_subplot(4, 2, 4, sharex=ax_synth_data)
    ax_mrate = figure.add_subplot(5, 2, 8, sharex=ax_synth_data)
    ax_proba = figure.add_subplot(5, 2, 10, sharex=ax_synth_data)
    draw_static_displacement(ax_map, itoh_static_displacement, station_coordinates_subset)

    norm = mcolors.TwoSlopeNorm(vmin=-5, vcenter=0., vmax=3)
    matshow_extent = [time_array[0], time_array[-1], station_coordinates_subset[:, 0].min(),
                      station_coordinates_subset[:, 0].max()]
    matshow_extent_synth = [time_array[0], time_array[-1],
                            station_coordinates_subset[latsort, 0][synth_matrix_limit].min(),
                            station_coordinates_subset[latsort, 0][synth_matrix_limit].max()]
    every_nth = 17
    _ = ax_synth_data.matshow(modeled_time_series[latsort, :, 0][synth_matrix_limit], aspect='auto', norm=norm,
                              cmap='RdBu_r', extent=matshow_extent_synth)

    ax_synth_data.set_yticks(np.around(station_coordinates_subset[latsort, 0][synth_matrix_limit], decimals=1))
    for n, label in enumerate(ax_synth_data.yaxis.get_ticklabels()):
        if n % every_nth != 0:
            label.set_visible(False)
            ax_synth_data.yaxis.get_major_ticks()[n].set_visible(False)

    # ax_synth_data.axvline(x=window_length//2 + delta_win, linestyle='--', linewidth=1)
    # ax_synth_data.axvline(x=window_length // 2 + delta_win + itoh_duration, linestyle='--', linewidth=1)

    ms = ax_data.matshow(synthetic_time_series[latsort, :, 0], aspect='auto', norm=norm,
                         cmap='RdBu_r', extent=matshow_extent)

    ax_data.set_yticks(np.around(station_coordinates_subset[latsort, 0], decimals=1))
    for n, label in enumerate(ax_data.yaxis.get_ticklabels()):
        if n % every_nth != 0:
            label.set_visible(False)
            ax_data.yaxis.get_major_ticks()[n].set_visible(False)

    ax_data.set_ylabel('Latitude', labelpad=7)
    ax_synth_data.set_ylabel('Latitude', labelpad=7)
    # ax_data.set_xticklabels([])
    # ax_synth_data.set_xticklabels([])

    for ax in [ax_synth_data, ax_data]:
        ax.tick_params(
            axis='x',  # changes apply to the x-axis
            which='both',  # both major and minor ticks are affected
            bottom=True,  # ticks along the bottom edge are on
            top=False,  # ticks along the top edge are off
            labelbottom=False,
            labeltop=False)  # labels along the bottom edge are off

    h = ax_synth_data.get_position().y1 / 2 - ax_data.get_position().y0 / 2
    w = 0.01
    cax = plt.gcf().add_axes(
        [ax_data.get_position().x1 * 1.03, ax_data.get_position().y0 + h / 2, w, h])  # add_axes([x1, y1, w1, h])

    cbar = plt.colorbar(ms, cax=cax)
    cbar.ax.set_ylabel('Displacement [mm]', rotation=270, labelpad=17)

    print('Predicted duration from proba', np.sum(proba > 0.5), 'days')

    # plt.colorbar(ms, ax=cbar_ax)

    ax_proba.plot(time_array, proba)

    prob_mask = proba > 0.5
    sse_cat_inferred = []
    t = 0
    while t < len(time_array):
        if prob_mask[t] != 0:
            start_t = t
            while prob_mask[t] != 0:
                t += 1
            sse_cat_inferred.append((start_t, t))
        else:
            t += 1
    sse_cat_inferred = np.array(sse_cat_inferred)

    sse_cat_inferred = sse_cat_inferred - delta_win - window_length // 2

    print(sse_cat_inferred)

    for i, span in enumerate(sse_cat_inferred):
        ax_proba.axvspan(span[0], span[1], ymin=0, alpha=0.25, color='C0', zorder=0, linewidth=0.)

    ax_proba.set_ylabel('SSE probability')

    extended_mrate_fcn = np.zeros(proba.shape)
    extended_mrate_fcn.fill(np.nan)
    extended_mrate_fcn[window_length // 2 + delta_win:-window_length // 2 - delta_win] = moment_rate_itoh

    ax_mrate.plot(time_array, extended_mrate_fcn)

    ax_mrate.set_ylabel('Moment rate [Nm/yr]')
    ax_proba.set_xlabel('Days from March 15, 2017')

    ax_mrate.tick_params(labelbottom=False)

    '''print(ax_proba.xaxis.get_ticklabels())
    print([item.get_text() for item in ax_proba.get_xticklabels()])
    time_labels = ax_proba.xaxis.get_ticklabels()
    shifted_time_labels = []
    for time_label in time_labels:
        print('retrieved', time_label.get_text())
        shifted_time_label = int(time_label.get_text()) - delta_win - window_length // 2
        shifted_time_labels.append(shifted_time_label)
    ax_proba.xaxis.set_ticklabels(shifted_time_labels)'''

    # plt.tight_layout()
    plt.savefig('./sse_figures/itoh_comparison.pdf')
    plt.close(figure)
    # plt.show()


def itoh_figure(synthetic_noise, weight_code='01Aug2022-144844', name='realgaps_ext_v3_0.001_6-7_135'):
    path = 'geo_data/itoh'
    filenames = [f for f in listdir(path) if isfile(join(path, f))]

    itoh_stations = []
    for filename in sorted(filenames):
        if filename.startswith('obs') and 'ew' in filename:
            station_name = get_station_name_from_filename(filename)
            itoh_stations.append(station_name)

    itoh_data = []
    itoh_valid_stations = []
    itoh_duration = 29
    for code in station_codes_subset.tolist():
        if code in itoh_stations:
            t, data = load_station_dat(code)
            itoh_valid_stations.append(code)
            itoh_data.append(data)
        else:
            itoh_data.append(np.zeros((itoh_duration, 2)))

    itoh_data = np.array(itoh_data)

    moment_rate_itoh = np.loadtxt('geo_data/itoh/moment_rate_history_30GPa', usecols=1)
    moment_rate_itoh = moment_rate_itoh[24::48]

    # plt.plot(itoh_data[0, :, 0])
    # plt.show()

    '''plt.matshow(itoh_data[latsort, :, 0], aspect='auto', vmin=-5, vmax=3, cmap='RdBu_r')
    plt.colorbar()
    plt.show()'''

    # static displacement
    itoh_static_displacement = itoh_data[:, -1, :] - itoh_data[:, 0, :]

    # plot_static_displacement(itoh_static_displacement, station_coordinates_subset)

    # building of the synthetic time series
    # we add 30 days before and after to be able to fully scan the itoh modeled time series
    # we also add 14 days before and after to be able to see the evolution of the proba curve

    delta_win = 30  # days
    window_length = 60

    rnd_idx = np.random.randint(synthetic_noise.shape[1] - window_length)
    print('random index:', rnd_idx)
    cut_noise = synthetic_noise[:,
                rnd_idx - window_length // 2 - delta_win:rnd_idx + itoh_duration + window_length // 2 + delta_win, :]
    modeled_time_series = np.zeros(cut_noise.shape)
    modeled_time_series[:, window_length // 2 + delta_win:-window_length // 2 - delta_win, :] = itoh_data
    for i in range(n_directions):
        modeled_time_series[:, -window_length // 2 - delta_win:, i] = itoh_data[:, -1, i][..., np.newaxis] * np.ones(
            (n_selected_stations, window_length // 2 + delta_win))

    synthetic_time_series = cut_noise + modeled_time_series

    time_array = selected_time_array[
                 rnd_idx - window_length // 2 - delta_win:rnd_idx + itoh_duration + window_length // 2 + delta_win]

    time_array = np.arange(synthetic_time_series.shape[1]) - delta_win - window_length // 2

    time_cut_filter = np.logical_and(time_array >= 0, time_array < 30)

    print(time_array[time_cut_filter])

    print(synthetic_time_series.shape)

    '''fig, axes = plt.subplots(1, 3)
    axes[0].matshow(cut_noise[latsort, :, 0], aspect='auto', vmin=-5, vmax=3, cmap='RdBu_r')
    axes[1].matshow(modeled_time_series[latsort, :, 0], aspect='auto', vmin=-5, vmax=3, cmap='RdBu_r')
    axes[2].matshow(synthetic_time_series[latsort, :, 0], aspect='auto', vmin=-5, vmax=3, cmap='RdBu_r')
    plt.show()'''
    proba = predict_runinng_window(synthetic_time_series)

    synth_matrix_limit = station_coordinates_subset[latsort, 0] > 45.

    modeled_time_series[modeled_time_series == 0] = np.nan

    modeled_time_series = modeled_time_series[:, time_cut_filter, :]
    synthetic_time_series = synthetic_time_series[:, time_cut_filter, :]

    # final figure
    figure = plt.figure(figsize=(14.5, 8.5), dpi=100)
    ax_map = figure.add_subplot(1, 2, 1)
    ax_synth_data = figure.add_subplot(5, 2, 2)
    ax_data = figure.add_subplot(4, 2, 4, sharex=ax_synth_data)
    ax_mrate = figure.add_subplot(5, 2, 8, sharex=ax_synth_data)
    ax_proba = figure.add_subplot(5, 2, 10, sharex=ax_synth_data)
    draw_static_displacement(ax_map, itoh_static_displacement, station_coordinates_subset)

    norm = mcolors.TwoSlopeNorm(vmin=-5, vcenter=0., vmax=3)
    matshow_extent = [time_array[time_cut_filter][0], time_array[time_cut_filter][-1],
                      station_coordinates_subset[:, 0].min(),
                      station_coordinates_subset[:, 0].max()]
    matshow_extent_synth = [time_array[time_cut_filter][0], time_array[time_cut_filter][-1],
                            station_coordinates_subset[latsort, 0][synth_matrix_limit].min(),
                            station_coordinates_subset[latsort, 0][synth_matrix_limit].max()]

    print(matshow_extent_synth)
    print(matshow_extent)

    every_nth = 17
    _ = ax_synth_data.matshow(modeled_time_series[latsort, :, 0][synth_matrix_limit], aspect='auto', norm=norm,
                              cmap='RdBu_r', extent=matshow_extent_synth)

    ax_synth_data.set_yticks(np.around(station_coordinates_subset[latsort, 0][synth_matrix_limit], decimals=1))
    for n, label in enumerate(ax_synth_data.yaxis.get_ticklabels()):
        if n % every_nth != 0:
            label.set_visible(False)
            ax_synth_data.yaxis.get_major_ticks()[n].set_visible(False)

    # ax_synth_data.axvline(x=window_length//2 + delta_win, linestyle='--', linewidth=1)
    # ax_synth_data.axvline(x=window_length // 2 + delta_win + itoh_duration, linestyle='--', linewidth=1)

    ms = ax_data.matshow(synthetic_time_series[latsort, :, 0], aspect='auto', norm=norm,
                         cmap='RdBu_r', extent=matshow_extent)

    ax_data.set_yticks(np.around(station_coordinates_subset[latsort, 0], decimals=1))
    for n, label in enumerate(ax_data.yaxis.get_ticklabels()):
        if n % every_nth != 0:
            label.set_visible(False)
            ax_data.yaxis.get_major_ticks()[n].set_visible(False)

    ax_data.set_ylabel('Latitude', labelpad=7)
    ax_synth_data.set_ylabel('Latitude', labelpad=7)
    # ax_data.set_xticklabels([])
    # ax_synth_data.set_xticklabels([])

    for ax in [ax_synth_data, ax_data]:
        ax.tick_params(
            axis='x',  # changes apply to the x-axis
            which='both',  # both major and minor ticks are affected
            bottom=True,  # ticks along the bottom edge are on
            top=False,  # ticks along the top edge are off
            labelbottom=False,
            labeltop=False)  # labels along the bottom edge are off

    h = ax_synth_data.get_position().y1 / 2 - ax_data.get_position().y0 / 2
    w = 0.01
    cax = plt.gcf().add_axes(
        [ax_data.get_position().x1 * 1.03, ax_data.get_position().y0 + h / 2, w, h])  # add_axes([x1, y1, w1, h])

    cbar = plt.colorbar(ms, cax=cax)
    cbar.ax.set_ylabel('Displacement [mm]', rotation=270, labelpad=17)

    print('Predicted duration from proba', np.sum(proba > 0.5), 'days')

    # plt.colorbar(ms, ax=cbar_ax)

    prob_mask = proba > 0.5
    sse_cat_inferred = []
    t = 0
    while t < len(time_array):
        if prob_mask[t] != 0:
            start_t = t
            while prob_mask[t] != 0:
                t += 1
            sse_cat_inferred.append((start_t, t))
        else:
            t += 1
    sse_cat_inferred = np.array(sse_cat_inferred)

    sse_cat_inferred = sse_cat_inferred - delta_win - window_length // 2

    print(sse_cat_inferred)

    for i, span in enumerate(sse_cat_inferred):
        # ax_proba.axvspan(span[0], span[1], ymin=0, alpha=0.25, color='C0', zorder=0, linewidth=0.)
        pass

    ax_proba.plot(time_array[time_cut_filter], proba[time_cut_filter], label='synthetic')
    ax_proba.set_ylabel('SSE \n probability')

    extended_mrate_fcn = np.zeros(proba.shape)
    extended_mrate_fcn.fill(np.nan)
    extended_mrate_fcn[window_length // 2 + delta_win:-window_length // 2 - delta_win] = moment_rate_itoh

    ax_mrate.plot(time_array[time_cut_filter], extended_mrate_fcn[time_cut_filter])

    ax_mrate.set_ylabel('Moment rate \n [Nm/yr]')
    ax_proba.set_xlabel('Days from March 15, 2017')

    ax_mrate.tick_params(labelbottom=False)

    with np.load(f'predictions/pred_cascadia_running_win_{weight_code}_{name}.npz') as f:
        original_proba, original_time_span = f['proba'], f['time']

    original_filter = np.logical_and(original_time_span > 2017.2019,
                                     original_time_span <= 2017.3114)  # 15 MAR - 24 APR 2017

    original_proba = original_proba[original_filter]

    original_proba = original_proba[:-10]  # 15 MAR - 14 APR

    ax_proba.plot(time_array[time_cut_filter], original_proba, color='red', label='real')
    ax_proba.legend()

    '''print(ax_proba.xaxis.get_ticklabels())
    print([item.get_text() for item in ax_proba.get_xticklabels()])
    time_labels = ax_proba.xaxis.get_ticklabels()
    shifted_time_labels = []
    for time_label in time_labels:
        print('retrieved', time_label.get_text())
        shifted_time_label = int(time_label.get_text()) - delta_win - window_length // 2
        shifted_time_labels.append(shifted_time_label)
    ax_proba.xaxis.set_ticklabels(shifted_time_labels)'''

    # plt.tight_layout()
    plt.savefig('./sse_figures/itoh_comparison.pdf')
    plt.close(figure)
    # plt.show()


def short_event_figure_old(noise, window_length=60):
    # Synthetic SSE

    n = 1
    admissible_depth, admissible_strike, admissible_dip, region = read_from_slab2()
    uniform_vector = np.random.uniform(0, 1, (n, 3))
    max_stress_drop = 0.1 * 1e06  # Gao et al., 2012
    min_stress_drop = 0.01 * 1e06  # Gao et al., 2012
    mean_stress_drop = 0.5 * (max_stress_drop + min_stress_drop)
    variation_factor = 10
    std_underlying_normal = np.sqrt(np.log(variation_factor ** 2 + 1))  # from coefficient of variation
    mean_underlying_normal = np.log(mean_stress_drop) - std_underlying_normal ** 2 / 2
    lognormal_vector = np.random.lognormal(mean_underlying_normal, std_underlying_normal, (n,))
    n_stations = station_coordinates_subset.shape[0]
    disp_stations = np.zeros((n_stations, 3))

    results = _custom_synthetic_displacement_stations_cascadia(0, admissible_depth, admissible_strike, admissible_dip,
                                                               station_coordinates_subset,
                                                               uniform_vector=uniform_vector,
                                                               lognormal_vector=lognormal_vector,
                                                               magnitude_range=(6.5, 6.6))

    for direction in range(3):
        disp_stations[:, direction] = results[0][direction]
    catalog = results[1:]

    print(catalog)

    transients = np.zeros((len(station_codes_subset), 60, 2))
    window_length = 60
    center = window_length // 2
    random_duration = 3
    transient_time_array = np.linspace(0, window_length, window_length)
    for station in range(len(station_codes_subset)):
        for direction in range(2):
            transients[station, :, direction] = sigmoidal_rise_time(transient_time_array,
                                                                    disp_stations[station, direction],
                                                                    random_duration, center)

    delta_win = 30

    rnd_idx = np.random.randint(noise.shape[1] - window_length)
    print('random index:', rnd_idx)
    cut_noise = noise[:,
                rnd_idx - window_length // 2 - delta_win:rnd_idx + window_length + window_length // 2 + delta_win, :]

    modeled_time_series = np.zeros(cut_noise.shape)
    modeled_time_series[:, window_length // 2 + delta_win:-window_length // 2 - delta_win, :] = transients

    for i in range(n_directions):
        modeled_time_series[:, -window_length // 2 - delta_win:, i] = transients[:, -1, i][..., np.newaxis] * np.ones(
            (n_selected_stations, window_length // 2 + delta_win))

    # plt.matshow(transients[latsort, :, 0])
    # plt.show()

    synthetic_ts = cut_noise + modeled_time_series
    ########

    time_array = np.arange(synthetic_ts.shape[1])
    time_filter = np.logical_and(time_array >= 80, time_array < 100)

    proba = predict_runinng_window(synthetic_ts)

    synthetic_ts = synthetic_ts[:, time_filter, :]
    modeled_time_series = modeled_time_series[:, time_filter, :]

    # final figure
    figure = plt.figure(figsize=(14.5, 8.5), dpi=100)
    ax_map = figure.add_subplot(1, 2, 1)
    ax_synth_data = figure.add_subplot(3, 2, 2)
    ax_data = figure.add_subplot(3, 2, 4, sharex=ax_synth_data)
    ax_proba = figure.add_subplot(4, 2, 8, sharex=ax_synth_data)
    draw_static_displacement(ax_map, disp_stations, station_coordinates_subset, cut_map=False, draw_tremors=False)

    norm = mcolors.TwoSlopeNorm(vmin=-5, vcenter=0., vmax=3)
    matshow_extent = [time_array[0], time_array[-1], station_coordinates_subset[:, 0].min(),
                      station_coordinates_subset[:, 0].max()]
    print(matshow_extent)
    _ = ax_synth_data.matshow(modeled_time_series[latsort, :, 0], aspect='auto', norm=norm,
                              cmap='RdBu_r', extent=matshow_extent)

    every_nth = 17
    ax_synth_data.set_yticks(np.around(station_coordinates_subset[latsort, 0], decimals=1))
    for n, label in enumerate(ax_synth_data.yaxis.get_ticklabels()):
        if n % every_nth != 0:
            label.set_visible(False)
            ax_synth_data.yaxis.get_major_ticks()[n].set_visible(False)

    # ax_synth_data.axvline(x=window_length//2 + delta_win, linestyle='--', linewidth=1)
    # ax_synth_data.axvline(x=window_length // 2 + delta_win + itoh_duration, linestyle='--', linewidth=1)

    ms = ax_data.matshow(synthetic_ts[latsort, :, 0], aspect='auto', norm=norm,
                         cmap='RdBu_r', extent=matshow_extent)

    ax_data.set_yticks(np.around(station_coordinates_subset[latsort, 0], decimals=1))
    for n, label in enumerate(ax_data.yaxis.get_ticklabels()):
        if n % every_nth != 0:
            label.set_visible(False)
            ax_data.yaxis.get_major_ticks()[n].set_visible(False)

    for ax in [ax_synth_data, ax_data]:
        ax.tick_params(
            axis='x',  # changes apply to the x-axis
            which='both',  # both major and minor ticks are affected
            bottom=True,  # ticks along the bottom edge are on
            top=False,  # ticks along the top edge are off
            labelbottom=False,
            labeltop=False)  # labels along the bottom edge are off

    ax_data.set_ylabel('Latitude', labelpad=7)
    ax_synth_data.set_ylabel('Latitude', labelpad=7)

    print('Predicted duration from proba', np.sum(proba > 0.5), 'days')

    # plt.colorbar(ms, ax=cbar_ax)

    ax_proba.plot(proba)
    prob_mask = proba > 0.5
    sse_cat_inferred = []
    t = 0
    while t < len(time_array):
        if prob_mask[t] != 0:
            start_t = t
            while prob_mask[t] != 0:
                t += 1
            sse_cat_inferred.append((start_t, t))
        else:
            t += 1
    sse_cat_inferred = np.array(sse_cat_inferred)
    print(sse_cat_inferred)

    for i, span in enumerate(sse_cat_inferred):
        ax_proba.axvspan(span[0], span[1], ymin=0, alpha=0.25, color='C0', zorder=0, linewidth=0.)

    ax_proba.set_ylabel('SSE probability')
    ax_proba.set_xlabel('Synthetic time [days]')

    h = ax_synth_data.get_position().y1 / 2 - ax_data.get_position().y0 / 2
    w = 0.008
    cax = plt.gcf().add_axes(
        [ax_data.get_position().x1 * 1.03, ax_data.get_position().y0 + h / 2, w, h])  # add_axes([x1, y1, w1, h])

    cbar = plt.colorbar(ms, cax=cax)
    cbar.ax.set_ylabel('Displacement [mm]', rotation=270, labelpad=17)

    # plt.tight_layout()
    plt.savefig('./sse_figures/short_event.pdf')
    plt.close(figure)
    # plt.show()


def short_event_figure(noise, window_length=60):
    # Synthetic SSE

    n = 1
    admissible_depth, admissible_strike, admissible_dip, region = read_from_slab2()
    uniform_vector = np.random.uniform(0, 1, (n, 3))
    max_stress_drop = 0.1 * 1e06  # Gao et al., 2012
    min_stress_drop = 0.01 * 1e06  # Gao et al., 2012
    mean_stress_drop = 0.5 * (max_stress_drop + min_stress_drop)
    variation_factor = 10
    std_underlying_normal = np.sqrt(np.log(variation_factor ** 2 + 1))  # from coefficient of variation
    mean_underlying_normal = np.log(mean_stress_drop) - std_underlying_normal ** 2 / 2
    lognormal_vector = np.random.lognormal(mean_underlying_normal, std_underlying_normal, (n,))
    n_stations = station_coordinates_subset.shape[0]
    disp_stations = np.zeros((n_stations, 3))

    results = _custom_synthetic_displacement_stations_cascadia(0, admissible_depth, admissible_strike, admissible_dip,
                                                               station_coordinates_subset,
                                                               uniform_vector=uniform_vector,
                                                               lognormal_vector=lognormal_vector,
                                                               magnitude_range=(6.5, 6.6))

    for direction in range(3):
        disp_stations[:, direction] = results[0][direction]
    catalog = results[1:]

    print(catalog)

    transients = np.zeros((len(station_codes_subset), 60, 2))
    window_length = 60
    center = window_length // 2
    random_duration = 3
    transient_time_array = np.linspace(0, window_length, window_length)
    for station in range(len(station_codes_subset)):
        for direction in range(2):
            transients[station, :, direction] = sigmoidal_rise_time(transient_time_array,
                                                                    disp_stations[station, direction],
                                                                    random_duration, center)

    delta_win = 30

    rnd_idx = np.random.randint(noise.shape[1] - window_length)
    print('random index:', rnd_idx)
    cut_noise = noise[:,
                rnd_idx - window_length // 2 - delta_win:rnd_idx + window_length + window_length // 2 + delta_win, :]

    modeled_time_series = np.zeros(cut_noise.shape)
    modeled_time_series[:, window_length // 2 + delta_win:-window_length // 2 - delta_win, :] = transients

    for i in range(n_directions):
        modeled_time_series[:, -window_length // 2 - delta_win:, i] = transients[:, -1, i][..., np.newaxis] * np.ones(
            (n_selected_stations, window_length // 2 + delta_win))

    # plt.matshow(transients[latsort, :, 0])
    # plt.show()

    synthetic_ts = cut_noise + modeled_time_series
    ########

    time_array = np.arange(synthetic_ts.shape[1])
    time_filter = np.logical_and(time_array >= 80, time_array < 100)

    proba = predict_runinng_window(synthetic_ts)

    synthetic_ts = synthetic_ts[:, time_filter, :]
    modeled_time_series = modeled_time_series[:, time_filter, :]

    # final figure
    figure = plt.figure(figsize=(14.5, 8.5), dpi=100)
    ax_map = figure.add_subplot(1, 2, 1)
    ax_synth_data = figure.add_subplot(3, 2, 2)
    ax_data = figure.add_subplot(3, 2, 4, sharex=ax_synth_data)
    ax_proba = figure.add_subplot(4, 2, 8, sharex=ax_synth_data)
    draw_static_displacement(ax_map, disp_stations, station_coordinates_subset, cut_map=False, draw_tremors=False)

    norm = mcolors.TwoSlopeNorm(vmin=-5, vcenter=0., vmax=3)
    matshow_extent = [0, len(time_array[time_filter]), station_coordinates_subset[:, 0].min(),
                      station_coordinates_subset[:, 0].max()]
    print(matshow_extent)
    _ = ax_synth_data.matshow(modeled_time_series[latsort, :, 0], aspect='auto', norm=norm,
                              cmap='RdBu_r', extent=matshow_extent)

    every_nth = 17
    ax_synth_data.set_yticks(np.around(station_coordinates_subset[latsort, 0], decimals=1))
    for n, label in enumerate(ax_synth_data.yaxis.get_ticklabels()):
        if n % every_nth != 0:
            label.set_visible(False)
            ax_synth_data.yaxis.get_major_ticks()[n].set_visible(False)

    # ax_synth_data.axvline(x=window_length//2 + delta_win, linestyle='--', linewidth=1)
    # ax_synth_data.axvline(x=window_length // 2 + delta_win + itoh_duration, linestyle='--', linewidth=1)

    ms = ax_data.matshow(synthetic_ts[latsort, :, 0], aspect='auto', norm=norm,
                         cmap='RdBu_r', extent=matshow_extent)

    '''upsampling_factor = 2
    upsampled_synthetic_ts = np.zeros((synthetic_ts.shape[0], synthetic_ts.shape[1] * upsampling_factor))
    upsampled_time = np.linspace(80, 99, 20 * upsampling_factor)
    from scipy.interpolate import interp1d

    for k in range(synthetic_ts.shape[0]):
        f = interp1d(time_array[time_filter], synthetic_ts[k, :, 0])
        upsampled_synthetic_ts[k] = f(upsampled_time)

    ms = ax_data.matshow(upsampled_synthetic_ts[latsort], aspect='auto', norm=norm,
                         cmap='RdBu_r', extent=matshow_extent)'''

    ax_data.set_yticks(np.around(station_coordinates_subset[latsort, 0], decimals=1))
    for n, label in enumerate(ax_data.yaxis.get_ticklabels()):
        if n % every_nth != 0:
            label.set_visible(False)
            ax_data.yaxis.get_major_ticks()[n].set_visible(False)

    for ax in [ax_synth_data, ax_data]:
        ax.tick_params(
            axis='x',  # changes apply to the x-axis
            which='both',  # both major and minor ticks are affected
            bottom=True,  # ticks along the bottom edge are on
            top=False,  # ticks along the top edge are off
            labelbottom=False,
            labeltop=False)  # labels along the bottom edge are off

    ax_data.set_ylabel('Latitude', labelpad=7)
    ax_synth_data.set_ylabel('Latitude', labelpad=7)

    print('Predicted duration from proba', np.sum(proba > 0.5), 'days')

    # plt.colorbar(ms, ax=cbar_ax)

    ax_proba.plot(proba[time_filter])
    prob_mask = proba > 0.5
    sse_cat_inferred = []
    t = 0
    while t < len(time_array):
        if prob_mask[t] != 0:
            start_t = t
            while prob_mask[t] != 0:
                t += 1
            sse_cat_inferred.append((start_t, t))
        else:
            t += 1
    sse_cat_inferred = np.array(sse_cat_inferred)
    print(sse_cat_inferred)

    '''for i, span in enumerate(sse_cat_inferred):
        ax_proba.axvspan(span[0], span[1], ymin=0, alpha=0.25, color='C0', zorder=0, linewidth=0.)'''

    ax_proba.axvspan(sse_cat_inferred[0, 0] - 80, sse_cat_inferred[0, 1] - 80, ymin=0, alpha=0.25, color='C0', zorder=0,
                     linewidth=0.)

    ax_proba.set_ylabel('SSE probability')
    ax_proba.set_xlabel('Synthetic time [days]')

    h = ax_synth_data.get_position().y1 / 2 - ax_data.get_position().y0 / 2
    w = 0.008
    cax = plt.gcf().add_axes(
        [ax_data.get_position().x1 * 1.03, ax_data.get_position().y0 + h / 2, w, h])  # add_axes([x1, y1, w1, h])

    cbar = plt.colorbar(ms, cax=cax)
    cbar.ax.set_ylabel('Displacement [mm]', rotation=270, labelpad=17)

    # plt.tight_layout()
    plt.savefig('./sse_figures/short_event.pdf')
    plt.close(figure)
    # plt.show()


def large_event_figure_old(noise, window_length=60):
    # Synthetic SSE

    n = 1
    admissible_depth, admissible_strike, admissible_dip, region = read_from_slab2()
    uniform_vector = np.random.uniform(0, 1, (n, 3))
    max_stress_drop = 0.1 * 1e06  # Gao et al., 2012
    min_stress_drop = 0.01 * 1e06  # Gao et al., 2012
    mean_stress_drop = 0.5 * (max_stress_drop + min_stress_drop)
    variation_factor = 10
    std_underlying_normal = np.sqrt(np.log(variation_factor ** 2 + 1))  # from coefficient of variation
    mean_underlying_normal = np.log(mean_stress_drop) - std_underlying_normal ** 2 / 2
    lognormal_vector = np.random.lognormal(mean_underlying_normal, std_underlying_normal, (n,))
    n_stations = station_coordinates_subset.shape[0]
    disp_stations = np.zeros((n_stations, 3))

    positions = [(48.5, -125), (48.0, -124), (46.0, -124)]
    # positions = [(46.0, -124)]
    delta_win = 30
    offset = 0

    rnd_idx = 128  # np.random.randint(noise.shape[1] - window_length - delta_win)
    print('random index:', rnd_idx)
    cut_noise = noise[:,
                rnd_idx - window_length // 2 - delta_win:rnd_idx + window_length + window_length // 2 + delta_win, :]
    modeled_time_series = np.zeros(cut_noise.shape)
    for j, pos in enumerate(positions):
        partial_modeled_time_series = np.zeros(cut_noise.shape)
        partial_disp_stations = np.zeros((n_stations, 3))
        results = _custom_synthetic_displacement_stations_cascadia(0, admissible_depth, admissible_strike,
                                                                   admissible_dip,
                                                                   station_coordinates_subset,
                                                                   uniform_vector=uniform_vector,
                                                                   lognormal_vector=lognormal_vector,
                                                                   magnitude_range=(6.5, 6.6),
                                                                   epi_lat=pos[0], epi_lon=pos[1])

        for direction in range(3):
            partial_disp_stations[:, direction] = results[0][direction]
        catalog = results[1:]

        print(catalog)

        transients = np.zeros((len(station_codes_subset), 60, 2))
        window_length = 60
        center = window_length // 2
        random_duration = 20
        transient_time_array = np.linspace(0, window_length, window_length)
        for station in range(len(station_codes_subset)):
            for direction in range(2):
                transients[station, :, direction] = sigmoidal_rise_time(transient_time_array,
                                                                        partial_disp_stations[station, direction],
                                                                        random_duration, center)
        disp_stations += partial_disp_stations
        # modeled_time_series[:, window_length // 2 + delta_win:-window_length // 2 - delta_win, :] = transients
        print(window_length // 2 + delta_win + (j - 1) * random_duration,
              window_length // 2 + delta_win + (j - 1) * random_duration + window_length)

        partial_modeled_time_series[:,
        window_length // 2 + delta_win + (j - 1) * random_duration - offset:window_length // 2 + delta_win + (
                j - 1) * random_duration + window_length - offset, :] += transients
        # plt.matshow(partial_modeled_time_series[latsort, :, 0]); plt.show()
        for i in range(n_directions):
            partial_modeled_time_series[:,
            window_length // 2 + delta_win + (j - 1) * random_duration + window_length - offset:, i] += \
                transients[:, -1, i][..., np.newaxis] * np.ones(
                    (n_selected_stations, window_length // 2 + delta_win - (j - 1) * random_duration + offset))
        modeled_time_series += partial_modeled_time_series
        # plt.matshow(modeled_time_series[latsort, :, 0]); plt.show()
    # plt.matshow(transients[latsort, :, 0])
    # plt.show()

    synthetic_ts = cut_noise + modeled_time_series

    synthetic_ts[np.isnan(synthetic_ts)] = 0.

    ########

    time_array = np.arange(synthetic_ts.shape[1])

    proba = predict_runinng_window(synthetic_ts)

    # final figure
    figure = plt.figure(figsize=(14.5, 8.5), dpi=100)
    ax_map = figure.add_subplot(1, 2, 1)
    ax_synth_data = figure.add_subplot(3, 2, 2)
    ax_data = figure.add_subplot(3, 2, 4, sharex=ax_synth_data)
    ax_proba = figure.add_subplot(4, 2, 8, sharex=ax_synth_data)
    draw_static_displacement(ax_map, disp_stations, station_coordinates_subset, cut_map=False, draw_tremors=False)

    norm = mcolors.TwoSlopeNorm(vmin=-5, vcenter=0., vmax=3)
    matshow_extent = [time_array[0], time_array[-1], station_coordinates_subset[:, 0].min(),
                      station_coordinates_subset[:, 0].max()]
    print(matshow_extent)
    _ = ax_synth_data.matshow(modeled_time_series[latsort, :, 0], aspect='auto', norm=norm,
                              cmap='RdBu_r', extent=matshow_extent)

    every_nth = 17
    ax_synth_data.set_yticks(np.around(station_coordinates_subset[latsort, 0], decimals=1))
    for n, label in enumerate(ax_synth_data.yaxis.get_ticklabels()):
        if n % every_nth != 0:
            label.set_visible(False)
            ax_synth_data.yaxis.get_major_ticks()[n].set_visible(False)

    # ax_synth_data.axvline(x=window_length//2 + delta_win, linestyle='--', linewidth=1)
    # ax_synth_data.axvline(x=window_length // 2 + delta_win + itoh_duration, linestyle='--', linewidth=1)

    ms = ax_data.matshow(synthetic_ts[latsort, :, 0], aspect='auto', norm=norm,
                         cmap='RdBu_r', extent=matshow_extent)

    ax_data.set_yticks(np.around(station_coordinates_subset[latsort, 0], decimals=1))
    for n, label in enumerate(ax_data.yaxis.get_ticklabels()):
        if n % every_nth != 0:
            label.set_visible(False)
            ax_data.yaxis.get_major_ticks()[n].set_visible(False)

    ax_data.set_ylabel('Latitude', labelpad=7)
    ax_synth_data.set_ylabel('Latitude', labelpad=7)

    for ax in [ax_synth_data, ax_data]:
        ax.tick_params(
            axis='x',  # changes apply to the x-axis
            which='both',  # both major and minor ticks are affected
            bottom=True,  # ticks along the bottom edge are on
            top=False,  # ticks along the top edge are off
            labelbottom=False,
            labeltop=False)  # labels along the bottom edge are off

    print('Predicted duration from proba', np.sum(proba > 0.5), 'days')

    # plt.colorbar(ms, ax=cbar_ax)

    ax_proba.plot(proba)
    prob_mask = proba > 0.5
    sse_cat_inferred = []
    t = 0
    while t < len(time_array):
        if prob_mask[t] != 0:
            start_t = t
            while prob_mask[t] != 0:
                t += 1
            sse_cat_inferred.append((start_t, t))
        else:
            t += 1
    sse_cat_inferred = np.array(sse_cat_inferred)
    print(sse_cat_inferred)

    for i, span in enumerate(sse_cat_inferred):
        ax_proba.axvspan(span[0], span[1], ymin=0, alpha=0.25, color='C0', zorder=0, linewidth=0.)

    ax_proba.set_ylabel('SSE probability')
    ax_proba.set_xlabel('Synthetic time [days]')

    h = ax_synth_data.get_position().y1 / 2 - ax_data.get_position().y0 / 2
    w = 0.01
    cax = plt.gcf().add_axes(
        [ax_data.get_position().x1 * 1.03, ax_data.get_position().y0 + h / 2, w, h])  # add_axes([x1, y1, w1, h])

    cbar = plt.colorbar(ms, cax=cax)
    cbar.ax.set_ylabel('Displacement [mm]', rotation=270, labelpad=17)

    # plt.tight_layout()
    plt.savefig('./sse_figures/long_event.pdf')
    plt.close(figure)
    # plt.show()


def large_event_figure(noise, window_length=60):
    # Synthetic SSE

    n = 1
    admissible_depth, admissible_strike, admissible_dip, region = read_from_slab2()
    uniform_vector = np.random.uniform(0, 1, (n, 3))
    max_stress_drop = 0.1 * 1e06  # Gao et al., 2012
    min_stress_drop = 0.01 * 1e06  # Gao et al., 2012
    mean_stress_drop = 0.5 * (max_stress_drop + min_stress_drop)
    variation_factor = 10
    std_underlying_normal = np.sqrt(np.log(variation_factor ** 2 + 1))  # from coefficient of variation
    mean_underlying_normal = np.log(mean_stress_drop) - std_underlying_normal ** 2 / 2
    lognormal_vector = np.random.lognormal(mean_underlying_normal, std_underlying_normal, (n,))
    n_stations = station_coordinates_subset.shape[0]
    disp_stations = np.zeros((n_stations, 3))

    positions = [(48.5, -125), (48.0, -124), (46.0, -124)]
    # positions = [(46.0, -124)]
    delta_win = 30
    offset = 0

    rnd_idx = 128  # np.random.randint(noise.shape[1] - window_length - delta_win)
    print('random index:', rnd_idx)
    cut_noise = noise[:,
                rnd_idx - window_length // 2 - delta_win:rnd_idx + window_length + window_length // 2 + delta_win, :]
    modeled_time_series = np.zeros(cut_noise.shape)
    for j, pos in enumerate(positions):
        partial_modeled_time_series = np.zeros(cut_noise.shape)
        partial_disp_stations = np.zeros((n_stations, 3))
        results = _custom_synthetic_displacement_stations_cascadia(0, admissible_depth, admissible_strike,
                                                                   admissible_dip,
                                                                   station_coordinates_subset,
                                                                   uniform_vector=uniform_vector,
                                                                   lognormal_vector=lognormal_vector,
                                                                   magnitude_range=(6.5, 6.6),
                                                                   epi_lat=pos[0], epi_lon=pos[1])

        for direction in range(3):
            partial_disp_stations[:, direction] = results[0][direction]
        catalog = results[1:]

        print(catalog)

        transients = np.zeros((len(station_codes_subset), 60, 2))
        window_length = 60
        center = window_length // 2
        random_duration = 20
        transient_time_array = np.linspace(0, window_length, window_length)
        for station in range(len(station_codes_subset)):
            for direction in range(2):
                transients[station, :, direction] = sigmoidal_rise_time(transient_time_array,
                                                                        partial_disp_stations[station, direction],
                                                                        random_duration, center)
        disp_stations += partial_disp_stations
        # modeled_time_series[:, window_length // 2 + delta_win:-window_length // 2 - delta_win, :] = transients
        print(window_length // 2 + delta_win + (j - 1) * random_duration,
              window_length // 2 + delta_win + (j - 1) * random_duration + window_length)

        partial_modeled_time_series[:,
        window_length // 2 + delta_win + (j - 1) * random_duration - offset:window_length // 2 + delta_win + (
                j - 1) * random_duration + window_length - offset, :] += transients
        # plt.matshow(partial_modeled_time_series[latsort, :, 0]); plt.show()
        for i in range(n_directions):
            partial_modeled_time_series[:,
            window_length // 2 + delta_win + (j - 1) * random_duration + window_length - offset:, i] += \
                transients[:, -1, i][..., np.newaxis] * np.ones(
                    (n_selected_stations, window_length // 2 + delta_win - (j - 1) * random_duration + offset))
        modeled_time_series += partial_modeled_time_series
        # plt.matshow(modeled_time_series[latsort, :, 0]); plt.show()
    # plt.matshow(transients[latsort, :, 0])
    # plt.show()

    synthetic_ts = cut_noise + modeled_time_series

    synthetic_ts[np.isnan(synthetic_ts)] = 0.

    ########

    time_array = np.arange(synthetic_ts.shape[1])
    time_filter = np.logical_and(time_array >= 50, time_array < 135)

    proba = predict_runinng_window(synthetic_ts)

    synthetic_ts = synthetic_ts[:, time_filter, :]
    modeled_time_series = modeled_time_series[:, time_filter, :]

    # final figure
    figure = plt.figure(figsize=(14.5, 8.5), dpi=100)
    ax_map = figure.add_subplot(1, 2, 1)
    ax_synth_data = figure.add_subplot(3, 2, 2)
    ax_data = figure.add_subplot(3, 2, 4, sharex=ax_synth_data)
    ax_proba = figure.add_subplot(4, 2, 8, sharex=ax_synth_data)
    draw_static_displacement(ax_map, disp_stations, station_coordinates_subset, cut_map=False, draw_tremors=False)

    norm = mcolors.TwoSlopeNorm(vmin=-5, vcenter=0., vmax=3)
    matshow_extent = [0, len(time_array[time_filter]), station_coordinates_subset[:, 0].min(),
                      station_coordinates_subset[:, 0].max()]
    print(matshow_extent)
    _ = ax_synth_data.matshow(modeled_time_series[latsort, :, 0], aspect='auto', norm=norm,
                              cmap='RdBu_r', extent=matshow_extent)

    every_nth = 17
    ax_synth_data.set_yticks(np.around(station_coordinates_subset[latsort, 0], decimals=1))
    for n, label in enumerate(ax_synth_data.yaxis.get_ticklabels()):
        if n % every_nth != 0:
            label.set_visible(False)
            ax_synth_data.yaxis.get_major_ticks()[n].set_visible(False)

    # ax_synth_data.axvline(x=window_length//2 + delta_win, linestyle='--', linewidth=1)
    # ax_synth_data.axvline(x=window_length // 2 + delta_win + itoh_duration, linestyle='--', linewidth=1)

    ms = ax_data.matshow(synthetic_ts[latsort, :, 0], aspect='auto', norm=norm,
                         cmap='RdBu_r', extent=matshow_extent)

    ax_data.set_yticks(np.around(station_coordinates_subset[latsort, 0], decimals=1))
    for n, label in enumerate(ax_data.yaxis.get_ticklabels()):
        if n % every_nth != 0:
            label.set_visible(False)
            ax_data.yaxis.get_major_ticks()[n].set_visible(False)

    ax_data.set_ylabel('Latitude', labelpad=7)
    ax_synth_data.set_ylabel('Latitude', labelpad=7)

    for ax in [ax_synth_data, ax_data]:
        ax.tick_params(
            axis='x',  # changes apply to the x-axis
            which='both',  # both major and minor ticks are affected
            bottom=True,  # ticks along the bottom edge are on
            top=False,  # ticks along the top edge are off
            labelbottom=False,
            labeltop=False)  # labels along the bottom edge are off

    print('Predicted duration from proba', np.sum(proba > 0.5), 'days')

    # plt.colorbar(ms, ax=cbar_ax)

    ax_proba.plot(proba[time_filter])
    prob_mask = proba > 0.5
    sse_cat_inferred = []
    t = 0
    while t < len(time_array):
        if prob_mask[t] != 0:
            start_t = t
            while prob_mask[t] != 0:
                t += 1
            sse_cat_inferred.append((start_t, t))
        else:
            t += 1
    sse_cat_inferred = np.array(sse_cat_inferred)
    print(sse_cat_inferred)

    '''for i, span in enumerate(sse_cat_inferred):
        ax_proba.axvspan(span[0], span[1], ymin=0, alpha=0.25, color='C0', zorder=0, linewidth=0.)'''

    ax_proba.axvspan(sse_cat_inferred[1, 0] - 50, sse_cat_inferred[1, 1] - 50, ymin=0, alpha=0.25, color='C0', zorder=0,
                     linewidth=0.)

    ax_proba.set_ylabel('SSE probability')
    ax_proba.set_xlabel('Synthetic time [days]')

    h = ax_synth_data.get_position().y1 / 2 - ax_data.get_position().y0 / 2
    w = 0.01
    cax = plt.gcf().add_axes(
        [ax_data.get_position().x1 * 1.03, ax_data.get_position().y0 + h / 2, w, h])  # add_axes([x1, y1, w1, h])

    cbar = plt.colorbar(ms, cax=cax)
    cbar.ax.set_ylabel('Displacement [mm]', rotation=270, labelpad=17)

    # plt.tight_layout()
    plt.savefig('./sse_figures/long_event.pdf')
    plt.close(figure)
    # plt.show()


def two_event_figure_old(noise, window_length=60):
    # Synthetic SSE

    n = 1
    admissible_depth, admissible_strike, admissible_dip, region = read_from_slab2()
    uniform_vector = np.random.uniform(0, 1, (n, 3))
    max_stress_drop = 0.1 * 1e06  # Gao et al., 2012
    min_stress_drop = 0.01 * 1e06  # Gao et al., 2012
    mean_stress_drop = 0.5 * (max_stress_drop + min_stress_drop)
    variation_factor = 10
    std_underlying_normal = np.sqrt(np.log(variation_factor ** 2 + 1))  # from coefficient of variation
    mean_underlying_normal = np.log(mean_stress_drop) - std_underlying_normal ** 2 / 2
    lognormal_vector = np.random.lognormal(mean_underlying_normal, std_underlying_normal, (n,))
    n_stations = station_coordinates_subset.shape[0]
    disp_stations = np.zeros((n_stations, 3))

    # positions = [(48.5, -125), (48.0, -124), (46.0, -124)]
    # positions = [(48.5, -125), (46.0, -124)]
    positions = [(48.5, -125), (44.0, -124)]
    delta_win = 30
    offset = 0

    rnd_idx = 128 + 60 * 10  # np.random.randint(noise.shape[1] - window_length)
    print('random index:', rnd_idx)
    cut_noise = noise[:,
                rnd_idx - window_length // 2 - delta_win:rnd_idx + window_length + window_length // 2 + delta_win, :]
    modeled_time_series = np.zeros(cut_noise.shape)
    for j, pos in enumerate(positions):
        partial_modeled_time_series = np.zeros(cut_noise.shape)
        partial_disp_stations = np.zeros((n_stations, 3))
        results = _custom_synthetic_displacement_stations_cascadia(0, admissible_depth, admissible_strike,
                                                                   admissible_dip,
                                                                   station_coordinates_subset,
                                                                   uniform_vector=uniform_vector,
                                                                   lognormal_vector=lognormal_vector,
                                                                   magnitude_range=(6.5, 6.6),
                                                                   epi_lat=pos[0], epi_lon=pos[1])

        for direction in range(3):
            partial_disp_stations[:, direction] = results[0][direction]
        catalog = results[1:]

        print(catalog)

        transients = np.zeros((len(station_codes_subset), 60, 2))
        window_length = 60
        center = window_length // 2
        random_duration = 20
        transient_time_array = np.linspace(0, window_length, window_length)
        for station in range(len(station_codes_subset)):
            for direction in range(2):
                transients[station, :, direction] = sigmoidal_rise_time(transient_time_array,
                                                                        partial_disp_stations[station, direction],
                                                                        random_duration, center)
        disp_stations += partial_disp_stations
        # modeled_time_series[:, window_length // 2 + delta_win:-window_length // 2 - delta_win, :] = transients
        print(window_length // 2 + delta_win + (j - 1) * random_duration,
              window_length // 2 + delta_win + (j - 1) * random_duration + window_length)

        partial_modeled_time_series[:,
        window_length // 2 + delta_win + (j - 1) * random_duration - offset:window_length // 2 + delta_win + (
                j - 1) * random_duration + window_length - offset, :] += transients
        # plt.matshow(partial_modeled_time_series[latsort, :, 0]);plt.show()
        for i in range(n_directions):
            partial_modeled_time_series[:,
            window_length // 2 + delta_win + (j - 1) * random_duration + window_length - offset:, i] += \
                transients[:, -1, i][..., np.newaxis] * np.ones(
                    (n_selected_stations, window_length // 2 + delta_win - (j - 1) * random_duration + offset))
        modeled_time_series += partial_modeled_time_series
        # plt.matshow(modeled_time_series[latsort, :, 0]);plt.show()
    # plt.matshow(transients[latsort, :, 0])
    # plt.show()

    synthetic_ts = cut_noise + modeled_time_series
    ########

    time_array = np.arange(synthetic_ts.shape[1])

    proba = predict_runinng_window(synthetic_ts)

    # final figure
    figure = plt.figure(figsize=(14.5, 8.5), dpi=100)
    ax_map = figure.add_subplot(1, 2, 1)
    ax_synth_data = figure.add_subplot(3, 2, 2)
    ax_data = figure.add_subplot(3, 2, 4, sharex=ax_synth_data)
    ax_proba = figure.add_subplot(4, 2, 8, sharex=ax_synth_data)
    draw_static_displacement(ax_map, disp_stations, station_coordinates_subset, cut_map=False, draw_tremors=False)

    norm = mcolors.TwoSlopeNorm(vmin=-5, vcenter=0., vmax=3)
    matshow_extent = [time_array[0], time_array[-1], station_coordinates_subset[:, 0].min(),
                      station_coordinates_subset[:, 0].max()]
    print(matshow_extent)
    _ = ax_synth_data.matshow(modeled_time_series[latsort, :, 0], aspect='auto', norm=norm,
                              cmap='RdBu_r', extent=matshow_extent)

    every_nth = 17
    ax_synth_data.set_yticks(np.around(station_coordinates_subset[latsort, 0], decimals=1))
    for n, label in enumerate(ax_synth_data.yaxis.get_ticklabels()):
        if n % every_nth != 0:
            label.set_visible(False)
            ax_synth_data.yaxis.get_major_ticks()[n].set_visible(False)

    # ax_synth_data.axvline(x=window_length//2 + delta_win, linestyle='--', linewidth=1)
    # ax_synth_data.axvline(x=window_length // 2 + delta_win + itoh_duration, linestyle='--', linewidth=1)

    ms = ax_data.matshow(synthetic_ts[latsort, :, 0], aspect='auto', norm=norm,
                         cmap='RdBu_r', extent=matshow_extent)

    ax_data.set_yticks(np.around(station_coordinates_subset[latsort, 0], decimals=1))
    for n, label in enumerate(ax_data.yaxis.get_ticklabels()):
        if n % every_nth != 0:
            label.set_visible(False)
            ax_data.yaxis.get_major_ticks()[n].set_visible(False)

    for ax in [ax_synth_data, ax_data]:
        ax.tick_params(
            axis='x',  # changes apply to the x-axis
            which='both',  # both major and minor ticks are affected
            bottom=True,  # ticks along the bottom edge are on
            top=False,  # ticks along the top edge are off
            labelbottom=False,
            labeltop=False)  # labels along the bottom edge are off

    ax_data.set_ylabel('Latitude', labelpad=7)
    ax_synth_data.set_ylabel('Latitude', labelpad=7)

    print('Predicted duration from proba', np.sum(proba > 0.5), 'days')

    # plt.colorbar(ms, ax=cbar_ax)

    ax_proba.plot(proba)
    prob_mask = proba > 0.5
    sse_cat_inferred = []
    t = 0
    while t < len(time_array):
        if prob_mask[t] != 0:
            start_t = t
            while prob_mask[t] != 0:
                t += 1
            sse_cat_inferred.append((start_t, t))
        else:
            t += 1
    sse_cat_inferred = np.array(sse_cat_inferred)
    print(sse_cat_inferred)

    for i, span in enumerate(sse_cat_inferred):
        ax_proba.axvspan(span[0], span[1], ymin=0, alpha=0.25, color='C0', zorder=0, linewidth=0.)

    ax_proba.set_ylabel('SSE probability')
    ax_proba.set_xlabel('Synthetic time [days]')

    h = ax_synth_data.get_position().y1 / 2 - ax_data.get_position().y0 / 2
    w = 0.01
    cax = plt.gcf().add_axes(
        [ax_data.get_position().x1 * 1.03, ax_data.get_position().y0 + h / 2, w, h])  # add_axes([x1, y1, w1, h])

    cbar = plt.colorbar(ms, cax=cax)
    cbar.ax.set_ylabel('Displacement [mm]', rotation=270, labelpad=17)

    # plt.tight_layout()
    plt.savefig('./sse_figures/two_non_synch_events.pdf')
    plt.close(figure)
    # plt.show()


def two_event_figure(noise, window_length=60):
    # Synthetic SSE

    n = 1
    admissible_depth, admissible_strike, admissible_dip, region = read_from_slab2()
    uniform_vector = np.random.uniform(0, 1, (n, 3))
    max_stress_drop = 0.1 * 1e06  # Gao et al., 2012
    min_stress_drop = 0.01 * 1e06  # Gao et al., 2012
    mean_stress_drop = 0.5 * (max_stress_drop + min_stress_drop)
    variation_factor = 10
    std_underlying_normal = np.sqrt(np.log(variation_factor ** 2 + 1))  # from coefficient of variation
    mean_underlying_normal = np.log(mean_stress_drop) - std_underlying_normal ** 2 / 2
    lognormal_vector = np.random.lognormal(mean_underlying_normal, std_underlying_normal, (n,))
    n_stations = station_coordinates_subset.shape[0]
    disp_stations = np.zeros((n_stations, 3))

    # positions = [(48.5, -125), (48.0, -124), (46.0, -124)]
    # positions = [(48.5, -125), (46.0, -124)]
    positions = [(48.5, -125), (44.0, -124)]
    delta_win = 30
    offset = 0

    rnd_idx = 128 + 60 * 10  # np.random.randint(noise.shape[1] - window_length)
    print('random index:', rnd_idx)
    cut_noise = noise[:,
                rnd_idx - window_length // 2 - delta_win:rnd_idx + window_length + window_length // 2 + delta_win, :]
    modeled_time_series = np.zeros(cut_noise.shape)
    for j, pos in enumerate(positions):
        partial_modeled_time_series = np.zeros(cut_noise.shape)
        partial_disp_stations = np.zeros((n_stations, 3))
        results = _custom_synthetic_displacement_stations_cascadia(0, admissible_depth, admissible_strike,
                                                                   admissible_dip,
                                                                   station_coordinates_subset,
                                                                   uniform_vector=uniform_vector,
                                                                   lognormal_vector=lognormal_vector,
                                                                   magnitude_range=(6.5, 6.6),
                                                                   epi_lat=pos[0], epi_lon=pos[1])

        for direction in range(3):
            partial_disp_stations[:, direction] = results[0][direction]
        catalog = results[1:]

        print(catalog)

        transients = np.zeros((len(station_codes_subset), 60, 2))
        window_length = 60
        center = window_length // 2
        random_duration = 20
        transient_time_array = np.linspace(0, window_length, window_length)
        for station in range(len(station_codes_subset)):
            for direction in range(2):
                transients[station, :, direction] = sigmoidal_rise_time(transient_time_array,
                                                                        partial_disp_stations[station, direction],
                                                                        random_duration, center)
        disp_stations += partial_disp_stations
        # modeled_time_series[:, window_length // 2 + delta_win:-window_length // 2 - delta_win, :] = transients
        print(window_length // 2 + delta_win + (j - 1) * random_duration,
              window_length // 2 + delta_win + (j - 1) * random_duration + window_length)

        partial_modeled_time_series[:,
        window_length // 2 + delta_win + (j - 1) * random_duration - offset:window_length // 2 + delta_win + (
                j - 1) * random_duration + window_length - offset, :] += transients
        # plt.matshow(partial_modeled_time_series[latsort, :, 0]);plt.show()
        for i in range(n_directions):
            partial_modeled_time_series[:,
            window_length // 2 + delta_win + (j - 1) * random_duration + window_length - offset:, i] += \
                transients[:, -1, i][..., np.newaxis] * np.ones(
                    (n_selected_stations, window_length // 2 + delta_win - (j - 1) * random_duration + offset))
        modeled_time_series += partial_modeled_time_series
        # plt.matshow(modeled_time_series[latsort, :, 0]);plt.show()
    # plt.matshow(transients[latsort, :, 0])
    # plt.show()

    synthetic_ts = cut_noise + modeled_time_series
    ########

    time_array = np.arange(synthetic_ts.shape[1])
    time_filter = np.logical_and(time_array >= 64, time_array < 115)

    proba = predict_runinng_window(synthetic_ts)

    synthetic_ts = synthetic_ts[:, time_filter, :]
    modeled_time_series = modeled_time_series[:, time_filter, :]

    # final figure
    figure = plt.figure(figsize=(14.5, 8.5), dpi=100)
    ax_map = figure.add_subplot(1, 2, 1)
    ax_synth_data = figure.add_subplot(3, 2, 2)
    ax_data = figure.add_subplot(3, 2, 4, sharex=ax_synth_data)
    ax_proba = figure.add_subplot(4, 2, 8, sharex=ax_synth_data)
    draw_static_displacement(ax_map, disp_stations, station_coordinates_subset, cut_map=False, draw_tremors=False)

    norm = mcolors.TwoSlopeNorm(vmin=-5, vcenter=0., vmax=3)
    matshow_extent = [0, len(time_array[time_filter]), station_coordinates_subset[:, 0].min(),
                      station_coordinates_subset[:, 0].max()]
    print(matshow_extent)
    _ = ax_synth_data.matshow(modeled_time_series[latsort, :, 0], aspect='auto', norm=norm,
                              cmap='RdBu_r', extent=matshow_extent)

    every_nth = 17
    ax_synth_data.set_yticks(np.around(station_coordinates_subset[latsort, 0], decimals=1))
    for n, label in enumerate(ax_synth_data.yaxis.get_ticklabels()):
        if n % every_nth != 0:
            label.set_visible(False)
            ax_synth_data.yaxis.get_major_ticks()[n].set_visible(False)

    # ax_synth_data.axvline(x=window_length//2 + delta_win, linestyle='--', linewidth=1)
    # ax_synth_data.axvline(x=window_length // 2 + delta_win + itoh_duration, linestyle='--', linewidth=1)

    ms = ax_data.matshow(synthetic_ts[latsort, :, 0], aspect='auto', norm=norm,
                         cmap='RdBu_r', extent=matshow_extent)

    ax_data.set_yticks(np.around(station_coordinates_subset[latsort, 0], decimals=1))
    for n, label in enumerate(ax_data.yaxis.get_ticklabels()):
        if n % every_nth != 0:
            label.set_visible(False)
            ax_data.yaxis.get_major_ticks()[n].set_visible(False)

    for ax in [ax_synth_data, ax_data]:
        ax.tick_params(
            axis='x',  # changes apply to the x-axis
            which='both',  # both major and minor ticks are affected
            bottom=True,  # ticks along the bottom edge are on
            top=False,  # ticks along the top edge are off
            labelbottom=False,
            labeltop=False)  # labels along the bottom edge are off

    ax_data.set_ylabel('Latitude', labelpad=7)
    ax_synth_data.set_ylabel('Latitude', labelpad=7)

    print('Predicted duration from proba', np.sum(proba > 0.5), 'days')

    # plt.colorbar(ms, ax=cbar_ax)

    ax_proba.plot(proba[time_filter])
    prob_mask = proba > 0.5
    sse_cat_inferred = []
    t = 0
    while t < len(time_array):
        if prob_mask[t] != 0:
            start_t = t
            while prob_mask[t] != 0:
                t += 1
            sse_cat_inferred.append((start_t, t))
        else:
            t += 1
    sse_cat_inferred = np.array(sse_cat_inferred)
    print(sse_cat_inferred)

    '''for i, span in enumerate(sse_cat_inferred):
        ax_proba.axvspan(span[0], span[1], ymin=0, alpha=0.25, color='C0', zorder=0, linewidth=0.)'''

    ax_proba.axvspan(sse_cat_inferred[1, 0] - 64, sse_cat_inferred[1, 1] - 64, ymin=0, alpha=0.25, color='C0', zorder=0,
                     linewidth=0.)

    ax_proba.set_ylabel('SSE probability')
    ax_proba.set_xlabel('Synthetic time [days]')

    h = ax_synth_data.get_position().y1 / 2 - ax_data.get_position().y0 / 2
    w = 0.01
    cax = plt.gcf().add_axes(
        [ax_data.get_position().x1 * 1.03, ax_data.get_position().y0 + h / 2, w, h])  # add_axes([x1, y1, w1, h])

    cbar = plt.colorbar(ms, cax=cax)
    cbar.ax.set_ylabel('Displacement [mm]', rotation=270, labelpad=17)

    # plt.tight_layout()
    plt.savefig('./sse_figures/two_non_synch_events.pdf')
    plt.close(figure)
    # plt.show()


def two_event_same_time_figure_old(noise, window_length=60):
    # Synthetic SSE

    n = 1
    admissible_depth, admissible_strike, admissible_dip, region = read_from_slab2()
    uniform_vector = np.random.uniform(0, 1, (n, 3))
    max_stress_drop = 0.1 * 1e06  # Gao et al., 2012
    min_stress_drop = 0.01 * 1e06  # Gao et al., 2012
    mean_stress_drop = 0.5 * (max_stress_drop + min_stress_drop)
    variation_factor = 10
    std_underlying_normal = np.sqrt(np.log(variation_factor ** 2 + 1))  # from coefficient of variation
    mean_underlying_normal = np.log(mean_stress_drop) - std_underlying_normal ** 2 / 2
    lognormal_vector = np.random.lognormal(mean_underlying_normal, std_underlying_normal, (n,))
    n_stations = station_coordinates_subset.shape[0]
    disp_stations = np.zeros((n_stations, 3))

    # positions = [(48.5, -125), (48.0, -124), (46.0, -124)]
    positions = [(48.5, -125), (43.0, -124)]
    delta_win = 30
    offset = 0

    rnd_idx = 128 + 60 * 7  # np.random.randint(noise.shape[1] - window_length)
    print('random index:', rnd_idx)
    cut_noise = noise[:,
                rnd_idx - window_length // 2 - delta_win:rnd_idx + window_length + window_length // 2 + delta_win, :]
    modeled_time_series = np.zeros(cut_noise.shape)
    for j, pos in enumerate(positions):
        partial_modeled_time_series = np.zeros(cut_noise.shape)
        partial_disp_stations = np.zeros((n_stations, 3))
        results = _custom_synthetic_displacement_stations_cascadia(0, admissible_depth, admissible_strike,
                                                                   admissible_dip,
                                                                   station_coordinates_subset,
                                                                   uniform_vector=uniform_vector,
                                                                   lognormal_vector=lognormal_vector,
                                                                   magnitude_range=(6.5, 6.6),
                                                                   epi_lat=pos[0], epi_lon=pos[1])

        for direction in range(3):
            partial_disp_stations[:, direction] = results[0][direction]
        catalog = results[1:]

        print(catalog)

        transients = np.zeros((len(station_codes_subset), 60, 2))
        window_length = 60
        center = window_length // 2
        random_duration = 20
        transient_time_array = np.linspace(0, window_length, window_length)
        for station in range(len(station_codes_subset)):
            for direction in range(2):
                transients[station, :, direction] = sigmoidal_rise_time(transient_time_array,
                                                                        partial_disp_stations[station, direction],
                                                                        random_duration, center)
        disp_stations += partial_disp_stations
        # modeled_time_series[:, window_length // 2 + delta_win:-window_length // 2 - delta_win, :] = transients

        partial_modeled_time_series[:,
        window_length // 2 + delta_win - offset:window_length // 2 + delta_win + window_length - offset,
        :] += transients
        # plt.matshow(partial_modeled_time_series[latsort, :, 0]);plt.show()
        for i in range(n_directions):
            partial_modeled_time_series[:, window_length // 2 + delta_win + window_length - offset:, i] += \
                transients[:, -1, i][..., np.newaxis] * np.ones(
                    (n_selected_stations, window_length // 2 + delta_win + offset))
        modeled_time_series += partial_modeled_time_series
        # plt.matshow(modeled_time_series[latsort, :, 0]);plt.show()
    # plt.matshow(transients[latsort, :, 0])
    # plt.show()

    synthetic_ts = cut_noise + modeled_time_series
    ########

    time_array = np.arange(synthetic_ts.shape[1])

    proba = predict_runinng_window(synthetic_ts)

    # final figure
    figure = plt.figure(figsize=(14.5, 8.5), dpi=100)
    ax_map = figure.add_subplot(1, 2, 1)
    ax_synth_data = figure.add_subplot(3, 2, 2)
    ax_data = figure.add_subplot(3, 2, 4, sharex=ax_synth_data)
    ax_proba = figure.add_subplot(4, 2, 8, sharex=ax_synth_data)
    draw_static_displacement(ax_map, disp_stations, station_coordinates_subset, cut_map=False, draw_tremors=False)

    norm = mcolors.TwoSlopeNorm(vmin=-5, vcenter=0., vmax=3)
    matshow_extent = [time_array[0], time_array[-1], station_coordinates_subset[:, 0].min(),
                      station_coordinates_subset[:, 0].max()]
    print(matshow_extent)
    _ = ax_synth_data.matshow(modeled_time_series[latsort, :, 0], aspect='auto', norm=norm,
                              cmap='RdBu_r', extent=matshow_extent)

    every_nth = 17
    ax_synth_data.set_yticks(np.around(station_coordinates_subset[latsort, 0], decimals=1))
    for n, label in enumerate(ax_synth_data.yaxis.get_ticklabels()):
        if n % every_nth != 0:
            label.set_visible(False)
            ax_synth_data.yaxis.get_major_ticks()[n].set_visible(False)

    # ax_synth_data.axvline(x=window_length//2 + delta_win, linestyle='--', linewidth=1)
    # ax_synth_data.axvline(x=window_length // 2 + delta_win + itoh_duration, linestyle='--', linewidth=1)

    ms = ax_data.matshow(synthetic_ts[latsort, :, 0], aspect='auto', norm=norm,
                         cmap='RdBu_r', extent=matshow_extent)

    ax_data.set_yticks(np.around(station_coordinates_subset[latsort, 0], decimals=1))
    for n, label in enumerate(ax_data.yaxis.get_ticklabels()):
        if n % every_nth != 0:
            label.set_visible(False)
            ax_data.yaxis.get_major_ticks()[n].set_visible(False)

    for ax in [ax_synth_data, ax_data]:
        ax.tick_params(
            axis='x',  # changes apply to the x-axis
            which='both',  # both major and minor ticks are affected
            bottom=True,  # ticks along the bottom edge are on
            top=False,  # ticks along the top edge are off
            labelbottom=False,
            labeltop=False)  # labels along the bottom edge are off

    ax_data.set_ylabel('Latitude', labelpad=7)
    ax_synth_data.set_ylabel('Latitude', labelpad=7)

    print('Predicted duration from proba', np.sum(proba > 0.5), 'days')

    # plt.colorbar(ms, ax=cbar_ax)

    ax_proba.plot(proba)
    prob_mask = proba > 0.5
    sse_cat_inferred = []
    t = 0
    while t < len(time_array):
        if prob_mask[t] != 0:
            start_t = t
            while prob_mask[t] != 0:
                t += 1
            sse_cat_inferred.append((start_t, t))
        else:
            t += 1
    sse_cat_inferred = np.array(sse_cat_inferred)
    print(sse_cat_inferred)

    for i, span in enumerate(sse_cat_inferred):
        ax_proba.axvspan(span[0], span[1], ymin=0, alpha=0.25, color='C0', zorder=0, linewidth=0.)

    ax_proba.set_ylabel('SSE probability')
    ax_proba.set_xlabel('Synthetic time [days]')

    h = ax_synth_data.get_position().y1 / 2 - ax_data.get_position().y0 / 2
    w = 0.01
    cax = plt.gcf().add_axes(
        [ax_data.get_position().x1 * 1.03, ax_data.get_position().y0 + h / 2, w, h])  # add_axes([x1, y1, w1, h])

    cbar = plt.colorbar(ms, cax=cax)
    cbar.ax.set_ylabel('Displacement [mm]', rotation=270, labelpad=17)

    # plt.tight_layout()
    plt.savefig('./sse_figures/two_synch_events.pdf')
    plt.close(figure)
    # plt.show()


def two_event_same_time_figure(noise, window_length=60):
    # Synthetic SSE

    n = 1
    admissible_depth, admissible_strike, admissible_dip, region = read_from_slab2()
    uniform_vector = np.random.uniform(0, 1, (n, 3))
    max_stress_drop = 0.1 * 1e06  # Gao et al., 2012
    min_stress_drop = 0.01 * 1e06  # Gao et al., 2012
    mean_stress_drop = 0.5 * (max_stress_drop + min_stress_drop)
    variation_factor = 10
    std_underlying_normal = np.sqrt(np.log(variation_factor ** 2 + 1))  # from coefficient of variation
    mean_underlying_normal = np.log(mean_stress_drop) - std_underlying_normal ** 2 / 2
    lognormal_vector = np.random.lognormal(mean_underlying_normal, std_underlying_normal, (n,))
    n_stations = station_coordinates_subset.shape[0]
    disp_stations = np.zeros((n_stations, 3))

    # positions = [(48.5, -125), (48.0, -124), (46.0, -124)]
    positions = [(48.5, -125), (43.0, -124)]
    delta_win = 30
    offset = 0

    rnd_idx = 128 + 60 * 7  # np.random.randint(noise.shape[1] - window_length)
    print('random index:', rnd_idx)
    cut_noise = noise[:,
                rnd_idx - window_length // 2 - delta_win:rnd_idx + window_length + window_length // 2 + delta_win, :]
    modeled_time_series = np.zeros(cut_noise.shape)
    for j, pos in enumerate(positions):
        partial_modeled_time_series = np.zeros(cut_noise.shape)
        partial_disp_stations = np.zeros((n_stations, 3))
        results = _custom_synthetic_displacement_stations_cascadia(0, admissible_depth, admissible_strike,
                                                                   admissible_dip,
                                                                   station_coordinates_subset,
                                                                   uniform_vector=uniform_vector,
                                                                   lognormal_vector=lognormal_vector,
                                                                   magnitude_range=(6.5, 6.6),
                                                                   epi_lat=pos[0], epi_lon=pos[1])

        for direction in range(3):
            partial_disp_stations[:, direction] = results[0][direction]
        catalog = results[1:]

        print(catalog)

        transients = np.zeros((len(station_codes_subset), 60, 2))
        window_length = 60
        center = window_length // 2
        random_duration = 20
        transient_time_array = np.linspace(0, window_length, window_length)
        for station in range(len(station_codes_subset)):
            for direction in range(2):
                transients[station, :, direction] = sigmoidal_rise_time(transient_time_array,
                                                                        partial_disp_stations[station, direction],
                                                                        random_duration, center)
        disp_stations += partial_disp_stations
        # modeled_time_series[:, window_length // 2 + delta_win:-window_length // 2 - delta_win, :] = transients

        partial_modeled_time_series[:,
        window_length // 2 + delta_win - offset:window_length // 2 + delta_win + window_length - offset,
        :] += transients
        # plt.matshow(partial_modeled_time_series[latsort, :, 0]);plt.show()
        for i in range(n_directions):
            partial_modeled_time_series[:, window_length // 2 + delta_win + window_length - offset:, i] += \
                transients[:, -1, i][..., np.newaxis] * np.ones(
                    (n_selected_stations, window_length // 2 + delta_win + offset))
        modeled_time_series += partial_modeled_time_series
        # plt.matshow(modeled_time_series[latsort, :, 0]);plt.show()
    # plt.matshow(transients[latsort, :, 0])
    # plt.show()

    synthetic_ts = cut_noise + modeled_time_series
    ########

    time_array = np.arange(synthetic_ts.shape[1])
    time_filter = np.logical_and(time_array >= 80, time_array < 100)

    proba = predict_runinng_window(synthetic_ts)

    synthetic_ts = synthetic_ts[:, time_filter, :]
    modeled_time_series = modeled_time_series[:, time_filter, :]

    # final figure
    figure = plt.figure(figsize=(14.5, 8.5), dpi=100)
    ax_map = figure.add_subplot(1, 2, 1)
    ax_synth_data = figure.add_subplot(3, 2, 2)
    ax_data = figure.add_subplot(3, 2, 4, sharex=ax_synth_data)
    ax_proba = figure.add_subplot(4, 2, 8, sharex=ax_synth_data)
    draw_static_displacement(ax_map, disp_stations, station_coordinates_subset, cut_map=False, draw_tremors=False)

    norm = mcolors.TwoSlopeNorm(vmin=-5, vcenter=0., vmax=3)
    matshow_extent = [0, len(time_array[time_filter]), station_coordinates_subset[:, 0].min(),
                      station_coordinates_subset[:, 0].max()]
    print(matshow_extent)
    _ = ax_synth_data.matshow(modeled_time_series[latsort, :, 0], aspect='auto', norm=norm,
                              cmap='RdBu_r', extent=matshow_extent)

    every_nth = 17
    ax_synth_data.set_yticks(np.around(station_coordinates_subset[latsort, 0], decimals=1))
    for n, label in enumerate(ax_synth_data.yaxis.get_ticklabels()):
        if n % every_nth != 0:
            label.set_visible(False)
            ax_synth_data.yaxis.get_major_ticks()[n].set_visible(False)

    # ax_synth_data.axvline(x=window_length//2 + delta_win, linestyle='--', linewidth=1)
    # ax_synth_data.axvline(x=window_length // 2 + delta_win + itoh_duration, linestyle='--', linewidth=1)

    ms = ax_data.matshow(synthetic_ts[latsort, :, 0], aspect='auto', norm=norm,
                         cmap='RdBu_r', extent=matshow_extent)

    ax_data.set_yticks(np.around(station_coordinates_subset[latsort, 0], decimals=1))
    for n, label in enumerate(ax_data.yaxis.get_ticklabels()):
        if n % every_nth != 0:
            label.set_visible(False)
            ax_data.yaxis.get_major_ticks()[n].set_visible(False)

    for ax in [ax_synth_data, ax_data]:
        ax.tick_params(
            axis='x',  # changes apply to the x-axis
            which='both',  # both major and minor ticks are affected
            bottom=True,  # ticks along the bottom edge are on
            top=False,  # ticks along the top edge are off
            labelbottom=False,
            labeltop=False)  # labels along the bottom edge are off

    ax_data.set_ylabel('Latitude', labelpad=7)
    ax_synth_data.set_ylabel('Latitude', labelpad=7)

    print('Predicted duration from proba', np.sum(proba > 0.5), 'days')

    # plt.colorbar(ms, ax=cbar_ax)

    ax_proba.plot(proba[time_filter])
    prob_mask = proba > 0.5
    sse_cat_inferred = []
    t = 0
    while t < len(time_array):
        if prob_mask[t] != 0:
            start_t = t
            while prob_mask[t] != 0:
                t += 1
            sse_cat_inferred.append((start_t, t))
        else:
            t += 1
    sse_cat_inferred = np.array(sse_cat_inferred)
    print(sse_cat_inferred)

    '''for i, span in enumerate(sse_cat_inferred):
        ax_proba.axvspan(span[0], span[1], ymin=0, alpha=0.25, color='C0', zorder=0, linewidth=0.)'''

    ax_proba.axvspan(sse_cat_inferred[2, 0] - 80, sse_cat_inferred[2, 1] - 80, ymin=0, alpha=0.25, color='C0', zorder=0,
                     linewidth=0.)

    ax_proba.set_ylabel('SSE probability')
    ax_proba.set_xlabel('Synthetic time [days]')

    h = ax_synth_data.get_position().y1 / 2 - ax_data.get_position().y0 / 2
    w = 0.01
    cax = plt.gcf().add_axes(
        [ax_data.get_position().x1 * 1.03, ax_data.get_position().y0 + h / 2, w, h])  # add_axes([x1, y1, w1, h])

    cbar = plt.colorbar(ms, cax=cax)
    cbar.ax.set_ylabel('Displacement [mm]', rotation=270, labelpad=17)

    # plt.tight_layout()
    plt.savefig('./sse_figures/two_synch_events.pdf')
    plt.close(figure)
    # plt.show()


if __name__ == '__main__':
    n_selected_stations = 135
    n_directions = 2
    reference_period = (2014, 2023)
    station_codes_subset, station_coordinates_subset, _, _, stations_subset_idx = cascadia_filtered_stations(
        n_selected_stations, reference_period=reference_period)
    selected_gnss_data, selected_time_array, _, _ = _preliminary_operations(reference_period=reference_period)

    selected_gnss_data = selected_gnss_data[stations_subset_idx]

    print(selected_gnss_data.shape)
    latsort = np.argsort(station_coordinates_subset[:, 0])[::-1]

    '''plt.matshow(detrend_nan(selected_time_array, selected_gnss_data)[latsort, :, 0], aspect='auto', vmin=-5, vmax=3, cmap='RdBu_r')
    plt.colorbar()
    plt.show()'''

    # noise generation
    nan_pattern = np.isnan(selected_gnss_data[:, :, 0])
    selected_gnss_data[nan_pattern] = 0.  # NaNs are replaced with zeros

    '''print(nan_pattern.shape)
    print(nan_pattern.sum(axis=1).shape)
    print(np.argmax(nan_pattern.sum(axis=1)))
    print(nan_pattern.sum(axis=1)[125])
    print(nan_pattern.sum(axis=1)[124])
    print(station_codes_subset[124], station_coordinates_subset[124])
    plt.plot(nan_pattern[124])
    plt.show()
    exit(0)'''

    synthetic_noise = []

    for direction in range(n_directions):
        surrogates = n_surrogates_stack_pca_lib(selected_gnss_data[:, :, direction], 1, n_iterations=5, parallel=False)
        synthetic_noise.append(surrogates[0])

    synthetic_noise = np.array(synthetic_noise).transpose((1, 2, 0))
    print(synthetic_noise.shape)

    synthetic_noise[nan_pattern] = np.nan

    '''plt.matshow(synthetic_noise[latsort, :, 0], aspect='auto', vmin=-5, vmax=3, cmap='RdBu_r')
    plt.colorbar()
    plt.show()'''

    # strategy: given the set of 135 stations, we set a displacement equal to Itoh's displacement to stations
    # where this is possible, zero otherwise

    # itoh_figure(synthetic_noise)

    # short_event_figure(synthetic_noise)

    # large_event_figure(synthetic_noise)

    # two_event_figure(synthetic_noise)

    two_event_same_time_figure(synthetic_noise)
