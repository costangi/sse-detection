import datetime
import sys

import matplotlib
import numpy as np
from matplotlib import pyplot as plt
from matplotlib.colors import Normalize
from matplotlib.gridspec import GridSpec
from matplotlib.patches import FancyArrowPatch
from mpl_toolkits.basemap import Basemap
from scipy.interpolate import interp1d
from scipy.ndimage import gaussian_filter
from scipy.optimize import curve_fit, least_squares
from scipy.signal import correlate, correlation_lags
from scipy.signal import peak_widths, find_peaks
from sklearn import linear_model
from sklearn.cluster import DBSCAN

from sse_generator.okada import forward as okada85
from utils import cascadia_coordinates, sse_catalogue, ymd_decimal_year_lookup, _find_nearest_val

plt.rc('font', family='Helvetica')

SMALL_SIZE = 8 + 2 + 1
MEDIUM_SIZE = 10 + 2 + 1
BIGGER_SIZE = 12 + 2 + 1

plt.rc('font', size=SMALL_SIZE)  # controls default text sizes
plt.rc('axes', titlesize=SMALL_SIZE)  # fontsize of the axes title
plt.rc('axes', labelsize=MEDIUM_SIZE)  # fontsize of the x and y labels
plt.rc('xtick', labelsize=SMALL_SIZE)  # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)  # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)  # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title


def _add_double_sided_arrow(axis, x0, y0, x1, y1, offset=0.1, size=15, color='C0'):
    positions = [[(x0 + offset, y0), (x1, y1)], [(x1 - offset, y1), (x0, y0)]]
    for pos_a, pos_b in positions:
        myArrow = FancyArrowPatch(posA=pos_a, posB=pos_b,
                                  arrowstyle='simple', color=color,
                                  mutation_scale=size, shrinkA=0, shrinkB=0)
        myArrow.set_clip_on(False)
        axis.add_artist(myArrow)


def data_normalization(data):
    return (data - np.min(data)) / (np.max(data) - np.min(data))

def _lagged_correlation(a, b, max_lag, upsampling_factor=1):
    x = np.linspace(-max_lag, max_lag, 2 * max_lag * upsampling_factor + 1, dtype=np.int_)
    corr = []
    for lag in x:
        p = np.corrcoef(a[max_lag + lag:a.size - (max_lag - lag)], b[max_lag:-max_lag])[0, 1]
        corr.append(p)
    corr = np.array(corr)
    return x, corr


def _phase_correlation(a, b):
    G_a = np.fft.fft(a)
    G_b = np.fft.fft(b)
    conj_b = np.ma.conjugate(G_b)
    R = G_a*conj_b
    R /= np.absolute(R)
    r = np.fft.ifft(R).real
    return r


def tremor_catalogue(north=False, south=False):
    """Return tremors from PNSN + Ide (2012) tremor catalogue.
    The dates are in the JPL-style format."""
    tremors = []
    date_table = ymd_decimal_year_lookup()
    catalogue_names = ['tremor_catalogue', 'tremor_catalogue2']
    for name in catalogue_names:
        with open(f'geo_data/{name}.txt') as f:
            next(f)
            for line in f:
                splitted_line = line.split(',')
                latitude = float(splitted_line[0].replace(' ', ''))
                longitude = float(splitted_line[1].replace(' ', ''))
                depth = float(splitted_line[2].replace(' ', ''))
                time = splitted_line[3][1:].split(' ')[0]
                year, month, day = time.split('-')
                year, month, day = float(year), float(month), float(day)
                decimal_date = date_table[(year, month, day)]
                tremors.append([latitude, longitude, depth, decimal_date])
    with open(f'geo_data/tremor_catalogue_ide.txt') as f:
        for line in f:
            splitted_line = line.split(',')
            latitude = float(splitted_line[2].replace(' ', ''))
            longitude = float(splitted_line[3].replace(' ', ''))
            depth = float(splitted_line[4].replace(' ', ''))
            year, month, day = splitted_line[0].split('-')
            year, month, day = float(year), float(month), float(day)
            decimal_date = date_table[(year, month, day)]
            tremors.append([latitude, longitude, depth, decimal_date])
    tremor_array = np.array(tremors)
    if north:
        valid_tremor_events = np.where(tremor_array[:, 0] >= 47.)[0]
        tremor_array = tremor_array[valid_tremor_events]
    if south:
        valid_tremor_events = np.where(tremor_array[:, 0] < 47.)[0]
        tremor_array = tremor_array[valid_tremor_events]
    return tremor_array


def get_n_tremors_per_day(time_span, tremor_catalogue):
    cascadia_box = [40 - 1, 51.8 - 0.2, -128.3 - 1, -121 + 0.5]  # min/max_latitude, min/max_longitude
    lat_cut = \
        np.where(np.logical_and(tremor_catalogue[:, 0] > cascadia_box[0], tremor_catalogue[:, 0] < cascadia_box[1]))[0]
    # time_cut = np.where(np.logical_and(tremor_catalogue[:, 3][lat_cut] > time_span[0], tremor_catalogue[:, 3][lat_cut] < time_span[-1]))[0]
    time_cut = np.where(np.logical_and(tremor_catalogue[:, 3][lat_cut] >= time_span[0],
                                       tremor_catalogue[:, 3][lat_cut] <= time_span[-1]))[0]
    tremors = tremor_catalogue[time_cut]
    unique, counts = np.unique(tremors[:, 3], return_counts=True)
    extended_counts = np.zeros(time_span.shape)
    for i in range(unique.shape[0]):
        idx_time = np.argwhere(time_span == unique[i])
        extended_counts[idx_time] = counts[i]
    # idx_valid = np.isin(time_span, unique)
    # valid_counts = np.zeros(time_span.shape)
    # valid_counts[idx_valid] = counts
    # return unique, counts
    return extended_counts


def asfunctionof(x, y, bins=10, return_vals=False):
    histvals, binedges = np.histogram(x, bins=bins)
    means = []
    stds = []
    bins = []
    nums = []
    all_vals = []
    for i in range(len(binedges) - 1):
        ind = np.where(np.logical_and(x >= binedges[i], x < binedges[i + 1]))[0]
        if len(ind) > 0:
            yi = y[ind]
            means.append(np.mean(yi))
            stds.append(np.std(yi))
            bins.append(binedges[i])
            nums.append(len(ind))
            all_vals.append(yi)
    if return_vals:
        return means, stds, bins, nums, all_vals
    return means, stds, bins, nums


def overlap_percentage(xlist, ylist):
    min1 = min(xlist)
    max1 = max(xlist)
    min2 = min(ylist)
    max2 = max(ylist)

    overlap = max(0, min(max1, max2) - max(min1, min2))
    length = max1 - min1 + max2 - min2
    lengthx = max1 - min1
    lengthy = max2 - min2
    return 2 * overlap / length * 100, overlap / lengthx * 100, overlap / lengthy * 100


def plot_comparison_durations(cat1, cat2, duration_thresh, north=False):
    filtered_cat1 = []
    filtered_cat2 = []
    overlap_percentages = []

    for i, event in enumerate(cat1):
        for j in range(cat2.shape[0]):
            overlap_percent = overlap_percentage(event, cat2[j].tolist())[0]
            if overlap_percent >= duration_thresh:
                filtered_cat1.append(event)
                filtered_cat2.append(cat2[j])
                overlap_percentages.append(overlap_percent)

    filtered_cat2 = np.array(filtered_cat2)
    filtered_cat1 = np.array(filtered_cat1)
    overlap_percentages = np.array(overlap_percentages)

    print(filtered_cat2)
    print(filtered_cat1)

    duration_cat2 = (filtered_cat2[:, 1] - filtered_cat2[:, 0]) * 365
    duration_cat1 = (filtered_cat1[:, 1] - filtered_cat1[:, 0]) * 365

    # plt.scatter(duration_cat2, duration_cat1, c=overlap_percentages, cmap='coolwarm')

    # cmap = matplotlib.colors.LinearSegmentedColormap.from_list("coolwarm", overlap_percentages)
    # cmappable = ScalarMappable(norm=Normalize(np.min(overlap_percentages), np.max(overlap_percentages)), cmap=cmap)
    fig = plt.figure(dpi=100)
    cmap = plt.cm.coolwarm
    # create normalization instance
    norm = matplotlib.colors.Normalize(np.min(overlap_percentages), np.max(overlap_percentages))
    # create a scalarmappable from the colormap
    sm = matplotlib.cm.ScalarMappable(cmap=cmap, norm=norm)
    sm.set_array([])
    last_marker_square_idx, last_marker_circle_idx = 0, 0
    for i in range(len(duration_cat2)):
        if 2014. <= filtered_cat1[i][0] < 2018.:
            marker = 's'
            last_marker_square_idx = i
        else:
            marker = 'o'
            last_marker_circle_idx = i
        plt.scatter(duration_cat2[i], duration_cat1[i], color=cmap(norm(overlap_percentages[i])), marker=marker)

    plt.scatter(duration_cat2[last_marker_square_idx], duration_cat1[last_marker_square_idx], color=cmap(1.),
                marker='s', label='2014-2017')
    # plt.scatter(duration_cat2[last_marker_circle_idx], duration_cat1[last_marker_circle_idx], color=cmap(1.), marker='o', label='2010-2013')
    plt.scatter(duration_cat2[last_marker_circle_idx], duration_cat1[last_marker_circle_idx], color=cmap(1.),
                marker='o', label='2007-2013')
    p1 = max(max(duration_cat1), max(duration_cat2))
    p2 = min(min(duration_cat1), min(duration_cat2))
    plt.plot([p1, p2], [p1, p2], alpha=0.5, color='black', linewidth=1, zorder=0)
    plt.xlabel('Durations [days] (Michel et al., 2019)')
    plt.ylabel('Durations [days] (ours)')
    # cbar = plt.colorbar()
    cbar = plt.colorbar(sm)
    cbar.ax.set_ylabel('Amount of overlap (%)')
    plt.gca().set_aspect('equal')
    plt.legend()
    plt.savefig('sse_figures/' + ('north_selection/' if north else '') + 'dur_michel.pdf')
    # plt.show()
    plt.close(fig)


def plot_comparison_duration_tremors(cat, time_array, tremors, rel_height=0.99, north=False):
    """Take duration on catalogue, extract the same time period on tremors, find a peak and find a duration
    over that peak on tremors. Finally compare the two."""
    tremor_durations = []
    valid_cat = []
    overlap_percentages = []
    for time_span in cat:
        print('time_span', time_span)
        time_idx = np.logical_and(time_array >= time_span[0], time_array <= time_span[1])
        current_tremor_window = tremors[time_idx]
        peaks, _ = find_peaks(current_tremor_window)
        if len(peaks) > 0:
            widths = peak_widths(current_tremor_window, peaks, rel_height=rel_height)
            # per ogni picco in widths, calcola il min a sx il max a dx e metti come durata
            start_time, end_time = time_array[time_idx][round(np.min(widths[2]))], time_array[time_idx][
                round(np.max(widths[3]))]
            print('SSE starts:', time_span[0], 'tremors start:', start_time, 'and end:', end_time)
            tremor_durations.append((end_time - start_time) * 365)
            valid_cat.append((time_span[1] - time_span[0]) * 365)
            # overlap_percent = overlap_percentage((start_time, end_time), time_span)[0]

            # overlap_percentages.append(overlap_percent)
            overlap_percentages.append((time_span[0] - start_time) * 365)

            '''plt.plot(time_array, tremors)
            plt.axvline(start_time)
            plt.axvline(end_time)
            plt.show()'''

    cmap = plt.cm.coolwarm
    fig = plt.figure(dpi=100)
    # create normalization instance
    norm = matplotlib.colors.Normalize(np.min(overlap_percentages), np.max(overlap_percentages))
    # create a scalarmappable from the colormap
    sm = matplotlib.cm.ScalarMappable(cmap=cmap, norm=norm)
    sm.set_array([])
    last_marker_square_idx, last_marker_circle_idx, last_marker_star_idx = 0, 0, 0
    for i in range(len(valid_cat)):
        if 2014. <= cat[i][0] < 2018.1:
            marker = 's'
            last_marker_square_idx = i
        # elif 2010. <= cat[i][0] < 2014.:
        elif 2007. <= cat[i][0] < 2014.:
            marker = 'o'
            last_marker_circle_idx = i
        else:
            marker = '*'
            last_marker_star_idx = i
        plt.scatter(tremor_durations[i], valid_cat[i], color=cmap(norm(overlap_percentages[i])), marker=marker)

    # X, Y, Z = density_estimation(tremor_durations, valid_cat)
    # plt.contour(X, Y, Z)
    # seaborn.kdeplot(tremor_durations, valid_cat, fill=False, levels=5, thresh=.2)

    plt.scatter(tremor_durations[last_marker_circle_idx], valid_cat[last_marker_circle_idx], color=cmap(1.),
                # marker='o', label='2010-2013')
                marker='o', label='2007-2013')
    plt.scatter(tremor_durations[last_marker_square_idx], valid_cat[last_marker_square_idx], color=cmap(1.),
                marker='s', label='2014-2017')
    plt.scatter(tremor_durations[last_marker_star_idx], valid_cat[last_marker_star_idx], color=cmap(1.),
                marker='*', label='2018-2022')

    p1 = max(max(valid_cat), max(tremor_durations))
    p2 = min(min(valid_cat), min(tremor_durations))
    plt.plot([p1, p2], [p1, p2], alpha=0.5, color='black', linewidth=1, zorder=0)
    plt.xlabel('Tremor durations [days]')
    plt.ylabel('SSE durations [days] (ours)')
    # cbar = plt.colorbar()
    cbar = plt.colorbar(sm)
    # cbar.ax.set_ylabel('Amount of overlap (%)')
    cbar.ax.set_ylabel('Time lag between tremor and SSE onsets [days]')
    plt.legend()
    # plt.gca().set_aspect(abs(plt.xlim()[1] - plt.xlim()[0]) / abs(plt.ylim()[1] - plt.ylim()[0]))
    plt.gca().set_aspect('equal')
    plt.savefig('sse_figures/' + ('north_selection/' if north else '') + 'dur_tremors.pdf')
    # plt.show()
    plt.close(fig)

    return valid_cat, tremor_durations


def plot_comparison_duration_tremors2(cat, time_array, tremors, rel_height=0.99, north=False):
    """Take duration on catalogue, extract the same time period on tremors, find a peak and find a duration
    over that peak on tremors. Finally compare the two."""
    tremor_durations = []
    valid_cat = []
    overlap_percentages = []
    tremor_search_offset = 2
    tremor_search_offset_date = tremor_search_offset / 365
    for time_span in cat:
        print('time_span', time_span)
        time_idx = np.logical_and(time_array >= time_span[0], time_array <= time_span[1])
        current_tremor_window = tremors[time_idx]
        peaks, _ = find_peaks(current_tremor_window)
        extended_time_idx = np.logical_and(time_array >= time_span[0] - tremor_search_offset_date,
                                           time_array <= time_span[1] + tremor_search_offset_date)
        extended_current_tremor_window = tremors[extended_time_idx]
        if len(peaks) > 0:
            widths = peak_widths(extended_current_tremor_window, peaks + tremor_search_offset, rel_height=rel_height)
            # per ogni picco in widths, calcola il min a sx il max a dx e metti come durata
            start_time, end_time = time_array[extended_time_idx][round(np.min(widths[2]))], \
                                   time_array[extended_time_idx][
                                       round(np.max(widths[3]))]
            print('SSE starts:', time_span[0], 'tremors start:', start_time, 'and end:', end_time)
            tremor_durations.append((end_time - start_time) * 365)
            valid_cat.append((time_span[1] - time_span[0]) * 365)
            # overlap_percent = overlap_percentage((start_time, end_time), time_span)[0]

            # overlap_percentages.append(overlap_percent)
            overlap_percentages.append((time_span[0] - start_time) * 365)

            '''plt.plot(time_array, tremors)
            plt.axvline(start_time)
            plt.axvline(end_time)
            plt.show()'''

    cmap = plt.cm.coolwarm
    fig = plt.figure(dpi=100)
    # create normalization instance
    norm = matplotlib.colors.Normalize(np.min(overlap_percentages), np.max(overlap_percentages))
    # create a scalarmappable from the colormap
    sm = matplotlib.cm.ScalarMappable(cmap=cmap, norm=norm)
    sm.set_array([])
    last_marker_square_idx, last_marker_circle_idx, last_marker_star_idx = 0, 0, 0
    for i in range(len(valid_cat)):
        if 2014. <= cat[i][0] < 2018.1:
            marker = 's'
            last_marker_square_idx = i
        # elif 2010. <= cat[i][0] < 2014.:
        elif 2007. <= cat[i][0] < 2014.:
            marker = 'o'
            last_marker_circle_idx = i
        else:
            marker = '*'
            last_marker_star_idx = i
        plt.scatter(tremor_durations[i], valid_cat[i], color=cmap(norm(overlap_percentages[i])), marker=marker)

    # X, Y, Z = density_estimation(tremor_durations, valid_cat)
    # plt.contour(X, Y, Z)
    # seaborn.kdeplot(tremor_durations, valid_cat, fill=False, levels=5, thresh=.2)

    plt.scatter(tremor_durations[last_marker_circle_idx], valid_cat[last_marker_circle_idx], color=cmap(1.),
                # marker='o', label='2010-2013')
                marker='o', label='2007-2013')
    plt.scatter(tremor_durations[last_marker_square_idx], valid_cat[last_marker_square_idx], color=cmap(1.),
                marker='s', label='2014-2017')
    plt.scatter(tremor_durations[last_marker_star_idx], valid_cat[last_marker_star_idx], color=cmap(1.),
                marker='*', label='2018-2022')

    p1 = max(max(valid_cat), max(tremor_durations))
    p2 = min(min(valid_cat), min(tremor_durations))
    plt.plot([p1, p2], [p1, p2], alpha=0.5, color='black', linewidth=1, zorder=0)
    plt.xlabel('Tremor durations [days]')
    plt.ylabel('SSE durations [days] (ours)')
    # cbar = plt.colorbar()
    cbar = plt.colorbar(sm)
    # cbar.ax.set_ylabel('Amount of overlap (%)')
    cbar.ax.set_ylabel('Time lag between tremor and SSE onsets [days]')
    plt.legend()
    # plt.gca().set_aspect(abs(plt.xlim()[1] - plt.xlim()[0]) / abs(plt.ylim()[1] - plt.ylim()[0]))
    plt.gca().set_aspect('equal')
    plt.savefig('sse_figures/' + ('north_selection/' if north else '') + 'dur_tremors2.pdf')
    # plt.show()
    plt.close(fig)

    return valid_cat, tremor_durations


def plot_comparison_duration_tremors_peaks(cat, proba, time_array, tremors, rel_height=0.99, north=False):
    """Take duration on catalogue, extract the same time period on tremors, find a peak and find a duration
    over that peak on tremors. Finally compare the two."""
    tremor_durations = []
    valid_cat = []
    # overlap_percentages = []
    peak_time_lags = []
    for time_span in cat:
        print('time_span', time_span)
        time_idx = np.logical_and(time_array >= time_span[0], time_array <= time_span[1])
        current_tremor_window = tremors[time_idx]
        current_proba_window = proba[time_idx]
        proba_peaks, _ = find_peaks(current_proba_window)
        tremor_peaks, _ = find_peaks(current_tremor_window)
        if len(tremor_peaks) > 0:  # if len(proba_peaks) > 0 : redudant
            widths = peak_widths(current_tremor_window, tremor_peaks, rel_height=rel_height)
            # per ogni picco in widths, calcola il min a sx il max a dx e metti come durata
            start_time, end_time = time_array[time_idx][round(np.min(widths[2]))], time_array[time_idx][
                round(np.max(widths[3]))]
            tremor_durations.append((end_time - start_time) * 365)
            valid_cat.append((time_span[1] - time_span[0]) * 365)
            # overlap_percent = overlap_percentage((start_time, end_time), time_span)[0]

            # overlap_percentages.append(overlap_percent)
            # overlap_percentages.append((time_span[0] - start_time) * 365)

            # print('calcolo time lag:', proba_peaks, current_proba_window[proba_peaks], np.argmax(current_proba_window[proba_peaks]), proba_peaks[np.argmax(current_proba_window[proba_peaks])], time_array[time_idx][proba_peaks[np.argmax(current_proba_window[proba_peaks])]])
            # print('time array:', time_array[time_idx])

            peak_time_lag = (time_array[time_idx][proba_peaks[np.argmax(current_proba_window[proba_peaks])]] -
                             time_array[time_idx][tremor_peaks[np.argmax(current_tremor_window[tremor_peaks])]]) * 365
            peak_time_lags.append(peak_time_lag)

    cmap = plt.cm.Spectral  # plt.cm.coolwarm
    plt.figure(dpi=100)
    # create normalization instance
    norm = matplotlib.colors.Normalize(np.min(peak_time_lags), np.max(peak_time_lags))
    # norm = matplotlib.colors.Normalize(-20, 20)
    # create a scalarmappable from the colormap
    sm = matplotlib.cm.ScalarMappable(cmap=cmap, norm=norm)
    sm.set_array([])
    last_marker_square_idx, last_marker_circle_idx, last_marker_star_idx = 0, 0, 0
    for i in range(len(valid_cat)):
        if 2014. <= cat[i][0] < 2018.1:
            marker = 's'
            last_marker_square_idx = i
        # elif 2010. <= cat[i][0] < 2014.:
        elif 2007. <= cat[i][0] < 2014.:
            marker = 'o'
            last_marker_circle_idx = i
        else:
            marker = '*'
            last_marker_star_idx = i
        plt.scatter(tremor_durations[i], valid_cat[i], color=cmap(norm(peak_time_lags[i])), marker=marker)

    # X, Y, Z = density_estimation(tremor_durations, valid_cat)
    # plt.contour(X, Y, Z)
    # seaborn.kdeplot(tremor_durations, valid_cat, fill=False, levels=5, thresh=.2)

    plt.scatter(tremor_durations[last_marker_circle_idx], valid_cat[last_marker_circle_idx], color=cmap(1.),
                # marker='o', label='2010-2013')
                marker='o', label='2007-2013')
    plt.scatter(tremor_durations[last_marker_square_idx], valid_cat[last_marker_square_idx], color=cmap(1.),
                marker='s', label='2014-2017')
    plt.scatter(tremor_durations[last_marker_star_idx], valid_cat[last_marker_star_idx], color=cmap(1.),
                marker='*', label='2018-2022')

    p1 = max(max(valid_cat), max(tremor_durations))
    p2 = min(min(valid_cat), min(tremor_durations))
    plt.plot([p1, p2], [p1, p2], alpha=0.5, color='black', linewidth=1, zorder=0)
    plt.xlabel('Tremor durations [days]')
    plt.ylabel('SSE durations [days] (ours)')
    # cbar = plt.colorbar()
    cbar = plt.colorbar(sm)
    # cbar.ax.set_ylabel('Amount of overlap (%)')
    cbar.ax.set_ylabel('Time lag between tremor and SSE peaks [days]')
    plt.legend()
    # plt.gca().set_aspect(abs(plt.xlim()[1] - plt.xlim()[0]) / abs(plt.ylim()[1] - plt.ylim()[0]))
    plt.gca().set_aspect('equal')
    plt.show()

    return valid_cat, tremor_durations


def plot_comparison_duration_tremors_with_michel(cat, time_array, tremors, rel_height=0.99):
    """Take duration on catalogue, extract the same time period on tremors, find a peak and find a duration
    over that peak on tremors. Finally compare the two. Color code: overlap with Michel SSEs."""
    tremor_durations = []
    valid_cat = []
    overlap_percentages = []
    cat_michel = np.array(sse_catalogue())
    time_delta = 10 / 365
    correspondence_michel = []  # track if an event in the final catalogue needs to be color coded or not
    for i, time_span in enumerate(cat):
        print('time_span', time_span)
        time_idx = np.logical_and(time_array >= time_span[0], time_array <= time_span[1])
        current_tremor_window = tremors[time_idx]
        peaks, _ = find_peaks(current_tremor_window)
        if len(peaks) > 0:
            widths = peak_widths(current_tremor_window, peaks, rel_height=rel_height)
            # per ogni picco in widths, calcola il min a sx il max a dx e metti come durata
            start_time, end_time = time_array[time_idx][round(np.min(widths[2]))], time_array[time_idx][
                round(np.max(widths[3]))]
            tremor_durations.append((end_time - start_time) * 365)
            valid_cat.append((time_span[1] - time_span[0]) * 365)

            # take the overlap (or time lag) associated to the nearest Michel's SSE in time
            idx_subset_michel = np.logical_and(cat_michel[:, 0] >= time_span[0] - time_delta,
                                               cat_michel[:, 1] <= time_span[1] + time_delta)
            # print('trovato:', np.count_nonzero(idx_subset_michel))
            if np.count_nonzero(idx_subset_michel) > 0:
                # print(idx_subset_michel)
                subset_michel = cat_michel[idx_subset_michel]
                idx_min = np.argmin(subset_michel[:, 1] - subset_michel[:, 0])
                overlap_percent = overlap_percentage((subset_michel[idx_min, 0], subset_michel[idx_min, 1]), time_span)[
                    0]
                # overlap_percentages.append(overlap_percent)
                overlap_percentages.append(overlap_percent / 100 * (time_span[1] - time_span[0]) * 365)
                print('append:', overlap_percentages[-1])
                # overlap_percentages.append((time_span[0] - start_time) * 365)
                correspondence_michel.append(i)
            else:
                overlap_percentages.append(np.nan)
            '''plt.plot(time_array, tremors)
            plt.axvline(start_time)
            plt.axvline(end_time)
            plt.show()'''
    print('correspondence Michel:', correspondence_michel)
    cmap = plt.cm.coolwarm
    plt.figure(dpi=100)
    # create normalization instance
    norm = matplotlib.colors.Normalize(np.nanmin(overlap_percentages), np.nanmax(overlap_percentages))
    # create a scalarmappable from the colormap
    sm = matplotlib.cm.ScalarMappable(cmap=cmap, norm=norm)
    sm.set_array([])
    last_marker_square_idx, last_marker_circle_idx, last_marker_star_idx = 0, 0, 0
    for i in range(len(valid_cat)):
        if 2014. <= cat[i][0] < 2018.1:
            marker = 's'
            if i in correspondence_michel:
                last_marker_square_idx = i
        elif 2010. <= cat[i][0] < 2014.:
            marker = 'o'
            if i in correspondence_michel:
                last_marker_circle_idx = i
        else:
            marker = '*'
            if i in correspondence_michel:
                last_marker_star_idx = i
        if i in correspondence_michel:
            plt.scatter(tremor_durations[i], valid_cat[i], color=cmap(norm(overlap_percentages[i])), marker=marker)
        else:
            plt.scatter(tremor_durations[i], valid_cat[i], color='black', marker=marker, zorder=-100)
            pass

    plt.scatter(tremor_durations[last_marker_circle_idx], valid_cat[last_marker_circle_idx], color=cmap(1.),
                marker='o', label='2010-2013', zorder=-100)
    plt.scatter(tremor_durations[last_marker_square_idx], valid_cat[last_marker_square_idx], color=cmap(1.),
                marker='s', label='2014-2017', zorder=-100)
    plt.scatter(tremor_durations[last_marker_star_idx], valid_cat[last_marker_star_idx], color=cmap(1.),
                marker='*', label='2018-2022', zorder=-100)

    p1 = max(max(valid_cat), max(tremor_durations))
    p2 = min(min(valid_cat), min(tremor_durations))
    plt.plot([p1, p2], [p1, p2], alpha=0.5, color='black', linewidth=1, zorder=0)
    plt.xlabel('Tremor durations [days]')
    plt.ylabel('SSE durations [days] (ours)')
    # cbar = plt.colorbar()
    cbar = plt.colorbar(sm)
    # cbar.ax.set_ylabel('Amount of overlap with respect to Michel (%)')
    cbar.ax.set_ylabel('Overlap between our SSEs and Michel ones [days]')
    # cbar.ax.set_ylabel('Time lag between tremor and detected SSE onsets [days]')
    plt.legend()
    # plt.gca().set_aspect(abs(plt.xlim()[1] - plt.xlim()[0]) / abs(plt.ylim()[1] - plt.ylim()[0]))
    plt.gca().set_aspect('equal')
    plt.show()

    return valid_cat, tremor_durations


def sse_selection_from_proba(probability, time_array, probability_threshold=0.5, rel_height=0.8, nearest_threshold=5, sigma=2., save_catalog=True, north=False):
    """Select SSEs in the probability curve based on a threshold on the probability and by extracting an
    estimation of the slow slip duration."""
    peaks, _ = find_peaks(probability)
    peak_heights = probability[peaks]
    peaks = peaks[peak_heights > probability_threshold]  # filter peaks based on its height

    smoothed_proba = gaussian_filter(probability, sigma=sigma)
    peaks_smoothed, _ = find_peaks(smoothed_proba)
    widths_smoothed = peak_widths(smoothed_proba, peaks_smoothed, rel_height=rel_height)

    sse_cat_inferred = []
    for i in range(len(peaks_smoothed)):
        nearest_peak_idx = _find_nearest_val(peaks, peaks_smoothed[i])[0]
        if np.abs(peaks_smoothed[i] - nearest_peak_idx) < nearest_threshold:
            original_proba = probability[_find_nearest_val(peaks, peaks_smoothed[i])[0]]
            if original_proba > probability_threshold:
                start_time, end_time = time_array[round(widths_smoothed[2][i])], time_array[
                    round(widths_smoothed[3][i])]
                sse_cat_inferred.append((start_time, end_time))
    sse_cat_inferred = np.array(sse_cat_inferred)
    if save_catalog:
        np.savetxt('geo_data/catalogue_sses_costantino_all_soft_thresh' + ('_north' if north else '') + '.txt', sse_cat_inferred, fmt='%f')
    return sse_cat_inferred, peaks, peaks_smoothed, smoothed_proba


def sse_selection_from_proba_hard_thresh(probability, time_array, probability_threshold=0.5, sigma=2., save_catalog=True, north=False, south=False):
    """Select SSEs in the probability curve based on a threshold on the probability and by extracting an
    estimation of the slow slip duration."""
    smoothed_proba = gaussian_filter(probability, sigma=sigma)
    prob_mask = smoothed_proba > probability_threshold
    sse_cat_inferred = []
    t = 0
    while t < len(time_array):
        if t < len(time_array) and prob_mask[t] != 0:
            start_t = t
            while t < len(time_array) and prob_mask[t] != 0:
                t += 1
            sse_cat_inferred.append((time_array[start_t], time_array[t - 1]))
        else:
            t += 1
    sse_cat_inferred = np.array(sse_cat_inferred)
    if save_catalog:
        np.savetxt('geo_data/catalogue_sses_costantino_all_hard_thresh' + ('_north' if north else '') + ('_south' if south else '') + '.txt', sse_cat_inferred, fmt='%f')
    return sse_cat_inferred


def plot_scaling_law_duration_magnitude(cat_ours, cat_michel, time_array, tremors, non_smoothed_tremors, probability,
                                        duration_thresh=0.01, rel_height=0.99, north=False):
    """Given the scaling law between duration and moment of the catalogued SSEs, we plot also the
    deviation of our SSEs with respect to the catalogued magnitude."""
    filtered_cat_ours, filtered_cat_michel = [], []
    overlap_percentages = []
    magnitudes = []  # track magnitudes for each couple

    for i, event in enumerate(cat_ours):
        for j in range(cat_michel.shape[0]):
            overlap_percent = overlap_percentage(event, cat_michel[j, (0, 1)].tolist())[0]
            if overlap_percent >= duration_thresh:
                filtered_cat_ours.append(event)
                filtered_cat_michel.append(cat_michel[j, (0, 1)])
                overlap_percentages.append(overlap_percent)
                magnitudes.append(cat_michel[j, 2])

    filtered_cat_ours = np.array(filtered_cat_ours)
    filtered_cat_michel = np.array(filtered_cat_michel)
    overlap_percentages = np.array(overlap_percentages)
    magnitudes = np.array(magnitudes)
    moments = 10 ** (magnitudes * 1.5 + 9.1)

    duration_ours = (filtered_cat_ours[:, 1] - filtered_cat_ours[:, 0]) * 365 * 86400  # conversion in seconds
    duration_michel = (filtered_cat_michel[:, 1] - filtered_cat_michel[:, 0]) * 365 * 86400

    m, q = np.polyfit(np.log10(moments), np.log10(duration_michel), 1)

    plt.figure(dpi=100)
    plt.scatter(np.log10(moments), np.log10(duration_michel), marker='o', color='black')
    # plt.scatter(np.log10(moments), np.log10(duration_ours), marker='s')
    plt.scatter(np.log10(moments), np.log10(duration_ours), marker='s', c=overlap_percentages, cmap='coolwarm')
    for i, dur in enumerate(duration_ours):
        if overlap_percentages[i] > 50:
            plt.plot([np.log10(moments[i]), np.log10(moments[i])], [np.log10(duration_michel[i]), np.log10(dur)],
                     color='black')
    cbar = plt.colorbar()
    plt.scatter(np.log10(moments[0]), np.log10(duration_michel[0]), marker='o', color=plt.get_cmap('coolwarm')(255),
                zorder=-100, label='Michel et al., 2019')
    plt.scatter(np.log10(moments[0]), np.log10(duration_ours[0]), marker='s', color=plt.get_cmap('coolwarm')(255),
                zorder=-100, label='Ours')

    # plt.plot(np.log10(moments), np.log10(moments) / 3, alpha=0.5, color='black', linewidth=1, zorder=0, label='$M_0 \propto T^3$')
    plt.plot(np.log10(moments), m * np.log10(moments) + q, alpha=0.5, color='black', linewidth=1, zorder=0,
             label='linear reg. from Michel et al., 2019')
    # plt.axline((x0, 1/3 * x0), slope=1/3, alpha=0.5, color='black', linewidth=1, zorder=0)
    plt.xlabel('log$_{10}$[moment (N $\cdot$ m)]')
    plt.ylabel('log$_{10}$[duration (s)]')
    cbar.ax.set_ylabel('Amount of overlap (%)')
    plt.gca().set_aspect('equal')
    plt.legend()
    plt.show()

    plt.figure(dpi=100)
    plt.scatter(np.log10(moments), np.log10(duration_michel), marker='o', color='black')
    plt.plot(np.log10(moments), m * np.log10(moments) + q, label='lin. reg.')
    plt.xlabel('log$_{10}$[moment (N $\cdot$ m)]')
    plt.ylabel('log$_{10}$[duration (s)]')
    plt.title('Michel et al. catalogue with linear regression')
    plt.show()

    # we assign a Mw proxy to our detections (log[M] = (log[duration] - q)/m)

    # cat_costantino_full = np.loadtxt('geo_data/catalogue_sses_costantino_all.txt', delimiter=',', usecols=(1, 2))
    cat_costantino_full = np.loadtxt('geo_data/catalogue_sses_costantino_all' + ('_north' if north else '') + '.txt')
    duration_ours_full = (cat_costantino_full[:, 1] - cat_costantino_full[:, 0]) * 365 * 86400
    log_moments_obtained = (np.log10(duration_ours_full) - q) / m

    plt.figure(dpi=100)
    plt.scatter(log_moments_obtained, np.log10(duration_ours_full), marker='o', color='black')
    plt.xlabel('log$_{10}$[moment (N $\cdot$ m)]')
    plt.ylabel('log$_{10}$[duration (s)]')
    plt.title('our catalogue obtained from Michel et al. scaling law')

    ax1 = plt.gca()
    ax2 = ax1.secondary_xaxis('top', functions=(moment_to_mw, mw_to_moment))
    ax2.set_xlabel('$M_w$ proxy')
    log_mo_ticks = ax1.get_xticks()
    log_mo_ticks = prevent_division_by_zero(log_mo_ticks)
    mw_ticks = moment_to_mw(log_mo_ticks)
    ax2.set_xticks(mw_ticks)

    plt.show()

    # check if the histogram of the detections as a function of Mw follows a G-R law

    mw_obtained = (log_moments_obtained - 9.1) / 1.5
    n_detections, mw_bins = np.histogram(mw_obtained, bins=20)
    mw_bins = mw_bins[:-1]
    log_n_detections = np.log10(n_detections)
    # idx_mw_valid = np.where(mw_bins > 5)[0]
    idx_mw_valid1 = np.where(mw_bins > 5.5)[0]
    idx_mw_valid2 = np.where(mw_bins > 6.)[0]
    b1, a1 = np.polyfit(mw_bins[idx_mw_valid1], log_n_detections[idx_mw_valid1], 1)
    b2, a2 = np.polyfit(mw_bins[idx_mw_valid2], log_n_detections[idx_mw_valid2], 1)

    '''plt.figure(dpi=100)
    plt.scatter(mw_bins, np.log10(n_detections), s=15, color='black')
    plt.plot([mw_bins[idx_mw_valid1][0], mw_bins[idx_mw_valid1][-1]], [mw_bins[idx_mw_valid1][0] * b1 + a1, mw_bins[idx_mw_valid1][-1] * b1 + a1], alpha=0.7, color='C0', linewidth=1, zorder=0, label=f'$M_c=5.5$, b-value $={b1:.2}$')
    plt.plot([mw_bins[idx_mw_valid2][0], mw_bins[idx_mw_valid2][-1]], [mw_bins[idx_mw_valid2][0] * b2 + a2, mw_bins[idx_mw_valid2][-1] * b2 + a2], alpha=0.7, color='C3', linewidth=1, zorder=0, label=f'$M_c=6$, b-value $={b2:.2}$')
    plt.ylabel('log$_{10}$[n. detections]')
    plt.xlabel('$M_w$ proxy')
    # plt.title(f'b-value: {np.abs(b):.2}')
    plt.legend()
    plt.show()'''

    idx_mw_valid3 = np.where(mw_bins >= 5.4)[0]
    ransac = linear_model.RANSACRegressor(random_state=42)
    ransac.fit(mw_bins[idx_mw_valid3].reshape(-1, 1), log_n_detections[idx_mw_valid3].reshape(-1, 1))
    # b3, a3 = np.polyfit(mw_bins[idx_mw_valid3], log_n_detections[idx_mw_valid3], 1)
    b3, a3 = float(ransac.estimator_.coef_), float(ransac.estimator_.intercept_)

    fig = plt.figure(dpi=100)
    plt.scatter(mw_bins, np.log10(n_detections), s=18, color='black')
    plt.plot([mw_bins[idx_mw_valid3][0], mw_bins[idx_mw_valid3][-1]],
             [mw_bins[idx_mw_valid3][0] * b3 + a3, mw_bins[idx_mw_valid3][-1] * b3 + a3], alpha=0.7, color='black',
             linewidth=1, zorder=0, label=f'b-value $={b3:.2}$')
    plt.plot()
    plt.ylabel('log$_{10}$[n. detections]')
    plt.xlabel('$M_w$ proxy')
    # plt.title(f'b-value: {np.abs(b):.2}')
    # plt.gca().set_aspect('equal', 'box')
    # plt.axis('equal')
    plt.legend()
    plt.savefig('sse_figures/' + ('north_selection/' if north else '') + 'gr.pdf')

    # plt.show()
    plt.close(fig)

    # test if any link exists between magnitude and tremor peak amplitudes
    peak_amplitudes = []
    full_peak_amplitudes = []
    non_smoothed_peak_amplitudes = []
    valid_mw_obt = []
    full_valid_mw_obt = []
    mw_without_peak = []
    tremor_durations = []
    valid_durations = []
    tremor_start_dates = []
    valid_sse_start_dates = []
    tremor_peaks = []  # max
    dates_peaks = []
    peak_time_lags = []
    tot_number_tremors_in_peaks = []
    for i, event in enumerate(cat_ours):
        time_idx = np.logical_and(time_array >= event[0], time_array <= event[1])
        current_tremor_window = tremors[time_idx]
        current_proba_window = probability[time_idx]
        tremor_peaks, _ = find_peaks(current_tremor_window)
        proba_peaks, _ = find_peaks(current_proba_window)
        if len(tremor_peaks) > 0:
            # peak_amplitudes.append(np.mean(tremors[time_idx][peak_ind]))
            argmax_peak = np.argmax(tremors[time_idx][tremor_peaks])
            peak_amplitudes.append(np.max(tremors[time_idx][tremor_peaks]))
            dates_peaks.append(time_array[time_idx][tremor_peaks][argmax_peak])
            # non_smoothed_peak_amplitudes.append(np.mean(non_smoothed_tremors[time_idx][peak_ind]))
            non_smoothed_peak_amplitudes.append(np.max(non_smoothed_tremors[time_idx][tremor_peaks]))
            valid_mw_obt.append(mw_obtained[i])
            peak_time_lag = (time_array[time_idx][proba_peaks[np.argmax(current_proba_window[proba_peaks])]] -
                             time_array[time_idx][tremor_peaks[np.argmax(current_tremor_window[tremor_peaks])]]) * 365
            peak_time_lags.append(peak_time_lag)
            for subpeak in tremor_peaks:
                full_peak_amplitudes.append(tremors[time_idx][subpeak])
                full_valid_mw_obt.append(mw_obtained[i])
            ### tremor stuff
            widths = peak_widths(current_tremor_window, tremor_peaks, rel_height=rel_height)
            start_time, end_time = time_array[time_idx][round(np.min(widths[2]))], time_array[time_idx][
                round(np.max(widths[3]))]
            tremor_durations.append((end_time - start_time) * 365)
            valid_durations.append((event[1] - event[0]) * 365)
            tremor_start_dates.append(start_time)
            valid_sse_start_dates.append(event[0])
            start_idx, end_idx = np.where(time_array[time_idx] == start_time)[0][0], \
                                 np.where(time_array[time_idx] == end_time)[0][0]
            tot_number_tremors_in_peaks.append(np.sum(tremors[time_idx][start_idx:end_idx]))
        else:
            mw_without_peak.append(mw_obtained[i])
    print('# events not associated to a peak of tremor:', len(mw_without_peak))

    idx_valid_mw_obt = np.where(np.array(valid_mw_obt) > 5.5)[0]

    w1 = np.polyfit(np.array(valid_mw_obt)[idx_valid_mw_obt], np.array(peak_amplitudes)[idx_valid_mw_obt], 1)
    w2 = np.polyfit(full_valid_mw_obt, full_peak_amplitudes, 1)
    p1 = np.poly1d(w1)
    p2 = np.poly1d(w2)

    plt.figure(dpi=100)
    plt.scatter(valid_mw_obt, peak_amplitudes, s=15, color='black', alpha=0.7)
    plt.plot(np.array(valid_mw_obt)[idx_valid_mw_obt], p1(np.array(valid_mw_obt)[idx_valid_mw_obt]), color='black',
             alpha=0.7)
    # plt.scatter(valid_mw_obt, non_smoothed_peak_amplitudes, s=15, color='red', alpha=0.7)
    # plt.scatter(full_valid_mw_obt, full_peak_amplitudes, s=25, color='blue', alpha=0.4)
    # plt.plot(full_valid_mw_obt, p2(full_valid_mw_obt), color='blue', alpha=0.7)
    # plt.scatter(mw_without_peak, [0]*len(mw_without_peak), s=50, color='red', marker='*')
    plt.scatter(mw_without_peak, [0] * len(mw_without_peak), s=15, edgecolors='black', facecolors='none',
                label='SSEs not associated to tremors')
    plt.ylabel('Tremor peak amplitude')
    plt.xlabel('$M_w$ proxy')
    plt.legend()
    plt.show()

    plt.figure(dpi=100)
    sc = plt.scatter(valid_mw_obt, peak_amplitudes, s=15, c=dates_peaks, cmap='coolwarm', alpha=1.)
    plt.plot(np.array(valid_mw_obt)[idx_valid_mw_obt], p1(np.array(valid_mw_obt)[idx_valid_mw_obt]), color='black',
             alpha=0.7)
    # plt.scatter(valid_mw_obt, non_smoothed_peak_amplitudes, s=15, color='red', alpha=0.7)
    # plt.scatter(full_valid_mw_obt, full_peak_amplitudes, s=25, color='blue', alpha=0.4)
    # plt.plot(full_valid_mw_obt, p2(full_valid_mw_obt), color='blue', alpha=0.7)
    plt.scatter(mw_without_peak, [0] * len(mw_without_peak), s=50, color='red', marker='*')
    plt.ylabel('Tremor peak amplitude')
    plt.xlabel('$M_w$ proxy')
    plt.colorbar(sc)
    plt.show()

    # test whether the time lag between tremors and SSE onset is linked to the duration (or Mw)

    time_lag_tremor_sse = (np.array(valid_sse_start_dates) - np.array(tremor_start_dates)) * 365

    plt.figure(dpi=100)
    plt.scatter(valid_durations, time_lag_tremor_sse, color='black')
    plt.ylabel('Time lag between tremor and SSE onset [days]')
    plt.xlabel('Duration [days]')
    # ax2 = plt.gca().twiny()
    # ax2.set_xlim([np.min(valid_durations), np.max(valid_durations)])
    # ax2.scatter((((np.log10(np.array(valid_durations) * 86400) - q) / m) - 9.1) / 1.5, time_lag_tremor_sse, color='red')
    # ax2.set_xlabel('$M_w$')

    ax1 = plt.gca()
    ax2 = ax1.secondary_xaxis('top', functions=(duration_to_mw, mw_to_duration))
    ax2.set_xlabel('$M_w$ proxy')
    duration_ticks = ax1.get_xticks()
    duration_ticks = prevent_division_by_zero(duration_ticks)
    mw_ticks = duration_to_mw(duration_ticks)
    ax2.set_xticks(mw_ticks)
    plt.show()

    # time lag between peaks

    plt.figure(dpi=100)
    plt.scatter(valid_durations, peak_time_lags, color='black')
    plt.ylabel('Time lag between tremor and SSE peaks [days]')
    plt.xlabel('Duration [days]')
    # ax2 = plt.gca().twiny()
    # ax2.set_xlim([np.min(valid_durations), np.max(valid_durations)])
    # ax2.scatter((((np.log10(np.array(valid_durations) * 86400) - q) / m) - 9.1) / 1.5, time_lag_tremor_sse, color='red')
    # ax2.set_xlabel('$M_w$')

    ax1 = plt.gca()
    ax2 = ax1.secondary_xaxis('top', functions=(duration_to_mw, mw_to_duration))
    ax2.set_xlabel('$M_w$ proxy')
    duration_ticks = ax1.get_xticks()
    duration_ticks = prevent_division_by_zero(duration_ticks)
    mw_ticks = duration_to_mw(duration_ticks)
    ax2.set_xticks(mw_ticks)
    plt.show()

    # total number of tremors in peak
    plt.figure(dpi=100)
    plt.scatter(valid_durations, tot_number_tremors_in_peaks, color='black', s=15, alpha=0.8)
    plt.plot(np.array(valid_durations),
             np.poly1d(np.polyfit(valid_durations, tot_number_tremors_in_peaks, 1))(valid_durations), color='black',
             alpha=0.7)
    plt.ylabel('Total number of tremors in a burst')
    plt.xlabel('Duration [days]')
    # ax2 = plt.gca().twiny()
    # ax2.set_xlim([np.min(valid_durations), np.max(valid_durations)])
    # ax2.scatter((((np.log10(np.array(valid_durations) * 86400) - q) / m) - 9.1) / 1.5, time_lag_tremor_sse, color='red')
    # ax2.set_xlabel('$M_w$')

    ax1 = plt.gca()
    ax2 = ax1.secondary_xaxis('top', functions=(duration_to_mw, mw_to_duration))
    ax2.set_xlabel('$M_w$ proxy')
    duration_ticks = ax1.get_xticks()
    duration_ticks = prevent_division_by_zero(duration_ticks)
    mw_ticks = duration_to_mw(duration_ticks)
    ax2.set_xticks(mw_ticks)
    plt.show()

    # test if any link exists between magnitude and probability peak amplitudes
    proba_peak_amplitudes = []
    mw_obtained_for_plot_proba = []
    dates_for_proba = []
    for i, event in enumerate(cat_ours):
        time_idx = np.logical_and(time_array >= event[0], time_array <= event[1])
        current_proba_window = probability[time_idx]
        proba_peaks, _ = find_peaks(current_proba_window)
        if len(proba_peaks) > 0:
            argmax_peak = np.argmax(probability[time_idx][proba_peaks])
            proba_peak_amplitudes.append(np.max(probability[time_idx][proba_peaks]))
            mw_obtained_for_plot_proba.append(mw_obtained[i])
            dates_for_proba.append(time_array[time_idx][proba_peaks][argmax_peak])

    mw_obtained_for_plot_proba = np.array(mw_obtained_for_plot_proba)
    proba_peak_amplitudes = np.array(proba_peak_amplitudes)
    dates_for_proba = np.array(dates_for_proba)

    def sigmoid(x, a, x0, c, d):
        return a / (1 + np.exp(-c * (x - x0))) + d

    idx_filter = mw_obtained_for_plot_proba > 0  # 5.5
    x = mw_obtained_for_plot_proba[idx_filter]
    y = proba_peak_amplitudes[idx_filter]
    plt.scatter(x, y, c=dates_for_proba[idx_filter])
    plt.xlabel('Mw')
    plt.ylabel('proba')
    plt.colorbar()

    # bounds = ([0., 4., 0.1, 0.], [1., 7., 10, 1.])
    bounds = ([0.49, 5.5, 9., 0.45], [0.51, 6., 10., 0.55])
    popt, pcov = curve_fit(sigmoid, x, y, bounds=bounds)
    estimated_a, estimated_x0, estimated_c, estimated_d = popt
    print(popt)
    y_fitted = sigmoid(x, a=estimated_a, x0=estimated_x0, c=estimated_c, d=estimated_d)
    plt.scatter(x, y_fitted, color='red')

    plt.show()


def prevent_division_by_zero(some_array):
    corrected_array = some_array.copy()
    for i, entry in enumerate(some_array):
        # If element is zero, set to some small value
        if abs(entry) < sys.float_info.epsilon:
            corrected_array[i] = sys.float_info.epsilon
    return corrected_array


def duration_to_mw(duration):
    # input: duration in days
    m, q = 0.3712741342967931, -0.4407328538326572
    invalid_idx = np.where(duration < 0)[0]
    duration[invalid_idx] = 1.
    duration = prevent_division_by_zero(duration)
    return (((np.log10(np.array(duration) * 86400) - q) / m) - 9.1) / 1.5


def moment_to_mw(log_mo):
    return (log_mo - 9.1) / 1.5


def mw_to_moment(mw):
    # returns log10(moment)
    # print('mw:', mw)
    return 1.5 * mw + 9.1


def mw_to_duration(mw):
    # output: duration in days
    m, q = 0.3712741342967931, - 0.4407328538326572
    print('mw', mw)
    return (10 ** (m * (1.5 * mw + 9.1) + q)) / 86400


def plot_correlation(proba, tremors):
    proba = np.diff(proba)
    tremors = np.diff(tremors)
    max_lag = 15
    x = np.linspace(-max_lag, max_lag, 2 * max_lag + 1, dtype=np.int_)
    corr = []
    for lag in x:
        p = np.corrcoef(proba[max_lag + lag:proba.size - (max_lag - lag)], tremors[max_lag:-max_lag])[0, 1]
        corr.append(p)
    corr = np.array(corr)

    plt.plot(x, corr)
    plt.axvline(x=0, linewidth=0.5)
    plt.xlabel('time lag between tremor and deformation initiation')
    plt.ylabel('Cross correlation')
    plt.show()


def plot_proba_tremors_new(duration_thresh=0.01):
    weight_code, name = '01Aug2022-144844', 'realgaps_ext_v3_0.001_6-7_135'

    # global time_span, proba, selected_gnss_data, station_codes, station_coordinates, detrend, attn_matrices, attn_matrices2
    with np.load(f'pred_cascadia_running_win_{weight_code}_{name}.npz') as f:
        proba, time_span, attn_matrices = f['proba'], f['time'], f['attn_matrices']

    tremors = tremor_catalogue()

    reference_period = (2010, 2023)  # (2010, 2018.1) # (2007, 2018.1) # (2014, 2018.1)
    # selected_gnss_data, _, _, _ = _preliminary_operations(reference_period=reference_period, n_directions=3)
    time_filter = np.where(np.logical_and(time_span >= reference_period[0], time_span <= reference_period[1]))[0]
    time_span, proba = time_span[time_filter], proba[time_filter]

    valid_filter = proba != 0
    time_span, proba = time_span[valid_filter], proba[valid_filter]

    cat_ours, p, sp, spr = sse_selection_from_proba(proba, time_span, probability_threshold=0.5, rel_height=0.7)
    cat_michel = sse_catalogue()
    cat_michel_mw = sse_catalogue(return_magnitude=True)

    print(cat_ours)
    np.savetxt('geo_data/catalogue_sses_costantino_all.txt', cat_ours)
    '''with open('geo_data/catalogue_sses_costantino_all.txt', 'w') as f:
        for i in range(len(cat_ours)):
            f.write(f'#{i+1},{cat_ours[i][0]},{cat_ours[i][1]}\n')'''

    fig, axes = plt.subplots(2, 1, figsize=(13.5, 7.5), sharex=True)
    ax_proba, ax_tremors = axes
    ax_proba.ticklabel_format(useOffset=False)
    ax_tremors.ticklabel_format(useOffset=False)

    ax_proba.plot(time_span, proba, linewidth=1.3)

    xlims1 = ax_proba.get_xlim()
    for i, span in enumerate(cat_michel):
        if span[0] > xlims1[0] and span[1] < xlims1[1]:
            ax_proba.axvspan(span[0], span[1], alpha=0.15, color='red')
            arrow_offset = np.random.random() * 0.1
            myArrow = FancyArrowPatch(posA=(span[0], 0.85 + arrow_offset), posB=(span[1], 0.85 + arrow_offset),
                                      arrowstyle='<|-|>', color='red',
                                      mutation_scale=20, shrinkA=0, shrinkB=0)
            # ax_proba.add_artist(myArrow)

    for i, span in enumerate(cat_ours):
        if span[0] > xlims1[0] and span[1] < xlims1[1]:
            ax_proba.axvspan(span[0], span[1], alpha=0.15, color='C0')
            arrow_offset = np.random.random() * 0.1
            myArrow = FancyArrowPatch(posA=(span[0], 0.3 + arrow_offset), posB=(span[1], 0.3 + arrow_offset),
                                      arrowstyle='<|-|>', color='C0',
                                      mutation_scale=20, shrinkA=0, shrinkB=0)
            # ax_proba.add_artist(myArrow)

    ax_proba.set_ylabel('Probability of SSE detection')

    # unique, counts = get_n_tremors_per_day(time_span, tremors)
    n_tremors_per_day = get_n_tremors_per_day(time_span, tremors)
    markerline, stemlines, baseline = ax_tremors.stem(time_span, n_tremors_per_day, use_line_collection=True,
                                                      markerfmt=" ", basefmt=" ")
    plt.setp(stemlines, 'linewidth', 0.15)
    ax_tremors.set_ylabel('Number of tremor events per day')
    smoothed_tremors = gaussian_filter(n_tremors_per_day, 2.5)
    ax_tremors.plot(time_span, smoothed_tremors, color='C0', linewidth=1.3)

    # ax_proba.scatter(time_span[p], proba[p], marker='x', color='C0')
    # ax_proba.scatter(time_span[sp], proba[sp], marker='x', color='C1')
    # ax_proba.plot(time_span, spr, color='C1', linewidth=1.3)

    # plt.tight_layout()
    plt.show()

    color_proba = 'C0'
    color_tremors = 'C7'
    color_michel = 'orangered'
    color_synthetics = 'mediumseagreen'

    fig, axis = plt.subplots(1, 1, figsize=(13.5, 7.5 / 2))
    ax_proba, ax_tremors = axis, axis.twinx()
    ax_proba.ticklabel_format(useOffset=False)
    ax_tremors.ticklabel_format(useOffset=False)

    ax_proba.plot(time_span, proba, linewidth=1, color=color_proba)

    ax_proba.set_ylim([0, 1])

    ax_proba.margins(x=0)
    ax_tremors.margins(x=0)

    xlims1 = ax_proba.get_xlim()
    for i, span in enumerate(cat_michel):
        if span[0] > xlims1[0] and span[1] < xlims1[1]:
            ax_proba.plot(span, [1.01, 1.01], clip_on=False, color=color_michel, linewidth=2.5)

    ax_proba.plot([2010, 2017.65], [-0.1, -0.1], clip_on=False, color=color_michel, linewidth=2)
    ax_proba.plot([2010, 2014], [-0.12, -0.12], clip_on=False, color=color_synthetics, linewidth=2)
    ax_proba.plot([2018, 2022.2], [-0.12, -0.12], clip_on=False, color=color_synthetics, linewidth=2)

    # ax_proba.annotate('', xy=(0.5, -0.1), xycoords='axes fraction', xytext=(1, -0.1), arrowprops=dict(arrowstyle="<->", color='b'))
    # ax_proba.annotate('', xy=(2015, -0.1), xycoords=ax_proba.get_xaxis_transform(), xytext=(2020, -0.1), arrowprops=dict(arrowstyle="|-|", linewidth=5, facecolor=color_michel))
    # ax_proba.annotate('', xy=(2015, -0.1), xycoords=trans)
    # ax_proba.add_artist(myArrow)


    for i, span in enumerate(cat_ours):
        if span[0] > xlims1[0] and span[1] < xlims1[1]:
            ax_proba.axvspan(span[0], span[1], alpha=0.25, color=color_proba, zorder=0)

    ax_proba.set_ylabel('Confidence of SSE detection', color=color_proba)
    ax_proba.tick_params(axis='y', labelcolor=color_proba)

    # unique, counts = get_n_tremors_per_day(time_span, tremors)
    n_tremors_per_day = get_n_tremors_per_day(time_span, tremors)
    markerline, stemlines, baseline = ax_tremors.stem(time_span, n_tremors_per_day, use_line_collection=True,
                                                      markerfmt=" ", basefmt=" ")
    plt.setp(stemlines, 'linewidth', 0.15)
    plt.setp(stemlines, 'color', color_tremors)
    ax_tremors.set_ylabel('Number of tremor events per day', color=color_tremors)
    smoothed_tremors = gaussian_filter(n_tremors_per_day, 2.5)
    ax_tremors.plot(time_span, smoothed_tremors, color=color_tremors, linewidth=1.)
    ax_tremors.tick_params(axis='y', labelcolor=color_tremors)
    ax_tremors.set_ylim([np.min(smoothed_tremors), 3 * np.max(smoothed_tremors)])
    ax_tremors.set_yticks([200, 500])
    # ax_proba.margins(x=0)
    # ax_tremors.margins(x=0)
    # plt.tight_layout()
    plt.savefig('sse_figures/det_overview.pdf')
    plt.show()
    # plt.close(fig)

    fig = plt.figure(figsize=(13.5, 7.5 / 1.3))

    gs = GridSpec(4, 1, hspace=0)

    ax_proba = fig.add_subplot(gs[1:3])
    ax_tremors = fig.add_subplot(gs[0])  # fig.add_subplot(gs[1:4, 3])

    ax_proba.ticklabel_format(useOffset=False)
    ax_tremors.ticklabel_format(useOffset=False)

    time_span = time_span[:-30]
    proba = proba[:-30]
    tremors = tremors[:-30]

    ax_proba.plot(time_span, gaussian_filter(proba, 5), linewidth=1, color='red')

    ax_proba.axhline(0.5, linestyle=(0, (5, 10)), alpha=0.3, zorder=0)

    ax_proba.margins(x=0)
    ax_tremors.margins(x=0)

    xlims1 = ax_proba.get_xlim()
    for i, span in enumerate(cat_michel):
        if span[0] > xlims1[0] and span[1] < xlims1[1]:
            # if span[0] > time_span[0] and span[1] < time_span[1]:
            print('entro!')
            # ax_proba.axvspan(span[0], span[1], alpha=0.15, color='red')
            ax_tremors.axvspan(span[0], span[1], alpha=0.15, color='red')
            arrow_offset = np.random.random() * 0.1
            myArrow = FancyArrowPatch(posA=(span[0], 0.85 + arrow_offset), posB=(span[1], 0.85 + arrow_offset),
                                      arrowstyle='<|-|>', color='red',
                                      mutation_scale=20, shrinkA=0, shrinkB=0)
            # ax_proba.add_artist(myArrow)

    for i, span in enumerate(cat_ours):
        if span[0] > xlims1[0] and span[1] < xlims1[1]:
            ax_proba.axvspan(span[0], span[1], alpha=0.15, color='C0')
            arrow_offset = np.random.random() * 0.1
            myArrow = FancyArrowPatch(posA=(span[0], 0.3 + arrow_offset), posB=(span[1], 0.3 + arrow_offset),
                                      arrowstyle='<|-|>', color='C0',
                                      mutation_scale=20, shrinkA=0, shrinkB=0)
            # ax_proba.add_artist(myArrow)

    ax_proba.set_ylabel('Probability of SSE detection', color='red')
    ax_proba.tick_params(axis='y', labelcolor='red')

    # unique, counts = get_n_tremors_per_day(time_span, tremors)
    n_tremors_per_day = get_n_tremors_per_day(time_span, tremors)
    markerline, stemlines, baseline = ax_tremors.stem(time_span, n_tremors_per_day, use_line_collection=True,
                                                      markerfmt=" ", basefmt=" ")
    plt.setp(stemlines, 'linewidth', 0.15)
    ax_tremors.set_ylabel('Number of tremor\nevents per day', color='C0')
    smoothed_tremors = gaussian_filter(n_tremors_per_day, 2.5)
    ax_tremors.plot(time_span, smoothed_tremors, color='C0', linewidth=1.)
    ax_tremors.tick_params(axis='y', labelcolor='C0')
    ax_tremors.get_xaxis().set_visible(False)
    ax_tremors.get_xaxis().set_ticks([])

    ax_tremors.spines.right.set_visible(False)
    ax_tremors.spines.top.set_visible(False)

    plt.tight_layout()
    plt.show()

    plot_correlation(proba, gaussian_filter(n_tremors_per_day, 1.5))

    plot_comparison_durations(cat_ours, cat_michel, 0.01)
    plot_comparison_duration_tremors(cat_ours, time_span, gaussian_filter(n_tremors_per_day, 1.5))
    plot_comparison_duration_tremors2(cat_ours, time_span, gaussian_filter(n_tremors_per_day, 1.5))
    exit(0)
    plot_comparison_duration_tremors_with_michel(cat_ours, time_span, gaussian_filter(n_tremors_per_day, 1.5))

    plot_scaling_law_duration_magnitude(cat_ours, cat_michel_mw, time_span, gaussian_filter(n_tremors_per_day, 1.5),
                                        n_tremors_per_day, proba)

    plot_comparison_duration_tremors_peaks(cat_ours, proba, time_span, gaussian_filter(n_tremors_per_day, 1.5))

    time_span_tremor_idx = np.nonzero(np.in1d(time_span, time_span))[0]
    proba_comparable_tremors = proba[time_span_tremor_idx]
    time_span_comparable_tremors = time_span[time_span_tremor_idx]
    nonzero_idx_proba = np.where(proba_comparable_tremors != 0)[0]

    # BOX PLOT

    # color=cmap(norm(overlap_percentages[i]))

    means, stds, bins, _, all_vals = asfunctionof(smoothed_tremors[nonzero_idx_proba],
                                                  proba_comparable_tremors[nonzero_idx_proba], bins=10,
                                                  return_vals=True)

    '''cmap = plt.cm.coolwarm
    # create normalization instance
    norm = matplotlib.colors.Normalize(np.min(all_vals), np.max(all_vals))
    # create a scalarmappable from the colormap
    sm = matplotlib.cm.ScalarMappable(cmap=cmap, norm=norm)
    sm.set_array([])'''

    # plt.plot(bins, means)
    flierprops = dict(linestyle='none', markeredgecolor='none', markerfacecolor='black', alpha=0.05)
    box_info = plt.boxplot(all_vals, positions=np.around(bins, decimals=1), widths=20, showfliers=True,
                           flierprops=flierprops)
    fliers = box_info['fliers']
    for i in range(len(fliers)):
        fliers[i].set_markerfacecolor('red')

    # CI = 1.96 * np.std(means) / np.mean(means)
    # plt.fill_between(bins, (np.array(means) - CI), (np.array(means) + CI), color='blue', alpha=0.1)
    # plt.errorbar(bins, means, yerr=stds)
    # plt.locator_params(axis='x', nbins=5)

    every_nth = 2
    for n, label in enumerate(plt.gca().xaxis.get_ticklabels()):
        if n % every_nth != 0:
            label.set_visible(False)
            plt.gca().xaxis.get_major_ticks()[n].set_visible(False)

    plt.xlabel('#tremors')
    plt.ylabel('probability of detection')
    # plt.colorbar()
    plt.show()

    plt.errorbar(bins, means, yerr=stds)
    plt.show()

    plt.scatter(smoothed_tremors[nonzero_idx_proba], proba_comparable_tremors[nonzero_idx_proba],
                c=time_span_comparable_tremors[nonzero_idx_proba], cmap='coolwarm')
    plt.xlabel('#tremors')
    plt.ylabel('probability of detection')
    plt.colorbar()
    plt.show()


def plot_proba_tremors_north_selection(duration_thresh=0.01):
    weight_code, name = '01Aug2022-144844', 'realgaps_ext_v3_0.001_6-7_135'

    # global time_span, proba, selected_gnss_data, station_codes, station_coordinates, detrend, attn_matrices, attn_matrices2
    with np.load(f'pred_cascadia_running_win_{weight_code}_{name}_north_selection.npz') as f:
        proba, time_span, attn_matrices = f['proba'], f['time'], f['attn_matrices']

    tremors = tremor_catalogue()

    # north selection on tremors
    valid_tremor_events = np.where(tremors[:, 0] >= 47.)[0]
    tremors = tremors[valid_tremor_events]

    reference_period = (2010, 2023)  # (2010, 2018.1) # (2007, 2018.1) # (2014, 2018.1)
    # selected_gnss_data, _, _, _ = _preliminary_operations(reference_period=reference_period, n_directions=3)
    time_filter = np.where(np.logical_and(time_span >= reference_period[0], time_span <= reference_period[1]))[0]
    time_span, proba = time_span[time_filter], proba[time_filter]

    valid_filter = proba != 0
    time_span, proba = time_span[valid_filter], proba[valid_filter]

    cat_ours, p, sp, spr = sse_selection_from_proba(proba, time_span, probability_threshold=0.5, rel_height=0.7)
    cat_michel = sse_catalogue()
    cat_michel_mw = sse_catalogue(return_magnitude=True)

    valid_michel_events_north = [15, 18, 20, 22, 27, 33, 35, 37]
    cat_michel = cat_michel[valid_michel_events_north]
    cat_michel_mw = cat_michel_mw[valid_michel_events_north]

    print(cat_ours)
    np.savetxt('geo_data/catalogue_sses_costantino_all_north.txt', cat_ours)

    fig, axes = plt.subplots(2, 1, figsize=(13.5, 7.5), sharex=True)
    ax_proba, ax_tremors = axes
    ax_proba.ticklabel_format(useOffset=False)
    ax_tremors.ticklabel_format(useOffset=False)

    ax_proba.plot(time_span, proba, linewidth=1.3)

    xlims1 = ax_proba.get_xlim()
    for i, span in enumerate(cat_michel):
        if span[0] > xlims1[0] and span[1] < xlims1[1]:
            ax_proba.axvspan(span[0], span[1], alpha=0.15, color='red')
            arrow_offset = np.random.random() * 0.1
            myArrow = FancyArrowPatch(posA=(span[0], 0.85 + arrow_offset), posB=(span[1], 0.85 + arrow_offset),
                                      arrowstyle='<|-|>', color='red',
                                      mutation_scale=20, shrinkA=0, shrinkB=0)
            # ax_proba.add_artist(myArrow)

    for i, span in enumerate(cat_ours):
        if span[0] > xlims1[0] and span[1] < xlims1[1]:
            ax_proba.axvspan(span[0], span[1], alpha=0.15, color='C0')
            arrow_offset = np.random.random() * 0.1
            myArrow = FancyArrowPatch(posA=(span[0], 0.3 + arrow_offset), posB=(span[1], 0.3 + arrow_offset),
                                      arrowstyle='<|-|>', color='C0',
                                      mutation_scale=20, shrinkA=0, shrinkB=0)
            # ax_proba.add_artist(myArrow)

    ax_proba.set_ylabel('Probability of SSE detection')

    # unique, counts = get_n_tremors_per_day(time_span, tremors)
    n_tremors_per_day = get_n_tremors_per_day(time_span, tremors)
    markerline, stemlines, baseline = ax_tremors.stem(time_span, n_tremors_per_day, use_line_collection=True,
                                                      markerfmt=" ", basefmt=" ")
    plt.setp(stemlines, 'linewidth', 0.15)
    ax_tremors.set_ylabel('Number of tremor events per day')
    smoothed_tremors = gaussian_filter(n_tremors_per_day, 2.5)
    ax_tremors.plot(time_span, smoothed_tremors, color='C0', linewidth=1.3)

    # ax_proba.scatter(time_span[p], proba[p], marker='x', color='C0')
    # ax_proba.scatter(time_span[sp], proba[sp], marker='x', color='C1')
    # ax_proba.plot(time_span, spr, color='C1', linewidth=1.3)

    # plt.tight_layout()
    plt.show()

    color_proba = 'C0'
    color_tremors = 'C7'
    color_michel = 'orangered'
    color_synthetics = 'mediumseagreen'

    fig, axis = plt.subplots(1, 1, figsize=(13.5, 7.5 / 2))
    ax_proba, ax_tremors = axis, axis.twinx()
    ax_proba.ticklabel_format(useOffset=False)
    ax_tremors.ticklabel_format(useOffset=False)

    ax_proba.plot(time_span, proba, linewidth=1, color=color_proba)
    ax_proba.set_ylim([0, 1])

    ax_proba.margins(x=0)
    ax_tremors.margins(x=0)

    xlims1 = ax_proba.get_xlim()
    for i, span in enumerate(cat_michel):
        if span[0] > xlims1[0] and span[1] < xlims1[1]:
            ax_proba.plot(span, [1.01, 1.01], clip_on=False, color=color_michel, linewidth=2.5)

    ax_proba.plot([2010, 2017.65], [-0.1, -0.1], clip_on=False, color=color_michel, linewidth=2)
    ax_proba.plot([2010, 2014], [-0.12, -0.12], clip_on=False, color=color_synthetics, linewidth=2)
    ax_proba.plot([2018, 2022.2], [-0.12, -0.12], clip_on=False, color=color_synthetics, linewidth=2)

    # ax_proba.annotate('', xy=(0.5, -0.1), xycoords='axes fraction', xytext=(1, -0.1), arrowprops=dict(arrowstyle="<->", color='b'))
    # ax_proba.annotate('', xy=(2015, -0.1), xycoords=ax_proba.get_xaxis_transform(), xytext=(2020, -0.1), arrowprops=dict(arrowstyle="|-|", linewidth=5, facecolor=color_michel))
    # ax_proba.annotate('', xy=(2015, -0.1), xycoords=trans)
    # ax_proba.add_artist(myArrow)

    for i, span in enumerate(cat_ours):
        if span[0] > xlims1[0] and span[1] < xlims1[1]:
            ax_proba.axvspan(span[0], span[1], alpha=0.25, color=color_proba, zorder=0)

    ax_proba.set_ylabel('Confidence of SSE detection', color=color_proba)
    ax_proba.tick_params(axis='y', labelcolor=color_proba)

    # unique, counts = get_n_tremors_per_day(time_span, tremors)
    n_tremors_per_day = get_n_tremors_per_day(time_span, tremors)
    markerline, stemlines, baseline = ax_tremors.stem(time_span, n_tremors_per_day, use_line_collection=True,
                                                      markerfmt=" ", basefmt=" ")
    plt.setp(stemlines, 'linewidth', 0.15)
    plt.setp(stemlines, 'color', color_tremors)
    ax_tremors.set_ylabel('Number of tremor events per day', color=color_tremors)
    smoothed_tremors = gaussian_filter(n_tremors_per_day, 2.5)
    ax_tremors.plot(time_span, smoothed_tremors, color=color_tremors, linewidth=1.)
    ax_tremors.tick_params(axis='y', labelcolor=color_tremors)
    ax_tremors.set_ylim([np.min(smoothed_tremors), 3 * np.max(smoothed_tremors)])
    ax_tremors.set_yticks([200, 500])
    # ax_proba.margins(x=0)
    # ax_tremors.margins(x=0)
    # plt.tight_layout()
    plt.savefig('sse_figures/north_selection/det_overview.pdf')
    plt.show()
    # plt.close(fig)

    fig = plt.figure(figsize=(13.5, 7.5 / 1.3))

    gs = GridSpec(4, 1, hspace=0)

    ax_proba = fig.add_subplot(gs[1:3])
    ax_tremors = fig.add_subplot(gs[0])  # fig.add_subplot(gs[1:4, 3])

    ax_proba.ticklabel_format(useOffset=False)
    ax_tremors.ticklabel_format(useOffset=False)

    time_span = time_span[:-30]
    proba = proba[:-30]
    tremors = tremors[:-30]

    ax_proba.plot(time_span, gaussian_filter(proba, 5), linewidth=1, color='red')

    ax_proba.axhline(0.5, linestyle=(0, (5, 10)), alpha=0.3, zorder=0)

    ax_proba.margins(x=0)
    ax_tremors.margins(x=0)

    xlims1 = ax_proba.get_xlim()
    for i, span in enumerate(cat_michel):
        if span[0] > xlims1[0] and span[1] < xlims1[1]:
            # if span[0] > time_span[0] and span[1] < time_span[1]:
            print('entro!')
            # ax_proba.axvspan(span[0], span[1], alpha=0.15, color='red')
            ax_tremors.axvspan(span[0], span[1], alpha=0.15, color='red')
            arrow_offset = np.random.random() * 0.1
            myArrow = FancyArrowPatch(posA=(span[0], 0.85 + arrow_offset), posB=(span[1], 0.85 + arrow_offset),
                                      arrowstyle='<|-|>', color='red',
                                      mutation_scale=20, shrinkA=0, shrinkB=0)
            # ax_proba.add_artist(myArrow)

    for i, span in enumerate(cat_ours):
        if span[0] > xlims1[0] and span[1] < xlims1[1]:
            ax_proba.axvspan(span[0], span[1], alpha=0.15, color='C0')
            arrow_offset = np.random.random() * 0.1
            myArrow = FancyArrowPatch(posA=(span[0], 0.3 + arrow_offset), posB=(span[1], 0.3 + arrow_offset),
                                      arrowstyle='<|-|>', color='C0',
                                      mutation_scale=20, shrinkA=0, shrinkB=0)
            # ax_proba.add_artist(myArrow)

    ax_proba.set_ylabel('Probability of SSE detection', color='red')
    ax_proba.tick_params(axis='y', labelcolor='red')

    # unique, counts = get_n_tremors_per_day(time_span, tremors)
    n_tremors_per_day = get_n_tremors_per_day(time_span, tremors)
    markerline, stemlines, baseline = ax_tremors.stem(time_span, n_tremors_per_day, use_line_collection=True,
                                                      markerfmt=" ", basefmt=" ")
    plt.setp(stemlines, 'linewidth', 0.15)
    ax_tremors.set_ylabel('Number of tremor\nevents per day', color='C0')
    smoothed_tremors = gaussian_filter(n_tremors_per_day, 2.5)
    ax_tremors.plot(time_span, smoothed_tremors, color='C0', linewidth=1.)
    ax_tremors.tick_params(axis='y', labelcolor='C0')
    ax_tremors.get_xaxis().set_visible(False)
    ax_tremors.get_xaxis().set_ticks([])

    ax_tremors.spines.right.set_visible(False)
    ax_tremors.spines.top.set_visible(False)

    plt.tight_layout()
    plt.show()

    plot_comparison_durations(cat_ours, cat_michel, 0.01, north=True)
    plot_comparison_duration_tremors(cat_ours, time_span, gaussian_filter(n_tremors_per_day, 1.5), north=True)
    plot_comparison_duration_tremors_with_michel(cat_ours, time_span, gaussian_filter(n_tremors_per_day, 1.5))

    plot_scaling_law_duration_magnitude(cat_ours, cat_michel_mw, time_span, gaussian_filter(n_tremors_per_day, 1.5),
                                        n_tremors_per_day, proba, north=True)

    plot_comparison_duration_tremors_peaks(cat_ours, proba, time_span, gaussian_filter(n_tremors_per_day, 1.5),
                                           north=True)

    time_span_tremor_idx = np.nonzero(np.in1d(time_span, time_span))[0]
    proba_comparable_tremors = proba[time_span_tremor_idx]
    time_span_comparable_tremors = time_span[time_span_tremor_idx]
    nonzero_idx_proba = np.where(proba_comparable_tremors != 0)[0]

    # BOX PLOT

    # color=cmap(norm(overlap_percentages[i]))

    means, stds, bins, _, all_vals = asfunctionof(smoothed_tremors[nonzero_idx_proba],
                                                  proba_comparable_tremors[nonzero_idx_proba], bins=10,
                                                  return_vals=True)

    '''cmap = plt.cm.coolwarm
    # create normalization instance
    norm = matplotlib.colors.Normalize(np.min(all_vals), np.max(all_vals))
    # create a scalarmappable from the colormap
    sm = matplotlib.cm.ScalarMappable(cmap=cmap, norm=norm)
    sm.set_array([])'''

    # plt.plot(bins, means)
    flierprops = dict(linestyle='none', markeredgecolor='none', markerfacecolor='black', alpha=0.05)
    box_info = plt.boxplot(all_vals, positions=np.around(bins, decimals=1), widths=20, showfliers=True,
                           flierprops=flierprops)
    fliers = box_info['fliers']
    for i in range(len(fliers)):
        fliers[i].set_markerfacecolor('red')

    # CI = 1.96 * np.std(means) / np.mean(means)
    # plt.fill_between(bins, (np.array(means) - CI), (np.array(means) + CI), color='blue', alpha=0.1)
    # plt.errorbar(bins, means, yerr=stds)
    # plt.locator_params(axis='x', nbins=5)

    every_nth = 2
    for n, label in enumerate(plt.gca().xaxis.get_ticklabels()):
        if n % every_nth != 0:
            label.set_visible(False)
            plt.gca().xaxis.get_major_ticks()[n].set_visible(False)

    plt.xlabel('#tremors')
    plt.ylabel('probability of detection')
    # plt.colorbar()
    plt.show()

    plt.errorbar(bins, means, yerr=stds)
    plt.show()

    plt.scatter(smoothed_tremors[nonzero_idx_proba], proba_comparable_tremors[nonzero_idx_proba],
                c=time_span_comparable_tremors[nonzero_idx_proba], cmap='coolwarm')
    plt.xlabel('#tremors')
    plt.ylabel('probability of detection')
    plt.colorbar()
    plt.show()


##########################################################################################

def load_proba(reference_period, weight_code='01Aug2022-144844', name='realgaps_ext_v3_0.001_6-7_135', north=False, south=False):
    with np.load(f'predictions/pred_cascadia_running_win_{weight_code}_{name}' + ('_north_selection' if north else '') + ('_south_selection' if south else '') + '.npz') as f:
        proba, time_span, attn_matrices = f['proba'], f['time'], f['attn_matrices']
    time_filter = np.where(np.logical_and(time_span >= reference_period[0], time_span <= reference_period[1]))[0]
    time_span, proba = time_span[time_filter], proba[time_filter]
    valid_filter = proba != 0
    time_span, proba = time_span[valid_filter], proba[valid_filter]
    return time_span, proba


##########################################################################################

def detection_overview(time, proba, n_tremors_per_day, catalog_ours, catalog_michel, north=False):
    color_proba = 'C0'
    color_tremors = 'C7'
    color_michel = 'orangered'
    color_synthetics = 'mediumseagreen'
    offset_proba = 0.3
    offset_lines = 0.03
    fig, axis = plt.subplots(1, 1, figsize=(13.5, 7.5 / 2))
    ax_proba, ax_tremors = axis, axis.twinx()
    ax_proba.ticklabel_format(useOffset=False)
    ax_tremors.ticklabel_format(useOffset=False)

    ax_proba.plot(time, proba, linewidth=1, color=color_proba)
    #ax_proba.axhline(y=0.5)
    ax_proba.set_ylim([0 - offset_proba, 1 + offset_proba / 6])
    # ax_proba.set_yticks([0.3, 0.5, 0.7, 0.9])
    ax_proba.set_yticks([0.4, 0.6, 0.8, 1])

    ax_proba.margins(x=0)
    ax_tremors.margins(x=0)

    xlims1 = ax_proba.get_xlim()
    for i, span in enumerate(catalog_michel):
        if span[0] > xlims1[0] and span[1] < xlims1[1]:
            # ax_proba.plot(span, [1.01, 1.01], clip_on=False, color=color_michel, linewidth=2.5)
            ax_proba.plot(span, [0.2, 0.2], clip_on=False, color=color_michel, linewidth=2.5)

    #ax_proba.plot([2010, 2017.65], [-0.1 - offset_proba - offset_lines, -0.1 - offset_proba - offset_lines], clip_on=False, color=color_michel, linewidth=2)
    #ax_proba.plot([2010, 2014], [-0.12 - offset_proba - offset_lines, -0.12 - offset_proba - offset_lines], clip_on=False, color=color_synthetics, linewidth=2)
    #ax_proba.plot([2018, 2022.2], [-0.12 - offset_proba - offset_lines, -0.12 - offset_proba - offset_lines], clip_on=False, color=color_synthetics, linewidth=2)
    position_catalog = 1.12  # 1.1  # -0.1 - offset_proba - offset_lines
    position_synthetics = -0.11 - offset_proba - offset_lines  # -0.12 - offset_proba - offset_lines - 0.05
    _add_double_sided_arrow(ax_proba, 2010., position_catalog, 2017.65, position_catalog, color=color_michel)
    _add_double_sided_arrow(ax_proba, 2010., position_synthetics, 2014., position_synthetics, color=color_synthetics)
    _add_double_sided_arrow(ax_proba, 2018., position_synthetics, 2022.2, position_synthetics, color=color_synthetics)

    for i, span in enumerate(catalog_ours):
        if span[0] > xlims1[0] and span[1] < xlims1[1]:
            ax_proba.axvspan(span[0], span[1], alpha=0.25, color=color_proba, zorder=0, linewidth=0.)

    ax_proba.set_ylabel('Confidence of SSE detection', color=color_proba)
    ax_proba.tick_params(axis='y', labelcolor=color_proba)

    markerline, stemlines, baseline = ax_tremors.stem(time, n_tremors_per_day, use_line_collection=True,
                                                      markerfmt=" ", basefmt=" ")
    plt.setp(stemlines, 'linewidth', 0.15)
    plt.setp(stemlines, 'color', color_tremors)
    ax_tremors.set_ylabel('Number of tremor events per day', color=color_tremors)
    smoothed_tremors = gaussian_filter(n_tremors_per_day, 2.5)
    ax_tremors.plot(time, smoothed_tremors, color=color_tremors, linewidth=1.)
    ax_tremors.tick_params(axis='y', labelcolor=color_tremors)
    ax_tremors.set_ylim([np.min(smoothed_tremors), 3 * np.max(smoothed_tremors)])
    ax_tremors.set_yticks([200, 500])

    plt.savefig('sse_figures/' + ('north_selection/' if north else '') + 'det_overview.pdf')
    #plt.show()
    plt.close(fig)


def detection_overview2(time, proba, n_tremors_per_day, catalog_ours, catalog_michel, draw_sse_durations=True, draw_michel_catalog=True, draw_tremor_rectangle=True, draw_tremors=True, north=False, south=False, base_fig_folder='./sse_figures/'):
    color_proba = 'C0'
    color_tremors = 'C7'
    color_michel = 'orangered'
    color_synthetics = 'mediumseagreen'
    offset_proba = 0.3
    offset_lines = 0.03
    fig, axis = plt.subplots(1, 1, figsize=(13.5, 7.5 / 2))
    ax_proba, ax_tremors = axis, axis.twinx()
    ax_proba.ticklabel_format(useOffset=False)
    ax_tremors.ticklabel_format(useOffset=False)

    ax_proba.plot(time, proba, linewidth=1, color=color_proba)
    #ax_proba.axhline(y=0.5)
    ax_proba.set_ylim([0 - offset_proba, 1 + offset_proba / 6])
    # ax_proba.set_yticks([0.3, 0.5, 0.7, 0.9])
    ax_proba.set_yticks([0.4, 0.6, 0.8, 1])

    ax_proba.margins(x=0)
    ax_tremors.margins(x=0)

    xlims1 = ax_proba.get_xlim()
    if draw_michel_catalog:
        for i, span in enumerate(catalog_michel):
            if span[0] > xlims1[0] and span[1] < xlims1[1]:
                # ax_proba.plot(span, [1.01, 1.01], clip_on=False, color=color_michel, linewidth=2.5)
                ax_proba.plot(span, [0.2, 0.2], clip_on=False, color=color_michel, linewidth=2.5)

    #ax_proba.plot([2010, 2017.65], [-0.1 - offset_proba - offset_lines, -0.1 - offset_proba - offset_lines], clip_on=False, color=color_michel, linewidth=2)
    #ax_proba.plot([2010, 2014], [-0.12 - offset_proba - offset_lines, -0.12 - offset_proba - offset_lines], clip_on=False, color=color_synthetics, linewidth=2)
    #ax_proba.plot([2018, 2022.2], [-0.12 - offset_proba - offset_lines, -0.12 - offset_proba - offset_lines], clip_on=False, color=color_synthetics, linewidth=2)
    position_catalog = 1.12  # 1.1  # -0.1 - offset_proba - offset_lines
    position_synthetics = -0.11 - offset_proba - offset_lines  # -0.12 - offset_proba - offset_lines - 0.05
    #_add_double_sided_arrow(ax_proba, 2010., position_catalog, 2017.65, position_catalog, color=color_michel)
    _add_double_sided_arrow(ax_proba, xlims1[0], position_catalog, 2017.65, position_catalog, color=color_michel)
    #_add_double_sided_arrow(ax_proba, 2010., position_synthetics, 2014., position_synthetics, color=color_synthetics)
    _add_double_sided_arrow(ax_proba, xlims1[0], position_synthetics, 2014., position_synthetics, color=color_synthetics)
    _add_double_sided_arrow(ax_proba, 2018., position_synthetics, 2022.2, position_synthetics, color=color_synthetics)

    if draw_sse_durations:
        for i, span in enumerate(catalog_ours):
            if span[0] > xlims1[0] and span[1] < xlims1[1]:
                ax_proba.axvspan(span[0], span[1], ymin=0.37, alpha=0.25, color=color_proba, zorder=0, linewidth=0.)

    if draw_tremor_rectangle:
        ax_tremors.axvspan(xlims1[0], 2009.55, ymax=0.37, alpha=0.25, color='grey', zorder=0, linewidth=0.)

    # ax_proba.set_ylabel('Confidence of SSE detection', color=color_proba)
    ax_proba.set_ylabel('Probability of SSE detection', color=color_proba)
    ax_proba.tick_params(axis='y', labelcolor=color_proba)

    if draw_tremors:
        markerline, stemlines, baseline = ax_tremors.stem(time, n_tremors_per_day, use_line_collection=True,
                                                          markerfmt=" ", basefmt=" ")
        plt.setp(stemlines, 'linewidth', 0.15)
        plt.setp(stemlines, 'color', color_tremors)
        ax_tremors.set_ylabel('Number of tremor events per day', color=color_tremors)
        smoothed_tremors = gaussian_filter(n_tremors_per_day, 2.5)
        ax_tremors.plot(time, smoothed_tremors, color=color_tremors, linewidth=1.)
        ax_tremors.tick_params(axis='y', labelcolor=color_tremors)
        ax_tremors.set_ylim([np.min(smoothed_tremors), 3 * np.max(smoothed_tremors)])
        ax_tremors.set_yticks([200, 500])
    else:
        ax_tremors.set_yticks([])

    plt.savefig(base_fig_folder + ('north_selection/' if north else '') + ('south_selection/' if south else '') + 'det_overview2.pdf')
    #plt.show()
    plt.close(fig)

def zoom_on_overview_plot(time, proba, n_tremors_per_day, catalog_ours, catalog_michel, draw_sse_durations=True, draw_michel_catalog=True, draw_tremor_rectangle=True, draw_tremors=True, north=False, south=False, base_fig_folder='./sse_figures/'):
    labelsize = BIGGER_SIZE + 2
    plt.rcParams.update({'ytick.labelsize': labelsize, 'xtick.labelsize': labelsize})  # fontsize of the x and y labels
    zoom_periods = [(2015.94, 2017), (2019.15, 2020.9), (2017., 2017.5)]
    color_proba = 'C0'
    color_tremors = 'C7'
    color_michel = 'orangered'
    color_synthetics = 'mediumseagreen'
    offset_proba = 0.3
    offset_lines = 0.03
    for i, zoom in enumerate(zoom_periods):
        print(f'zoom period: {zoom}')
        time_span_idx = np.where(np.logical_and(time >= zoom[0], time <= zoom[1]))
        fig, axis = plt.subplots(1, 1, figsize=(13.5, 7.5 / 2))
        if i == 2:
            fig, axis = plt.subplots(1, 1, figsize=(13.5 / 2, 7.5 / 2))
        ax_proba, ax_tremors = axis, axis.twinx()
        ax_proba.ticklabel_format(useOffset=False)
        ax_tremors.ticklabel_format(useOffset=False)


        ax_proba.plot(time[time_span_idx], proba[time_span_idx], linewidth=1, color=color_proba)

        ax_proba.set_ylim([0 - offset_proba, 1 + offset_proba / 6])
        ax_proba.set_yticks([0.4, 0.6, 0.8, 1])
        ax_proba.margins(x=0)
        ax_tremors.margins(x=0)

        xlims1 = ax_proba.get_xlim()
        if draw_michel_catalog:
            for i, span in enumerate(catalog_michel):
                if span[0] > xlims1[0] and span[1] < xlims1[1]:
                    # ax_proba.plot(span, [1.01, 1.01], clip_on=False, color=color_michel, linewidth=2.5)
                    ax_proba.plot(span, [0.2, 0.2], clip_on=False, color=color_michel, linewidth=2.5)

        if draw_sse_durations:
            for i, span in enumerate(catalog_ours):
                if span[0] > xlims1[0]-0.01 and span[1] < xlims1[1] + 0.01:
                    start = np.maximum(xlims1[0], span[0])
                    end = np.minimum(xlims1[1], span[1])
                    ax_proba.axvspan(start, end, ymin=0.37, alpha=0.25, color=color_proba, zorder=0, linewidth=0.)

        # ax_proba.set_ylabel('Confidence of SSE detection', color=color_proba)
        ax_proba.tick_params(axis='y', labelcolor=color_proba)

        if draw_tremors:
            markerline, stemlines, baseline = ax_tremors.stem(time[time_span_idx], n_tremors_per_day[time_span_idx], use_line_collection=True,
                                                              markerfmt=" ", basefmt=" ")
            plt.setp(stemlines, 'linewidth', 0.15)
            plt.setp(stemlines, 'color', color_tremors)
            #ax_tremors.set_ylabel('Number of tremor events per day', color=color_tremors)
            smoothed_tremors = gaussian_filter(n_tremors_per_day, 2.5)
            ax_tremors.plot(time[time_span_idx], smoothed_tremors[time_span_idx], color=color_tremors, linewidth=1.)
            ax_tremors.tick_params(axis='y', labelcolor=color_tremors)
            ax_tremors.set_ylim([np.min(smoothed_tremors), 3 * np.max(smoothed_tremors)])
            ax_tremors.set_yticks([200, 500])
        else:
            ax_tremors.set_yticks([])

        ### xlabel conversion in datetime format
        decyr_conversion = {v: k for k, v in ymd_decimal_year_lookup().items()}  # decimal -> DMY
        current_ticks = ax_proba.get_xticks()
        labels_as_datetime = [datetime.datetime(year=decyr_conversion[_find_nearest_val(time[time_span_idx], date)[0]][0], month=decyr_conversion[_find_nearest_val(time[time_span_idx], date)[0]][1], day=decyr_conversion[_find_nearest_val(time[time_span_idx], date)[0]][2]).strftime('%m/%Y') for date in current_ticks]
        #ax_proba.set_xticks(current_ticks, labels_as_datetime)
        ax_proba.set_xticks(current_ticks[1:-1], labels_as_datetime[1:-1])  # avoid the first and the last one

        plt.savefig(base_fig_folder + ('north_selection/' if north else '') +  ('south_selection/' if south else '') + f'zoom_{zoom[0]}_{zoom[1]}.pdf')
        #plt.show()
        plt.close(fig)
    plt.rcParams.update({'ytick.labelsize': MEDIUM_SIZE, 'xtick.labelsize': MEDIUM_SIZE})  # fontsize of the x and y labels


def correlation_proba_tremor_lag(time_array, proba, n_tremors_per_day, sigma=1.5, derivative=False, upsampling_factor=1, max_lag=15, smooth_tremors=True, scale=True, north=False, south=False, tremor_time_ref=None, base_fig_folder='./sse_figures/'):
    if tremor_time_ref is not None:
        time_cut = np.logical_and(time_array >= tremor_time_ref[0], time_array <= tremor_time_ref[1])
        proba = proba[time_cut]
        n_tremors_per_day = n_tremors_per_day[time_cut]

    if smooth_tremors:
        n_tremors_per_day = gaussian_filter(n_tremors_per_day, sigma)
    if scale:
        proba = (proba - np.min(proba)) / (np.max(proba) - np.min(proba))
        n_tremors_per_day = (n_tremors_per_day - np.min(n_tremors_per_day)) / (np.max(n_tremors_per_day) - np.min(n_tremors_per_day))
    if derivative:
        proba = np.diff(proba)
        n_tremors_per_day = np.diff(n_tremors_per_day)
    if upsampling_factor > 1:
        x = np.linspace(0, len(proba), len(proba))
        x_upsampled = np.linspace(0, len(proba), len(proba) * upsampling_factor)
        lin_proba = interp1d(x, proba)
        lin_tremors = interp1d(x, n_tremors_per_day)
        proba = lin_proba(x_upsampled)
        n_tremors_per_day = lin_tremors(x_upsampled)

    print('Correlation coefficient (at zero):', np.corrcoef(proba, n_tremors_per_day)[0, 1])
    x, corr = _lagged_correlation(proba, n_tremors_per_day, max_lag, upsampling_factor=upsampling_factor)

    fig = plt.figure(dpi=100)
    plt.plot(x, corr, color='black')
    plt.axvline(x=0, linewidth=0.3, color='black')
    # plt.xlabel('Global-scale time lag between tremor activity and detection probability [days]')
    plt.xlabel('Global-scale tremor-SSE time lag [days]')
    # plt.ylabel('Correlation coefficient')
    plt.ylabel('Cross-correlation')
    plt.savefig(base_fig_folder + ('north_selection/' if north else '') + ('south_selection/' if south else '') + 'global_corr.pdf')
    plt.close(fig)



def plot_comparison_duration_ours_michel(cat_ours, cat_michel, overlap_thresh, save_diff_catalog=False, north=False, south=False, base_fig_folder='./sse_figures/'):
    filtered_cat_ours = []
    filtered_cat_michel = []
    overlap_percentages = []
    unique_set_michel = set()  # contains the Michel et al. events that are retrieved by SSEdetector
    for i, event in enumerate(cat_ours):
        for j in range(cat_michel.shape[0]):
            overlap_percent = overlap_percentage(event, cat_michel[j].tolist())[0]
            if overlap_percent >= overlap_thresh:
                filtered_cat_ours.append(event)
                filtered_cat_michel.append(cat_michel[j])
                overlap_percentages.append(overlap_percent)
                unique_set_michel.add((cat_michel[j][0], cat_michel[j][1]))

    unique_set_michel = np.array(list(unique_set_michel))
    unique_set_michel = unique_set_michel[np.argsort(unique_set_michel[:, 0])]  # sort by starting date
    if save_diff_catalog:
        np.savetxt('geo_data/catalogue_sse_costantino_michel_retrieved.txt', unique_set_michel, fmt='%f')

    filtered_cat_michel = np.array(filtered_cat_michel)
    filtered_cat_ours = np.array(filtered_cat_ours)
    overlap_percentages = np.array(overlap_percentages)

    duration_cat_michel = (filtered_cat_michel[:, 1] - filtered_cat_michel[:, 0]) * 365
    duration_cat_ours = (filtered_cat_ours[:, 1] - filtered_cat_ours[:, 0]) * 365

    fig = plt.figure(dpi=100)
    cmap = plt.cm.coolwarm
    # create normalization instance
    norm = matplotlib.colors.Normalize(np.min(overlap_percentages), np.max(overlap_percentages))
    # create a scalarmappable from the colormap
    sm = matplotlib.cm.ScalarMappable(cmap=cmap, norm=norm)
    sm.set_array([])
    last_marker_square_idx, last_marker_circle_idx = 0, 0
    for i in range(len(duration_cat_michel)):
        if 2014. <= filtered_cat_ours[i][0] < 2018.:
            marker = 's'
            last_marker_square_idx = i
        else:
            marker = 'o'
            last_marker_circle_idx = i
        plt.scatter(duration_cat_michel[i], duration_cat_ours[i], color=cmap(norm(overlap_percentages[i])), marker=marker)

    plt.scatter(duration_cat_michel[last_marker_square_idx], duration_cat_ours[last_marker_square_idx], color=cmap(1.), marker='s', label='2014-2017')
    plt.scatter(duration_cat_michel[last_marker_circle_idx], duration_cat_ours[last_marker_circle_idx], color=cmap(1.),
                marker='o', label='2007-2013')
    p1 = max(max(duration_cat_ours), max(duration_cat_michel))
    p2 = min(min(duration_cat_ours), min(duration_cat_michel))
    plt.plot([p1, p2], [p1, p2], alpha=0.5, color='black', linewidth=1, zorder=0)
    plt.xlabel('Durations [days] (Michel et al., 2019)')
    plt.ylabel('Durations [days] (ours)')
    cbar = plt.colorbar(sm)
    cbar.ax.set_ylabel('Amount of overlap (%)')
    plt.gca().set_aspect('equal')
    plt.legend()
    plt.savefig(base_fig_folder + ('north_selection/' if north else '') + ('south_selection/' if south else '') + 'dur_michel.pdf')
    plt.close(fig)


def plot_comparison_duration_ours_michel2(cat_ours, cat_michel, overlap_thresh, save_diff_catalog=False, north=False, south=False, base_fig_folder='./sse_figures/'):
    filtered_cat_ours = []
    filtered_cat_michel = []
    overlap_percentages = []
    unique_set_michel = set()  # contains the Michel et al. events that are retrieved by SSEdetector
    unique_set_missed = set()
    idx_michel_visited = set()
    for i, event in enumerate(cat_ours):
        for j in range(cat_michel.shape[0]):
            overlap_percent = overlap_percentage(event, cat_michel[j].tolist())[0]
            if overlap_percent >= overlap_thresh:
                if (cat_michel[j][1] - cat_michel[j][0]) * 365 > 50:
                    print(cat_michel[j])
                filtered_cat_ours.append(event)
                filtered_cat_michel.append(cat_michel[j])
                overlap_percentages.append(overlap_percent)
                unique_set_michel.add((cat_michel[j][0], cat_michel[j][1]))
                idx_michel_visited.add(j)

    idx_michel_missed = list(set(range(cat_michel.shape[0])).difference(idx_michel_visited))
    unique_set_michel = np.array(list(unique_set_michel))
    unique_set_michel = unique_set_michel[np.argsort(unique_set_michel[:, 0])]  # sort by starting date
    if save_diff_catalog:
        np.savetxt('geo_data/catalogue_sse_costantino_michel_retrieved.txt', unique_set_michel, fmt='%f')

    print('Michel et al. missed events:')
    print(cat_michel[idx_michel_missed])

    filtered_cat_michel = np.array(filtered_cat_michel)
    filtered_cat_ours = np.array(filtered_cat_ours)
    overlap_percentages = np.array(overlap_percentages)

    duration_cat_michel = (filtered_cat_michel[:, 1] - filtered_cat_michel[:, 0]) * 365
    duration_cat_ours = (filtered_cat_ours[:, 1] - filtered_cat_ours[:, 0]) * 365

    fig = plt.figure(dpi=100)

    plt.scatter(duration_cat_michel, duration_cat_ours, c=overlap_percentages, cmap='coolwarm')

    p1 = max(max(duration_cat_ours), max(duration_cat_michel))
    p2 = min(min(duration_cat_ours), min(duration_cat_michel))
    plt.plot([p1, p2], [p1, p2], alpha=0.5, color='black', linewidth=1, zorder=0)
    plt.xlabel('Durations [days] (Michel et al., 2019)')
    plt.ylabel('Durations [days] (ours)')
    cbar = plt.colorbar()
    cbar.ax.set_ylabel('Amount of overlap (%)')
    plt.gca().set_aspect('equal')
    plt.savefig(base_fig_folder + ('north_selection/' if north else '') + ('south_selection/' if south else '') + 'dur_michel2.pdf')
    plt.close(fig)


def plot_comparison_duration_proba_tremors_old(cat, time_array, proba, n_tremors_per_day, rel_height=0.99, sigma=1.5, north=False):
    """Take duration on catalogue, extract the same time period on tremors, find a peak and find a duration
    over that peak on tremors. Finally compare the two."""
    n_tremors_per_day = gaussian_filter(n_tremors_per_day, sigma)
    tremor_durations = []
    valid_cat = []
    time_lags = []
    tremor_search_offset = 20
    tremor_search_offset_date = tremor_search_offset / 365
    for time_span in cat:
        print('time_span', time_span)
        time_idx = np.logical_and(time_array >= time_span[0], time_array <= time_span[1])
        current_tremor_window = n_tremors_per_day[time_idx]
        peaks, _ = find_peaks(current_tremor_window)
        extended_time_idx = np.logical_and(time_array >= time_span[0] - tremor_search_offset_date, time_array <= time_span[1] + tremor_search_offset_date)
        extended_current_tremor_window = n_tremors_per_day[extended_time_idx]
        if len(peaks) > 0:
            print(f'Found {len(peaks)} peak(s) at:', time[time_idx][peaks])
            print(f'Searching for widths from {time[extended_time_idx][0]} to {time[extended_time_idx][-1]}')
            print(f'Check peak still in: {time[extended_time_idx][peaks + tremor_search_offset]}')
            widths = peak_widths(extended_current_tremor_window, peaks + tremor_search_offset, rel_height=rel_height)
            # per ogni picco in widths, calcola il min a sx il max a dx e metti come durata
            start_time, end_time = time_array[extended_time_idx][round(np.min(widths[2]))], time_array[extended_time_idx][
                round(np.max(widths[3]))]
            print('SSE starts:', time_span[0], 'tremors start:', start_time, 'and end:', end_time)
            tremor_durations.append((end_time - start_time) * 365)
            valid_cat.append((time_span[1] - time_span[0]) * 365)
            # overlap_percent = overlap_percentage((start_time, end_time), time_span)[0]

            # overlap_percentages.append(overlap_percent)
            time_lags.append((time_span[0] - start_time) * 365)

            ######################################################
            tremor_visualization_offset_date = 30 / 365
            visualization_time_idx = np.logical_and(time_array >= time_span[0] - tremor_visualization_offset_date,
                                               time_array <= time_span[1] + tremor_visualization_offset_date)
            fig, axes = plt.subplots(2, 1)
            ax1, ax2 = axes
            ax1.plot(time_array[visualization_time_idx], proba[visualization_time_idx])
            ax1.axvline(time_span[0], linewidth=0.7, color='red')
            ax1.axvline(time_span[1], linewidth=0.7, color='red')
            ax2.plot(time_array[visualization_time_idx], n_tremors_per_day[visualization_time_idx])
            ax2.axvline(start_time, linewidth=0.7)
            ax2.axvline(end_time, linewidth=0.7)

            plt.show()

    cmap = plt.cm.coolwarm
    fig = plt.figure(dpi=100)
    # create normalization instance
    norm = matplotlib.colors.Normalize(np.min(time_lags), np.max(time_lags))
    # create a scalarmappable from the colormap
    sm = matplotlib.cm.ScalarMappable(cmap=cmap, norm=norm)
    sm.set_array([])
    last_marker_square_idx, last_marker_circle_idx, last_marker_star_idx = 0, 0, 0
    for i in range(len(valid_cat)):
        if 2014. <= cat[i][0] < 2018.1:
            marker = 's'
            last_marker_square_idx = i
        # elif 2010. <= cat[i][0] < 2014.:
        elif 2007. <= cat[i][0] < 2014.:
            marker = 'o'
            last_marker_circle_idx = i
        else:
            marker = '*'
            last_marker_star_idx = i
        plt.scatter(tremor_durations[i], valid_cat[i], color=cmap(norm(time_lags[i])), marker=marker)

    #X, Y, Z = density_estimation(tremor_durations, valid_cat)
    #plt.contour(X, Y, Z)
    #seaborn.kdeplot(tremor_durations, valid_cat, fill=False, levels=5, thresh=.2)

    plt.scatter(tremor_durations[last_marker_circle_idx], valid_cat[last_marker_circle_idx], color=cmap(1.),
                # marker='o', label='2010-2013')
                marker='o', label='2007-2013')
    plt.scatter(tremor_durations[last_marker_square_idx], valid_cat[last_marker_square_idx], color=cmap(1.),
                marker='s', label='2014-2017')
    plt.scatter(tremor_durations[last_marker_star_idx], valid_cat[last_marker_star_idx], color=cmap(1.),
                marker='*', label='2018-2022')

    p1 = max(max(valid_cat), max(tremor_durations))
    p2 = min(min(valid_cat), min(tremor_durations))
    plt.plot([p1, p2], [p1, p2], alpha=0.5, color='black', linewidth=1, zorder=0)
    plt.xlabel('Tremor durations [days]')
    plt.ylabel('SSE durations [days] (ours)')
    # cbar = plt.colorbar()
    cbar = plt.colorbar(sm)
    # cbar.ax.set_ylabel('Amount of overlap (%)')
    cbar.ax.set_ylabel('Time lag between tremor and SSE onsets [days]')
    plt.legend()
    # plt.gca().set_aspect(abs(plt.xlim()[1] - plt.xlim()[0]) / abs(plt.ylim()[1] - plt.ylim()[0]))
    plt.gca().set_aspect('equal')
    plt.savefig('sse_figures/' + ('north_selection/' if north else '') + 'dur_tremors2.pdf')
    # plt.show()
    plt.close(fig)

    return valid_cat, tremor_durations

def plot_comparison_duration_proba_tremors(cat, time_array, proba, n_tremors_per_day, rel_height=0.99, sigma=1.5, north=False):
    """Take duration on catalogue, extract the same time period on tremors with a certain variability,
    find a peak and find a duration over that peak on tremors. Finally compare the two."""
    n_tremors_per_day = gaussian_filter(n_tremors_per_day, sigma)
    tremor_durations = []
    valid_cat = []
    time_lags = []
    sse_duration_variability = 0  # days
    tremor_search_offset_date = sse_duration_variability / 365
    for time_span in cat:
        print('time_span', time_span)
        time_idx = np.logical_and(time_array >= time_span[0] - tremor_search_offset_date,
                                  time_array <= time_span[1] + tremor_search_offset_date)
        print('but searching in:', time_span[0] - tremor_search_offset_date, time_span[1] + tremor_search_offset_date)
        current_tremor_window = n_tremors_per_day[time_idx]
        peaks, _ = find_peaks(current_tremor_window)
        if len(peaks) > 0:
            print(f'Found {len(peaks)} peak(s) at:', time[time_idx][peaks])
            print(f'Searching for widths from {time[time_idx][0]} to {time[time_idx][-1]}')
            print(f'Check peak still in: {time[time_idx][peaks]}')
            widths = peak_widths(current_tremor_window, peaks, rel_height=rel_height)
            # per ogni picco in widths, calcola il min a sx il max a dx e metti come durata
            start_time, end_time = time_array[time_idx][round(np.min(widths[2]))], time_array[time_idx][
                round(np.max(widths[3]))]
            print('SSE starts:', time_span[0], 'tremors start:', start_time, 'and end:', end_time)

            tremor_durations.append((end_time - start_time) * 365)
            valid_cat.append((time_span[1] - time_span[0]) * 365)
            time_lags.append((time_span[0] - start_time) * 365)

            ######################################################
            tremor_visualization_offset_date = 30 / 365
            visualization_time_idx = np.logical_and(time_array >= time_span[0] - tremor_visualization_offset_date,
                                               time_array <= time_span[1] + tremor_visualization_offset_date)
            fig, axes = plt.subplots(2, 1)
            ax1, ax2 = axes
            ax1.plot(time_array[visualization_time_idx], proba[visualization_time_idx])
            ax1.axvline(time_span[0], linewidth=0.7, color='red')
            ax1.axvline(time_span[1], linewidth=0.7, color='red')
            ax1.axvline(time_span[0] - tremor_search_offset_date, linewidth=0.7, color='blue')
            ax1.axvline(time_span[1] + tremor_search_offset_date, linewidth=0.7, color='blue')
            ax2.plot(time_array[visualization_time_idx], n_tremors_per_day[visualization_time_idx])
            ax2.axvline(start_time, linewidth=0.7)
            ax2.axvline(end_time, linewidth=0.7)
            #ax2.axhline(np.abs(np.median(current_tremor_window) - 3 * median_abs_deviation(n_tremors_per_day)), linewidth=0.7, color='red')
            #ax2.axhline(np.median(current_tremor_window), linewidth=0.7, color='blue')
            plt.show()

    cmap = plt.cm.coolwarm
    fig = plt.figure(dpi=100)
    # create normalization instance
    norm = matplotlib.colors.Normalize(np.min(time_lags), np.max(time_lags))
    # create a scalarmappable from the colormap
    sm = matplotlib.cm.ScalarMappable(cmap=cmap, norm=norm)
    sm.set_array([])
    last_marker_square_idx, last_marker_circle_idx, last_marker_star_idx = 0, 0, 0
    for i in range(len(valid_cat)):
        if 2014. <= cat[i][0] < 2018.1:
            marker = 's'
            last_marker_square_idx = i
        # elif 2010. <= cat[i][0] < 2014.:
        elif 2007. <= cat[i][0] < 2014.:
            marker = 'o'
            last_marker_circle_idx = i
        else:
            marker = '*'
            last_marker_star_idx = i
        plt.scatter(tremor_durations[i], valid_cat[i], color=cmap(norm(time_lags[i])), marker=marker)

    #X, Y, Z = density_estimation(tremor_durations, valid_cat)
    #plt.contour(X, Y, Z)
    #seaborn.kdeplot(tremor_durations, valid_cat, fill=False, levels=5, thresh=.2)

    plt.scatter(tremor_durations[last_marker_circle_idx], valid_cat[last_marker_circle_idx], color=cmap(1.),
                # marker='o', label='2010-2013')
                marker='o', label='2007-2013')
    plt.scatter(tremor_durations[last_marker_square_idx], valid_cat[last_marker_square_idx], color=cmap(1.),
                marker='s', label='2014-2017')
    plt.scatter(tremor_durations[last_marker_star_idx], valid_cat[last_marker_star_idx], color=cmap(1.),
                marker='*', label='2018-2022')

    p1 = max(max(valid_cat), max(tremor_durations))
    p2 = min(min(valid_cat), min(tremor_durations))
    plt.plot([p1, p2], [p1, p2], alpha=0.5, color='black', linewidth=1, zorder=0)
    plt.xlabel('Tremor durations [days]')
    plt.ylabel('SSE durations [days] (ours)')
    # cbar = plt.colorbar()
    cbar = plt.colorbar(sm)
    # cbar.ax.set_ylabel('Amount of overlap (%)')
    cbar.ax.set_ylabel('Time lag between tremor and SSE onsets [days]')
    plt.legend()
    # plt.gca().set_aspect(abs(plt.xlim()[1] - plt.xlim()[0]) / abs(plt.ylim()[1] - plt.ylim()[0]))
    plt.gca().set_aspect('equal')
    plt.savefig('sse_figures/' + ('north_selection/' if north else '') + 'dur_tremors2.pdf')
    # plt.show()
    plt.close(fig)

    return valid_cat, tremor_durations

def plot_comparison_duration_proba_tremors_corr(cat, time_array, proba, n_tremors_per_day, rel_height=0.99, sigma=1.5, sigma_d=0, sigma_t=0, north=False, tremor_time_ref=None, base_fig_folder='./sse_figures/'):
    """Take duration on catalogue, extract the same time period on tremors with a certain variability,
    find a peak and find a duration over that peak on tremors. Finally compare the two."""
    if tremor_time_ref is not None:
        time_cut = np.logical_and(time_array >= tremor_time_ref[0], time_array <= tremor_time_ref[1])
        time_array = time_array[time_cut]
        proba = proba[time_cut]
        n_tremors_per_day = n_tremors_per_day[time_cut]
    n_tremors_per_day = gaussian_filter(n_tremors_per_day, sigma)
    tremor_durations = []
    valid_cat = []
    time_lags = []
    sse_duration_variability = sigma_d  # days
    tremor_duration_variability = sigma_t  # days
    tremor_search_offset_date = sse_duration_variability / 365
    tremor_extention_date = tremor_duration_variability / 365
    for time_span in cat:
        #print('time_span', time_span)
        time_idx = np.logical_and(time_array >= time_span[0] - tremor_search_offset_date,
                                  time_array <= time_span[1] + tremor_search_offset_date)
        #print('but searching in:', time_span[0] - tremor_search_offset_date, time_span[1] + tremor_search_offset_date)
        current_tremor_window = n_tremors_per_day[time_idx]
        peaks, _ = find_peaks(current_tremor_window)
        if len(peaks) > 0:
            #print(f'Found {len(peaks)} peak(s) at:', time[time_idx][peaks])
            #print(f'Searching for widths from {time[time_idx][0]} to {time[time_idx][-1]}')
            #print(f'Check peak still in: {time[time_idx][peaks]}')
            widths = peak_widths(current_tremor_window, peaks, rel_height=rel_height)
            # per ogni picco in widths, calcola il min a sx il max a dx e metti come durata
            start_time, end_time = time_array[time_idx][round(np.min(widths[2]))], time_array[time_idx][
                round(np.max(widths[3]))]
            #print('SSE starts:', time_span[0], 'tremors start:', start_time, 'and end:', end_time)

            # time_idx_tremors = np.logical_and(time_array >= start_time, time_array <= end_time)

            time_idx_tremors = np.logical_and(time_array >= start_time - tremor_extention_date,
                                              time_array <= end_time + tremor_extention_date)

            proba_corr_window = data_normalization(proba[time_idx])
            tremor_corr_window = data_normalization(n_tremors_per_day[time_idx_tremors])
            proba_window_size = proba_corr_window.size
            tremor_window_size = tremor_corr_window.size

            length = 1000  # length = np.maximum(proba_window_size, tremor_window_size)
            x_proba = np.linspace(0, proba_window_size, proba_window_size)
            x_trem = np.linspace(0, tremor_window_size, tremor_window_size)
            x_ups_proba = np.linspace(0, proba_window_size, length)
            x_ups_tremors = np.linspace(0, tremor_window_size, length)
            lin_proba = interp1d(x_proba, proba_corr_window)
            lin_tremors = interp1d(x_trem, tremor_corr_window)
            proba_corr_window_up = lin_proba(x_ups_proba)
            tremor_corr_window_up = lin_tremors(x_ups_tremors)

            corr_coeff = np.corrcoef(proba_corr_window_up, tremor_corr_window_up)[0, 1]
            #print(corr_coeff)

            correlation = correlate(tremor_corr_window, proba_corr_window)
            lags = correlation_lags(tremor_corr_window.size, proba_corr_window.size)
            lag = lags[np.argmax(correlation)]

            #lags, correlation = _lagged_correlation(proba_corr_window, tremor_corr_window, max_time_lag, upsampling_factor=1)
            #lag = lags[np.argmax(correlation)]

            '''plt.plot(lags, correlation)
            plt.show()'''

            if np.abs(corr_coeff) >= 0.2:
                tremor_durations.append((end_time - start_time) * 365)
                valid_cat.append((time_span[1] - time_span[0]) * 365)
                time_lags.append(lag)

            ######################################################
            tremor_visualization_offset_date = 30 / 365
            visualization_time_idx = np.logical_and(time_array >= time_span[0] - tremor_visualization_offset_date,
                                               time_array <= time_span[1] + tremor_visualization_offset_date)
            '''fig, axes = plt.subplots(3, 1)
            ax1, ax2, ax3 = axes
            ax1.plot(time_array[visualization_time_idx], proba[visualization_time_idx])
            ax1.axvline(time_span[0], linewidth=0.7, color='red')
            ax1.axvline(time_span[1], linewidth=0.7, color='red')
            ax1.axvline(time_span[0] - tremor_search_offset_date, linewidth=0.7, color='blue')
            ax1.axvline(time_span[1] + tremor_search_offset_date, linewidth=0.7, color='blue')
            ax2.plot(time_array[visualization_time_idx], n_tremors_per_day[visualization_time_idx])
            ax2.axvline(start_time, linewidth=0.7)
            ax2.axvline(end_time, linewidth=0.7)
            ax2.axvline(start_time - tremor_extention_date, linewidth=0.7, color='red')
            ax2.axvline(end_time + tremor_extention_date, linewidth=0.7, color='red')
            ax3.plot(lags, correlation)
            ax3.axvline(lag, linewidth=0.7, color='red')
            plt.suptitle(f'lag: {lag}, corr_coeff: {corr_coeff}')
            plt.show()'''

    cmap = plt.cm.coolwarm
    fig = plt.figure(dpi=100)
    # create normalization instance
    # norm = matplotlib.colors.Normalize(np.min(time_lags), np.max(time_lags))
    norm = matplotlib.colors.TwoSlopeNorm(vmin=np.min(time_lags), vcenter=0., vmax=np.max(time_lags))
    # create a scalarmappable from the colormap
    sm = matplotlib.cm.ScalarMappable(cmap=cmap, norm=norm)
    sm.set_array([])
    last_marker_square_idx, last_marker_circle_idx, last_marker_star_idx = 0, 0, 0
    for i in range(len(valid_cat)):
        if 2014. <= cat[i][0] < 2018.1:
            marker = 's'
            last_marker_square_idx = i
        # elif 2010. <= cat[i][0] < 2014.:
        elif 2007. <= cat[i][0] < 2014.:
            marker = 'o'
            last_marker_circle_idx = i
        else:
            marker = '*'
            last_marker_star_idx = i
        plt.scatter(tremor_durations[i], valid_cat[i], color=cmap(norm(time_lags[i])), marker=marker)

    #X, Y, Z = density_estimation(tremor_durations, valid_cat)
    #plt.contour(X, Y, Z)
    #seaborn.kdeplot(tremor_durations, valid_cat, fill=False, levels=5, thresh=.2)

    plt.scatter(tremor_durations[last_marker_circle_idx], valid_cat[last_marker_circle_idx], color=cmap(1.),
                # marker='o', label='2010-2013')
                marker='o', label='2007-2013')
    plt.scatter(tremor_durations[last_marker_square_idx], valid_cat[last_marker_square_idx], color=cmap(1.),
                marker='s', label='2014-2017')
    plt.scatter(tremor_durations[last_marker_star_idx], valid_cat[last_marker_star_idx], color=cmap(1.),
                marker='*', label='2018-2022')

    p1 = max(max(valid_cat), max(tremor_durations))
    p2 = min(min(valid_cat), min(tremor_durations))
    plt.plot([p1, p2], [p1, p2], alpha=0.5, color='black', linewidth=1, zorder=0)
    plt.xlabel('Tremor durations [days]')
    plt.ylabel('SSE durations [days] (ours)')
    # cbar = plt.colorbar()
    cbar = plt.colorbar(sm)
    # cbar.ax.set_ylabel('Amount of overlap (%)')
    cbar.ax.set_ylabel('Time lag between tremor and SSE pulses [days]')
    plt.legend()
    # plt.gca().set_aspect(abs(plt.xlim()[1] - plt.xlim()[0]) / abs(plt.ylim()[1] - plt.ylim()[0]))
    plt.gca().set_aspect('equal')
    plt.savefig(base_fig_folder + ('north_selection/' if north else '') + 'dur_tremors_corr.pdf')
    #plt.title(f'sigma_d={sigma_d} days, sigma_t={sigma_t} days')
    # plt.show()
    plt.close(fig)

    return valid_cat, tremor_durations

def plot_comparison_duration_proba_tremors_no_color(cat, time_array, proba, n_tremors_per_day, rel_height=0.99, sigma=1.5, sigma_d=0, sigma_t=0, north=False, tremor_time_ref=None, base_fig_folder='./sse_figures/'):
    """Take duration on catalogue, extract the same time period on tremors with a certain variability,
    find a peak and find a duration over that peak on tremors. Finally compare the two."""
    if tremor_time_ref is not None:
        time_cut = np.logical_and(time_array >= tremor_time_ref[0], time_array <= tremor_time_ref[1])
        proba = proba[time_cut]
        time_array = time_array[time_cut]
        n_tremors_per_day = n_tremors_per_day[time_cut]
    n_tremors_per_day = gaussian_filter(n_tremors_per_day, sigma)
    tremor_durations = []
    valid_cat = []
    time_lags = []
    events_no_tremor = []
    sse_duration_variability = sigma_d  # days
    tremor_duration_variability = sigma_t  # days
    tremor_search_offset_date = sse_duration_variability / 365
    tremor_extention_date = tremor_duration_variability / 365
    for time_span in cat:
        #print('time_span', time_span)
        time_idx = np.logical_and(time_array >= time_span[0] - tremor_search_offset_date,
                                  time_array <= time_span[1] + tremor_search_offset_date)
        #print('but searching in:', time_span[0] - tremor_search_offset_date, time_span[1] + tremor_search_offset_date)
        current_tremor_window = n_tremors_per_day[time_idx]
        peaks, _ = find_peaks(current_tremor_window)
        if len(peaks) > 0:
            widths = peak_widths(current_tremor_window, peaks, rel_height=rel_height)
            # per ogni picco in widths, calcola il min a sx il max a dx e metti come durata
            start_time, end_time = time_array[time_idx][round(np.min(widths[2]))], time_array[time_idx][
                round(np.max(widths[3]))]

            tremor_durations.append((end_time - start_time) * 365)
            valid_cat.append((time_span[1] - time_span[0]) * 365)
        else:
            # events not associated to a peak of tremor
            events_no_tremor.append(time_span)

    print('Number of SSEs having a corresponding peak of tremor:', len(valid_cat))
    print(f'{len(events_no_tremor)} events are not associated with tremors:')
    print(np.array(events_no_tremor))

    fig = plt.figure(dpi=100)
    plt.scatter(tremor_durations, valid_cat, color='black', s=15)

    p1 = max(max(valid_cat), max(tremor_durations))
    p2 = min(min(valid_cat), min(tremor_durations))
    plt.plot([p1, p2], [p1, p2], alpha=0.5, color='black', linewidth=1, zorder=0)
    plt.xlabel('Tremor durations [days]')
    plt.ylabel('SSE durations [days] (ours)')
    # plt.gca().set_aspect(abs(plt.xlim()[1] - plt.xlim()[0]) / abs(plt.ylim()[1] - plt.ylim()[0]))
    plt.gca().set_aspect('equal')
    plt.savefig(base_fig_folder + ('north_selection/' if north else '') + 'dur_tremors_no_color.pdf')
    #plt.title(f'sigma_d={sigma_d} days, sigma_t={sigma_t} days')
    # plt.show()
    plt.close(fig)

    return valid_cat, tremor_durations


def plot_comparison_duration_proba_tremors_no_color_mean_std(cat, time_array, proba, n_tremors_per_day, rel_height=0.99, sigma=1.5, sigma_d=0, sigma_t=0, north=False, tremor_time_ref=None, base_fig_folder='./sse_figures/'):
    """Take duration on catalogue, extract the same time period on tremors with a certain variability,
    find a peak and find a duration over that peak on tremors. Finally compare the two."""
    if tremor_time_ref is not None:
        time_cut = np.logical_and(time_array >= tremor_time_ref[0], time_array <= tremor_time_ref[1])
        proba = proba[time_cut]
        time_array = time_array[time_cut]
        n_tremors_per_day = n_tremors_per_day[time_cut]
    n_tremors_per_day = gaussian_filter(n_tremors_per_day, sigma)
    tremor_durations_mean, tremor_durations_std = [], []
    valid_cat_mean, valid_cat_std = [], []
    time_lags = []
    events_no_tremor = []
    #sse_duration_variability = sigma_d  # days
    #tremor_duration_variability = sigma_t  # days
    #tremor_search_offset_date = sse_duration_variability / 365
    #tremor_extention_date = tremor_duration_variability / 365
    sigma_d_list = np.linspace(0, 60, 60)  # [0, 3, 7, 10, 14]
    for time_span in cat:
        #print('time_span', time_span)
        partial_tremor_durations = []
        partial_valid_cat = []
        for sse_duration_variability in sigma_d_list:
            tremor_search_offset_date = sse_duration_variability / 365
            time_idx = np.logical_and(time_array >= time_span[0] - tremor_search_offset_date,
                                      time_array <= time_span[1] + tremor_search_offset_date)
            #print('but searching in:', time_span[0] - tremor_search_offset_date, time_span[1] + tremor_search_offset_date)
            current_tremor_window = n_tremors_per_day[time_idx]
            peaks, _ = find_peaks(current_tremor_window)
            if len(peaks) > 0:
                widths = peak_widths(current_tremor_window, peaks, rel_height=rel_height)
                # per ogni picco in widths, calcola il min a sx il max a dx e metti come durata
                start_time, end_time = time_array[time_idx][round(np.min(widths[2]))], time_array[time_idx][
                    round(np.max(widths[3]))]

                partial_tremor_durations.append((end_time - start_time) * 365)
                partial_valid_cat.append((time_span[1] - time_span[0]) * 365)
            else:
                # events not associated to a peak of tremor
                # events_no_tremor.append(time_span)
                pass
        valid_cat_mean.append(np.mean(partial_valid_cat))
        valid_cat_std.append(np.std(partial_valid_cat))
        tremor_durations_mean.append(np.mean(partial_tremor_durations))
        tremor_durations_std.append(np.std(partial_tremor_durations))

    #print('Number of SSEs having a corresponding peak of tremor:', len(valid_cat))
    #print(f'{len(events_no_tremor)} events are not associated with tremors:')
    #print(np.array(events_no_tremor))

    fig = plt.figure(dpi=100)
    # plt.scatter(tremor_durations, valid_cat, color='black', s=15)
    plt.errorbar(tremor_durations_mean, valid_cat_mean, yerr=valid_cat_std, xerr=tremor_durations_std,
                 color='black', fmt='o', capsize=2, markersize=5)

    p1 = max(max(valid_cat_mean), max(tremor_durations_mean))
    p2 = min(min(valid_cat_mean), min(tremor_durations_mean))
    plt.plot([p1, p2], [p1, p2], alpha=0.5, color='black', linewidth=1, zorder=0)
    plt.xlabel('Tremor durations [days]')
    plt.ylabel('SSE durations [days] (ours)')
    # plt.gca().set_aspect(abs(plt.xlim()[1] - plt.xlim()[0]) / abs(plt.ylim()[1] - plt.ylim()[0]))
    plt.gca().set_aspect('equal')
    plt.savefig(base_fig_folder + ('north_selection/' if north else '') + 'dur_tremors_no_color_mean_std.pdf')
    #plt.title(f'sigma_d={sigma_d} days, sigma_t={sigma_t} days')
    # plt.show()
    plt.close(fig)

    return valid_cat_mean, tremor_durations_mean



def plot_comparison_sse_tremor_local_corr(cat, time_array, proba, n_tremors_per_day, rel_height=0.99, sigma=1.5, sigma_d=0, sigma_t=0, north=False, tremor_time_ref=None, max_lag=15, base_fig_folder='./sse_figures/'):
    if tremor_time_ref is not None:
        time_cut = np.logical_and(time_array >= tremor_time_ref[0], time_array <= tremor_time_ref[1])
        proba = proba[time_cut]
        time_array = time_array[time_cut]
        n_tremors_per_day = n_tremors_per_day[time_cut]

    n_tremors_per_day = gaussian_filter(n_tremors_per_day, sigma)
    tremor_durations = []
    valid_cat = []
    time_lags = []
    corr_values = []
    events_no_tremor = []
    sse_duration_variability = sigma_d  # days
    tremor_search_offset_date = sse_duration_variability / 365
    for time_span in cat:
        time_idx = np.logical_and(time_array >= time_span[0] - tremor_search_offset_date,
                                  time_array <= time_span[1] + tremor_search_offset_date)
        if np.sum(time_idx) < (time_span[1] - time_span[0]) * 365 + sse_duration_variability:
            continue  # out of bounds
        current_tremor_window = n_tremors_per_day[time_idx]
        current_proba_window = proba[time_idx]

        proba_corr_window = data_normalization(current_proba_window)
        tremor_corr_window = data_normalization(current_tremor_window)
        proba_window_size = proba_corr_window.size
        tremor_window_size = tremor_corr_window.size

        '''correlation = correlate(tremor_corr_window, proba_corr_window)
        lags = correlation_lags(tremor_window_size, proba_window_size)
        lag = lags[np.argmax(correlation)]
        corr_values.append(np.max(correlation))
        time_lags.append(lag)
        valid_cat.append(time_span)'''
        # lags, corr_val = _lagged_correlation(proba_corr_window, tremor_corr_window, max_lag)
        corr_val = np.corrcoef(proba_corr_window, tremor_corr_window)[0, 1]
        #time_lags.append(lags[np.argmax(corr_val)])
        time_lags.append(0)
        valid_cat.append(time_span)
        corr_values.append(corr_val)

    sse_durations = ((np.array(valid_cat)[:, 1] - np.array(valid_cat)[:, 0]) * 365).tolist()

    '''fig = plt.figure(dpi=100)
    plt.scatter(time_lags, sse_durations, s=15, c=corr_values)
    p1 = max(max(sse_durations), max(time_lags))
    p2 = min(min(sse_durations), min(time_lags))
    # plt.plot([p1, p2], [p1, p2], alpha=0.5, color='black', linewidth=1, zorder=0)
    plt.xlabel('Time lag between SSEs and tremors [days]')
    plt.ylabel('SSE durations [days]')
    # plt.gca().set_aspect(abs(plt.xlim()[1] - plt.xlim()[0]) / abs(plt.ylim()[1] - plt.ylim()[0]))
    cbar = plt.colorbar()
    cbar.ax.set_ylabel('Correlation coefficient')
    plt.gca().set_aspect('equal')
    plt.savefig('sse_figures/' + ('north_selection/' if north else '') + 'dur_tremors_local_corr.pdf')
    #plt.title(f'sigma_d={sigma_d} days, sigma_t={sigma_t} days')
    # plt.show()
    plt.close(fig)'''

    '''fig = plt.figure(dpi=100)
    plt.scatter(sse_durations, corr_values, s=15, c=time_lags)
    p1 = max(max(sse_durations), max(time_lags))
    p2 = min(min(sse_durations), min(time_lags))
    # plt.plot([p1, p2], [p1, p2], alpha=0.5, color='black', linewidth=1, zorder=0)
    plt.xlabel('SSE durations [days]')
    plt.ylabel('Correlation coefficient')
    # plt.gca().set_aspect(abs(plt.xlim()[1] - plt.xlim()[0]) / abs(plt.ylim()[1] - plt.ylim()[0]))
    cbar = plt.colorbar()
    cbar.ax.set_ylabel('Time lag between SSE and tremors [days]')
    #plt.gca().set_aspect('equal')
    plt.savefig('sse_figures/' + ('north_selection/' if north else '') + 'dur_tremors_local_corr.pdf')
    # plt.title(f'sigma_d={sigma_d} days, sigma_t={sigma_t} days')
    # plt.show()
    plt.close(fig)'''
    '''cmap = plt.cm.coolwarm
    norm = matplotlib.colors.TwoSlopeNorm(vmin=np.min(time_lags), vcenter=0., vmax=np.max(time_lags))
    # create a scalarmappable from the colormap
    sm = matplotlib.cm.ScalarMappable(cmap=cmap, norm=norm)
    sm.set_array([])
    fig = plt.figure(dpi=100)
    for i, sse_dur in enumerate(sse_durations):
        markerline, stemlines, baseline = plt.stem(sse_dur, corr_values[i], use_line_collection=True,
                                                          markerfmt=" ", basefmt=" ")
        plt.setp(stemlines, 'linewidth', 3.)
        plt.setp(stemlines, 'color', cmap(norm(time_lags[i])))

    cbar = plt.colorbar(sm)
    cbar.ax.set_ylabel('Time lag between SSE and tremors [days]')
    plt.savefig('sse_figures/' + ('north_selection/' if north else '') + 'dur_tremors_local_corr.pdf')
    plt.close(fig)'''
    cmap = plt.cm.coolwarm
    norm = matplotlib.colors.Normalize(vmin=np.min(corr_values),  vmax=np.max(corr_values))
    # create a scalarmappable from the colormap
    sm = matplotlib.cm.ScalarMappable(cmap=cmap, norm=norm)
    sm.set_array([])
    fig = plt.figure(dpi=100)
    '''for i, sse_dur in enumerate(sse_durations):
        markerline, stemlines, baseline = plt.stem(sse_dur, time_lags[i], use_line_collection=True,
                                                   markerfmt=" ", basefmt=" ")
        plt.setp(stemlines, 'linewidth', 3.)
        plt.setp(stemlines, 'color', cmap(norm(corr_values[i])))'''
    plt.scatter(sse_durations, corr_values, s=15)
    plt.axhline(y=0)
    plt.xlabel('SSE duration [days]')
    plt.ylabel('Correlation coefficient')
    #cbar = plt.colorbar(sm)
    #cbar.ax.set_ylabel('Correlation coefficient')
    plt.savefig(base_fig_folder + ('north_selection/' if north else '') + 'dur_tremors_local_corr.pdf')
    plt.close(fig)

    return valid_cat, tremor_durations


def plot_comparison_sse_tremor_local_corr2(cat, time_array, proba, n_tremors_per_day, rel_height=0.99, min_corr_coeff_thresh=0.4, sigma=1.5, sigma_d=0, sigma_t=0, sigma_d_cut=0, north=False, south=False, tremor_time_ref=None, max_lag=15, base_fig_folder='./sse_figures/'):
    if tremor_time_ref is not None:
        time_cut = np.logical_and(time_array >= tremor_time_ref[0], time_array <= tremor_time_ref[1])
        proba = proba[time_cut]
        time_array = time_array[time_cut]
        n_tremors_per_day = n_tremors_per_day[time_cut]

    n_tremors_per_day = gaussian_filter(n_tremors_per_day, sigma)
    tremor_durations = []
    valid_cat, not_significant_cat = [], []
    time_lags, not_significant_time_lags = [], []
    corr_values, not_significant_corr_val = [], []
    valid_cat_dur = []
    events_no_tremor = []
    sse_duration_variability = sigma_d  # days
    sse_duration_variability_cut = sigma_d_cut
    tremor_search_offset_date = sse_duration_variability / 365
    tremor_search_offset_date_cut = sse_duration_variability_cut / 365
    for time_span in cat:
        time_idx = np.logical_and(time_array >= time_span[0] - tremor_search_offset_date,
                                  time_array <= time_span[1] + tremor_search_offset_date)
        if np.sum(time_idx) < (time_span[1] - time_span[0]) * 365 + sse_duration_variability:
            continue  # out of bounds
        current_tremor_window = n_tremors_per_day[time_idx]
        current_proba_window = proba[time_idx]

        proba_corr_window = data_normalization(current_proba_window)
        tremor_corr_window = data_normalization(current_tremor_window)
        proba_window_size = proba_corr_window.size
        tremor_window_size = tremor_corr_window.size

        lags, corr_val = _lagged_correlation(proba_corr_window, tremor_corr_window, max_lag)
        #corr_coeff = np.corrcoef(proba_corr_window, tremor_corr_window)[0, 1]
        #time_lags.append(lags[np.argmax(corr_val)])
        #print(corr_coeff, min_corr_coeff_thresh, corr_coeff > min_corr_coeff_thresh)
        if corr_val[np.where(lags == 0)[0]] > min_corr_coeff_thresh:  # if corr_coeff > min_corr_coeff_thresh:
            #print('argmax:', np.argmax(corr_val), 'corr_coeff was:', corr_coeff)
            #print(np.max(corr_val), corr_val[np.argmax(corr_val)])
            '''if np.max(corr_val) == -0.03964284376934453:
                plt.plot(lags, corr_val)
                plt.show()'''
            time_lags.append(lags[np.argmax(corr_val)])
            valid_cat.append(time_span)
            corr_values.append(np.max(corr_val))

            time_idx_cut = np.logical_and(time_array >= time_span[0] - tremor_search_offset_date_cut,
                                      time_array <= time_span[1] + tremor_search_offset_date_cut)
            cut_tremor_window = n_tremors_per_day[time_idx_cut]
            peaks, _ = find_peaks(cut_tremor_window)
            if len(peaks) > 0:
                widths = peak_widths(cut_tremor_window, peaks, rel_height=rel_height)
                # per ogni picco in widths, calcola il min a sx il max a dx e metti come durata
                start_time, end_time = time_array[time_idx_cut][round(np.min(widths[2]))], time_array[time_idx_cut][
                    round(np.max(widths[3]))]

                tremor_durations.append((end_time - start_time) * 365)
                valid_cat_dur.append((time_span[1] - time_span[0]) * 365)
            else:
                # events not associated to a peak of tremor
                events_no_tremor.append(time_span)

        else:
            not_significant_cat.append(time_span)
            not_significant_corr_val.append(np.max(corr_val))
            not_significant_time_lags.append(lags[np.argmax(corr_val)])

    sse_durations = ((np.array(valid_cat)[:, 1] - np.array(valid_cat)[:, 0]) * 365).tolist()
    not_significant_sse_durations = ((np.array(not_significant_cat)[:, 1] - np.array(not_significant_cat)[:, 0]) * 365).tolist()

    print('Number of not significant SSEs:', len(not_significant_time_lags))

    '''fig, axes = plt.subplots(1, 2)
    ax1,ax2=axes
    ax1.hist(time_lags, bins=15)
    ax2.hist(corr_values, bins=15)
    ax1.set_title('time lag histogram')
    ax2.set_title('correlation histogram')
    plt.show()'''

    cmap = plt.cm.coolwarm
    norm = matplotlib.colors.Normalize(vmin=np.min(time_lags),  vmax=np.max(time_lags))
    # create a scalarmappable from the colormap
    sm = matplotlib.cm.ScalarMappable(cmap=cmap, norm=norm)
    sm.set_array([])
    fig = plt.figure(dpi=100)
    plt.scatter(sse_durations, corr_values, s=15, c=cmap(norm(time_lags)))
    plt.scatter(not_significant_sse_durations, not_significant_corr_val, s=15, facecolors='none',
                alpha=0.4, edgecolors=cmap(norm(not_significant_time_lags)))
    #plt.axhline(y=0)
    plt.xlabel('SSE duration [days]')
    # plt.ylabel('Maximum value of correlation coefficient')
    plt.ylabel('Maximum cross-correlation')
    cbar = plt.colorbar(sm)
    cbar.ax.set_ylabel('Local-scale tremor-SSE time lag [days]')
    plt.savefig(base_fig_folder + ('north_selection/' if north else '') + ('south_selection/' if south else '') + 'dur_tremors_local_corr.pdf')
    plt.close(fig)


    fig = plt.figure(dpi=100)
    plt.hist(time_lags, bins=14)
    # plt.axhline(y=0)
    # plt.xlabel('Time lag between tremors and SSE probability [days]')
    plt.xlabel('Local-scale tremor-SSE time lag [days]')
    plt.ylabel('Number of occurrences')
    plt.savefig(base_fig_folder + ('north_selection/' if north else '') + ('south_selection/' if south else '') + 'dur_tremors_local_corr_hist.pdf')
    plt.close(fig)


    '''fig = plt.figure(dpi=100)
    plt.scatter(sse_durations, time_lags, s=15, c=corr_values, cmap='Reds')
    plt.axhline(y=0)
    plt.xlabel('SSE duration [days]')
    plt.ylabel('Time lag [days]')
    cbar = plt.colorbar()
    cbar.ax.set_ylabel('Correlation coefficient')
    plt.savefig(base_fig_folder + ('north_selection/' if north else '') + 'dur_tremors_local_corr2.pdf')
    plt.close(fig)'''


    print('Number of SSEs having a corresponding peak of tremor:', len(valid_cat_dur))
    print(f'{len(events_no_tremor)} events are not associated with tremors:')
    print(np.array(events_no_tremor))

    fig = plt.figure(dpi=100)
    plt.scatter(tremor_durations, valid_cat_dur, color='black', s=15)
    #plt.scatter(tremor_durations, valid_cat_dur, c=corr_values, s=15, cmap='coolwarm')
    #plt.colorbar()

    p1 = max(max(valid_cat_dur), max(tremor_durations))
    p2 = min(min(valid_cat_dur), min(tremor_durations))

    plt.plot([p1, p2], [p1, p2], alpha=0.5, color='black', linewidth=1, zorder=0)

    # plt.plot([p1 - 14, p2 - 14], [p1, p2], alpha=0.3, color='black', linewidth=1, zorder=0)
    ####plt.plot([p1 - 14, p2], [p1, -(p2 - 14)], alpha=0.3, color='black', linewidth=1, zorder=0)

    # plt.plot([p1 + 14, p2 + 14], [p1, p2], alpha=0.3, color='black', linewidth=1, zorder=0)
    plt.plot([p2 + 14, p1], [p2, p1 - 14], alpha=0.3, color='black', linewidth=1, linestyle='--', zorder=0)


    #plt.scatter(np.array(tremor_durations), (np.array(valid_cat_dur) + 7))
    #plt.scatter(np.array(tremor_durations), (np.array(valid_cat_dur) - 7))
    #plt.scatter((np.array(valid_cat_dur) - 7), (np.array(valid_cat_dur) - 7) * np.sqrt(2) / 2)

    plt.xlabel('Tremor durations [days]')
    plt.ylabel('SSE durations [days]')
    # plt.gca().set_aspect(abs(plt.xlim()[1] - plt.xlim()[0]) / abs(plt.ylim()[1] - plt.ylim()[0]))
    plt.gca().set_aspect('equal')
    plt.savefig(base_fig_folder + ('north_selection/' if north else '') + ('south_selection/' if south else '') + 'dur_tremors_no_color_revised.pdf')
    #plt.title(f'sigma_d={sigma_d} days, sigma_t={sigma_t} days')
    # plt.show()
    plt.close(fig)


    return valid_cat, tremor_durations



def plot_scaling_law_duration_magnitude(cat_ours, north=False, base_fig_folder='./sse_figures/'):
    """We plot the Gutenberg-Richter scaling law for our catalog, inferred from both the linear and cubic
    time-moment scaling laws."""

    duration_ours = (cat_ours[:, 1] - cat_ours[:, 0]) * 365 * 86400  # conversion in seconds
    q_lin = -12  # Gao et al., 2012
    m_cub = 1 / 3
    q_cub = -0.5
    m_lin = 1
    log_moments_linear = (np.log10(duration_ours) - q_lin) / m_lin
    log_moments_cubic = (np.log10(duration_ours) - q_cub) / m_cub

    '''plt.scatter(log_moments_linear, np.log10(duration_ours), s=18, color='black', label='linear')
    plt.scatter(log_moments_cubic, np.log10(duration_ours), s=18, color='red', label='cubic')
    plt.ylabel('log$_{10}$[n. detections]')
    plt.xlabel('Inferred moment')
    plt.legend()
    plt.show()'''

    # check if the histogram of the detections as a function of Mw follows a G-R law

    mw_obtained_lin = (log_moments_linear - 9.1) / 1.5
    mw_obtained_cub = (log_moments_cubic - 9.1) / 1.5

    n_detections_lin, mw_bins_lin = np.histogram(mw_obtained_lin, bins=20)
    n_detections_cub, mw_bins_cub = np.histogram(mw_obtained_cub, bins=20)
    mw_bins_lin = mw_bins_lin[:-1]
    mw_bins_cub = mw_bins_cub[:-1]
    log_n_detections_lin = np.log10(n_detections_lin)
    log_n_detections_cub = np.log10(n_detections_cub)
    # idx_mw_valid = np.where(mw_bins > 5)[0]
    idx_mw_valid = np.where(mw_bins_lin > 5.8)[0]
    b1, a1 = np.polyfit(mw_bins_lin[idx_mw_valid], log_n_detections_lin[idx_mw_valid], 1)
    #b2, a2 = np.polyfit(mw_bins[idx_mw_valid], log_n_detections_cub[idx_mw_valid], 1)


    fig = plt.figure(dpi=100)
    plt.scatter(mw_bins_lin, log_n_detections_lin, s=18, color='black')
    plt.plot([mw_bins_lin[idx_mw_valid][0], mw_bins_lin[idx_mw_valid][-1]],
             [mw_bins_lin[idx_mw_valid][0] * b1 + a1, mw_bins_lin[idx_mw_valid][-1] * b1 + a1], alpha=0.7, color='black',
             linewidth=1, zorder=0, label=f'b-value $={-b1:.2}$')
    #plt.scatter(mw_bins_cub, log_n_detections_cub, s=18, color='red', label='cubic')
    plt.ylabel('log$_{10}$[n. detections]')
    plt.xlabel('$M_w$ proxy')
    # plt.title(f'b-value: {np.abs(b):.2}')
    #plt.gca().set_aspect('equal', 'box')
    # plt.axis('equal')
    plt.legend()
    plt.savefig(base_fig_folder + ('north_selection/' if north else '') + 'gr_2.pdf')
    # plt.show()
    plt.close(fig)

def plot_histogram_durations(catalog_ours, catalog_michel, bins=50, north=False, south=False, base_fig_folder='./sse_figures/'):

    duration = (catalog_ours[:, 1] - catalog_ours[:, 0]) * 365
    #cat_retrieved = np.loadtxt('geo_data/catalogue_sse_costantino_michel_retrieved_soft.txt')
    #idx_same_michel = np.where(np.logical_and(catalog_ours[:, 0] > 2007, catalog_ours[:, 1] < 2018))[0]
    print(f'{len(duration)} SSEs in the catalog')
    print(f'min duration: {np.min(duration)}days, max duration: {np.max(duration)}days')
    idx_short = duration < 2
    #print(catalog[idx_short])
    table = {v: k for k, v in ymd_decimal_year_lookup().items()}
    #print([table[date] for date, _ in catalog[idx_short]])
    #print(duration[idx_short])
    fig = plt.figure(dpi=100)
    _, x, _ = plt.hist(duration, bins=bins, density=False)
    #kde = gaussian_kde(duration)
    #plt.plot(x, kde.pdf(x))
    plt.xlabel('SSE duration [days]')
    plt.ylabel('Count')
    plt.savefig(base_fig_folder + ('north_selection/' if north else '') + ('south_selection/' if south else '') + 'dur_hist.pdf')
    plt.close(fig)


def plot_histogram_durations_stacked(catalog_ours, catalog_michel, overlap_thresh, bins=50, north=False, south=False, base_fig_folder='./sse_figures/'):
    # histogram with : (1) common events in the michel's catalog, (2) other events in the same period of michel
    # (3) other events outside the michel's catalog

    common_michel_catalogue, other_michel_period, other_outside_michel_period = set(), set(), set()  # already contain durations
    max_date_michel = np.max(catalog_michel[:, 1])
    max_date_ours = np.max(catalog_ours[:, 1])

    '''for i, event in enumerate(catalog_ours):
        for j in range(catalog_michel.shape[0]):
            event_tuple = (event[0], event[1])
            overlap_percent = overlap_percentage(event, catalog_michel[j].tolist())[0]
            if overlap_percent >= overlap_thresh:
                common_michel_catalogue.add(event_tuple)
            else:
                if event[0] > max_date_michel:
                    other_outside_michel_period.add(event_tuple)
                else:
                    if event_tuple not in common_michel_catalogue:
                        #print(f'{event_tuple} not in', common_michel_catalogue)
                        other_michel_period.add(event_tuple)'''

    for i, event in enumerate(catalog_ours):
        for j in range(catalog_michel.shape[0]):
            event_tuple = (event[0], event[1])
            overlap_percent = overlap_percentage(event, catalog_michel[j].tolist())[0]
            if overlap_percent >= overlap_thresh:
                common_michel_catalogue.add(event_tuple)

    for i, event in enumerate(catalog_ours):
        event_tuple = (event[0], event[1])
        if event_tuple not in common_michel_catalogue:
            if event[0] > max_date_michel:
                other_outside_michel_period.add(event_tuple)
            else:
                if event_tuple not in common_michel_catalogue:
                    other_michel_period.add(event_tuple)


    print('# common events:', len(common_michel_catalogue)) # 34 (they are 35, but the first one is cut because of border effects)
    print('# events in the Michel et al. period:', len(other_michel_period))
    print('# events in 2017-2022:', len(other_outside_michel_period))

    common_michel_catalogue = np.array(list(common_michel_catalogue))
    other_michel_period = np.array(list(other_michel_period))
    other_outside_michel_period = np.array(list(other_outside_michel_period))

    common_michel_catalogue_dur = (common_michel_catalogue[:, 1] - common_michel_catalogue[:, 0]) * 365
    other_michel_period_dur = (other_michel_period[:, 1] - other_michel_period[:, 0]) * 365
    other_outside_michel_period_dur = (other_outside_michel_period[:, 1] - other_outside_michel_period[:, 0]) * 365

    duration_stack = [list(common_michel_catalogue_dur), list(other_michel_period_dur), list(other_outside_michel_period_dur)]
    fig = plt.figure(dpi=100)
    _, x, _ = plt.hist(duration_stack, bins=bins, stacked=True)
    plt.xlabel('SSE duration [days]')
    plt.ylabel('Number of detected events')
    plt.legend(['Common events', 'New events in the Michel et al. period', f'New events in the period {int(max_date_michel)}-{int(max_date_ours)}'])
    plt.savefig(base_fig_folder + ('north_selection/' if north else '') + ('south_selection/' if south else '') + 'dur_hist_stacked.pdf')
    plt.close(fig)


def comparison_moment_rate_known_sses(time_array, probability, tremors):
    # start, end = 2013.6537, 2013.7823  # Nocquet - Bletery
    # start, end = 2017.2019, 2017.2868  # Itoh (high-rate)
    # start, end = 2015.9, 2016.2  # Michel # 54
    # start, end = 2017.1, 2017.3  # Michel # 59 -> 2017.1239,2017.279
    # start, end = 2014.4, 2014.55  # Michel # 45 & 46 -> 2014.438,2014.4928
    dates = [(2013.6537, 2013.81), (2017.17, 2017.3), (2015.9, 2016.2),
             (2017.1, 2017.3), (2014.4, 2014.55)]
    ref_papers = ['Noquet-Bletery', 'Itoh', 'Michel #54', 'Michel #59', 'Michel #45&46']

    our_cat_sse = [(2013.6975, 2013.8015), (2017.1855, 2017.2923), (2015.9097, 2016.1177),
                   (2017.1444, 2017.2923), (2014.4394, 2014.4887)]

    smoothed_tremors = gaussian_filter(tremors, 2.5)
    sigma_d = 3
    tremor_search_offset_date = sigma_d / 365
    tremor_extention_date = 0
    for i, (start, end) in enumerate(dates):
        fig, axes = plt.subplots(2, 1, figsize=(9,6))
        ax_proba, ax_tremors = axes
        time_idx = np.logical_and(time_array > start, time_array < end)


        #############
        # print('time_span', time_span)
        win_idx = np.logical_and(time_array >= our_cat_sse[i][0] - tremor_search_offset_date,
                                  time_array <= our_cat_sse[i][1] + tremor_search_offset_date)
        # print('but searching in:', time_span[0] - tremor_search_offset_date, time_span[1] + tremor_search_offset_date)
        current_tremor_window = tremors[win_idx]
        peaks, _ = find_peaks(current_tremor_window)
        if len(peaks) > 0:
            # print(f'Found {len(peaks)} peak(s) at:', time[time_idx][peaks])
            # print(f'Searching for widths from {time[time_idx][0]} to {time[time_idx][-1]}')
            # print(f'Check peak still in: {time[time_idx][peaks]}')
            widths = peak_widths(current_tremor_window, peaks, rel_height=0.7)
            # per ogni picco in widths, calcola il min a sx il max a dx e metti come durata
            start_time, end_time = time_array[win_idx][round(np.min(widths[2]))], time_array[win_idx][
                round(np.max(widths[3]))]
            # print('SSE starts:', time_span[0], 'tremors start:', start_time, 'and end:', end_time)

            # time_idx_tremors = np.logical_and(time_array >= start_time, time_array <= end_time)

            time_idx_tremors = np.logical_and(time_array >= start_time - tremor_extention_date,
                                              time_array <= end_time + tremor_extention_date)

            proba_corr_window = data_normalization(proba[win_idx])
            tremor_corr_window = data_normalization(n_tremors_per_day[time_idx_tremors])

            correlation = correlate(tremor_corr_window, proba_corr_window)
            lags = correlation_lags(tremor_corr_window.size, proba_corr_window.size)
            lag = lags[np.argmax(correlation)]

        #############


        ax_proba.plot(time_array[time_idx], probability[time_idx])
        ax_proba.set_ylabel('Confidence of detection')
        ax_proba.axvline(our_cat_sse[i][0] - tremor_search_offset_date)
        ax_proba.axvline(our_cat_sse[i][1] + tremor_search_offset_date)

        ax_proba.axvline(our_cat_sse[i][0], color='red')
        ax_proba.axvline(our_cat_sse[i][1], color='red')

        markerline, stemlines, baseline = ax_tremors.stem(time_array[time_idx], tremors[time_idx], use_line_collection=True,
                                                          markerfmt=" ", basefmt=" ")

        ax_tremors.set_ylabel('Number of tremor events per day')
        ax_tremors.plot(time_array[time_idx], smoothed_tremors[time_idx], linewidth=1.)

        ax_proba.ticklabel_format(useOffset=False)
        ax_tremors.ticklabel_format(useOffset=False)
        plt.suptitle(f'event: {ref_papers[i]}, lag tremor-sse: {lag} days')
        plt.show()


def plot_deformation_field(station_coords, x1, y1, tremors=None, x2=None, y2=None, model=None, draw_dislocation=False, set_title=False, draw_tremors=False, **best_params):
    plt.figure()
    cascadia_box = [40 - 2, 51.8 - 2, -128.3 - 2, -121 + 2]  # min/max_latitude, min/max_longitude
    cascadia_map = Basemap(llcrnrlon=cascadia_box[2], llcrnrlat=cascadia_box[0],
                           urcrnrlon=cascadia_box[3], urcrnrlat=cascadia_box[1],
                           lat_0=cascadia_box[0], lon_0=cascadia_box[2],
                           resolution='i', projection='tmerc')
    cascadia_map.drawcoastlines()
    cascadia_map.drawparallels(np.arange(cascadia_box[0], cascadia_box[1], (cascadia_box[1] - cascadia_box[0]) / 4),
                               labels=[0, 1, 0, 0])
    cascadia_map.drawmeridians(np.arange(cascadia_box[2], cascadia_box[3], (cascadia_box[3] - cascadia_box[2]) / 4),
                               labels=[0, 0, 0, 1])

    N = 1  # N = np.sqrt(x1 ** 2 + y1 ** 2)  # normalize data

    if x2 is not None and y2 is not None:
        x1, x2, y1, y2 = x1 / N, x2 / N, y1 / N, y2 / N

    x1_rot, y1_rot, x, y = cascadia_map.rotate_vector(x1, y1, station_coords[:, 1], station_coords[:, 0], returnxy=True)
    cascadia_map.quiver(x, y, x1_rot, y1_rot, color='black', latlon=False, label='data')

    if x2 is not None and y2 is not None:
        x1, x2, y1, y2 = x1 / N, x2 / N, y1 / N, y2 / N
        x2_rot, y2_rot = cascadia_map.rotate_vector(x2, y2, station_coords[:, 1], station_coords[:, 0], returnxy=False)
        cascadia_map.quiver(x, y, x2_rot, y2_rot, color='red', latlon=False, label='model')  # scale=2, width=0.007
        plt.legend()
    # cascadia_map.scatter(x, y, c=opt_displacement[:, 2], s=50, cmap='jet')
    if draw_dislocation:
        strike_best, dip_best = best_params['strike'], best_params['dip']
        x_center = model.x[0]  # lon
        y_center = model.x[1]  # lat
        strike = np.deg2rad(strike_best)
        dip = np.deg2rad(dip_best)
        rake = np.deg2rad(model.x[5])  # np.deg2rad(rake)
        slip = model.x[4]
        L = model.x[2] / 111.3194
        W = model.x[3] / 111.3194
        alpha = np.pi / 2 - strike
        x_fault = L / 2 * np.cos(alpha) * np.array([-1., 1., 1., -1.]) + np.sin(alpha) * np.cos(dip) * W / 2 * np.array(
            [-1., -1., 1., 1.])
        y_fault = L / 2 * np.sin(alpha) * np.array([-1., 1., 1., -1.]) + np.cos(alpha) * np.cos(dip) * W / 2 * np.array(
            [1., 1., -1., -1.])
        x_fault += x_center
        y_fault += y_center
        x_fault_map, y_fault_map = cascadia_map(x_fault, y_fault)
        # x_fault_map, y_fault_map = x_fault, y_fault
        plt.gca().add_patch(matplotlib.patches.Polygon(np.vstack((x_fault_map, y_fault_map)).T,
                                                       closed=True, fill=True, color='black', lw=1.,
                                                       alpha=.6))

        '''for i in range(len(x_fault_map)):
            idx_la = np.where(np.abs(admissible_xyz[:, 1] - y_fault[i]) < 0.1)[0]
            idx_po = np.where(np.abs(admissible_xyz[idx_la, 0] - x_fault[i]) < 0.1)[0]
            d = -admissible_xyz[idx_la][idx_po][:, 2][0]
            plt.gca().text(x_fault_map[i], y_fault_map[i],
                           f'{np.around(x_fault[i], 2)} {np.around(y_fault[i], 2)} {np.around(d, 2)}')'''
    if draw_tremors:
        x_trem, y_trem = cascadia_map(tremors[:, 1], tremors[:, 0])
        # cascadia_map.scatter(x_trem, y_trem, color='red', alpha=0.5, s=10, zorder=-1)
        db = DBSCAN(eps=15 / 111.3194, min_samples=15, n_jobs=-1).fit(tremors[:, :2])
        cascadia_map.scatter(x_trem, y_trem, c=db.labels_, alpha=0.5, s=10, zorder=-1)
        print(np.unique(db.labels_))
    if set_title:
        depth_best = best_params['depth']
        mu = 30e09
        # Mo = mu * model.x[3] * 1e03 * model.x[4] * 1e03 * model.x[5] * 1e-03
        # Mo = mu * model.x[0] * 1e03 * model.x[1] * 1e03 * model.x[2] * 1e-03
        Mo = mu * model.x[2] * 1e03 * model.x[3] * 1e03 * model.x[4] * 1e-03
        # Mo = mu * L * 1e03 * W * 1e03 * slip * 1e-03
        print('Mo', Mo)
        Mw = (np.log10(Mo) - 9.1) / 1.5
        print('Mw', Mw)
        plt.title(f'lat: {model.x[1]}, lon: {model.x[0]}, depth: {depth_best}, Mw: {Mw}')
    #plt.colorbar()
    plt.show()


def plot_inversion_gps_param(draw_dislocation=True):
    def residuals_on_slab(x, tol_pos=0.1):
        # [lon0,lat0, L, W, slip, rake]
        # [-125, 48, 250, 45, 5, 75], [-123, 49.5, 450, 100, 50, 105]

        idx_lat = np.where(np.abs(admissible_xyz[:, 1] - x[1]) < tol_pos)[0]
        idx_pos = np.where(np.abs(admissible_xyz[idx_lat, 0] - x[0]) < tol_pos)[0]
        #print('idx_pos->', idx_pos)
        if len(idx_pos) == 0:
            return 1e10
        depth = -admissible_xyz[idx_lat][idx_pos][:, 2][0]
        #print(depth)
        strike, dip = admissible_strike[idx_lat][idx_pos][:, 2][0], admissible_dip[idx_lat][idx_pos][:, 2][0]
        #print(x)

        displacement = forward(x[0], x[1], depth, x[2], x[3], x[4], strike, dip, x[5])
        displacement = displacement[:, :2]
        y = np.sum(static_disp[:, :2] - displacement, axis=1)
        #y = np.sum(static_disp[:, :2] - displacement)
        return y

    def forward(xx, yy, d, length, width, u, az, dp, rk):
        disp = okada85((station_coords[:, 1] - xx) * 111.3194,
                               (station_coords[:, 0] - yy) * 111.3194, 0, 0,
                               depth=d, length=length, width=width, slip=u, opening=0, strike=az, dip=dp,
                               rake=rk)
        disp = np.array(disp).T
        return disp

    def get_spatial_cut(lat_min, lat_max):
        idx = np.where(np.logical_and(station_coords[:, 0] > lat_min, station_coords[:, 0] < lat_max))[0]
        return idx

    # ev 34 from Michel : 34,2012.609,2012.6684,2012.791,2012.7926, 6.54
    # start, end = 2012.6684, 2012.791
    start, end = 2017.2019, 2017.2868  # Yuji
    #start, end = 2015.974, 2016.168  # Michel # 54
    # start, end = 2017.889100, 2018.067100 # Costantino et al. ?
    #start, end = 2019.206000, 2019.384000 # Costantino et al., may be
    events = [(2017.2019, 2017.2868), (2015.974, 2016.168), (2019.206000, 2019.384000), (2017.889100, 2018.067100),
              (2012.6684, 2012.791)]

    reference_period = (2007, 2023)
    n_selected_stations = 135

    selected_gnss_data, selected_time_array, _, _ = _preliminary_operations(reference_period=reference_period, n_directions=3)
    _, station_coordinates, _, _,  station_subset = cascadia_filtered_stations(n_selected_stations, reference_period=(2007, 2014))

    cut_lats = [(47, 60), (43.9, 60), (43, 48), (43.9, 47), (47, 60)]
    initial_guesses = [[-124., 49, 300, 50, 30, 90], [-124., 49, 300, 50, 30, 90],
                       [-124., 49, 300, 50, 30, 90], [-124., 45.5, 300, 50, 30, 90],
                       [-124.5, 48.5, 300, 50, 30, 90]]  # [lon_0, lat_0, L_0, W_0, slip_0, rake_0]
    bounds = [([-125, 48, 250, 45, 5, 75], [-123, 49.5, 450, 100, 50, 105]),
              ([-125, 46, 250, 45, 5, 75], [-122, 49.5, 650, 100, 50, 105]),
              ([-125, 46, 250, 45, 5, 75], [-122, 49.5, 650, 100, 50, 105]),
              ([-125, 43, 250, 45, 5, 75], [-122, 49.5, 650, 100, 50, 105]),
              ([-125, 47, 250, 45, 5, 75], [-122, 49.5, 650, 100, 50, 105])]

    for i, event in enumerate(events):
        if i < 4:
            continue
        start, end = event
        station_coords = station_coordinates.copy()
        start_idx, end_idx = _find_nearest_val(selected_time_array, start)[1], _find_nearest_val(selected_time_array, end)[1]

        #static_disp = selected_gnss_data[station_subset, end_idx, :] - selected_gnss_data[station_subset, start_idx, :]

        static_disp = np.median(selected_gnss_data[station_subset, end_idx:end_idx+7, :], axis=1) - np.median(selected_gnss_data[station_subset, start_idx-7:start_idx, :], axis=1)
        print(selected_gnss_data.shape)
        print(static_disp.shape)
        plot_deformation_field(station_coords, static_disp[:, 0], static_disp[:, 1])

        #static_disp = static_disp[:, (1, 0, 2)]

        # spatial_cut_idx = np.where(station_coords[:, 0] > 47)[0]
        spatial_cut_idx = get_spatial_cut(cut_lats[i][0], cut_lats[i][1])
        station_coords = station_coords[spatial_cut_idx]
        static_disp = static_disp[spatial_cut_idx]


        avail_stations = ~np.isnan(static_disp[:, 0])
        static_disp = static_disp[avail_stations]
        station_coords = station_coords[avail_stations]


        admissible_xyz, admissible_strike, admissible_dip, _ = read_from_slab2()


        '''model = least_squares(residuals_on_slab, [lon_0, lat_0, L_0, W_0, slip_0, rake_0],
                              bounds=([-125, 48, 250, 45, 5, 75], [-123, 49.5, 450, 100, 50, 105]),
                              loss='linear', verbose=2)'''

        model = least_squares(residuals_on_slab, initial_guesses[i],
                              bounds=bounds[i],
                              loss='linear', verbose=2)

        idx_lat_best = np.where(np.abs(admissible_xyz[:, 1] - model.x[1]) < 0.1)[0]
        idx_pos_best = np.where(np.abs(admissible_xyz[idx_lat_best, 0] - model.x[0]) < 0.1)[0]
        depth_best = -admissible_xyz[idx_lat_best][idx_pos_best][:, 2][0]
        strike_best, dip_best = admissible_strike[idx_lat_best][idx_pos_best][:, 2][0], admissible_dip[idx_lat_best][idx_pos_best][:, 2][0]
        # xx, yy, d, length, width, u, az, dp, rk
        opt_displacement = forward(model.x[0], model.x[1], depth_best, model.x[2], model.x[3], model.x[4], strike_best, dip_best, model.x[5])

        print('Best model.x ->', model.x)
        print('depth, strike, dip', depth_best, strike_best, dip_best)


        #plt.plot(opt_displacement);plt.show()

        # opt_displacement = opt_displacement[:, :2]

        print('données', np.max(np.abs(static_disp[:, 0])))
        print('modèle', np.max(np.abs(opt_displacement[:, 0])))

        plot_deformation_field(station_coords, static_disp[:, 0], static_disp[:, 1],
                               opt_displacement[:, 0], opt_displacement[:, 1], model,
                               draw_dislocation=True, set_title=True,
                               **{'strike': strike_best, 'dip': dip_best, 'depth': depth_best})

def plot_inversion_gps_param2(tremors, draw_dislocation=True):
    def residuals_on_slab(x, tol_pos=0.1):
        # [lon0,lat0, L, W, slip, rake]
        # [-125, 48, 250, 45, 5, 75], [-123, 49.5, 450, 100, 50, 105]

        idx_lat = np.where(np.abs(admissible_xyz[:, 1] - x[1]) < tol_pos)[0]
        idx_pos = np.where(np.abs(admissible_xyz[idx_lat, 0] - x[0]) < tol_pos)[0]
        #print('idx_pos->', idx_pos)
        if len(idx_pos) == 0:
            return 1e10
        depth = -admissible_xyz[idx_lat][idx_pos][:, 2][0]
        #print(depth)
        strike, dip = admissible_strike[idx_lat][idx_pos][:, 2][0], admissible_dip[idx_lat][idx_pos][:, 2][0]
        #print(x)

        displacement = forward(x[0], x[1], depth, x[2], x[3], x[4], strike, dip, x[5])
        displacement = displacement[:, :2]
        y = np.sum(static_disp[:, :2] - displacement, axis=1)
        #y = np.sum(static_disp[:, :2] - displacement)
        return y

    def forward(xx, yy, d, length, width, u, az, dp, rk):
        disp = okada85((station_coords[:, 1] - xx) * 111.3194,
                               (station_coords[:, 0] - yy) * 111.3194, 0, 0,
                               depth=d, length=length, width=width, slip=u, opening=0, strike=az, dip=dp,
                               rake=rk)
        disp = np.array(disp).T
        return disp

    def get_spatial_cut(lat_min, lat_max):
        idx = np.where(np.logical_and(station_coords[:, 0] > lat_min, station_coords[:, 0] < lat_max))[0]
        return idx

    # ev 34 from Michel : 34,2012.609,2012.6684,2012.791,2012.7926, 6.54
    # start, end = 2012.6684, 2012.791
    start, end = 2017.2019, 2017.2868  # Yuji
    #start, end = 2015.974, 2016.168  # Michel # 54
    # start, end = 2017.889100, 2018.067100 # Costantino et al. ?
    #start, end = 2019.206000, 2019.384000 # Costantino et al., may be
    events = [(2017.2019, 2017.2868), (2015.974, 2016.168), (2019.206000, 2019.384000), (2017.889100, 2018.067100),
              (2012.6684, 2012.791)]

    reference_period = (2007, 2023)
    n_selected_stations = 135

    selected_gnss_data, selected_time_array, _, _ = _preliminary_operations(reference_period=reference_period, n_directions=3)
    _, station_coordinates, _, _,  station_subset = cascadia_filtered_stations(n_selected_stations, reference_period=(2007, 2014))


    cut_lats = [(47, 60), (43.9, 60), (43, 48), (43.9, 47), (47, 60)]
    initial_guesses = [[-124., 49, 300, 50, 30, 90], [-124., 49, 300, 50, 30, 90],
                       [-124., 49, 300, 50, 30, 90], [-124., 45.5, 300, 50, 30, 90],
                       [-124.5, 48.5, 300, 50, 30, 90]]  # [lon_0, lat_0, L_0, W_0, slip_0, rake_0]
    bounds = [([-125, 48, 250, 45, 5, 75], [-123, 49.5, 450, 100, 50, 105]),
              ([-125, 46, 250, 45, 5, 75], [-122, 49.5, 650, 100, 50, 105]),
              ([-125, 46, 250, 45, 5, 75], [-122, 49.5, 650, 100, 50, 105]),
              ([-125, 43, 250, 45, 5, 75], [-122, 49.5, 650, 100, 50, 105]),
              ([-125, 47, 250, 45, 5, 75], [-122, 49.5, 650, 100, 50, 105])]

    for i, event in enumerate(events):
        if i < 4 and False:
            continue
        start, end = event
        station_coords = station_coordinates.copy()
        start_idx, end_idx = _find_nearest_val(selected_time_array, start)[1], _find_nearest_val(selected_time_array, end)[1]

        #static_disp = selected_gnss_data[station_subset, end_idx, :] - selected_gnss_data[station_subset, start_idx, :]

        static_disp = np.median(selected_gnss_data[station_subset, end_idx:end_idx+7, :], axis=1) - np.median(selected_gnss_data[station_subset, start_idx-7:start_idx, :], axis=1)

        plot_deformation_field(station_coords, static_disp[:, 0], static_disp[:, 1],
                               tremors[np.logical_and(tremors[:, 3] >= start, tremors[:, 3] <= end)], draw_tremors=True)

        #static_disp = static_disp[:, (1, 0, 2)]

        # spatial_cut_idx = np.where(station_coords[:, 0] > 47)[0]
        spatial_cut_idx = get_spatial_cut(cut_lats[i][0], cut_lats[i][1])
        station_coords = station_coords[spatial_cut_idx]
        static_disp = static_disp[spatial_cut_idx]


        avail_stations = ~np.isnan(static_disp[:, 0])
        static_disp = static_disp[avail_stations]
        station_coords = station_coords[avail_stations]


        admissible_xyz, admissible_strike, admissible_dip, _ = read_from_slab2()


        '''model = least_squares(residuals_on_slab, [lon_0, lat_0, L_0, W_0, slip_0, rake_0],
                              bounds=([-125, 48, 250, 45, 5, 75], [-123, 49.5, 450, 100, 50, 105]),
                              loss='linear', verbose=2)'''

        model = least_squares(residuals_on_slab, initial_guesses[i],
                              bounds=bounds[i],
                              loss='linear', verbose=2)

        idx_lat_best = np.where(np.abs(admissible_xyz[:, 1] - model.x[1]) < 0.1)[0]
        idx_pos_best = np.where(np.abs(admissible_xyz[idx_lat_best, 0] - model.x[0]) < 0.1)[0]
        depth_best = -admissible_xyz[idx_lat_best][idx_pos_best][:, 2][0]
        strike_best, dip_best = admissible_strike[idx_lat_best][idx_pos_best][:, 2][0], admissible_dip[idx_lat_best][idx_pos_best][:, 2][0]
        # xx, yy, d, length, width, u, az, dp, rk
        opt_displacement = forward(model.x[0], model.x[1], depth_best, model.x[2], model.x[3], model.x[4], strike_best, dip_best, model.x[5])

        print('Best model.x ->', model.x)
        print('depth, strike, dip', depth_best, strike_best, dip_best)


        #plt.plot(opt_displacement);plt.show()

        # opt_displacement = opt_displacement[:, :2]

        print('données', np.max(np.abs(static_disp[:, 0])))
        print('modèle', np.max(np.abs(opt_displacement[:, 0])))

        plot_deformation_field(station_coords, static_disp[:, 0], static_disp[:, 1],
                               opt_displacement[:, 0], opt_displacement[:, 1], None, model,
                               draw_dislocation=True, set_title=True,
                               **{'strike': strike_best, 'dip': dip_best, 'depth': depth_best})


def displacement_field_plot(catalog, north=False, base_fig_folder='./sse_figures/'):
    n_selected_stations = 135
    selected_gnss_data, selected_time_array, _, _ = _preliminary_operations((2007, 2023), detrend=False)
    station_codes, station_coordinates = cascadia_coordinates()
    stations_to_remove = ['WSLB', 'YBHB', 'P687', 'BELI', 'PMAR', 'TGUA', 'OYLR', 'FTS5', 'RPT5', 'RPT6', 'P791',
                          'P674', 'P656', 'TWRI', 'WIFR', 'FRID', 'PNHG', 'COUR', 'SKMA', 'CSHR', 'HGP1', 'CBLV',
                          'PNHR', 'NCS2', 'TSEP', 'BCSC']
    station_codes, station_coordinates, selected_gnss_data = _remove_stations(stations_to_remove, station_codes,
                                                                              station_coordinates, selected_gnss_data)
    original_nan_pattern = np.isnan(selected_gnss_data[:, :, 0])
    n_nans_stations = original_nan_pattern.sum(axis=1)

    # _draw_table_data(selected_gnss_data[np.argsort(n_nans_stations)], (selected_time_array[0], selected_time_array[-1]), station_coordinates)

    stations_subset = np.sort(np.argsort(n_nans_stations)[:n_selected_stations])
    station_codes_subset, station_coordinates_subset = np.array(station_codes)[stations_subset], station_coordinates[
                                                                                                 stations_subset, :]
    original_nan_pattern = original_nan_pattern[stations_subset, :]
    gnss_data_subset = selected_gnss_data[stations_subset, :, :]
    # gnss_data_subset, trend_info = detrend_nan_v2(selected_time_array, gnss_data_subset)

    cascadia_box = [40 - 1.5, 51.8 - 0.2, -128.3 - 2.5, -121 + 2]  # min/max_latitude, min/max_longitude
    for i, event in enumerate(catalog):
        start_idx, end_idx = np.where(selected_time_array == event[0])[0][0], np.where(selected_time_array == event[1])[0][0]
        static_displacement = np.median(gnss_data_subset[:, end_idx - 1:end_idx + 2, :], axis=1) - np.nanmedian(gnss_data_subset[:, start_idx - 1:start_idx + 2, :], axis=1)
        fig = plt.figure(figsize=(7.8, 7.8), dpi=100)
        cascadia_map = Basemap(llcrnrlon=cascadia_box[2], llcrnrlat=cascadia_box[0],
                               urcrnrlon=cascadia_box[3], urcrnrlat=cascadia_box[1],
                               lat_0=cascadia_box[0], lon_0=cascadia_box[2],
                               resolution='i', projection='lcc')  # , ax=ax9)

        cascadia_map.drawcoastlines(linewidth=0.5)
        cascadia_map.fillcontinents(color='bisque', lake_color='lightcyan')  # burlywood
        cascadia_map.drawmapboundary(fill_color='lightcyan')
        # cascadia_map.drawmapscale(-123.5, 51.05, -123.5, 51.05, 300, barstyle='fancy')
        cascadia_map.drawparallels(np.arange(cascadia_box[0], cascadia_box[1], (cascadia_box[1] - cascadia_box[0]) / 4),
                                   labels=[0, 1, 0, 0], linewidth=0.1)
        cascadia_map.drawmeridians(np.arange(cascadia_box[2], cascadia_box[3], (cascadia_box[3] - cascadia_box[2]) / 4),
                                   labels=[0, 0, 0, 1], linewidth=0.1)
        cascadia_map.readshapefile('geo_data/tectonicplates-master/PB2002_boundaries', '', linewidth=0.3)

        max_length = np.nanmax(np.sqrt(static_displacement[:, 0] ** 2 + static_displacement[:, 1] ** 2))
        # concatenate unit arrow
        unit_arrow_x, unit_arrow_y = -127., 39.5
        x = np.concatenate((station_coordinates_subset[:, 1], [unit_arrow_x]))
        y = np.concatenate((station_coordinates_subset[:, 0], [unit_arrow_y]))
        disp_x = np.concatenate((static_displacement[:, 0], [-max_length]))
        disp_y = np.concatenate((static_displacement[:, 1], [0.]))

        cascadia_map.quiver(x, y, disp_x, disp_y, color='black', latlon=True, width=0.007)

        label_pos_x, label_pos_y = cascadia_map(unit_arrow_x - 1.45, unit_arrow_y + 0.25)

        plt.annotate(f'{np.around(max_length, decimals=1)}mm', xy=(unit_arrow_x, unit_arrow_y), xycoords='data',
                     xytext=(label_pos_x, label_pos_y), color='black')
        plt.title(f'SSE date: {event[0]} - {event[1]}, duration: {int((event[1] - event[0]) * 365)} days')
        plt.savefig(base_fig_folder + ('north_selection/' if north else '') + 'disp/' + f'{i + 1}_{event[0]}_{event[1]}.pdf')
        # plt.show()
        plt.close(fig)

if __name__ == '__main__':
    reference_period = (2007, 2023)
    north_selection = False
    south_selection = True
    relative_height = 0.7
    time, proba = load_proba(reference_period, north=north_selection, south=south_selection)
    tremors = tremor_catalogue(north=north_selection, south=south_selection)
    n_tremors_per_day = get_n_tremors_per_day(time, tremors)

    #comparison_moment_rate_known_sses(time, proba, n_tremors_per_day)

    overlap_threshold = 1e-04
    probability_threshold = 0.4

    base_folder = './sse_figures/'

    #exit(0)
    save_catalog = False

    #cat_ours, _, _, _ = sse_selection_from_proba(proba, time, probability_threshold=0.5, rel_height=relative_height, save_catalog=False, north=north_selection)
    cat_ours = sse_selection_from_proba_hard_thresh(proba, time, probability_threshold=probability_threshold, save_catalog=save_catalog, north=north_selection, south=south_selection)
    cat_michel = sse_catalogue(north=north_selection, south=south_selection)
    cat_michel_mw = sse_catalogue(return_magnitude=True, north=north_selection, south=south_selection)

    '''plt.hist((cat_ours[:, 1] - cat_ours[:, 0])*365, bins=50)
    plt.show()'''

    # plot_inversion_gps_param()
    # plot_inversion_gps_param2(tremors)

    # detection_overview(time, proba, n_tremors_per_day, cat_ours, cat_michel, north=north_selection)
    detection_overview2(time, proba, n_tremors_per_day, cat_ours, cat_michel, north=north_selection,
                        draw_tremors=True, draw_tremor_rectangle=True, draw_michel_catalog=True,
                        draw_sse_durations=True, base_fig_folder=base_folder, south=south_selection)

    zoom_on_overview_plot(time, proba, n_tremors_per_day, cat_ours, cat_michel, north=north_selection,
                        draw_tremors=True, draw_tremor_rectangle=True, draw_michel_catalog=True,
                        draw_sse_durations=True, base_fig_folder=base_folder, south=south_selection)

    plot_comparison_duration_ours_michel(cat_ours, cat_michel, overlap_thresh=overlap_threshold, save_diff_catalog=False, north=north_selection, south=south_selection, base_fig_folder=base_folder)

    plot_comparison_duration_ours_michel2(cat_ours, cat_michel, overlap_thresh=overlap_threshold, save_diff_catalog=False, north=north_selection, south=south_selection, base_fig_folder=base_folder)



    #plot_comparison_duration_proba_tremors(cat_ours, time, proba, n_tremors_per_day, rel_height=0.7, north=north_selection)
    sigma_d, sigma_t = 3, 0
    tremor_time_ref = (2010, 2023)

    correlation_proba_tremor_lag(time, proba, n_tremors_per_day, derivative=False, upsampling_factor=1, max_lag=15, smooth_tremors=True, sigma=1.5, scale=True, tremor_time_ref=tremor_time_ref, north=north_selection, south=south_selection, base_fig_folder=base_folder)

    #plot_comparison_duration_proba_tremors_corr(cat_ours, time, proba, n_tremors_per_day, rel_height=relative_height, north=north_selection, sigma_d=sigma_d, sigma_t=sigma_t, tremor_time_ref=tremor_time_ref, base_fig_folder=base_folder)
    #plot_comparison_duration_proba_tremors_no_color(cat_ours, time, proba, n_tremors_per_day, rel_height=relative_height, north=north_selection, sigma_d=sigma_d, sigma_t=sigma_t, tremor_time_ref=tremor_time_ref, base_fig_folder=base_folder)

    #plot_comparison_duration_proba_tremors_no_color_mean_std(cat_ours, time, proba, n_tremors_per_day, rel_height=relative_height, north=north_selection, sigma_d=sigma_d, sigma_t=sigma_t, tremor_time_ref=tremor_time_ref, base_fig_folder=base_folder)

    sigma_d, sigma_t, max_lag, sigma_d_cut = 30, 0, 7, 7
    # plot_comparison_sse_tremor_local_corr(cat_ours, time, proba, n_tremors_per_day, rel_height=relative_height, north=north_selection, sigma_d=sigma_d, sigma_t=sigma_t, tremor_time_ref=tremor_time_ref, base_fig_folder=base_folder)
    plot_comparison_sse_tremor_local_corr2(cat_ours, time, proba, n_tremors_per_day, rel_height=relative_height, north=north_selection, south=south_selection, sigma_d_cut=sigma_d_cut, sigma_d=sigma_d, sigma_t=sigma_t, tremor_time_ref=tremor_time_ref, base_fig_folder=base_folder, max_lag=max_lag)



    #plot_scaling_law_duration_magnitude(cat_ours, north=north_selection, base_fig_folder=base_folder)
    plot_histogram_durations(cat_ours, cat_michel, bins=40, north=north_selection, south=south_selection, base_fig_folder=base_folder)
    plot_histogram_durations_stacked(cat_ours, cat_michel, overlap_thresh=overlap_threshold, bins=40, north=north_selection, south=south_selection, base_fig_folder=base_folder)
    # plot_proba_tremors_new()

    # plot_proba_tremors_north_selection()

    # displacement_field_plot(cat_ours, north=north_selection, base_fig_folder=base_folder)
