import alphashape
import matplotlib
import matplotlib.colors as mcolors
import numpy as np
from matplotlib import pyplot as plt
from mpl_toolkits.basemap import Basemap
from scipy import interpolate
from shapely.geometry import Point, Polygon as Poly
from sklearn.cluster import DBSCAN

from sse_generator.artificial_noise_cascadia import n_surrogates_stack_pca_lib
from sse_generator.synthetic_sse_cascadia import _synthetic_displacement_stations_cascadia, sigmoidal_rise_time
from utils import _preliminary_operations, cascadia_coordinates, _remove_stations, detrend_nan_v2, read_from_slab2, \
    grid, _train_contour, tremor_catalogue


def _draw_table_data(data_window, time_span, station_coordinates, fig_title, every_nth=17, title='', vmin=None,
                     vmax=None, colorbar=False, ylabel=True, fig_directory='sse_figures'):
    fig = plt.figure(figsize=(6, 4))
    ax = plt.gca()
    latsort = np.argsort(station_coordinates[:, 0])[::-1]
    cmap = matplotlib.cm.get_cmap("RdBu_r").copy()
    if vmin is None:
        vmin = -5
    if vmax is None:
        vmax = 10
    norm = mcolors.TwoSlopeNorm(vmin=vmin, vcenter=0., vmax=vmax)
    cmap.set_bad('black', 1.)
    matshow_extent = [time_span[0], time_span[1], station_coordinates[:, 0].min(),
                      station_coordinates[:, 0].max()]
    mat = ax.matshow(data_window[latsort, :, 0], cmap=cmap, norm=norm, aspect='auto',
                     extent=matshow_extent)
    if ylabel:
        ax.set_ylabel('Latitude [°]', labelpad=7)
    ax.set_xlabel('Time [days]')
    ax.set_title(title)
    ax.set_yticks(np.around(station_coordinates[latsort, 0], decimals=1))
    for n, label in enumerate(ax.yaxis.get_ticklabels()):
        if n % every_nth != 0:
            label.set_visible(False)
            ax.yaxis.get_major_ticks()[n].set_visible(False)
    if colorbar:
        cbar = fig.colorbar(mat, ax=ax)
        cbar.ax.set_ylabel('Displacement [mm]', rotation=270, labelpad=17)
    plt.tight_layout()
    plt.savefig(f'{fig_directory}/{fig_title}.pdf')
    # plt.show()
    plt.close(fig)


def get_data_from_period(reference_period, n_selected_stations):
    selected_gnss_data, selected_time_array, _, _ = _preliminary_operations(reference_period, detrend=False)
    station_codes, station_coordinates = cascadia_coordinates()
    stations_to_remove = ['WSLB', 'YBHB', 'P687', 'BELI', 'PMAR', 'TGUA', 'OYLR', 'FTS5', 'RPT5', 'RPT6', 'P791',
                          'P674', 'P656', 'TWRI', 'WIFR', 'FRID', 'PNHG', 'COUR', 'SKMA', 'CSHR', 'HGP1', 'CBLV',
                          'PNHR', 'NCS2', 'TSEP', 'BCSC']
    station_codes, station_coordinates, selected_gnss_data = _remove_stations(stations_to_remove, station_codes,
                                                                              station_coordinates, selected_gnss_data)
    original_nan_pattern = np.isnan(selected_gnss_data[:, :, 0])
    n_nans_stations = original_nan_pattern.sum(axis=1)

    stations_subset = np.sort(np.argsort(n_nans_stations)[:n_selected_stations])
    station_codes_subset, station_coordinates_subset = np.array(station_codes)[stations_subset], station_coordinates[
                                                                                                 stations_subset, :]
    original_nan_pattern = original_nan_pattern[stations_subset, :]
    gnss_data_subset = selected_gnss_data[stations_subset, :, :]
    gnss_data_subset, trend_info = detrend_nan_v2(selected_time_array, gnss_data_subset)

    gnss_data_subset[original_nan_pattern] = 0.  # NaNs are replaced with zeros
    return gnss_data_subset, selected_time_array, trend_info, original_nan_pattern, station_codes_subset, station_coordinates_subset


def map_figure_synth(synthetic_displacement, station_coordinates, dislocation_parameters, draw_tremors=True,
                     draw_disp_field=True, draw_gnss_network=True, draw_dislocation=True, draw_train_contour=True,
                     draw_isodepth=True, fig_directory='sse_figures', color_code_depth=True):
    with np.load(f'pred_cascadia_realgaps_ext_v3_0.001_6-7_135_01Aug2022-144844_2018_2023.npz') as f:
        cat = f['catalogue'][:4000]  # just a few data

    cascadia_box = [40 - 1.5, 51.8 - 0.2, -128.3 - 2.5, -121 + 2]  # min/max_latitude, min/max_longitude

    # fig, _ = plt.subplots(1, 1, figsize=(7.8, 7.8), dpi=100)
    fig = plt.figure(figsize=(7.8, 7.8), dpi=100)
    cascadia_map = Basemap(llcrnrlon=cascadia_box[2], llcrnrlat=cascadia_box[0],
                           urcrnrlon=cascadia_box[3], urcrnrlat=cascadia_box[1],
                           lat_0=cascadia_box[0], lon_0=cascadia_box[2],
                           resolution='i', projection='lcc')  # , ax=ax9)

    cascadia_map.drawcoastlines(linewidth=0.5)
    cascadia_map.fillcontinents(color='bisque', lake_color='lightcyan')  # burlywood
    cascadia_map.drawmapboundary(fill_color='lightcyan')
    cascadia_map.drawmapscale(-122.5, 51.05, -122.5, 51.05, 300, barstyle='fancy', zorder=10)
    cascadia_map.drawparallels(np.arange(cascadia_box[0], cascadia_box[1], (cascadia_box[1] - cascadia_box[0]) / 4),
                               labels=[0, 1, 0, 0], linewidth=0.1)
    cascadia_map.drawmeridians(np.arange(cascadia_box[2], cascadia_box[3], (cascadia_box[3] - cascadia_box[2]) / 4),
                               labels=[0, 0, 0, 1], linewidth=0.1)
    cascadia_map.readshapefile('geo_data/tectonicplates-master/PB2002_boundaries', '', linewidth=0.3)

    if color_code_depth:
        xyz = read_from_slab2()[0]
        x, y, z = xyz[:, 0], xyz[:, 1], - xyz[:, 2]
        X_depth, Y_depth, Z_depth = grid(x, y, z, resX=300, resY=300)

        train_poly_x, train_poly_y = _train_contour(cat)
        train_poly = Poly(np.vstack((train_poly_x, train_poly_y)).T)
        mesh_points = np.vstack((X_depth.ravel(), Y_depth.ravel())).T
        flattened_Z = Z_depth.ravel()
        for i, point in enumerate(mesh_points):
            if not Point(point).within(train_poly):
                flattened_Z[i] = np.nan
        Z_depth = Z_depth.reshape(X_depth.shape)

        XX_depth, YY_depth = cascadia_map(X_depth, Y_depth)
        cmap_depth = plt.cm.get_cmap('turbo')
        pcm_depth = cascadia_map.pcolormesh(XX_depth, YY_depth, Z_depth, shading='gouraud', cmap=cmap_depth, alpha=1.,
                                            antialiased=True, rasterized=True)

        cbar_depth = plt.colorbar(pcm_depth, orientation='horizontal', fraction=0.018, pad=0.065, shrink=0.6,
                                  ticks=[10, 20, 30, 40, 50])  # fraction=0.046, pad=0.04)
        cbar_depth.ax.set_xlabel('Slab depth (km)', labelpad=5)

    if draw_dislocation:
        x_center = dislocation_parameters['epi_lon']
        y_center = dislocation_parameters['epi_lat']
        strike = np.deg2rad(dislocation_parameters['strike'])
        dip = np.deg2rad(dislocation_parameters['dip'])
        rake = np.deg2rad(dislocation_parameters['rake'])
        slip = dislocation_parameters['slip']
        L = dislocation_parameters['L']
        W = dislocation_parameters['W']
        d = dislocation_parameters['depth'] + np.sin(dip) * W / 2
        U1 = np.cos(rake) * slip
        U2 = np.sin(rake) * slip
        U3 = 0
        alpha = np.pi / 2 - strike
        x_fault = L / 2 * np.cos(alpha) * np.array([-1., 1., 1., -1.]) + np.sin(alpha) * np.cos(dip) * W / 2 * np.array(
            [-1., -1., 1., 1.])
        y_fault = L / 2 * np.sin(alpha) * np.array([-1., 1., 1., -1.]) + np.cos(alpha) * np.cos(dip) * W / 2 * np.array(
            [1., 1., -1., -1.])
        x_fault += x_center
        y_fault += y_center
        x_fault_map, y_fault_map = cascadia_map(x_fault, y_fault)
        # x_fault_map, y_fault_map = x_fault, y_fault
        plt.gca().add_patch(matplotlib.patches.Polygon(np.vstack((x_fault_map, y_fault_map)).T,
                                                       closed=True, fill=True, color='black', lw=1.5,
                                                       alpha=.4))
    if draw_gnss_network:
        cascadia_map.scatter(station_coordinates[:, 1], station_coordinates[:, 0], marker='^', s=15, color='C3',
                             latlon=True, edgecolors='black', linewidth=0.7)
    if draw_tremors:
        tremors = tremor_catalogue()
        db = DBSCAN(eps=1.5e-02, min_samples=15, n_jobs=-1).fit(tremors[:, :2])
        # joblib.dump(db, 'dbscan_tremors')
        # db = joblib.load('dbscan_tremors')
        denoised_tremors = np.where(db.labels_ != -1)[0]
        alpha_shape = alphashape.alphashape(tremors[denoised_tremors, :2], alpha=1.9)
        hull_pts = alpha_shape.exterior.coords.xy
        x_hull, y_hull = np.array(hull_pts[1]), np.array(hull_pts[0])
        valid_pts_hull = np.where(x_hull < -121.95)[0]  # remove knot
        x_hull, y_hull = x_hull[valid_pts_hull], y_hull[valid_pts_hull]
        dist = np.sqrt((x_hull[:-1] - x_hull[1:]) ** 2 + (y_hull[:-1] - y_hull[1:]) ** 2)
        dist_along = np.concatenate(([0], dist.cumsum()))
        spline, u = interpolate.splprep([x_hull, y_hull], u=dist_along, s=0, per=1)
        interp_d = np.linspace(dist_along[0], dist_along[-1], 150)
        interp_x, interp_y = interpolate.splev(interp_d, spline)
        interp_x_map, interp_y_map = cascadia_map(interp_x, interp_y)
        cascadia_map.plot(interp_x_map, interp_y_map, linestyle='--', color='black', lw=2.)

    if draw_train_contour:
        alpha_shape = alphashape.alphashape(cat[:, :2], alpha=1.9)
        hull_pts = alpha_shape.exterior.coords.xy
        x_hull, y_hull = np.array(hull_pts[1]), np.array(hull_pts[0])
        # valid_pts_hull = np.where(x_hull < -121.95)[0]  # remove knot
        # x_hull, y_hull = x_hull[valid_pts_hull], y_hull[valid_pts_hull]
        dist = np.sqrt((x_hull[:-1] - x_hull[1:]) ** 2 + (y_hull[:-1] - y_hull[1:]) ** 2)
        dist_along = np.concatenate(([0], dist.cumsum()))
        spline, u = interpolate.splprep([x_hull, y_hull], u=dist_along, s=0, per=1)
        interp_d_cat = np.linspace(dist_along[0], dist_along[-1], 350)
        interp_x_cat, interp_y_cat = interpolate.splev(interp_d_cat, spline)
        interp_x_map_cat, interp_y_map_cat = cascadia_map(interp_x_cat, interp_y_cat)
        plt.gca().add_patch(matplotlib.patches.Polygon(np.vstack((interp_x_map_cat, interp_y_map_cat)).T,
                                                       closed=True, fill=False, color='black', lw=2.,
                                                       alpha=1.))

    if draw_disp_field:
        max_length = np.max(np.sqrt(synthetic_displacement[:, 0] ** 2 + synthetic_displacement[:, 1] ** 2))
        # concatenate unit arrow
        unit_arrow_x, unit_arrow_y = -127., 39.5
        x = np.concatenate((station_coordinates[:, 1], [unit_arrow_x]))
        y = np.concatenate((station_coordinates[:, 0], [unit_arrow_y]))
        disp_x = np.concatenate((synthetic_displacement[:, 0], [-max_length]))
        disp_y = np.concatenate((synthetic_displacement[:, 1], [0.]))

        cascadia_map.quiver(x, y, disp_x, disp_y, color='black', latlon=True, width=0.0035)

        label_pos_x, label_pos_y = cascadia_map(unit_arrow_x - 1.45, unit_arrow_y + 0.25)

        plt.annotate(f'{np.around(max_length, decimals=1)}mm', xy=(unit_arrow_x, unit_arrow_y), xycoords='data',
                     xytext=(label_pos_x, label_pos_y), color='black')

    if draw_isodepth:
        def isodepth_label_fmt(x):
            return rf"{int(x)} km"

        levels = [20, 40]
        admissible_depth, _, _, region = read_from_slab2()
        x, y = admissible_depth[:, 0], admissible_depth[:, 1]
        depth = admissible_depth[:, 2]
        x_map, y_map = cascadia_map(x, y)
        isodepth = plt.tricontour(x_map, y_map, -depth, levels=levels, colors='black', linewidths=0.7)
        label_loc_y = [48.3, 41.7]  # [48.3, 48.4]
        label_loc_x = [-125.5, -123.0]  # [-125.5, -124.5]
        label_loc_x_map, label_loc_y_map = cascadia_map(label_loc_x, label_loc_y)
        plt.clabel(isodepth, isodepth.levels, inline=True, fontsize=10, fmt=isodepth_label_fmt,
                   manual=list(zip(label_loc_x_map, label_loc_y_map)))

    ax = fig.gca()
    for axis in ['top', 'bottom', 'left', 'right']:
        ax.spines[axis].set_linewidth(0.5)
    plt.savefig(f'{fig_directory}/map_synth.pdf')
    plt.close(fig)


def data_gap_matrix(fig_directory, fig_title, ref_period=(2007, 2014), vmin=None, vmax=None, ylabel=True,
                    colorbar=True):
    """Shows the data in the refrence period, sorted by number of data gaps."""
    selected_gnss_data, selected_time_array, _, _ = _preliminary_operations(ref_period, detrend=False)
    station_codes, station_coordinates = cascadia_coordinates()
    stations_to_remove = ['WSLB', 'YBHB', 'P687', 'BELI', 'PMAR', 'TGUA', 'OYLR', 'FTS5', 'RPT5', 'RPT6', 'P791',
                          'P674', 'P656', 'TWRI', 'WIFR', 'FRID', 'PNHG', 'COUR', 'SKMA', 'CSHR', 'HGP1', 'CBLV',
                          'PNHR', 'NCS2', 'TSEP', 'BCSC']
    station_codes, station_coordinates, selected_gnss_data = _remove_stations(stations_to_remove, station_codes,
                                                                              station_coordinates, selected_gnss_data)
    original_nan_pattern = np.isnan(selected_gnss_data[:, :, 0])
    n_nans_stations = original_nan_pattern.sum(axis=1)

    selected_gnss_data, trend_info = detrend_nan_v2(selected_time_array, selected_gnss_data)

    fig = plt.figure(figsize=(6, 4), dpi=1200)
    ax = plt.gca()
    cmap = matplotlib.cm.get_cmap("RdBu_r").copy()
    cmap.set_bad('black', 1.)

    if vmin is None:
        vmin = -5
    if vmax is None:
        vmax = 10

    norm = mcolors.TwoSlopeNorm(vmin=vmin, vcenter=0., vmax=vmax)
    cmap.set_bad('black', 1.)
    matshow_extent = [ref_period[0], ref_period[1], len(station_coordinates), 0]
    mat = ax.matshow(selected_gnss_data[np.argsort(n_nans_stations), :, 0], cmap=cmap, norm=norm, aspect='auto',
                     extent=matshow_extent)
    if ylabel:
        ax.set_ylabel('Station ID', labelpad=7)
    ax.set_xlabel('Time [years]')

    yticks = ax.get_yticks().tolist()[:-1]  # exclude the very last one
    yticks += [135, 352]
    yticks = sorted(yticks)
    ax.set_yticks(yticks)
    print(yticks)
    ax.get_yticklabels()[2].set_color("red")
    ax.get_yticklabels()[5].set_color("red")

    if colorbar:
        cbar = fig.colorbar(mat, ax=ax)
        cbar.ax.set_ylabel('Displacement [mm]', rotation=270, labelpad=17)
    plt.tight_layout()
    plt.savefig(f'{fig_directory}/{fig_title}.pdf')
    plt.close(fig)


def map_352_stations(fig_directory, draw_gnss_network=True, draw_tremors=True, draw_isodepth=True):
    _, _, _, _, station_codes, station_coordinates = get_data_from_period((2018, 2023), 352)

    cascadia_box = [40 - 1.5, 51.8 - 0.2, -128.3 - 2.5, -121 + 2]  # min/max_latitude, min/max_longitude
    fig = plt.figure(figsize=(7.8, 7.8), dpi=100)
    cascadia_map = Basemap(llcrnrlon=cascadia_box[2], llcrnrlat=cascadia_box[0],
                           urcrnrlon=cascadia_box[3], urcrnrlat=cascadia_box[1],
                           lat_0=cascadia_box[0], lon_0=cascadia_box[2],
                           resolution='i', projection='lcc')  # , ax=ax9)

    cascadia_map.drawcoastlines(linewidth=0.5)
    cascadia_map.fillcontinents(color='bisque', lake_color='lightcyan')  # burlywood
    cascadia_map.drawmapboundary(fill_color='lightcyan')
    cascadia_map.drawmapscale(-122.5, 51.05, -122.5, 51.05, 300, barstyle='fancy', zorder=10)
    cascadia_map.drawparallels(np.arange(cascadia_box[0], cascadia_box[1], (cascadia_box[1] - cascadia_box[0]) / 4),
                               labels=[0, 1, 0, 0], linewidth=0.1)
    cascadia_map.drawmeridians(np.arange(cascadia_box[2], cascadia_box[3], (cascadia_box[3] - cascadia_box[2]) / 4),
                               labels=[0, 0, 0, 1], linewidth=0.1)
    cascadia_map.readshapefile('geo_data/tectonicplates-master/PB2002_boundaries', '', linewidth=0.3)

    if draw_gnss_network:
        cascadia_map.scatter(station_coordinates[:, 1], station_coordinates[:, 0], marker='^', s=15, color='C3',
                             latlon=True, edgecolors='black', linewidth=0.7)
    if draw_tremors:
        tremors = tremor_catalogue()
        db = DBSCAN(eps=1.5e-02, min_samples=15, n_jobs=-1).fit(tremors[:, :2])
        # joblib.dump(db, 'dbscan_tremors')
        # db = joblib.load('dbscan_tremors')
        denoised_tremors = np.where(db.labels_ != -1)[0]
        alpha_shape = alphashape.alphashape(tremors[denoised_tremors, :2], alpha=1.9)
        hull_pts = alpha_shape.exterior.coords.xy
        x_hull, y_hull = np.array(hull_pts[1]), np.array(hull_pts[0])
        valid_pts_hull = np.where(x_hull < -121.95)[0]  # remove knot
        x_hull, y_hull = x_hull[valid_pts_hull], y_hull[valid_pts_hull]
        dist = np.sqrt((x_hull[:-1] - x_hull[1:]) ** 2 + (y_hull[:-1] - y_hull[1:]) ** 2)
        dist_along = np.concatenate(([0], dist.cumsum()))
        spline, u = interpolate.splprep([x_hull, y_hull], u=dist_along, s=0, per=1)
        interp_d = np.linspace(dist_along[0], dist_along[-1], 150)
        interp_x, interp_y = interpolate.splev(interp_d, spline)
        interp_x_map, interp_y_map = cascadia_map(interp_x, interp_y)
        cascadia_map.plot(interp_x_map, interp_y_map, linestyle='--', color='black', lw=2.)

    if draw_isodepth:
        def isodepth_label_fmt(x):
            return rf"{int(x)} km"

        levels = [20, 40]
        admissible_depth, _, _, region = read_from_slab2()
        x, y = admissible_depth[:, 0], admissible_depth[:, 1]
        depth = admissible_depth[:, 2]
        x_map, y_map = cascadia_map(x, y)
        isodepth = plt.tricontour(x_map, y_map, -depth, levels=levels, colors='black', linewidths=0.7)
        label_loc_y = [48.3, 41.7]  # [48.3, 48.4]
        label_loc_x = [-125.5, -123.0]  # [-125.5, -124.5]
        # label_loc_x = [x[np.where(admissible_depth[:, 1:] == (label_loc_y[i], -level))[0][0]] for i, level in enumerate(levels)]
        label_loc_x_map, label_loc_y_map = cascadia_map(label_loc_x, label_loc_y)
        plt.clabel(isodepth, isodepth.levels, inline=True, fontsize=10, fmt=isodepth_label_fmt,
                   manual=list(zip(label_loc_x_map, label_loc_y_map)))

    ax = fig.gca()
    for axis in ['top', 'bottom', 'left', 'right']:
        ax.spines[axis].set_linewidth(0.5)
    plt.savefig(f'{fig_directory}/map_352_stations.pdf')
    plt.close(fig)


def map_north_subset(fig_directory, draw_gnss_network=True, draw_tremors=True, draw_isodepth=True):
    _, _, _, _, station_codes, station_coordinates = get_data_from_period((2018, 2023), 135)

    north_selection_idx = np.where(station_coordinates[:, 0] > 47)[0]
    other_stations = np.where(station_coordinates[:, 0] <= 47)[0]

    cascadia_box = [40 - 1.5, 51.8 - 0.2, -128.3 - 2.5, -121 + 2]  # min/max_latitude, min/max_longitude

    fig = plt.figure(figsize=(7.8, 7.8), dpi=100)
    cascadia_map = Basemap(llcrnrlon=cascadia_box[2], llcrnrlat=cascadia_box[0],
                           urcrnrlon=cascadia_box[3], urcrnrlat=cascadia_box[1],
                           lat_0=cascadia_box[0], lon_0=cascadia_box[2],
                           resolution='i', projection='lcc')  # , ax=ax9)

    cascadia_map.drawcoastlines(linewidth=0.5)
    cascadia_map.fillcontinents(color='bisque', lake_color='lightcyan')  # burlywood
    cascadia_map.drawmapboundary(fill_color='lightcyan')
    cascadia_map.drawmapscale(-122.5, 51.05, -122.5, 51.05, 300, barstyle='fancy', zorder=10)
    cascadia_map.drawparallels(np.arange(cascadia_box[0], cascadia_box[1], (cascadia_box[1] - cascadia_box[0]) / 4),
                               labels=[0, 1, 0, 0], linewidth=0.1)
    cascadia_map.drawmeridians(np.arange(cascadia_box[2], cascadia_box[3], (cascadia_box[3] - cascadia_box[2]) / 4),
                               labels=[0, 0, 0, 1], linewidth=0.1)
    cascadia_map.readshapefile('geo_data/tectonicplates-master/PB2002_boundaries', '', linewidth=0.3)

    if draw_gnss_network:
        cascadia_map.scatter(station_coordinates[north_selection_idx, 1], station_coordinates[north_selection_idx, 0],
                             marker='^', s=15, color='C3', latlon=True, edgecolors='black', linewidth=0.7)
        cascadia_map.scatter(station_coordinates[other_stations, 1], station_coordinates[other_stations, 0],
                             marker='^', s=15, facecolors='none', latlon=True, edgecolors='black', linewidth=0.7)

    if draw_tremors:
        tremors = tremor_catalogue(north=True)
        db = DBSCAN(eps=1.5e-02, min_samples=15, n_jobs=-1).fit(tremors[:, :2])
        # joblib.dump(db, 'dbscan_tremors')
        # db = joblib.load('dbscan_tremors')
        denoised_tremors = np.where(db.labels_ != -1)[0]
        alpha_shape = alphashape.alphashape(tremors[denoised_tremors, :2], alpha=1.9)
        hull_pts = alpha_shape.exterior.coords.xy
        x_hull, y_hull = np.array(hull_pts[1]), np.array(hull_pts[0])
        valid_pts_hull = np.where(x_hull < -121.95)[0]  # remove knot
        x_hull, y_hull = x_hull[valid_pts_hull], y_hull[valid_pts_hull]
        dist = np.sqrt((x_hull[:-1] - x_hull[1:]) ** 2 + (y_hull[:-1] - y_hull[1:]) ** 2)
        dist_along = np.concatenate(([0], dist.cumsum()))
        spline, u = interpolate.splprep([x_hull, y_hull], u=dist_along, s=0, per=1)
        interp_d = np.linspace(dist_along[0], dist_along[-1], 150)
        interp_x, interp_y = interpolate.splev(interp_d, spline)
        interp_x_map, interp_y_map = cascadia_map(interp_x, interp_y)
        cascadia_map.plot(interp_x_map, interp_y_map, linestyle='--', color='black', lw=2.)

    if draw_isodepth:
        def isodepth_label_fmt(x):
            return rf"{int(x)} km"

        levels = [20, 40]
        admissible_depth, _, _, region = read_from_slab2()
        x, y = admissible_depth[:, 0], admissible_depth[:, 1]
        depth = admissible_depth[:, 2]
        x_map, y_map = cascadia_map(x, y)
        isodepth = plt.tricontour(x_map, y_map, -depth, levels=levels, colors='black', linewidths=0.7)
        label_loc_y = [48.3, 41.7]  # [48.3, 48.4]
        label_loc_x = [-125.5, -123.0]  # [-125.5, -124.5]
        # label_loc_x = [x[np.where(admissible_depth[:, 1:] == (label_loc_y[i], -level))[0][0]] for i, level in enumerate(levels)]
        label_loc_x_map, label_loc_y_map = cascadia_map(label_loc_x, label_loc_y)
        plt.clabel(isodepth, isodepth.levels, inline=True, fontsize=10, fmt=isodepth_label_fmt,
                   manual=list(zip(label_loc_x_map, label_loc_y_map)))

    ax = fig.gca()
    for axis in ['top', 'bottom', 'left', 'right']:
        ax.spines[axis].set_linewidth(0.5)
    plt.savefig(f'{fig_directory}/map_north_selection.pdf')

    plt.close(fig)


if __name__ == '__main__':
    reference_period = (2018, 2023)
    n_selected_stations = 135
    directory = 'sse_figures'
    gnss_data_subset, selected_time_array, trend_info, original_nan_pattern, station_codes_subset, station_coordinates_subset = get_data_from_period(
        reference_period, n_selected_stations)

    trends = np.repeat(selected_time_array.reshape(-1, 1), n_selected_stations, axis=1) * trend_info[:, 0,
                                                                                          0] + trend_info[:, 0, 1]
    surrogate_ts = np.zeros(gnss_data_subset.shape)
    for direction in range(gnss_data_subset.shape[2]):
        # disable parallel for random seed persistence
        surrogate = n_surrogates_stack_pca_lib(gnss_data_subset[:, :, direction], 1, n_iterations=5, parallel=False)
        perm = np.random.permutation(original_nan_pattern.shape[0])
        permuted_pattern = original_nan_pattern[perm]
        surrogate[0, permuted_pattern] = 0.  # original nans restored
        surrogate_ts[:, :, direction] = surrogate

    surrogate_ts[original_nan_pattern] = np.nan  # back to NaN

    rnd_time = np.random.randint(60, len(selected_time_array) - 60)
    noise = surrogate_ts[:, rnd_time - 30:rnd_time + 30, :]

    _draw_table_data(noise, (0, 60), station_coordinates_subset, 'noise', colorbar=True, fig_directory=directory)

    # Synthetic SSE

    n = 1
    admissible_depth, admissible_strike, admissible_dip, region = read_from_slab2()
    uniform_vector = np.random.uniform(0, 1, (n, 3))
    max_stress_drop = 0.1 * 1e06  # Gao et al., 2012
    min_stress_drop = 0.01 * 1e06  # Gao et al., 2012
    mean_stress_drop = 0.5 * (max_stress_drop + min_stress_drop)
    variation_factor = 10
    std_underlying_normal = np.sqrt(np.log(variation_factor ** 2 + 1))  # from coefficient of variation
    mean_underlying_normal = np.log(mean_stress_drop) - std_underlying_normal ** 2 / 2
    lognormal_vector = np.random.lognormal(mean_underlying_normal, std_underlying_normal, (n,))
    n_stations = station_coordinates_subset.shape[0]
    disp_stations = np.zeros((n_stations, 3))

    results = _synthetic_displacement_stations_cascadia(0, admissible_depth, admissible_strike, admissible_dip,
                                                        station_coordinates_subset, uniform_vector=uniform_vector,
                                                        lognormal_vector=lognormal_vector)

    for direction in range(3):
        disp_stations[:, direction] = results[0][direction]
    catalog = results[1:]

    transients = np.zeros((len(station_codes_subset), 60, 2))
    window_length = 60
    center = window_length // 2
    random_duration = 20
    transient_time_array = np.linspace(0, window_length, window_length)
    for station in range(len(station_codes_subset)):
        for direction in range(2):
            transients[station, :, direction] = sigmoidal_rise_time(transient_time_array,
                                                                    disp_stations[station, direction],
                                                                    random_duration, center)

    synthetic_ts = noise + transients
    _draw_table_data(transients, (0, 60), station_coordinates_subset, 'synth_sse', colorbar=True,
                     fig_directory=directory)  # , vmin=-4., vmax=0.1)

    _draw_table_data(synthetic_ts, (0, 60), station_coordinates_subset, 'synth_ts', colorbar=True,
                     fig_directory=directory)

    ############################################################################################################

    disloc_par = {'strike': catalog[4], 'dip': catalog[5], 'rake': catalog[6], 'depth': catalog[2],
                  'L': catalog[7] / 111.3194, 'W': catalog[8] / 111.3194, 'slip': catalog[9],
                  'epi_lat': catalog[0], 'epi_lon': catalog[1]}

    map_figure_synth(disp_stations, station_coordinates_subset, disloc_par, draw_tremors=True,
                     draw_gnss_network=True, draw_disp_field=True, draw_dislocation=True, draw_train_contour=False,
                     draw_isodepth=False, color_code_depth=True)

    data_gap_matrix(directory, 'gaps_matrix')

    map_352_stations(directory, draw_isodepth=False)

    map_north_subset(directory, draw_isodepth=False)
