# *******************************************************************
#  model prediction at gps sites by slip model of Itoh+2022 Sci Rep
#  22 jun 2023 yuji itoh
# *******************************************************************

File type 1: obs_smooth????_XX.dat 
             model prediction from the preferred model using 30min data
             ???? = site
             XX = ew or ns (direction, no vertical)
	     $1: time in day (see date at t = 0 for paper)
             $2: sum of all the model prediction ($3+$4+$5) in cm
             $3: model prediction from fault slip in cm
             $4: local benchmark motion in cm
             $5: common mode error in cm

File type 2: slabDepth-at-site 
	     site location info
	     $1: site (baseline)
             $2,$3,$4: lon, lat, and depth (slab2)  

