import os

import numpy as np
import tensorflow as tf

from sse_detector.sse_detector import SSEdetector
from utils import _preliminary_operations, cascadia_filtered_stations


def _calculate_filtering_periods(n_selected_stations):
    global filter1, filter2, filter3, station_coords1, station_coords2, station_coords3
    _, station_coords1, _, _, filter1 = cascadia_filtered_stations(n_selected_stations, reference_period=(2007, 2014))
    _, station_coords2, _, _, filter2 = cascadia_filtered_stations(n_selected_stations, reference_period=(2014, 2018))
    _, station_coords3, _, _, filter3 = cascadia_filtered_stations(n_selected_stations, reference_period=(2018, 2023))


def _associate_station_subset_period(reference_period):
    global filter1, filter2, filter3, station_coords1, station_coords2, station_coords3
    if reference_period[0] >= 2007. and reference_period[0] <= 2014.:
        return filter1, station_coords1
    if reference_period[0] >= 2014. and reference_period[0] <= 2018.:
        return filter2, station_coords2
    if reference_period[0] >= 2018. and reference_period[0] <= 2023.:
        return filter3, station_coords3


def inference_continuous_window(detector, interm_layer_model, n_selected_stations, north_selection=False,
                                window_length=60, south_selection=False):
    reference_period = (2007, 2023)
    _calculate_filtering_periods(n_selected_stations)
    selected_gnss_data, selected_time_array, _, _ = _preliminary_operations(reference_period=reference_period)

    prediction_proba = np.zeros(selected_time_array.shape)  # probability for each time step
    attention_matrices = np.zeros((selected_time_array.shape[0], window_length, window_length))

    running_test_set = []

    for i in range(window_length // 2, selected_time_array.shape[0] - window_length // 2):
        print(selected_time_array[i])
        station_subset, coords = _associate_station_subset_period(
            (selected_time_array[i - window_length // 2], selected_time_array[i + window_length // 2]))
        data_window = selected_gnss_data[station_subset, i - window_length // 2:i + window_length // 2, :]

        data_window[np.isnan(data_window)] = 0.  # nans are replaced with zeros anyways

        if north_selection:
            useless_stations = np.where(coords[:, 0] < 47.)[0]
            data_window[useless_stations] = 0.
        if south_selection:
            useless_stations = np.where(coords[:, 0] > 47.)[0]
            data_window[useless_stations] = 0.
        running_test_set.append(data_window)

    running_test_set = np.array(running_test_set)
    detector_output = tf.squeeze(detector.predict(running_test_set))
    attention_output = tf.squeeze(interm_layer_model(running_test_set)[1])
    prediction_proba[window_length // 2:-window_length // 2] = detector_output
    attention_matrices[window_length // 2:-window_length // 2] = attention_output
    return prediction_proba, attention_matrices, selected_time_array


def inference_running_window(weight_code, name, north_selection=False, south_selection=False):
    params = {'n_stations': 135,
              'window_length': 60,
              'n_directions': 2,
              'batch_size': 128,
              'kernel_regularizer': None,
              'bias_regularizer': None,
              'embeddings_regularizer': None}

    detector = SSEdetector(**params)
    detector.build()
    detector.summary()

    detector.load_weights(os.path.join('weights', 'SSEdetector', f'best_cascadia_{name}_{weight_code}.h5'))

    intermediate_layer_model = tf.keras.Model(inputs=detector.get_model().input,
                                              outputs=detector.get_model().get_layer('transformer_layer').output)

    proba, attn_matrices, time = inference_continuous_window(detector.get_model(), intermediate_layer_model,
                                                             params['n_stations'], north_selection=north_selection,
                                                             south_selection=south_selection)

    np.savez(f'predictions/pred_cascadia_running_win_{name}_{weight_code}' + (
        '_north_selection' if north_selection else '') + ('_south_selection' if south_selection else ''), proba=proba,
             time=time, attn_matrices=attn_matrices)


if __name__ == '__main__':
    name = 'realgaps_ext_v3_0.001_6-7_135'
    weight_code = '01Aug2022-144844'
    north_selection = False
    south_selection = True
    inference_running_window(name, weight_code, north_selection=north_selection, south_selection=south_selection)
