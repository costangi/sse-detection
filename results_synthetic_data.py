import os.path

import numpy as np
from matplotlib import pyplot as plt
from mpl_toolkits.basemap import Basemap
from shapely.geometry import Point, Polygon as Poly
from sklearn.impute import SimpleImputer

from utils import recall_as_function_of, get_cascadia_box, read_from_slab2, _train_contour, grid

plt.rc('font', family='Helvetica')

SMALL_SIZE = 11
MEDIUM_SIZE = 13
BIGGER_SIZE = 15

plt.rc('font', size=SMALL_SIZE)  # controls default text sizes
plt.rc('axes', titlesize=SMALL_SIZE)  # fontsize of the axes title
plt.rc('axes', labelsize=MEDIUM_SIZE)  # fontsize of the x and y labels
plt.rc('xtick', labelsize=SMALL_SIZE)  # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)  # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)  # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title


def map_figure(region, resolution_factor, station_coordinates, draw_gnss_network=True, draw_heatmap=True,
               draw_isodepth=True, fig_directory='sse_figures', thresh=0.8):
    cascadia_box = [40 - 1.5, 51.8 - 0.2, -128.3 - 2.5, -121 + 2]  # min/max_latitude, min/max_longitude
    if draw_heatmap:
        # we discretize the lat/lon and we compute Mw_threshold for each square
        box_size_lat = int(round(region[1] - region[0] + 1) * resolution_factor)
        box_size_lon = int(round(region[3] - region[2] + 1) * resolution_factor)
        quant_lat = np.linspace(region[0], region[1], box_size_lat)
        quant_lon = np.linspace(region[2], region[3], box_size_lon)
        mean_lat, mean_lon = [], []  # middle point of each patch
        mw_threshold = []
        for i in range(len(quant_lat) - 1):
            for j in range(len(quant_lon) - 1):
                idx_box = np.where(
                    np.logical_and(np.logical_and(cat[:, 0] > quant_lat[i], cat[:, 0] < quant_lat[i + 1]),
                                   np.logical_and(cat[:, 1] > quant_lon[j], cat[:, 1] < quant_lon[j + 1])))[0]
                mw_true_pos_bin_mean, percentage_true_pos_bin_mw = recall_as_function_of(y_test[idx_box],
                                                                                         y_pred[idx_box],
                                                                                         cat[:, 3][idx_box], N_BINS=20)
                if np.count_nonzero(~np.isnan(percentage_true_pos_bin_mw)) > len(percentage_true_pos_bin_mw) * 0.2:
                    percentage_true_pos_bin_mw = SimpleImputer(missing_values=np.nan, strategy='median').fit_transform(
                        np.array(percentage_true_pos_bin_mw).reshape(-1, 1))
                percentage_true_pos_bin_mw = np.array(percentage_true_pos_bin_mw)
                idx_thresh_condition = np.where(percentage_true_pos_bin_mw > thresh)[0]
                if len(idx_thresh_condition) > 0:
                    mw_threshold.append(mw_true_pos_bin_mean[np.min(idx_thresh_condition)])
                else:
                    mw_threshold.append(np.nan)
                mean_lon.append(0.5 * (quant_lon[j] + quant_lon[j + 1]))
                mean_lat.append(0.5 * (quant_lat[i] + quant_lat[i + 1]))
        X, Y, Z = grid(mean_lon, mean_lat, mw_threshold, resX=300, resY=300, method='cubic')

        train_poly_x, train_poly_y = _train_contour(cat)
        train_poly = Poly(np.vstack((train_poly_x, train_poly_y)).T)
        mesh_points = np.vstack((X.ravel(), Y.ravel())).T
        flattened_Z = Z.ravel()
        for i, point in enumerate(mesh_points):
            if not Point(point).within(train_poly):
                flattened_Z[i] = np.nan
        Z = Z.reshape(X.shape)

    fig = plt.figure(figsize=(7.8, 7.8), dpi=100)
    cascadia_map = Basemap(llcrnrlon=cascadia_box[2], llcrnrlat=cascadia_box[0],
                           urcrnrlon=cascadia_box[3], urcrnrlat=cascadia_box[1],
                           lat_0=cascadia_box[0], lon_0=cascadia_box[2],
                           resolution='i', projection='lcc')  # , ax=ax9)

    cascadia_map.drawcoastlines(linewidth=0.5)
    cascadia_map.fillcontinents(color='bisque', lake_color='lightcyan')  # burlywood
    cascadia_map.drawmapboundary(fill_color='lightcyan')
    cascadia_map.drawmapscale(-122.5, 51.05, -122.5, 51.05, 300, barstyle='fancy', zorder=10)
    cascadia_map.drawparallels(np.arange(cascadia_box[0], cascadia_box[1], (cascadia_box[1] - cascadia_box[0]) / 4),
                               labels=[0, 1, 0, 0], linewidth=0.1)
    cascadia_map.drawmeridians(np.arange(cascadia_box[2], cascadia_box[3], (cascadia_box[3] - cascadia_box[2]) / 4),
                               labels=[0, 0, 0, 1], linewidth=0.1)
    cascadia_map.readshapefile('geo_data/tectonicplates-master/PB2002_boundaries', '', linewidth=0.3)

    if draw_isodepth:
        def isodepth_label_fmt(x):
            return rf"{int(x)} km"

        levels = [20, 40]
        admissible_depth, _, _, region = read_from_slab2()
        x, y = admissible_depth[:, 0], admissible_depth[:, 1]
        depth = admissible_depth[:, 2]
        x_map, y_map = cascadia_map(x, y)
        isodepth = plt.tricontour(x_map, y_map, -depth, levels=levels, colors='black', linewidths=0.7)
        label_loc_y = [48.3, 41.7]  # [48.3, 48.4]
        label_loc_x = [-125.5, -123.0]  # [-125.5, -124.5]
        label_loc_x_map, label_loc_y_map = cascadia_map(label_loc_x, label_loc_y)
        plt.clabel(isodepth, isodepth.levels, inline=True, fontsize=10, fmt=isodepth_label_fmt,
                   manual=list(zip(label_loc_x_map, label_loc_y_map)))

    if draw_heatmap:
        XX, YY = cascadia_map(X, Y)
        cmap = plt.cm.get_cmap('turbo')
        # pcm = cascadia_map.pcolormesh(XX, YY, Z, shading='auto', cmap=cmap, alpha=1., antialiased=True)
        pcm = cascadia_map.pcolormesh(XX, YY, Z, cmap=cmap, alpha=1., antialiased=True,
                                      shading='gouraud', rasterized=True)
        cbar = plt.colorbar(pcm, orientation='horizontal', fraction=0.018, pad=0.065, shrink=0.6,
                            ticks=[6.2, 6.5, 6.8])  # fraction=0.046, pad=0.04)
        cbar.ax.set_xlabel('$M_w$ threshold', labelpad=5)

    if draw_gnss_network:
        cascadia_map.scatter(station_coordinates[:, 1], station_coordinates[:, 0], marker='^', s=15, color='C3',
                             latlon=True, edgecolors='black', linewidth=0.8)

    ax = fig.gca()
    for axis in ['top', 'bottom', 'left', 'right']:
        ax.spines[axis].set_linewidth(0.5)
    plt.savefig(f'{fig_directory}/map_thresh.pdf')
    plt.close(fig)


if __name__ == '__main__':

    data_limit = 100000
    extended_option = True
    res_factor = 2

    fig_directory = f'./figures'
    if not os.path.isdir(fig_directory):
        os.mkdir(fig_directory)

    with np.load(
            f'pred_cascadia_realgaps_ext_v3_0.001_6-7_135_01Aug2022-144844_2018_2023.npz') as f:
        y_test, y_pred, templates, cat, dur = f['y_test'], f['y_pred'], f['synth_disp'], f['catalogue'], f['durations']

    with np.load(
            f'det_synth_ts_cascadia_TEST_realgaps_extended_v3_135stations_5.5_7_2018_2023.npz') as f:
        station_coordinates = f['station_coordinates']

    templates = templates[:data_limit]
    cat = cat[:data_limit]

    y_pred[y_pred >= 0.5] = 1
    y_pred[y_pred < 0.5] = 0

    n_samples = cat.shape[0]
    ind_test = int(n_samples * 0.9) if not extended_option else 0

    cat_test, templates_test = cat[ind_test:, :], templates[ind_test:, :, :]

    tpr_threshold = 0.7
    resolution = 2

    map_figure(get_cascadia_box(), resolution, station_coordinates, draw_gnss_network=True, draw_heatmap=True,
               draw_isodepth=True, thresh=tpr_threshold)

    fig, ax1 = plt.subplots()
    mw_true_pos_bin_mean, percentage_true_pos_bin_mw = recall_as_function_of(y_test, y_pred, cat_test[:, 3])
    plt.plot(mw_true_pos_bin_mean, percentage_true_pos_bin_mw)
    # plt.ylabel('% true positives (# true pos in a bin / #true pos)')
    plt.ylabel('Global true positive ratio (# true positives / positives)')
    plt.xlabel('Mw')
    plt.savefig(f'{fig_directory}/tpr_mw.pdf')
    plt.close(fig)

    # TPR as function of duration
    mw_ranges = [(5.5, 6.), (6., 6.5), (6.5, 7.)]
    fig, ax1 = plt.subplots(dpi=100)
    for mw_range in mw_ranges:
        idx_mw = np.logical_and(cat_test[:, 3] >= mw_range[0], cat_test[:, 3] < mw_range[1])
        dur_true_pos_bin_mean, percentage_true_pos_bin_dur = recall_as_function_of(y_test[idx_mw], y_pred[idx_mw],
                                                                                   dur[idx_mw], N_BINS=20)
        plt.plot(dur_true_pos_bin_mean, percentage_true_pos_bin_dur, label=f'$M_w \in ({mw_range[0]}, {mw_range[1]})$')
        # plt.ylabel('% true positives (# true pos in a bin / #true pos)')
    plt.ylabel('Global true positive ratio (# true positives / positives)')
    plt.xlabel('SSE duration [days]')
    plt.legend()
    plt.savefig(f'{fig_directory}/tpr_dur.pdf')
    plt.close(fig)
