import datetime
import multiprocessing
import os

import numpy as np
import tensorflow as tf

from sse_detector.positional_embedding import PositionalEmbedding
from sse_detector.tensorboard_callbacks import PlotsCallback, MetricsCallback
from sse_detector.transformer_utils import _transformer


class SSEdetector:
    def __init__(self, **kwargs):
        self.model = None
        self.n_stations = kwargs['n_stations']
        self.window_length = kwargs['window_length']
        self.n_directions = kwargs['n_directions']
        self.batch_size = kwargs['batch_size']
        self.n_epochs = kwargs['n_epochs']
        self.initial_learning_rate = kwargs['learning_rate']
        self.kernel_regularizer = kwargs['kernel_regularizer']
        self.bias_regularizer = kwargs['bias_regularizer']
        self.embedding_regularizer = kwargs['embedding_regularizer']
        self.activation = 'relu'
        self.initializer = tf.keras.initializers.HeUniform()
        self.input_shape = (self.n_stations, self.window_length, self.n_directions)
        self.kernels = [[5, 5], [5, 5], [3, 3]]
        self.pool_size = (2, 1)
        self.embed_dim = 1458  # fan out of last layer before Embedding
        self.transformer_drop_rate = 0.1
        self.conv_dropout_rate = 0.2
        self.use_dropout = False
        self.dropout_at_test = False
        self.use_batch_norm = True
        self.callback_list = None
        self.train_verbosity_level = kwargs['verbosity'] if 'verbosity' in kwargs else 0
        self.patience = kwargs['patience'] if 'patience' in kwargs else 50

    def _conv_layer(self, x, filters, kernel_size, name, use_bn=False, use_dropout=False, activ=None,
                    dropout_at_test=False,
                    initializer=None, kernel_regularizer=None, bias_regularizer=None, dropout_rate=0.1):
        """
        Convolution layer > batch normalisation > activation > dropout. From (Van Den Ende, Ampuero, 2020).
        """
        use_bias = True
        if use_bn:
            use_bias = False
        x = tf.keras.layers.Conv2D(
            filters=filters, kernel_size=kernel_size, padding="same",
            activation=None, kernel_initializer=initializer,
            use_bias=use_bias, name=name, kernel_regularizer=kernel_regularizer, bias_regularizer=bias_regularizer
        )(x)
        if use_bn:
            x = tf.keras.layers.BatchNormalization()(x)
        if activ is not None:
            x = tf.keras.layers.Activation(activ)(x)
        if use_dropout:
            x = tf.keras.layers.SpatialDropout2D(dropout_rate)(x, training=dropout_at_test)
            # x = tf.keras.layers.SpatialDropout2D(dropout_rate)(x)
        return x

    def _residual_CNN_bn_block(self, inpC, filters, ker, drop_rate, activation, padding):
        """From Mousavi et al., 2020"""
        prev = inpC
        layer_1 = tf.keras.layers.BatchNormalization()(prev)
        act_1 = tf.keras.layers.Activation(activation)(layer_1)
        # act_1 = tf.keras.layers.SpatialDropout1D(drop_rate)(act_1)
        act_1 = tf.keras.layers.SpatialDropout2D(drop_rate)(act_1)
        conv_1 = tf.keras.layers.Conv2D(filters, ker, padding=padding)(act_1)

        layer_2 = tf.keras.layers.BatchNormalization()(conv_1)
        act_2 = tf.keras.layers.Activation(activation)(layer_2)
        # act_2 = tf.keras.layers.SpatialDropout1D(drop_rate)(act_2)
        act_2 = tf.keras.layers.SpatialDropout2D(drop_rate)(act_2)
        conv_2 = tf.keras.layers.Conv2D(filters, ker, padding=padding)(act_2)

        res_out = tf.keras.layers.add([prev, conv_2])

        return res_out

    def _feature_extractor(self, input_data):
        use_dropout = True
        use_bn = True
        x = input_data
        n_feature_maps = self.n_directions
        for i in range(3):
            for j in range(2):
                n_feature_maps = n_feature_maps * 3
                x = self._conv_layer(
                    x, filters=n_feature_maps, kernel_size=self.kernels[i], name=f'convBlock{3 * i + j + 1}', use_bn=use_bn,
                    use_dropout=use_dropout, kernel_regularizer=self.kernel_regularizer,
                    bias_regularizer=self.bias_regularizer,
                    activ=self.activation, dropout_at_test=self.dropout_at_test, initializer=self.initializer,
                    dropout_rate=self.conv_dropout_rate
                )
                # Downsampling for space dimension
                x = tf.keras.layers.MaxPool2D(pool_size=self.pool_size)(x)
            # Residual block
            self._residual_CNN_bn_block(x, n_feature_maps, self.kernels[i], self.conv_dropout_rate, self.activation, "same")
        return x


    def _lr_schedule(self, epoch):
        """Learning rate is scheduled to be reduced after 40, 60, 80, 90 epochs (Mousavi et al., 2020)."""
        lr = self.initial_learning_rate
        if epoch > 90:
            lr *= 0.5e-3
        elif epoch > 60:
            lr *= 1e-3
        elif epoch > 40:
            lr *= 1e-2
        elif epoch > 20:
            lr *= 1e-1
        print('Learning rate: ', lr)
        return lr

    def _callbacks(self, stagename, val_idx=None, X_val=None, y_val=None, val_cat=None,
                   templates_val=None, batch_size=None, f_pointer=None):
        base_checkpoint_path, base_weight_path = 'models', 'weights'

        checkpoint_path = os.path.join(base_checkpoint_path, '2DCNN')
        weight_path = os.path.join(base_weight_path, '2DCNN')

        for path in [base_checkpoint_path, base_weight_path, checkpoint_path, weight_path]:
            if not os.path.exists(path):
                os.makedirs(path)

        # log_dir = "logs/fit/cascadia" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
        date_string = datetime.datetime.now().strftime("%d%b%Y-%H%M%S")
        log_dir = "logs/fit/cascadia" + date_string + f'_{stagename}'

        plots_callback = PlotsCallback(log_dir, self.model, f_pointer=f_pointer, val_idx=val_idx,
                                       X_val=X_val, y_val=y_val, val_cat=val_cat, templates_val=templates_val,
                                       batch_size=batch_size)
        metrics_callback = MetricsCallback(log_dir, self.model, f_pointer=f_pointer, val_idx=val_idx, X_val=X_val,
                                           y_val=y_val, batch_size=batch_size)
        tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)
        checkpoint_callback_best = tf.keras.callbacks.ModelCheckpoint(
            os.path.join(weight_path, f'best_cascadia_{date_string}_{stagename}.h5'),
            save_weights_only=True,
            save_best_only=True)
        checkpoint_callback_last = tf.keras.callbacks.ModelCheckpoint(
            os.path.join(weight_path, f'last_cascadia_{date_string}_{stagename}.h5'),
            save_weights_only=True,
            save_best_only=False)
        checkpoint_callback_last_epoch_model = tf.keras.callbacks.ModelCheckpoint(
            filepath=os.path.join(checkpoint_path, f'2DCNN.{stagename}' + '.{epoch:02d}.{val_loss:.4f}.hdf5'),
            monitor='val_loss', save_best_only=True, save_weights_only=False)
        early_stopping_monitor = tf.keras.callbacks.EarlyStopping(monitor='val_loss', patience=self.patience)
        lr_scheduler = tf.keras.callbacks.LearningRateScheduler(self._lr_schedule)
        lr_reducer = tf.keras.callbacks.ReduceLROnPlateau(factor=np.sqrt(0.1), cooldown=0, patience=self.patience - 2,
                                                          min_lr=0.5e-6)
        callbacks = [checkpoint_callback_best, checkpoint_callback_last, tensorboard_callback,
                     checkpoint_callback_last_epoch_model, early_stopping_monitor, lr_scheduler, lr_reducer,
                     metrics_callback, plots_callback]
        return callbacks

    def build(self):
        """2D Convolutional Neural Network for SSE detection."""
        inputs = tf.keras.Input(shape=self.input_shape)
        x = inputs
        x = self._feature_extractor(x)
        x = tf.keras.layers.Lambda(lambda x: tf.reduce_max(x, axis=1, keepdims=False))(x)  # max reduction over stations

        x = PositionalEmbedding(self.window_length, self.embed_dim,
                                embeddings_regularizer=self.embedding_regularizer)(x)
        x, _ = _transformer(self.transformer_drop_rate, None, 'transformer_layer', x,
                            kernel_regularizer=self.kernel_regularizer,
                            bias_regularizer=self.bias_regularizer)

        x = tf.keras.layers.GlobalAveragePooling1D()(x)

        outputs = tf.keras.layers.Dense(1, activation="sigmoid", kernel_initializer='he_uniform',
                                        kernel_regularizer=self.kernel_regularizer,
                                        bias_regularizer=self.bias_regularizer)(x)
        self.model = tf.keras.Model(inputs, outputs)

    def summary(self):
        self.model.summary()

    def compile(self):
        self.model.compile(
            optimizer=tf.keras.optimizers.Adam(learning_rate=self._lr_schedule(0)),
            loss='binary_crossentropy'
        )

    def set_callbacks(self, train_codename, val_indices, X_val, y_val, cat_val, templates_val):
        self.callback_list = self._callbacks(train_codename, val_indices, X_val=X_val, y_val=y_val,
                                             val_cat=cat_val,
                                             templates_val=templates_val, batch_size=self.batch_size)

    def train(self, X_train, X_val, y_train, y_val):
        try:
            self.model.fit(X_train, y_train,
                           validation_data=(X_val, y_val),
                           callbacks=self.callback_list,
                           verbose=self.train_verbosity_level, epochs=self.n_epochs,
                           batch_size=self.batch_size,
                           use_multiprocessing=True,
                           workers=multiprocessing.cpu_count() - 1
                           )

        except KeyboardInterrupt:
            print("Training of 2D-CNN interrupted and completed")

    def load_weights(self, weight_path):
        self.model.load_weights(weight_path)

    def infer(self, X_test):
        return self.model.predict(X_test)

    def get_model(self):
        return self.model
