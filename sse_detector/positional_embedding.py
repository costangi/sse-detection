import tensorflow as tf


class PositionalEmbedding(tf.keras.layers.Layer):
    def __init__(self, sequence_length, output_dim, embeddings_regularizer=None, **kwargs):
        super().__init__(**kwargs)
        self.position_embeddings = tf.keras.layers.Embedding(
            input_dim=sequence_length, output_dim=output_dim,
            embeddings_initializer='he_uniform',
            embeddings_regularizer=embeddings_regularizer
        )
        self.sequence_length = sequence_length
        self.output_dim = output_dim

    def get_config(self):
        config = super().get_config().copy()
        config.update({
            'position_embeddings': self.position_embeddings,
            'sequence_length': self.sequence_length,
            'output_dim': self.output_dim,
        })
        return config

    def call(self, inputs):
        # The inputs are of shape: `(batch_size, frames, num_features)`
        length = tf.shape(inputs)[1]
        positions = tf.range(start=0, limit=length, delta=1)
        embedded_positions = self.position_embeddings(positions)
        return inputs + embedded_positions

    def compute_mask(self, inputs, mask=None):
        mask = tf.reduce_any(tf.cast(inputs, "bool"), axis=-1)
        return mask
