import numpy as np
import tensorflow as tf
from sklearn.metrics import precision_score, recall_score, f1_score

from utils import plot_confusion_matrix, plot_roc_curve, plot_to_image, plot_recall_function_of


class PlotsCallback(tf.keras.callbacks.Callback):
    def __init__(self, log_dir, model, f_pointer=None, val_idx=None, X_val=None, y_val=None, val_cat=None,
                 templates_val=None, batch_size=None):
        self.file_writer_img = tf.summary.create_file_writer(log_dir + '/img')
        self.model = model
        self.f_pointer = f_pointer
        self.val_idx = val_idx
        if self.f_pointer is not None:
            self.X_val = f_pointer['synthetic_data'][val_idx]
            y_val = np.ones(len(val_idx))
            y_val[np.where(f_pointer['random_durations'][val_idx] == 0.)[0]] = 0.
        else:
            self.X_val = X_val
            self.val_cat = val_cat
        self.y_val = y_val
        if val_cat is not None:
            self.D = np.empty((self.y_val.shape[0],))
            self.D.fill(np.nan)
            for i in range(self.D.shape[0]):
                if self.y_val[i] == 1.:
                    self.D[i] = np.sum(np.sqrt(templates_val[i, :, 0] ** 2 + templates_val[i, :, 1] ** 2))
        if batch_size is not None:
            self.batch_size = batch_size
        super(PlotsCallback, self).__init__()

    def on_epoch_end(self, epoch, logs=None):
        val_pred_proba = self.model.predict(self.X_val, batch_size=self.batch_size)
        val_pred = np.zeros(val_pred_proba.shape)
        val_pred[val_pred_proba >= 0.5] = 1
        # Log the confusion matrix as an image summary.
        with self.file_writer_img.as_default():
            figure_cm = plot_confusion_matrix(self.y_val, val_pred)
            tf.summary.image("Confusion Matrix", plot_to_image(figure_cm), step=epoch)
            figure_roc = plot_roc_curve(self.y_val, val_pred_proba)
            tf.summary.image("ROC curve", plot_to_image(figure_roc), step=epoch)
            figure_mw = plot_recall_function_of(self.y_val, val_pred, self.val_cat[:, 3], 'Mw')
            tf.summary.image("Recall as function of Mw", plot_to_image(figure_mw), step=epoch)
            figure_D = plot_recall_function_of(self.y_val, val_pred, self.D, 'GNSS power')
            tf.summary.image("Recall as function of GNSS power", plot_to_image(figure_D), step=epoch)


class MetricsCallback(tf.keras.callbacks.Callback):
    def __init__(self, log_dir, model, f_pointer=None, val_idx=None, X_val=None, y_val=None, batch_size=None):
        self.file_writer = tf.summary.create_file_writer(log_dir + "/metrics")
        self.file_writer.set_as_default()
        self.model = model
        self.f_pointer = f_pointer
        self.val_idx = val_idx
        if f_pointer is not None:
            self.X_val = f_pointer['synthetic_data'][val_idx]
            y_val = np.ones(len(val_idx))
            y_val[np.where(f_pointer['random_durations'][val_idx] == 0.)[0]] = 0.
        else:
            self.X_val = X_val
        self.y_val = y_val
        if batch_size is not None:
            self.batch_size = batch_size
        super(MetricsCallback, self).__init__()

    def on_epoch_end(self, epoch, logs=None):
        val_pred_proba = self.model.predict(self.X_val, batch_size=self.batch_size)
        val_pred = np.zeros(val_pred_proba.shape)
        val_pred[val_pred_proba >= 0.5] = 1
        prec = precision_score(self.y_val, val_pred, average='binary')
        rec = recall_score(self.y_val, val_pred, average='binary')
        f1 = f1_score(self.y_val, val_pred, average='binary')
        tf.summary.scalar('precision', data=prec, step=epoch)
        tf.summary.scalar('recall', data=rec, step=epoch)
        tf.summary.scalar('F1 score', data=f1, step=epoch)
