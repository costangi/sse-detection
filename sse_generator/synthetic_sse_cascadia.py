"""
Script used to model a synthetic slow slip event in the Cascadia subduction zone.
"""
import multiprocessing

import numpy as np
from joblib import Parallel, delayed

from utils import read_from_slab2
from .okada import forward as okada85


def _synthetic_displacement_stations_cascadia(i, depth_list, strike_list, dip_list, station_coordinates, **params):
    """Generates one set of synthetic displacements by applying Okada model (Okada, 1985) on an event placed on
    the slab (slab2) of the Cascadia subduction, registered at a given set of stations. The slab information
    is passed in input as {depth_list}, {strike_list} and {dip_list}"""
    random_idx_slab = np.random.randint(low=0, high=depth_list.shape[0])
    epi_lat = depth_list[random_idx_slab, 1]
    epi_lon = depth_list[random_idx_slab, 0]
    hypo_depth = - depth_list[random_idx_slab, 2]  # add +-10km  # opposite sign for positive depths (Okada, 1985)
    depth_variability = -10 + 20 * params['uniform_vector'][i, 0]
    if hypo_depth > 14.6:
        hypo_depth = hypo_depth + depth_variability
    if hypo_depth < 0:
        raise Exception('Negative depth')
    strike = strike_list[random_idx_slab, 2]
    dip = dip_list[random_idx_slab, 2]
    rake = 75 + 25 * params['uniform_vector'][i, 1]  # rake from 75 to 100 deg
    min_mw, max_mw = 5, 7
    if 'magnitude_range' in params:
        min_mw, max_mw = params['magnitude_range']
    Mw = min_mw + (max_mw - min_mw) * params['uniform_vector'][i, 2]
    Mo = 10 ** (1.5 * Mw + 9.1)
    stress_drop = params['lognormal_vector'][i]
    R = (7 / 16 * Mo / stress_drop) ** (1 / 3)
    u = 16 / (7 * np.pi) * stress_drop / 30e09 * R * 10 ** 3  # converted in mm in order to have displacement in mm
    L = np.sqrt(2 * np.pi) * R  # meters
    W = L / 2
    L = L * 10 ** (-3)  # conversion in km and then in lat, lon (suppose 1 degree ~ 100 km) for okada
    W = W * 10 ** (-3)
    displacement = okada85((station_coordinates[:, 1] - epi_lon) * 111.3194,
                           (station_coordinates[:, 0] - epi_lat) * 111.3194, 0, 0,
                           hypo_depth + W / 2 * np.sin(np.deg2rad(dip)), L, W, u, 0, strike, dip, rake)
    return displacement, epi_lat, epi_lon, hypo_depth, Mw, strike, dip, rake, u, stress_drop


def synthetic_displacements_stations_cascadia(n, station_coordinates, **kwargs):
    """Generates {n} synthetic displacements by means of multiprocessing."""
    if 'max_depth' in kwargs:
        admissible_depth, admissible_strike, admissible_dip, region = read_from_slab2(max_depth=kwargs['max_depth'])
    else:
        admissible_depth, admissible_strike, admissible_dip, region = read_from_slab2()
    uniform_vector = np.random.uniform(0, 1, (n, 3))
    max_stress_drop = 0.1 * 1e06  # Gao et al., 2012
    min_stress_drop = 0.01 * 1e06  # Gao et al., 2012
    mean_stress_drop = 0.5 * (max_stress_drop + min_stress_drop)
    variation_factor = 10
    std_underlying_normal = np.sqrt(np.log(variation_factor ** 2 + 1))  # from coefficient of variation
    mean_underlying_normal = np.log(mean_stress_drop) - std_underlying_normal ** 2 / 2
    lognormal_vector = np.random.lognormal(mean_underlying_normal, std_underlying_normal, (n,))
    n_stations = station_coordinates.shape[0]
    disp_stations = np.zeros((n, n_stations, 3))
    catalogue = np.zeros((n, 9))
    results = Parallel(n_jobs=multiprocessing.cpu_count(), verbose=True)(
        delayed(_synthetic_displacement_stations_cascadia)(i, admissible_depth, admissible_strike, admissible_dip,
                                                           station_coordinates, uniform_vector=uniform_vector,
                                                           lognormal_vector=lognormal_vector, **kwargs) for i in
        range(n))
    for i in range(n):
        for direction in range(3):
            disp_stations[i, :, direction] = results[i][0][direction]
        catalogue[i, :] = results[i][1:]
    return disp_stations, catalogue


def synthetic_displacements_stations_cascadia_sequential(n, station_coordinates, **kwargs):
    """Generates {n} synthetic displacements by means of multiprocessing."""
    admissible_depth, admissible_strike, admissible_dip, region = read_from_slab2()
    uniform_vector = np.random.uniform(0, 1, (n, 3))
    max_stress_drop = 0.1 * 1e06  # Gao et al., 2012
    min_stress_drop = 0.01 * 1e06  # Gao et al., 2012
    mean_stress_drop = 0.5 * (max_stress_drop + min_stress_drop)
    variation_factor = 10
    std_underlying_normal = np.sqrt(np.log(variation_factor ** 2 + 1))  # from coefficient of variation
    mean_underlying_normal = np.log(mean_stress_drop) - std_underlying_normal ** 2 / 2
    lognormal_vector = np.random.lognormal(mean_underlying_normal, std_underlying_normal, (n,))
    n_stations = station_coordinates.shape[0]
    disp_stations = np.zeros((n, n_stations, 3))
    catalogue = np.zeros((n, 9))

    for i in range(n):
        results_i = _synthetic_displacement_stations_cascadia(i, admissible_depth, admissible_strike, admissible_dip,
                                                              station_coordinates, uniform_vector=uniform_vector,
                                                              lognormal_vector=lognormal_vector, **kwargs)
        for direction in range(3):
            disp_stations[i, :, direction] = results_i[0][direction]
        catalogue[i, :] = results_i[1:]

    return disp_stations, catalogue


def _sigmoid(x, alpha, beta, x0):
    return alpha / (1 + np.exp(-beta * (x - x0)))


def sigmoidal_rise_time(t, regime_value, duration, center, tol=0.01):
    gamma = regime_value * tol
    beta = 2 / duration * np.log((regime_value - gamma) / gamma)
    return _sigmoid(t, regime_value, beta, center)


def synthetic_sses(n_samples, window_length, station_codes, station_coordinates, **kwargs):
    """Computes {n_samples} SSE signals (sigmoid) for each station in Cascadia. Returns the SSE signals as well as
    the catalogued epicenters, static offsets and durations of each event."""
    min_days, max_days = 10, 30
    center = 30
    synthetic_displacement, catalogue = synthetic_displacements_stations_cascadia(n_samples, station_coordinates,
                                                                                  **kwargs)
    transients = np.zeros((n_samples, len(station_codes), window_length, 2))
    random_durations = np.random.randint(low=min_days, high=max_days, size=n_samples)
    transient_time_array = np.linspace(0, window_length, window_length)
    for sample in range(n_samples):
        for station in range(len(station_codes)):
            for direction in range(2):
                transients[sample, station, :, direction] = sigmoidal_rise_time(transient_time_array,
                                                                                synthetic_displacement[
                                                                                    sample, station, direction],
                                                                                random_durations[sample], center)
    return transients, random_durations, synthetic_displacement, catalogue


def synthetic_sses_sequential(n_samples, window_length, station_codes, station_coordinates, **kwargs):
    """Computes {n_samples} SSE signals (sigmoid) for each station in Cascadia. Returns the SSE signals as well as
    the catalogued epicenters, static offsets and durations of each event."""
    min_days, max_days = 10, 30
    center = 30
    synthetic_displacement, catalogue = synthetic_displacements_stations_cascadia_sequential(n_samples,
                                                                                             station_coordinates,
                                                                                             **kwargs)
    transients = np.zeros((n_samples, len(station_codes), window_length, 2))
    random_durations = np.random.randint(low=min_days, high=max_days, size=n_samples)
    transient_time_array = np.linspace(0, window_length, window_length)
    for sample in range(n_samples):
        for station in range(len(station_codes)):
            for direction in range(2):
                transients[sample, station, :, direction] = sigmoidal_rise_time(transient_time_array,
                                                                                synthetic_displacement[
                                                                                    sample, station, direction],
                                                                                random_durations[sample], center)
    return transients, random_durations, synthetic_displacement, catalogue
