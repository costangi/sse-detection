import numpy as np

from sse_generator.artificial_noise_cascadia import artificial_noise_full_randomized_v2
from sse_generator.synthetic_sse_cascadia import synthetic_sses


def synthetic_time_series_real_gaps_extended_v3(n_samples, n_selected_stations, detection, window_length=60, **kwargs):
    """Equivalent to synthetic_time_series_real_gaps_extended_v2() but we use artificial_noise_full_randomized_v2()
    for the noise generation."""
    periods = [(2007, 2014), (2018, 2023)]
    synthetic_data_all, random_durations_all, catalogue_all, synthetic_displacement_all = np.zeros(
        (n_samples, n_selected_stations, window_length, 2)), np.zeros((n_samples,)), np.zeros((n_samples, 9)), np.zeros(
        (n_samples, n_selected_stations, 3))
    old_n_samples = n_samples // 2  # we collect half of the samples for each reference period
    insert_idx = [(0, old_n_samples), (old_n_samples, 2 * old_n_samples)]
    for i, reference_period in enumerate(periods):
        n_samples = old_n_samples
        noise_windows, station_codes, station_coordinates = artificial_noise_full_randomized_v2(n_samples,
                                                                                                n_selected_stations,
                                                                                                window_length,
                                                                                                reference_period)
        if detection:
            n_samples = n_samples // 2
        transients, random_durations, synthetic_displacement, catalogue = synthetic_sses(n_samples, window_length,
                                                                                         station_codes,
                                                                                         station_coordinates,
                                                                                         **kwargs)
        if detection:
            ind_detection = np.sort(np.random.choice([i for i in range(n_samples * 2)], size=n_samples, replace=False))
            synthetic_data = noise_windows.copy()
            synthetic_data[ind_detection] = noise_windows[ind_detection] + transients
            synthetic_data = np.nan_to_num(synthetic_data, nan=0.)  # NaNs are put back to zero
            extended_catalogue = np.zeros((catalogue.shape[0] * 2, catalogue.shape[1]))
            extended_durations = np.zeros((random_durations.shape[0] * 2,))
            extended_synthetic_displacement = np.zeros(
                (synthetic_displacement.shape[0] * 2, synthetic_displacement.shape[1], synthetic_displacement.shape[2]))
            extended_catalogue[ind_detection] = catalogue
            extended_durations[ind_detection] = random_durations
            extended_synthetic_displacement[ind_detection] = synthetic_displacement
            catalogue = extended_catalogue  # [shuffling_sequence]
            random_durations = extended_durations  # [shuffling_sequence]
            synthetic_displacement = extended_synthetic_displacement  # [shuffling_sequence]
        else:
            synthetic_data = noise_windows + transients
            synthetic_data = np.nan_to_num(synthetic_data, nan=0.)  # NaNs are put back to zero
        synthetic_data_all[insert_idx[i][0]:insert_idx[i][1]] = synthetic_data
        random_durations_all[insert_idx[i][0]:insert_idx[i][1]] = random_durations
        catalogue_all[insert_idx[i][0]:insert_idx[i][1]] = catalogue
        synthetic_displacement_all[insert_idx[i][0]:insert_idx[i][1]] = synthetic_displacement
    rnd_idx = np.random.permutation(synthetic_displacement_all.shape[0])
    synthetic_data_all = synthetic_data_all[rnd_idx]
    random_durations_all = random_durations_all[rnd_idx]
    catalogue_all = catalogue_all[rnd_idx]
    synthetic_displacement_all = synthetic_displacement_all[rnd_idx]
    return synthetic_data_all, random_durations_all, catalogue_all, synthetic_displacement_all, station_codes, station_coordinates
