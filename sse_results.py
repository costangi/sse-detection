import datetime

import matplotlib
import numpy as np
from matplotlib import pyplot as plt
from matplotlib.colors import Normalize
from matplotlib.patches import FancyArrowPatch
from mpl_toolkits.basemap import Basemap
from scipy.interpolate import interp1d
from scipy.ndimage import gaussian_filter
from scipy.signal import peak_widths, find_peaks

from utils import cascadia_coordinates, sse_catalogue, ymd_decimal_year_lookup, \
    _find_nearest_val, tremor_catalogue, get_n_tremors_per_day, overlap_percentage, _preliminary_operations, \
    _remove_stations

plt.rc('font', family='Helvetica')

SMALL_SIZE = 11
MEDIUM_SIZE = 13
BIGGER_SIZE = 15

plt.rc('font', size=SMALL_SIZE)  # controls default text sizes
plt.rc('axes', titlesize=SMALL_SIZE)  # fontsize of the axes title
plt.rc('axes', labelsize=MEDIUM_SIZE)  # fontsize of the x and y labels
plt.rc('xtick', labelsize=SMALL_SIZE)  # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)  # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)  # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title


def _add_double_sided_arrow(axis, x0, y0, x1, y1, offset=0.1, size=15, color='C0'):
    positions = [[(x0 + offset, y0), (x1, y1)], [(x1 - offset, y1), (x0, y0)]]
    for pos_a, pos_b in positions:
        myArrow = FancyArrowPatch(posA=pos_a, posB=pos_b,
                                  arrowstyle='simple', color=color,
                                  mutation_scale=size, shrinkA=0, shrinkB=0)
        myArrow.set_clip_on(False)
        axis.add_artist(myArrow)


def data_normalization(data):
    return (data - np.min(data)) / (np.max(data) - np.min(data))


def _lagged_correlation(a, b, max_lag, upsampling_factor=1):
    x = np.linspace(-max_lag, max_lag, 2 * max_lag * upsampling_factor + 1, dtype=np.int_)
    corr = []
    for lag in x:
        p = np.corrcoef(a[max_lag + lag:a.size - (max_lag - lag)], b[max_lag:-max_lag])[0, 1]
        corr.append(p)
    corr = np.array(corr)
    return x, corr


def plot_comparison_durations(cat1, cat2, duration_thresh, north=False):
    filtered_cat1 = []
    filtered_cat2 = []
    overlap_percentages = []

    for i, event in enumerate(cat1):
        for j in range(cat2.shape[0]):
            overlap_percent = overlap_percentage(event, cat2[j].tolist())[0]
            if overlap_percent >= duration_thresh:
                filtered_cat1.append(event)
                filtered_cat2.append(cat2[j])
                overlap_percentages.append(overlap_percent)

    filtered_cat2 = np.array(filtered_cat2)
    filtered_cat1 = np.array(filtered_cat1)
    overlap_percentages = np.array(overlap_percentages)

    print(filtered_cat2)
    print(filtered_cat1)

    duration_cat2 = (filtered_cat2[:, 1] - filtered_cat2[:, 0]) * 365
    duration_cat1 = (filtered_cat1[:, 1] - filtered_cat1[:, 0]) * 365

    # plt.scatter(duration_cat2, duration_cat1, c=overlap_percentages, cmap='coolwarm')

    # cmap = matplotlib.colors.LinearSegmentedColormap.from_list("coolwarm", overlap_percentages)
    # cmappable = ScalarMappable(norm=Normalize(np.min(overlap_percentages), np.max(overlap_percentages)), cmap=cmap)
    fig = plt.figure(dpi=100)
    cmap = plt.cm.coolwarm
    # create normalization instance
    norm = matplotlib.colors.Normalize(np.min(overlap_percentages), np.max(overlap_percentages))
    # create a scalarmappable from the colormap
    sm = matplotlib.cm.ScalarMappable(cmap=cmap, norm=norm)
    sm.set_array([])
    last_marker_square_idx, last_marker_circle_idx = 0, 0
    for i in range(len(duration_cat2)):
        if 2014. <= filtered_cat1[i][0] < 2018.:
            marker = 's'
            last_marker_square_idx = i
        else:
            marker = 'o'
            last_marker_circle_idx = i
        plt.scatter(duration_cat2[i], duration_cat1[i], color=cmap(norm(overlap_percentages[i])), marker=marker)

    plt.scatter(duration_cat2[last_marker_square_idx], duration_cat1[last_marker_square_idx], color=cmap(1.),
                marker='s', label='2014-2017')
    # plt.scatter(duration_cat2[last_marker_circle_idx], duration_cat1[last_marker_circle_idx], color=cmap(1.), marker='o', label='2010-2013')
    plt.scatter(duration_cat2[last_marker_circle_idx], duration_cat1[last_marker_circle_idx], color=cmap(1.),
                marker='o', label='2007-2013')
    p1 = max(max(duration_cat1), max(duration_cat2))
    p2 = min(min(duration_cat1), min(duration_cat2))
    plt.plot([p1, p2], [p1, p2], alpha=0.5, color='black', linewidth=1, zorder=0)
    plt.xlabel('Durations [days] (Michel et al., 2019)')
    plt.ylabel('Durations [days] (ours)')
    # cbar = plt.colorbar()
    cbar = plt.colorbar(sm)
    cbar.ax.set_ylabel('Amount of overlap (%)')
    plt.gca().set_aspect('equal')
    plt.legend()
    plt.savefig('sse_figures/' + ('north_selection/' if north else '') + 'dur_michel.pdf')
    # plt.show()
    plt.close(fig)


def plot_comparison_duration_tremors(cat, time_array, tremors, rel_height=0.99, north=False):
    """Take duration on catalogue, extract the same time period on tremors, find a peak and find a duration
    over that peak on tremors. Finally compare the two."""
    tremor_durations = []
    valid_cat = []
    overlap_percentages = []
    for time_span in cat:
        print('time_span', time_span)
        time_idx = np.logical_and(time_array >= time_span[0], time_array <= time_span[1])
        current_tremor_window = tremors[time_idx]
        peaks, _ = find_peaks(current_tremor_window)
        if len(peaks) > 0:
            widths = peak_widths(current_tremor_window, peaks, rel_height=rel_height)
            # per ogni picco in widths, calcola il min a sx il max a dx e metti come durata
            start_time, end_time = time_array[time_idx][round(np.min(widths[2]))], time_array[time_idx][
                round(np.max(widths[3]))]
            print('SSE starts:', time_span[0], 'tremors start:', start_time, 'and end:', end_time)
            tremor_durations.append((end_time - start_time) * 365)
            valid_cat.append((time_span[1] - time_span[0]) * 365)
            # overlap_percent = overlap_percentage((start_time, end_time), time_span)[0]

            # overlap_percentages.append(overlap_percent)
            overlap_percentages.append((time_span[0] - start_time) * 365)

            '''plt.plot(time_array, tremors)
            plt.axvline(start_time)
            plt.axvline(end_time)
            plt.show()'''

    cmap = plt.cm.coolwarm
    fig = plt.figure(dpi=100)
    # create normalization instance
    norm = matplotlib.colors.Normalize(np.min(overlap_percentages), np.max(overlap_percentages))
    # create a scalarmappable from the colormap
    sm = matplotlib.cm.ScalarMappable(cmap=cmap, norm=norm)
    sm.set_array([])
    last_marker_square_idx, last_marker_circle_idx, last_marker_star_idx = 0, 0, 0
    for i in range(len(valid_cat)):
        if 2014. <= cat[i][0] < 2018.1:
            marker = 's'
            last_marker_square_idx = i
        # elif 2010. <= cat[i][0] < 2014.:
        elif 2007. <= cat[i][0] < 2014.:
            marker = 'o'
            last_marker_circle_idx = i
        else:
            marker = '*'
            last_marker_star_idx = i
        plt.scatter(tremor_durations[i], valid_cat[i], color=cmap(norm(overlap_percentages[i])), marker=marker)

    # X, Y, Z = density_estimation(tremor_durations, valid_cat)
    # plt.contour(X, Y, Z)
    # seaborn.kdeplot(tremor_durations, valid_cat, fill=False, levels=5, thresh=.2)

    plt.scatter(tremor_durations[last_marker_circle_idx], valid_cat[last_marker_circle_idx], color=cmap(1.),
                # marker='o', label='2010-2013')
                marker='o', label='2007-2013')
    plt.scatter(tremor_durations[last_marker_square_idx], valid_cat[last_marker_square_idx], color=cmap(1.),
                marker='s', label='2014-2017')
    plt.scatter(tremor_durations[last_marker_star_idx], valid_cat[last_marker_star_idx], color=cmap(1.),
                marker='*', label='2018-2022')

    p1 = max(max(valid_cat), max(tremor_durations))
    p2 = min(min(valid_cat), min(tremor_durations))
    plt.plot([p1, p2], [p1, p2], alpha=0.5, color='black', linewidth=1, zorder=0)
    plt.xlabel('Tremor durations [days]')
    plt.ylabel('SSE durations [days] (ours)')
    # cbar = plt.colorbar()
    cbar = plt.colorbar(sm)
    # cbar.ax.set_ylabel('Amount of overlap (%)')
    cbar.ax.set_ylabel('Time lag between tremor and SSE onsets [days]')
    plt.legend()
    # plt.gca().set_aspect(abs(plt.xlim()[1] - plt.xlim()[0]) / abs(plt.ylim()[1] - plt.ylim()[0]))
    plt.gca().set_aspect('equal')
    plt.savefig('sse_figures/' + ('north_selection/' if north else '') + 'dur_tremors.pdf')
    # plt.show()
    plt.close(fig)

    return valid_cat, tremor_durations


def plot_comparison_duration_tremors2(cat, time_array, tremors, rel_height=0.99, north=False):
    """Take duration on catalogue, extract the same time period on tremors, find a peak and find a duration
    over that peak on tremors. Finally compare the two."""
    tremor_durations = []
    valid_cat = []
    overlap_percentages = []
    tremor_search_offset = 2
    tremor_search_offset_date = tremor_search_offset / 365
    for time_span in cat:
        print('time_span', time_span)
        time_idx = np.logical_and(time_array >= time_span[0], time_array <= time_span[1])
        current_tremor_window = tremors[time_idx]
        peaks, _ = find_peaks(current_tremor_window)
        extended_time_idx = np.logical_and(time_array >= time_span[0] - tremor_search_offset_date,
                                           time_array <= time_span[1] + tremor_search_offset_date)
        extended_current_tremor_window = tremors[extended_time_idx]
        if len(peaks) > 0:
            widths = peak_widths(extended_current_tremor_window, peaks + tremor_search_offset, rel_height=rel_height)
            # per ogni picco in widths, calcola il min a sx il max a dx e metti come durata
            start_time, end_time = time_array[extended_time_idx][round(np.min(widths[2]))], \
                                   time_array[extended_time_idx][
                                       round(np.max(widths[3]))]
            print('SSE starts:', time_span[0], 'tremors start:', start_time, 'and end:', end_time)
            tremor_durations.append((end_time - start_time) * 365)
            valid_cat.append((time_span[1] - time_span[0]) * 365)
            # overlap_percent = overlap_percentage((start_time, end_time), time_span)[0]

            # overlap_percentages.append(overlap_percent)
            overlap_percentages.append((time_span[0] - start_time) * 365)

            '''plt.plot(time_array, tremors)
            plt.axvline(start_time)
            plt.axvline(end_time)
            plt.show()'''

    cmap = plt.cm.coolwarm
    fig = plt.figure(dpi=100)
    # create normalization instance
    norm = matplotlib.colors.Normalize(np.min(overlap_percentages), np.max(overlap_percentages))
    # create a scalarmappable from the colormap
    sm = matplotlib.cm.ScalarMappable(cmap=cmap, norm=norm)
    sm.set_array([])
    last_marker_square_idx, last_marker_circle_idx, last_marker_star_idx = 0, 0, 0
    for i in range(len(valid_cat)):
        if 2014. <= cat[i][0] < 2018.1:
            marker = 's'
            last_marker_square_idx = i
        # elif 2010. <= cat[i][0] < 2014.:
        elif 2007. <= cat[i][0] < 2014.:
            marker = 'o'
            last_marker_circle_idx = i
        else:
            marker = '*'
            last_marker_star_idx = i
        plt.scatter(tremor_durations[i], valid_cat[i], color=cmap(norm(overlap_percentages[i])), marker=marker)

    # X, Y, Z = density_estimation(tremor_durations, valid_cat)
    # plt.contour(X, Y, Z)
    # seaborn.kdeplot(tremor_durations, valid_cat, fill=False, levels=5, thresh=.2)

    plt.scatter(tremor_durations[last_marker_circle_idx], valid_cat[last_marker_circle_idx], color=cmap(1.),
                # marker='o', label='2010-2013')
                marker='o', label='2007-2013')
    plt.scatter(tremor_durations[last_marker_square_idx], valid_cat[last_marker_square_idx], color=cmap(1.),
                marker='s', label='2014-2017')
    plt.scatter(tremor_durations[last_marker_star_idx], valid_cat[last_marker_star_idx], color=cmap(1.),
                marker='*', label='2018-2022')

    p1 = max(max(valid_cat), max(tremor_durations))
    p2 = min(min(valid_cat), min(tremor_durations))
    plt.plot([p1, p2], [p1, p2], alpha=0.5, color='black', linewidth=1, zorder=0)
    plt.xlabel('Tremor durations [days]')
    plt.ylabel('SSE durations [days] (ours)')
    # cbar = plt.colorbar()
    cbar = plt.colorbar(sm)
    # cbar.ax.set_ylabel('Amount of overlap (%)')
    cbar.ax.set_ylabel('Time lag between tremor and SSE onsets [days]')
    plt.legend()
    # plt.gca().set_aspect(abs(plt.xlim()[1] - plt.xlim()[0]) / abs(plt.ylim()[1] - plt.ylim()[0]))
    plt.gca().set_aspect('equal')
    plt.savefig('sse_figures/' + ('north_selection/' if north else '') + 'dur_tremors2.pdf')
    # plt.show()
    plt.close(fig)

    return valid_cat, tremor_durations


def plot_comparison_duration_tremors_peaks(cat, proba, time_array, tremors, rel_height=0.99, north=False):
    """Take duration on catalogue, extract the same time period on tremors, find a peak and find a duration
    over that peak on tremors. Finally compare the two."""
    tremor_durations = []
    valid_cat = []
    # overlap_percentages = []
    peak_time_lags = []
    for time_span in cat:
        print('time_span', time_span)
        time_idx = np.logical_and(time_array >= time_span[0], time_array <= time_span[1])
        current_tremor_window = tremors[time_idx]
        current_proba_window = proba[time_idx]
        proba_peaks, _ = find_peaks(current_proba_window)
        tremor_peaks, _ = find_peaks(current_tremor_window)
        if len(tremor_peaks) > 0:  # if len(proba_peaks) > 0 : redudant
            widths = peak_widths(current_tremor_window, tremor_peaks, rel_height=rel_height)
            # per ogni picco in widths, calcola il min a sx il max a dx e metti come durata
            start_time, end_time = time_array[time_idx][round(np.min(widths[2]))], time_array[time_idx][
                round(np.max(widths[3]))]
            tremor_durations.append((end_time - start_time) * 365)
            valid_cat.append((time_span[1] - time_span[0]) * 365)
            # overlap_percent = overlap_percentage((start_time, end_time), time_span)[0]

            # overlap_percentages.append(overlap_percent)
            # overlap_percentages.append((time_span[0] - start_time) * 365)

            # print('calcolo time lag:', proba_peaks, current_proba_window[proba_peaks], np.argmax(current_proba_window[proba_peaks]), proba_peaks[np.argmax(current_proba_window[proba_peaks])], time_array[time_idx][proba_peaks[np.argmax(current_proba_window[proba_peaks])]])
            # print('time array:', time_array[time_idx])

            peak_time_lag = (time_array[time_idx][proba_peaks[np.argmax(current_proba_window[proba_peaks])]] -
                             time_array[time_idx][tremor_peaks[np.argmax(current_tremor_window[tremor_peaks])]]) * 365
            peak_time_lags.append(peak_time_lag)

    cmap = plt.cm.Spectral  # plt.cm.coolwarm
    plt.figure(dpi=100)
    # create normalization instance
    norm = matplotlib.colors.Normalize(np.min(peak_time_lags), np.max(peak_time_lags))
    # norm = matplotlib.colors.Normalize(-20, 20)
    # create a scalarmappable from the colormap
    sm = matplotlib.cm.ScalarMappable(cmap=cmap, norm=norm)
    sm.set_array([])
    last_marker_square_idx, last_marker_circle_idx, last_marker_star_idx = 0, 0, 0
    for i in range(len(valid_cat)):
        if 2014. <= cat[i][0] < 2018.1:
            marker = 's'
            last_marker_square_idx = i
        # elif 2010. <= cat[i][0] < 2014.:
        elif 2007. <= cat[i][0] < 2014.:
            marker = 'o'
            last_marker_circle_idx = i
        else:
            marker = '*'
            last_marker_star_idx = i
        plt.scatter(tremor_durations[i], valid_cat[i], color=cmap(norm(peak_time_lags[i])), marker=marker)

    # X, Y, Z = density_estimation(tremor_durations, valid_cat)
    # plt.contour(X, Y, Z)
    # seaborn.kdeplot(tremor_durations, valid_cat, fill=False, levels=5, thresh=.2)

    plt.scatter(tremor_durations[last_marker_circle_idx], valid_cat[last_marker_circle_idx], color=cmap(1.),
                # marker='o', label='2010-2013')
                marker='o', label='2007-2013')
    plt.scatter(tremor_durations[last_marker_square_idx], valid_cat[last_marker_square_idx], color=cmap(1.),
                marker='s', label='2014-2017')
    plt.scatter(tremor_durations[last_marker_star_idx], valid_cat[last_marker_star_idx], color=cmap(1.),
                marker='*', label='2018-2022')

    p1 = max(max(valid_cat), max(tremor_durations))
    p2 = min(min(valid_cat), min(tremor_durations))
    plt.plot([p1, p2], [p1, p2], alpha=0.5, color='black', linewidth=1, zorder=0)
    plt.xlabel('Tremor durations [days]')
    plt.ylabel('SSE durations [days] (ours)')
    # cbar = plt.colorbar()
    cbar = plt.colorbar(sm)
    # cbar.ax.set_ylabel('Amount of overlap (%)')
    cbar.ax.set_ylabel('Time lag between tremor and SSE peaks [days]')
    plt.legend()
    # plt.gca().set_aspect(abs(plt.xlim()[1] - plt.xlim()[0]) / abs(plt.ylim()[1] - plt.ylim()[0]))
    plt.gca().set_aspect('equal')
    plt.show()

    return valid_cat, tremor_durations


def plot_comparison_duration_tremors_with_michel(cat, time_array, tremors, rel_height=0.99):
    """Take duration on catalogue, extract the same time period on tremors, find a peak and find a duration
    over that peak on tremors. Finally compare the two. Color code: overlap with Michel SSEs."""
    tremor_durations = []
    valid_cat = []
    overlap_percentages = []
    cat_michel = np.array(sse_catalogue())
    time_delta = 10 / 365
    correspondence_michel = []  # track if an event in the final catalogue needs to be color coded or not
    for i, time_span in enumerate(cat):
        print('time_span', time_span)
        time_idx = np.logical_and(time_array >= time_span[0], time_array <= time_span[1])
        current_tremor_window = tremors[time_idx]
        peaks, _ = find_peaks(current_tremor_window)
        if len(peaks) > 0:
            widths = peak_widths(current_tremor_window, peaks, rel_height=rel_height)
            # per ogni picco in widths, calcola il min a sx il max a dx e metti come durata
            start_time, end_time = time_array[time_idx][round(np.min(widths[2]))], time_array[time_idx][
                round(np.max(widths[3]))]
            tremor_durations.append((end_time - start_time) * 365)
            valid_cat.append((time_span[1] - time_span[0]) * 365)

            # take the overlap (or time lag) associated to the nearest Michel's SSE in time
            idx_subset_michel = np.logical_and(cat_michel[:, 0] >= time_span[0] - time_delta,
                                               cat_michel[:, 1] <= time_span[1] + time_delta)
            # print('trovato:', np.count_nonzero(idx_subset_michel))
            if np.count_nonzero(idx_subset_michel) > 0:
                # print(idx_subset_michel)
                subset_michel = cat_michel[idx_subset_michel]
                idx_min = np.argmin(subset_michel[:, 1] - subset_michel[:, 0])
                overlap_percent = overlap_percentage((subset_michel[idx_min, 0], subset_michel[idx_min, 1]), time_span)[
                    0]
                # overlap_percentages.append(overlap_percent)
                overlap_percentages.append(overlap_percent / 100 * (time_span[1] - time_span[0]) * 365)
                print('append:', overlap_percentages[-1])
                # overlap_percentages.append((time_span[0] - start_time) * 365)
                correspondence_michel.append(i)
            else:
                overlap_percentages.append(np.nan)
            '''plt.plot(time_array, tremors)
            plt.axvline(start_time)
            plt.axvline(end_time)
            plt.show()'''
    print('correspondence Michel:', correspondence_michel)
    cmap = plt.cm.coolwarm
    plt.figure(dpi=100)
    # create normalization instance
    norm = matplotlib.colors.Normalize(np.nanmin(overlap_percentages), np.nanmax(overlap_percentages))
    # create a scalarmappable from the colormap
    sm = matplotlib.cm.ScalarMappable(cmap=cmap, norm=norm)
    sm.set_array([])
    last_marker_square_idx, last_marker_circle_idx, last_marker_star_idx = 0, 0, 0
    for i in range(len(valid_cat)):
        if 2014. <= cat[i][0] < 2018.1:
            marker = 's'
            if i in correspondence_michel:
                last_marker_square_idx = i
        elif 2010. <= cat[i][0] < 2014.:
            marker = 'o'
            if i in correspondence_michel:
                last_marker_circle_idx = i
        else:
            marker = '*'
            if i in correspondence_michel:
                last_marker_star_idx = i
        if i in correspondence_michel:
            plt.scatter(tremor_durations[i], valid_cat[i], color=cmap(norm(overlap_percentages[i])), marker=marker)
        else:
            plt.scatter(tremor_durations[i], valid_cat[i], color='black', marker=marker, zorder=-100)
            pass

    plt.scatter(tremor_durations[last_marker_circle_idx], valid_cat[last_marker_circle_idx], color=cmap(1.),
                marker='o', label='2010-2013', zorder=-100)
    plt.scatter(tremor_durations[last_marker_square_idx], valid_cat[last_marker_square_idx], color=cmap(1.),
                marker='s', label='2014-2017', zorder=-100)
    plt.scatter(tremor_durations[last_marker_star_idx], valid_cat[last_marker_star_idx], color=cmap(1.),
                marker='*', label='2018-2022', zorder=-100)

    p1 = max(max(valid_cat), max(tremor_durations))
    p2 = min(min(valid_cat), min(tremor_durations))
    plt.plot([p1, p2], [p1, p2], alpha=0.5, color='black', linewidth=1, zorder=0)
    plt.xlabel('Tremor durations [days]')
    plt.ylabel('SSE durations [days] (ours)')
    # cbar = plt.colorbar()
    cbar = plt.colorbar(sm)
    # cbar.ax.set_ylabel('Amount of overlap with respect to Michel (%)')
    cbar.ax.set_ylabel('Overlap between our SSEs and Michel ones [days]')
    # cbar.ax.set_ylabel('Time lag between tremor and detected SSE onsets [days]')
    plt.legend()
    # plt.gca().set_aspect(abs(plt.xlim()[1] - plt.xlim()[0]) / abs(plt.ylim()[1] - plt.ylim()[0]))
    plt.gca().set_aspect('equal')
    plt.show()

    return valid_cat, tremor_durations


def sse_selection_from_proba_hard_thresh(probability, time_array, probability_threshold=0.5, sigma=2.,
                                         save_catalog=True, north=False):
    """Select SSEs in the probability curve based on a threshold on the probability and by extracting an
    estimation of the slow slip duration."""
    smoothed_proba = gaussian_filter(probability, sigma=sigma)
    prob_mask = smoothed_proba > probability_threshold
    sse_cat_inferred = []
    t = 0
    while t < len(time_array):
        if prob_mask[t] != 0:
            start_t = t
            while prob_mask[t] != 0:
                t += 1
            sse_cat_inferred.append((time_array[start_t], time_array[t]))
        else:
            t += 1
    sse_cat_inferred = np.array(sse_cat_inferred)
    if save_catalog:
        np.savetxt('geo_data/catalogue_sses_costantino_all_hard_thresh' + ('_north' if north else '') + '.txt',
                   sse_cat_inferred, fmt='%f')
    return sse_cat_inferred


def plot_correlation(proba, tremors):
    proba = np.diff(proba)
    tremors = np.diff(tremors)
    max_lag = 15
    x = np.linspace(-max_lag, max_lag, 2 * max_lag + 1, dtype=np.int_)
    corr = []
    for lag in x:
        p = np.corrcoef(proba[max_lag + lag:proba.size - (max_lag - lag)], tremors[max_lag:-max_lag])[0, 1]
        corr.append(p)
    corr = np.array(corr)

    plt.plot(x, corr)
    plt.axvline(x=0, linewidth=0.5)
    plt.xlabel('time lag between tremor and deformation initiation')
    plt.ylabel('Cross correlation')
    plt.show()


def load_proba(reference_period, weight_code='01Aug2022-144844', name='realgaps_ext_v3_0.001_6-7_135', north=False):
    with np.load(
            f'pred_cascadia_running_win_{weight_code}_{name}' + ('_north_selection' if north else '') + '.npz') as f:
        proba, time_span, attn_matrices = f['proba'], f['time'], f['attn_matrices']
    time_filter = np.where(np.logical_and(time_span >= reference_period[0], time_span <= reference_period[1]))[0]
    time_span, proba = time_span[time_filter], proba[time_filter]
    valid_filter = proba != 0
    time_span, proba = time_span[valid_filter], proba[valid_filter]
    return time_span, proba


def detection_overview2(time, proba, n_tremors_per_day, catalog_ours, catalog_michel, draw_sse_durations=True,
                        draw_michel_catalog=True, draw_tremor_rectangle=True, draw_tremors=True, north=False,
                        base_fig_folder='./sse_figures/'):
    color_proba = 'C0'
    color_tremors = 'C7'
    color_michel = 'orangered'
    color_synthetics = 'mediumseagreen'
    offset_proba = 0.3
    offset_lines = 0.03
    fig, axis = plt.subplots(1, 1, figsize=(13.5, 7.5 / 2))
    ax_proba, ax_tremors = axis, axis.twinx()
    ax_proba.ticklabel_format(useOffset=False)
    ax_tremors.ticklabel_format(useOffset=False)

    ax_proba.plot(time, proba, linewidth=1, color=color_proba)

    ax_proba.set_ylim([0 - offset_proba, 1 + offset_proba / 6])

    ax_proba.set_yticks([0.4, 0.6, 0.8, 1])

    ax_proba.margins(x=0)
    ax_tremors.margins(x=0)

    xlims1 = ax_proba.get_xlim()
    if draw_michel_catalog:
        for i, span in enumerate(catalog_michel):
            if span[0] > xlims1[0] and span[1] < xlims1[1]:
                ax_proba.plot(span, [0.2, 0.2], clip_on=False, color=color_michel, linewidth=2.5)

    position_catalog = 1.12  # 1.1  # -0.1 - offset_proba - offset_lines
    position_synthetics = -0.11 - offset_proba - offset_lines  # -0.12 - offset_proba - offset_lines - 0.05
    _add_double_sided_arrow(ax_proba, xlims1[0], position_catalog, 2017.65, position_catalog, color=color_michel)
    _add_double_sided_arrow(ax_proba, xlims1[0], position_synthetics, 2014., position_synthetics,
                            color=color_synthetics)
    _add_double_sided_arrow(ax_proba, 2018., position_synthetics, 2022.2, position_synthetics, color=color_synthetics)

    if draw_sse_durations:
        for i, span in enumerate(catalog_ours):
            if span[0] > xlims1[0] and span[1] < xlims1[1]:
                ax_proba.axvspan(span[0], span[1], ymin=0.37, alpha=0.25, color=color_proba, zorder=0, linewidth=0.)

    if draw_tremor_rectangle:
        ax_tremors.axvspan(xlims1[0], 2009.55, ymax=0.37, alpha=0.25, color='grey', zorder=0, linewidth=0.)

    ax_proba.set_ylabel('Probability of SSE detection', color=color_proba)
    ax_proba.tick_params(axis='y', labelcolor=color_proba)

    if draw_tremors:
        markerline, stemlines, baseline = ax_tremors.stem(time, n_tremors_per_day, use_line_collection=True,
                                                          markerfmt=" ", basefmt=" ")
        plt.setp(stemlines, 'linewidth', 0.15)
        plt.setp(stemlines, 'color', color_tremors)
        ax_tremors.set_ylabel('Number of tremor events per day', color=color_tremors)
        smoothed_tremors = gaussian_filter(n_tremors_per_day, 2.5)
        ax_tremors.plot(time, smoothed_tremors, color=color_tremors, linewidth=1.)
        ax_tremors.tick_params(axis='y', labelcolor=color_tremors)
        ax_tremors.set_ylim([np.min(smoothed_tremors), 3 * np.max(smoothed_tremors)])
        ax_tremors.set_yticks([200, 500])
    else:
        ax_tremors.set_yticks([])

    plt.savefig(base_fig_folder + ('north_selection/' if north else '') + 'det_overview2.pdf')

    plt.close(fig)


def zoom_on_overview_plot(time, proba, n_tremors_per_day, catalog_ours, catalog_michel, draw_sse_durations=True,
                          draw_michel_catalog=True, draw_tremor_rectangle=True, draw_tremors=True, north=False,
                          base_fig_folder='./sse_figures/'):
    labelsize = BIGGER_SIZE + 2
    plt.rcParams.update({'ytick.labelsize': labelsize, 'xtick.labelsize': labelsize})  # fontsize of the x and y labels
    zoom_periods = [(2015.94, 2017), (2019.15, 2020.9), (2017., 2017.5)]
    color_proba = 'C0'
    color_tremors = 'C7'
    color_michel = 'orangered'
    offset_proba = 0.3

    for i, zoom in enumerate(zoom_periods):
        print(f'zoom period: {zoom}')
        time_span_idx = np.where(np.logical_and(time >= zoom[0], time <= zoom[1]))
        fig, axis = plt.subplots(1, 1, figsize=(13.5, 7.5 / 2))
        if i == 2:
            fig, axis = plt.subplots(1, 1, figsize=(13.5 / 2, 7.5 / 2))
        ax_proba, ax_tremors = axis, axis.twinx()
        ax_proba.ticklabel_format(useOffset=False)
        ax_tremors.ticklabel_format(useOffset=False)

        ax_proba.plot(time[time_span_idx], proba[time_span_idx], linewidth=1, color=color_proba)

        ax_proba.set_ylim([0 - offset_proba, 1 + offset_proba / 6])
        ax_proba.set_yticks([0.4, 0.6, 0.8, 1])
        ax_proba.margins(x=0)
        ax_tremors.margins(x=0)

        xlims1 = ax_proba.get_xlim()
        if draw_michel_catalog:
            for i, span in enumerate(catalog_michel):
                if span[0] > xlims1[0] and span[1] < xlims1[1]:
                    ax_proba.plot(span, [0.2, 0.2], clip_on=False, color=color_michel, linewidth=2.5)

        if draw_sse_durations:
            for i, span in enumerate(catalog_ours):
                if span[0] > xlims1[0] and span[1] < xlims1[1]:
                    ax_proba.axvspan(span[0], span[1], ymin=0.37, alpha=0.25, color=color_proba, zorder=0, linewidth=0.)

        ax_proba.tick_params(axis='y', labelcolor=color_proba)

        if draw_tremors:
            markerline, stemlines, baseline = ax_tremors.stem(time[time_span_idx], n_tremors_per_day[time_span_idx],
                                                              use_line_collection=True,
                                                              markerfmt=" ", basefmt=" ")
            plt.setp(stemlines, 'linewidth', 0.15)
            plt.setp(stemlines, 'color', color_tremors)
            smoothed_tremors = gaussian_filter(n_tremors_per_day, 2.5)
            ax_tremors.plot(time[time_span_idx], smoothed_tremors[time_span_idx], color=color_tremors, linewidth=1.)
            ax_tremors.tick_params(axis='y', labelcolor=color_tremors)
            ax_tremors.set_ylim([np.min(smoothed_tremors), 3 * np.max(smoothed_tremors)])
            ax_tremors.set_yticks([200, 500])
        else:
            ax_tremors.set_yticks([])

        decyr_conversion = {v: k for k, v in ymd_decimal_year_lookup().items()}  # decimal -> DMY
        current_ticks = ax_proba.get_xticks()
        labels_as_datetime = [
            datetime.datetime(year=decyr_conversion[_find_nearest_val(time[time_span_idx], date)[0]][0],
                              month=decyr_conversion[_find_nearest_val(time[time_span_idx], date)[0]][1],
                              day=decyr_conversion[_find_nearest_val(time[time_span_idx], date)[0]][2]).strftime(
                '%m/%Y') for date in current_ticks]
        ax_proba.set_xticks(current_ticks[1:-1], labels_as_datetime[1:-1])  # avoid the first and the last one

        plt.savefig(base_fig_folder + ('north_selection/' if north else '') + f'zoom_{zoom[0]}_{zoom[1]}.pdf')
        plt.close(fig)
    plt.rcParams.update(
        {'ytick.labelsize': MEDIUM_SIZE, 'xtick.labelsize': MEDIUM_SIZE})  # fontsize of the x and y labels


def plot_histogram_durations_stacked(catalog_ours, catalog_michel, overlap_thresh, bins=50, north=False,
                                     base_fig_folder='./sse_figures/'):
    common_michel_catalogue, other_michel_period, other_outside_michel_period = set(), set(), set()  # already contain durations
    max_date_michel = np.max(catalog_michel[:, 1])
    max_date_ours = np.max(catalog_ours[:, 1])

    for i, event in enumerate(catalog_ours):
        for j in range(catalog_michel.shape[0]):
            event_tuple = (event[0], event[1])
            overlap_percent = overlap_percentage(event, catalog_michel[j].tolist())[0]
            if overlap_percent >= overlap_thresh:
                common_michel_catalogue.add(event_tuple)

    for i, event in enumerate(catalog_ours):
        event_tuple = (event[0], event[1])
        if event_tuple not in common_michel_catalogue:
            if event[0] > max_date_michel:
                other_outside_michel_period.add(event_tuple)
            else:
                if event_tuple not in common_michel_catalogue:
                    other_michel_period.add(event_tuple)

    print('# common events:',
          len(common_michel_catalogue))  # 34 (they are 35, but the first one is cut because of border effects)
    print('# events in the Michel et al. period:', len(other_michel_period))
    print('# events in 2017-2022:', len(other_outside_michel_period))

    common_michel_catalogue = np.array(list(common_michel_catalogue))
    other_michel_period = np.array(list(other_michel_period))
    other_outside_michel_period = np.array(list(other_outside_michel_period))

    common_michel_catalogue_dur = (common_michel_catalogue[:, 1] - common_michel_catalogue[:, 0]) * 365
    other_michel_period_dur = (other_michel_period[:, 1] - other_michel_period[:, 0]) * 365
    other_outside_michel_period_dur = (other_outside_michel_period[:, 1] - other_outside_michel_period[:, 0]) * 365

    duration_stack = [list(common_michel_catalogue_dur), list(other_michel_period_dur),
                      list(other_outside_michel_period_dur)]
    fig = plt.figure(dpi=100)
    _, x, _ = plt.hist(duration_stack, bins=bins, stacked=True)
    plt.xlabel('SSE duration [days]')
    plt.ylabel('Number of detected events')
    plt.legend(['Common events', 'New events in the Michel et al. period',
                f'New events in the period {int(max_date_michel)}-{int(max_date_ours)}'])
    plt.savefig(base_fig_folder + ('north_selection/' if north else '') + 'dur_hist_stacked.pdf')
    plt.close(fig)


def plot_comparison_duration_ours_michel2(cat_ours, cat_michel, overlap_thresh, save_diff_catalog=False, north=False,
                                          base_fig_folder='./sse_figures/'):
    filtered_cat_ours = []
    filtered_cat_michel = []
    overlap_percentages = []
    unique_set_michel = set()  # contains the Michel et al. events that are retrieved by SSEdetector
    idx_michel_visited = set()
    for i, event in enumerate(cat_ours):
        for j in range(cat_michel.shape[0]):
            overlap_percent = overlap_percentage(event, cat_michel[j].tolist())[0]
            if overlap_percent >= overlap_thresh:
                if (cat_michel[j][1] - cat_michel[j][0]) * 365 > 50:
                    print(cat_michel[j])
                filtered_cat_ours.append(event)
                filtered_cat_michel.append(cat_michel[j])
                overlap_percentages.append(overlap_percent)
                unique_set_michel.add((cat_michel[j][0], cat_michel[j][1]))
                idx_michel_visited.add(j)

    idx_michel_missed = list(set(range(cat_michel.shape[0])).difference(idx_michel_visited))
    unique_set_michel = np.array(list(unique_set_michel))
    unique_set_michel = unique_set_michel[np.argsort(unique_set_michel[:, 0])]  # sort by starting date
    if save_diff_catalog:
        np.savetxt('geo_data/catalogue_sse_costantino_michel_retrieved.txt', unique_set_michel, fmt='%f')

    print('Michel et al. missed events:')
    print(cat_michel[idx_michel_missed])

    filtered_cat_michel = np.array(filtered_cat_michel)
    filtered_cat_ours = np.array(filtered_cat_ours)
    overlap_percentages = np.array(overlap_percentages)

    duration_cat_michel = (filtered_cat_michel[:, 1] - filtered_cat_michel[:, 0]) * 365
    duration_cat_ours = (filtered_cat_ours[:, 1] - filtered_cat_ours[:, 0]) * 365

    fig = plt.figure(dpi=100)

    plt.scatter(duration_cat_michel, duration_cat_ours, c=overlap_percentages, cmap='coolwarm')

    p1 = max(max(duration_cat_ours), max(duration_cat_michel))
    p2 = min(min(duration_cat_ours), min(duration_cat_michel))
    plt.plot([p1, p2], [p1, p2], alpha=0.5, color='black', linewidth=1, zorder=0)
    plt.xlabel('Durations [days] (Michel et al., 2019)')
    plt.ylabel('Durations [days] (ours)')
    cbar = plt.colorbar()
    cbar.ax.set_ylabel('Amount of overlap (%)')
    plt.gca().set_aspect('equal')
    plt.savefig(base_fig_folder + ('north_selection/' if north else '') + 'dur_michel2.pdf')
    plt.close(fig)


def correlation_proba_tremor_lag_global(time_array, proba, n_tremors_per_day, sigma=1.5, derivative=False,
                                        upsampling_factor=1,
                                        max_lag=15, smooth_tremors=True, scale=True, north=False, tremor_time_ref=None,
                                        base_fig_folder='./sse_figures/'):
    if tremor_time_ref is not None:
        time_cut = np.logical_and(time_array >= tremor_time_ref[0], time_array <= tremor_time_ref[1])
        proba = proba[time_cut]
        n_tremors_per_day = n_tremors_per_day[time_cut]

    if smooth_tremors:
        n_tremors_per_day = gaussian_filter(n_tremors_per_day, sigma)
    if scale:
        proba = (proba - np.min(proba)) / (np.max(proba) - np.min(proba))
        n_tremors_per_day = (n_tremors_per_day - np.min(n_tremors_per_day)) / (
                np.max(n_tremors_per_day) - np.min(n_tremors_per_day))
    if derivative:
        proba = np.diff(proba)
        n_tremors_per_day = np.diff(n_tremors_per_day)
    if upsampling_factor > 1:
        x = np.linspace(0, len(proba), len(proba))
        x_upsampled = np.linspace(0, len(proba), len(proba) * upsampling_factor)
        lin_proba = interp1d(x, proba)
        lin_tremors = interp1d(x, n_tremors_per_day)
        proba = lin_proba(x_upsampled)
        n_tremors_per_day = lin_tremors(x_upsampled)

    print('Correlation coefficient (at zero):', np.corrcoef(proba, n_tremors_per_day)[0, 1])
    x, corr = _lagged_correlation(proba, n_tremors_per_day, max_lag, upsampling_factor=upsampling_factor)

    fig = plt.figure(dpi=100)
    plt.plot(x, corr, color='black')
    plt.axvline(x=0, linewidth=0.3, color='black')
    plt.xlabel('Global-scale tremor-SSE time lag [days]')
    plt.ylabel('Cross-correlation')
    plt.savefig(base_fig_folder + ('north_selection/' if north else '') + 'global_corr.pdf')
    plt.close(fig)


def plot_comparison_sse_tremor_local_corr2(cat, time_array, proba, n_tremors_per_day, rel_height=0.99,
                                           min_corr_coeff_thresh=0.4, sigma=1.5, sigma_d=0, sigma_t=0, sigma_d_cut=0,
                                           north=False, tremor_time_ref=None, max_lag=15,
                                           base_fig_folder='./sse_figures/'):
    if tremor_time_ref is not None:
        time_cut = np.logical_and(time_array >= tremor_time_ref[0], time_array <= tremor_time_ref[1])
        proba = proba[time_cut]
        time_array = time_array[time_cut]
        n_tremors_per_day = n_tremors_per_day[time_cut]

    n_tremors_per_day = gaussian_filter(n_tremors_per_day, sigma)
    tremor_durations = []
    valid_cat, not_significant_cat = [], []
    time_lags, not_significant_time_lags = [], []
    corr_values, not_significant_corr_val = [], []
    valid_cat_dur = []
    events_no_tremor = []
    sse_duration_variability = sigma_d  # days
    sse_duration_variability_cut = sigma_d_cut
    tremor_search_offset_date = sse_duration_variability / 365
    tremor_search_offset_date_cut = sse_duration_variability_cut / 365
    for time_span in cat:
        time_idx = np.logical_and(time_array >= time_span[0] - tremor_search_offset_date,
                                  time_array <= time_span[1] + tremor_search_offset_date)
        if np.sum(time_idx) < (time_span[1] - time_span[0]) * 365 + sse_duration_variability:
            continue  # out of bounds
        current_tremor_window = n_tremors_per_day[time_idx]
        current_proba_window = proba[time_idx]

        proba_corr_window = data_normalization(current_proba_window)
        tremor_corr_window = data_normalization(current_tremor_window)

        lags, corr_val = _lagged_correlation(proba_corr_window, tremor_corr_window, max_lag)

        if corr_val[np.where(lags == 0)[0]] > min_corr_coeff_thresh:
            time_lags.append(lags[np.argmax(corr_val)])
            valid_cat.append(time_span)
            corr_values.append(np.max(corr_val))

            time_idx_cut = np.logical_and(time_array >= time_span[0] - tremor_search_offset_date_cut,
                                          time_array <= time_span[1] + tremor_search_offset_date_cut)
            cut_tremor_window = n_tremors_per_day[time_idx_cut]
            peaks, _ = find_peaks(cut_tremor_window)
            if len(peaks) > 0:
                widths = peak_widths(cut_tremor_window, peaks, rel_height=rel_height)
                start_time, end_time = time_array[time_idx_cut][round(np.min(widths[2]))], time_array[time_idx_cut][
                    round(np.max(widths[3]))]
                tremor_durations.append((end_time - start_time) * 365)
                valid_cat_dur.append((time_span[1] - time_span[0]) * 365)
            else:
                # events not associated to a peak of tremor
                events_no_tremor.append(time_span)

        else:
            not_significant_cat.append(time_span)
            not_significant_corr_val.append(np.max(corr_val))
            not_significant_time_lags.append(lags[np.argmax(corr_val)])

    sse_durations = ((np.array(valid_cat)[:, 1] - np.array(valid_cat)[:, 0]) * 365).tolist()
    not_significant_sse_durations = (
            (np.array(not_significant_cat)[:, 1] - np.array(not_significant_cat)[:, 0]) * 365).tolist()

    print('Number of not significant SSEs:', len(not_significant_time_lags))

    cmap = plt.cm.coolwarm
    norm = matplotlib.colors.Normalize(vmin=np.min(time_lags), vmax=np.max(time_lags))
    sm = matplotlib.cm.ScalarMappable(cmap=cmap, norm=norm)
    sm.set_array([])
    fig = plt.figure(dpi=100)
    plt.scatter(sse_durations, corr_values, s=15, c=cmap(norm(time_lags)))
    plt.scatter(not_significant_sse_durations, not_significant_corr_val, s=15, facecolors='none',
                alpha=0.4, edgecolors=cmap(norm(not_significant_time_lags)))
    plt.xlabel('SSE duration [days]')
    # plt.ylabel('Maximum value of correlation coefficient')
    plt.ylabel('Maximum cross-correlation')
    cbar = plt.colorbar(sm)
    cbar.ax.set_ylabel('Local-scale tremor-SSE time lag [days]')
    plt.savefig(base_fig_folder + ('north_selection/' if north else '') + 'dur_tremors_local_corr.pdf')
    plt.close(fig)

    fig = plt.figure(dpi=100)
    plt.hist(time_lags, bins=14)
    # plt.axhline(y=0)
    # plt.xlabel('Time lag between tremors and SSE probability [days]')
    plt.xlabel('Local-scale tremor-SSE time lag [days]')
    plt.ylabel('Number of occurrences')
    plt.savefig(base_fig_folder + ('north_selection/' if north else '') + 'dur_tremors_local_corr_hist.pdf')
    plt.close(fig)

    print('Number of SSEs having a corresponding peak of tremor:', len(valid_cat_dur))
    print(f'{len(events_no_tremor)} events are not associated with tremors:')
    print(np.array(events_no_tremor))

    fig = plt.figure(dpi=100)
    plt.scatter(tremor_durations, valid_cat_dur, color='black', s=15)

    p1 = max(max(valid_cat_dur), max(tremor_durations))
    p2 = min(min(valid_cat_dur), min(tremor_durations))

    plt.plot([p1, p2], [p1, p2], alpha=0.5, color='black', linewidth=1, zorder=0)

    plt.plot([p2 + 14, p1], [p2, p1 - 14], alpha=0.3, color='black', linewidth=1, linestyle='--', zorder=0)

    plt.xlabel('Tremor durations [days]')
    plt.ylabel('SSE durations [days]')
    plt.gca().set_aspect('equal')
    plt.savefig(base_fig_folder + ('north_selection/' if north else '') + 'dur_tremors_no_color_revised.pdf')
    plt.close(fig)

    return valid_cat, tremor_durations


def displacement_field_plot(catalog, north=False, base_fig_folder='./sse_figures/'):
    n_selected_stations = 135
    selected_gnss_data, selected_time_array, _, _ = _preliminary_operations((2007, 2023), detrend=False)
    station_codes, station_coordinates = cascadia_coordinates()
    stations_to_remove = ['WSLB', 'YBHB', 'P687', 'BELI', 'PMAR', 'TGUA', 'OYLR', 'FTS5', 'RPT5', 'RPT6', 'P791',
                          'P674', 'P656', 'TWRI', 'WIFR', 'FRID', 'PNHG', 'COUR', 'SKMA', 'CSHR', 'HGP1', 'CBLV',
                          'PNHR', 'NCS2', 'TSEP', 'BCSC']
    station_codes, station_coordinates, selected_gnss_data = _remove_stations(stations_to_remove, station_codes,
                                                                              station_coordinates, selected_gnss_data)
    original_nan_pattern = np.isnan(selected_gnss_data[:, :, 0])
    n_nans_stations = original_nan_pattern.sum(axis=1)

    stations_subset = np.sort(np.argsort(n_nans_stations)[:n_selected_stations])
    station_codes_subset, station_coordinates_subset = np.array(station_codes)[stations_subset], station_coordinates[
                                                                                                 stations_subset, :]
    gnss_data_subset = selected_gnss_data[stations_subset, :, :]

    cascadia_box = [40 - 1.5, 51.8 - 0.2, -128.3 - 2.5, -121 + 2]  # min/max_latitude, min/max_longitude
    for i, event in enumerate(catalog):
        start_idx, end_idx = np.where(selected_time_array == event[0])[0][0], \
                             np.where(selected_time_array == event[1])[0][0]
        static_displacement = np.median(gnss_data_subset[:, end_idx - 1:end_idx + 2, :], axis=1) - np.nanmedian(
            gnss_data_subset[:, start_idx - 1:start_idx + 2, :], axis=1)
        fig = plt.figure(figsize=(7.8, 7.8), dpi=100)
        cascadia_map = Basemap(llcrnrlon=cascadia_box[2], llcrnrlat=cascadia_box[0],
                               urcrnrlon=cascadia_box[3], urcrnrlat=cascadia_box[1],
                               lat_0=cascadia_box[0], lon_0=cascadia_box[2],
                               resolution='i', projection='lcc')  # , ax=ax9)

        cascadia_map.drawcoastlines(linewidth=0.5)
        cascadia_map.fillcontinents(color='bisque', lake_color='lightcyan')  # burlywood
        cascadia_map.drawmapboundary(fill_color='lightcyan')
        cascadia_map.drawparallels(np.arange(cascadia_box[0], cascadia_box[1], (cascadia_box[1] - cascadia_box[0]) / 4),
                                   labels=[0, 1, 0, 0], linewidth=0.1)
        cascadia_map.drawmeridians(np.arange(cascadia_box[2], cascadia_box[3], (cascadia_box[3] - cascadia_box[2]) / 4),
                                   labels=[0, 0, 0, 1], linewidth=0.1)
        cascadia_map.readshapefile('geo_data/tectonicplates-master/PB2002_boundaries', '', linewidth=0.3)

        max_length = np.nanmax(np.sqrt(static_displacement[:, 0] ** 2 + static_displacement[:, 1] ** 2))
        # concatenate unit arrow
        unit_arrow_x, unit_arrow_y = -127., 39.5
        x = np.concatenate((station_coordinates_subset[:, 1], [unit_arrow_x]))
        y = np.concatenate((station_coordinates_subset[:, 0], [unit_arrow_y]))
        disp_x = np.concatenate((static_displacement[:, 0], [-max_length]))
        disp_y = np.concatenate((static_displacement[:, 1], [0.]))

        cascadia_map.quiver(x, y, disp_x, disp_y, color='black', latlon=True, width=0.007)

        label_pos_x, label_pos_y = cascadia_map(unit_arrow_x - 1.45, unit_arrow_y + 0.25)

        plt.annotate(f'{np.around(max_length, decimals=1)}mm', xy=(unit_arrow_x, unit_arrow_y), xycoords='data',
                     xytext=(label_pos_x, label_pos_y), color='black')
        plt.title(f'SSE date: {event[0]} - {event[1]}, duration: {int((event[1] - event[0]) * 365)} days')
        plt.savefig(
            base_fig_folder + ('north_selection/' if north else '') + 'disp/' + f'{i + 1}_{event[0]}_{event[1]}.pdf')
        plt.close(fig)


if __name__ == '__main__':
    reference_period = (2007, 2023)
    north_selection = False
    relative_height = 0.7
    overlap_threshold = 1e-03
    probability_threshold = 0.5
    tremor_time_ref = (2010, 2023)
    global_win_width, tremor_offset, max_global_lag, max_local_lag, local_win_width = 30, 0, 15, 7, 7
    base_folder = './sse_figures/'

    weight_code_default, name_default = '01Aug2022-144844', 'realgaps_ext_v3_0.001_6-7_135'
    # weight_code_352, name_352 = '01Aug2022-144945', 'realgaps_ext_v3_0.001_6-7_352'
    # weight_code_2d_cnn, name_2d_cnn = '04Aug2022-235217', 'realgaps_ext_v3_2D_deeper_CNN_att__6-7352'

    time, proba = load_proba(reference_period, north=north_selection, name=name_default,
                             weight_code=weight_code_default)
    tremors = tremor_catalogue(north=north_selection)
    n_tremors_per_day = get_n_tremors_per_day(time, tremors)

    cat_ours = sse_selection_from_proba_hard_thresh(proba, time, probability_threshold=probability_threshold,
                                                    save_catalog=True, north=north_selection)
    cat_michel = sse_catalogue(north=north_selection)
    cat_michel_mw = sse_catalogue(return_magnitude=True, north=north_selection)

    detection_overview2(time, proba, n_tremors_per_day, cat_ours, cat_michel, north=north_selection,
                        draw_tremors=True, draw_tremor_rectangle=True, draw_michel_catalog=True,
                        draw_sse_durations=True, base_fig_folder=base_folder)

    zoom_on_overview_plot(time, proba, n_tremors_per_day, cat_ours, cat_michel, north=north_selection,
                          draw_tremors=True, draw_tremor_rectangle=True, draw_michel_catalog=True,
                          draw_sse_durations=True, base_fig_folder=base_folder)

    # Analysis of SSE durations

    plot_comparison_duration_ours_michel2(cat_ours, cat_michel, overlap_thresh=overlap_threshold,
                                          save_diff_catalog=False, north=north_selection, base_fig_folder=base_folder)
    plot_histogram_durations_stacked(cat_ours, cat_michel, overlap_thresh=overlap_threshold, bins=40,
                                     north=north_selection, base_fig_folder=base_folder)

    # Comparison with tremor episodes

    correlation_proba_tremor_lag_global(time, proba, n_tremors_per_day, derivative=False, upsampling_factor=1,
                                        max_lag=max_global_lag, smooth_tremors=True, sigma=1.5, scale=True,
                                        tremor_time_ref=tremor_time_ref, north=north_selection,
                                        base_fig_folder=base_folder)
    plot_comparison_sse_tremor_local_corr2(cat_ours, time, proba, n_tremors_per_day, rel_height=relative_height,
                                           north=north_selection, sigma_d_cut=local_win_width, sigma_d=global_win_width,
                                           sigma_t=tremor_offset, tremor_time_ref=tremor_time_ref,
                                           base_fig_folder=base_folder, max_lag=max_local_lag)

    # Supplementary analyses

    displacement_field_plot(cat_ours, north=north_selection, base_fig_folder=base_folder)
