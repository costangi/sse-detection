import numpy as np

from sse_generator.synthetic_time_series_cascadia import synthetic_time_series_real_gaps_extended_v3 as synth_ts

if __name__ == '__main__':
    n_samples = 1000
    n_stations = 135
    detection = True
    window_length = 60
    magnitude_range = (6, 7)

    synth_data, rand_dur, cat, synth_disp, stat_codes, stat_coord = synth_ts(n_samples, n_stations, detection,
                                                                             window_length=window_length,
                                                                             magnitude_range=magnitude_range)

    np.savez(f'example_synthetic_data_set', synthetic_data=synth_data, random_durations=rand_dur, catalogue=cat,
             synthetic_displacement=synth_disp, station_codes=stat_codes, station_coordinates=stat_coord)
