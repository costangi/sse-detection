import numpy as np

from sse_detector.sse_detector import SSEdetector

if __name__ == '__main__':
    train_codename = 'sample_train'
    dataset_filename = 'det_synth_ts_cascadia_TEST_realgaps_extended_v3_135stations_5.5_7'
    with np.load(f'{dataset_filename}.npz') as f:
        data, durations, cat, templates, station_codes, station_coordinates = f['synthetic_data'], f[
            'random_durations'], f['catalogue'], f['synthetic_displacement'], f['station_codes'], f[
                                                                                  'station_coordinates']

    n_samples = 1000
    n_stations = station_coordinates.shape[0]
    y = np.ones((durations.shape[0],))
    y[np.where(durations == 0.)[0]] = 0.
    y = y.reshape(-1, 1)

    data = data[:n_samples]
    y = y[:n_samples]

    # train, val, test split: e.g., 80/10%/10%
    ind_val = int(n_samples * 0.8)
    ind_test = int(n_samples * 0.9)

    X_train, X_val, X_test = data[:ind_val, :, :, :], data[ind_val:ind_test, :, :, :], data[ind_test:, :, :, :]
    y_train, y_val, y_test = y[:ind_val, :], y[ind_val:ind_test, :], y[ind_test:, :]
    cat_train, cat_val, cat_test = cat[:ind_val, :], cat[ind_val:ind_test, :], cat[ind_test:, :]
    templates_train, templates_val, templates_test = templates[:ind_val, :, :], templates[ind_val:ind_test, :,
                                                                                :], templates[ind_test:, :, :]

    train_indices = np.linspace(0, ind_val, ind_val, dtype=int)
    val_indices = np.linspace(ind_val, ind_test, ind_test - ind_val, dtype=int)


    params = {'n_stations': 135,
              'window_length': 60,
              'n_directions': 2,
              'batch_size': 128,
              'n_epochs': 50,
              'learning_rate': 0.001,
              'verbosity': 1,
              'kernel_regularizer': None,
              'bias_regularizer': None,
              'embedding_regularizer': None}

    detector = SSEdetector(**params)
    detector.build()
    detector.summary()
    detector.compile()

    # detector.set_callbacks(train_codename, val_indices, X_val, y_val, cat_val, templates_val)
    detector.set_callbacks(train_codename, val_indices, X_val, y_val, X_val, X_val)
    detector.train(X_train, X_val, y_train, y_val)