import io
import re

import alphashape
import numpy as np
import seaborn as sns
from matplotlib import pyplot as plt
from scipy import stats, interpolate
from scipy.interpolate import CloughTocher2DInterpolator, griddata
from sklearn.metrics import roc_curve, auc, confusion_matrix


def _preliminary_operations(reference_period, detrend=True, **kwargs):
    """Returns GNSS data in reference_period. When detrending is used, the trends are not returned."""
    gnss_data, time_array = load_gnss_data_cascadia(**kwargs)
    if detrend:
        gnss_data = detrend_nan(time_array, gnss_data)  # gnss data is detrended first
    reference_time_indices = get_reference_period(time_array, reference_period)
    reference_time_array = time_array[reference_time_indices]
    selected_gnss_data = gnss_data[:, reference_time_indices, :]
    return selected_gnss_data, reference_time_array, gnss_data, time_array


def _remove_stations(codes_to_remove, codes, coords, gnss_data):
    codes = np.array(codes)
    mask = ~np.isin(codes, codes_to_remove)
    new_coords = coords[mask]
    new_codes = codes[mask]
    new_gnss_data = gnss_data[mask]
    return new_codes, new_coords, new_gnss_data


def ymd_decimal_year_lookup():
    """Returns a lookup table for (year, month, day) to decimal year, with the convention introduced by Nasa JPL."""
    ymd_decimal_lookup = dict()
    with open('geo_data/decyr.txt', 'r') as f:
        next(f)
        for line in f:
            line = re.sub(' +', ' ', line)
            splitted_line = line.split(' ')
            decimal, year, month, day = splitted_line[1], splitted_line[2], splitted_line[3], splitted_line[4]
            decimal, year, month, day = float(decimal), int(year), int(month), int(day)
            ymd_decimal_lookup[(year, month, day)] = decimal
    return ymd_decimal_lookup


def load_gnss_data_cascadia(n_directions=2):
    """Loads all the GNSS data from Cascadia and returns it along with a time array.
    The data is taken by considering the time span associated to the longest available time series.
    The unit measure is converted in mm."""
    station_codes, station_coordinates = cascadia_coordinates()
    n_stations = len(station_codes)
    file_lines = np.zeros((len(station_codes),))
    for i, code in enumerate(station_codes):  # check the longest file to have the largest time span
        num_lines = sum(1 for line in open(f'./geo_data/GNSS_CASCADIA/txt/{code}.txt'))
        file_lines[i] = num_lines
    target_station = station_codes[np.argmax(file_lines)]
    time_array = np.loadtxt(f'./geo_data/GNSS_CASCADIA/txt/{target_station}.txt')[:, 0]
    gnss_data = np.zeros((n_stations, len(time_array), n_directions))
    gnss_data.fill(np.nan)
    for i, code in enumerate(station_codes):
        data = np.loadtxt(f'./geo_data/GNSS_CASCADIA/txt/{code}.txt')[:, :n_directions + 1]
        correspondence_indices = np.searchsorted(time_array, data[:, 0])
        gnss_data[i, correspondence_indices, :] = data[:, 1:] * 1e03
    return gnss_data, time_array


def get_reference_period(time_array, period):
    """Returns indices in the data for the specified reference period, supposed to be a tuple of (start,end) dates."""
    reference_time_indices = np.where(np.logical_and(time_array > period[0], time_array < period[1]))[0]
    return reference_time_indices


def _first_and_last_seq(x, n):
    a = np.r_[n - 1, x, n - 1]
    a = a == n
    start = np.r_[False, ~a[:-1] & a[1:]]
    end = np.r_[a[:-1] & ~a[1:], False]
    return np.where(start | end)[0] - 1


def sequence_bounds(data, value):
    """Returns all the bounds for the sequences of {value} passed as input in the data. It does not handle
    1-sample sequences, which must be handled before."""
    return list(zip(*(iter(_first_and_last_seq(data, value)),) * 2))


def detrend_nan(x, data):
    detrended_data = np.zeros(data.shape)
    for observation in range(data.shape[0]):
        for direction in range(data.shape[2]):
            detrended_data[observation, :, direction] = _detrend_nan_1d(x, data[observation, :, direction])
    return detrended_data


def _detrend_nan_1d(x, y):
    # find linear regression line, subtract off data to detrend
    not_nan_ind = ~np.isnan(y)
    detrend_y = np.zeros(y.shape)
    detrend_y.fill(np.nan)
    if y[not_nan_ind].size > 0:
        m, b, r_val, p_val, std_err = stats.linregress(x[not_nan_ind], y[not_nan_ind])
        detrend_y = y - (m * x + b)
    return detrend_y


def _detrend_nan_1d_v2(x, y):
    # find linear regression line, subtract off data to detrend
    not_nan_ind = ~np.isnan(y)
    detrend_y = np.zeros(y.shape)
    detrend_y.fill(np.nan)
    m, b = 0, 0
    if y[not_nan_ind].size > 0:
        m, b, r_val, p_val, std_err = stats.linregress(x[not_nan_ind], y[not_nan_ind])
        detrend_y = y - (m * x + b)
    return detrend_y, m, b


def detrend_nan_v2(x, data):
    detrended_data = np.zeros(data.shape)
    trend_params = np.zeros((data.shape[0], data.shape[2], 2))
    for observation in range(data.shape[0]):
        for direction in range(data.shape[2]):
            detrended_, m, b = _detrend_nan_1d_v2(x, data[observation, :, direction])
            detrended_data[observation, :, direction] = detrended_
            trend_params[observation, direction, :] = [m, b]
    return detrended_data, trend_params


def cascadia_coordinates():
    """3D position of GNSS stations in Cascadia"""
    with open('geo_data/NGL_stations_cascadia.txt') as f:
        rows = f.read().splitlines()
        station_codes = []
        station_coordinates = []
        for row in rows:
            station_code, station_lat, station_lon, station_height = row.split(' ')
            station_codes.append(station_code)
            station_coordinates.append([station_lat, station_lon, station_height])
        station_coordinates = np.array(station_coordinates, dtype=np.float)
    return station_codes, station_coordinates


def _find_nearest_val(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return array[idx], idx


def plot_roc_curve(y_test, y_proba):
    """Utility function for Tensorboard. Plots the ROC curve and returns the figure."""
    figure = plt.figure(figsize=(8, 8))
    fpr_keras, tpr_keras, thresholds_keras = roc_curve(y_test, y_proba)
    auc_keras = auc(fpr_keras, tpr_keras)
    plt.figure()
    plt.plot([0, 1], [0, 1], 'k--')
    plt.plot(fpr_keras, tpr_keras, label='AUC = {:.3f}'.format(auc_keras))
    # plt.plot(fpr_rf, tpr_rf, label='RF (area = {:.3f})'.format(auc_rf))
    plt.scatter(fpr_keras[_find_nearest_val(thresholds_keras, 0.5)[1]],
                tpr_keras[_find_nearest_val(thresholds_keras, 0.5)[1]])
    plt.xlabel('False positive rate')
    plt.ylabel('True positive rate')
    plt.title('ROC curve')
    plt.legend(loc='best')
    return figure


def plot_confusion_matrix(y_test, y_pred):
    """Utility function for Tensorboard. Plots the confusion matrix and returns the figure. The variable
    y_pred is assumed as an array obtained from y_proba after selecting a threshold."""
    figure = plt.figure(figsize=(8, 8))
    sns.heatmap(confusion_matrix(y_test, y_pred), annot=True, cmap='Blues', fmt='d',
                xticklabels=['noise', 'noise+signal'], yticklabels=['noise', 'noise+signal'])  # fmt='g'
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    return figure


def recall_as_function_of(y_true, y_pred, parameter, N_BINS=20):
    """
    author: Giuseppe Costantino
    Plots the binned recall as a function of the parameter passed as input.

    :param y_true: ground truth
    :param y_pred: predicted labels
    :param parameter: parameter with respect to which to plot the true positive ratio
    :param N_BINS: number of true positive ratio bins (default 20)
    :return: parameter bin positions, true positive ratio in a given parameter bin
    """
    true_pos_idx = np.where(np.logical_and(y_true == 1., y_pred == 1.))[0]
    false_neg_idx = np.where(np.logical_and(y_true == 1., y_pred == 0.))[0]
    # true positives and false negatives as function of the parameter
    param_true_pos = parameter[true_pos_idx]
    param_false_neg = parameter[false_neg_idx]
    _, bin_edges = np.histogram(parameter[np.where(y_true == 1.)[0]], bins=N_BINS)
    param_true_pos_bin_mean = []
    percentage_true_pos_bin_param = []
    for i in range(len(bin_edges) - 1):
        idx_bin_true_pos = np.where(np.logical_and(param_true_pos >= bin_edges[i], param_true_pos < bin_edges[i + 1]))[
            0]
        idx_bin_false_neg = \
            np.where(np.logical_and(param_false_neg >= bin_edges[i], param_false_neg < bin_edges[i + 1]))[0]
        if len(idx_bin_false_neg) > 0:
            percentage_true_pos_bin_param.append(
                len(idx_bin_true_pos) / (len(idx_bin_true_pos) + len(idx_bin_false_neg)))
        else:
            percentage_true_pos_bin_param.append(np.nan)
        param_true_pos_bin_mean.append(0.5 * (bin_edges[i] + bin_edges[i + 1]))
    return param_true_pos_bin_mean, percentage_true_pos_bin_param


def n_true_pos_as_function_of(y_true, y_pred, parameter, N_BINS=20):
    """
    author: Giuseppe Costantino
    Plots the binned number of true positives as a function of the parameter passed as input.

    :param y_true: ground truth
    :param y_pred: predicted labels
    :param parameter: parameter with respect to which to plot the # true positives
    :param N_BINS: number of true positive ratio bins (default 20)
    :return: parameter bin positions, # true positives in a given parameter bin
    """
    true_pos_idx = np.where(np.logical_and(y_true == 1., y_pred == 1.))[0]
    false_neg_idx = np.where(np.logical_and(y_true == 1., y_pred == 0.))[0]
    # true positives and false negatives as function of the parameter
    param_true_pos = parameter[true_pos_idx]
    param_false_neg = parameter[false_neg_idx]
    _, bin_edges = np.histogram(parameter[np.where(y_true == 1.)[0]], bins=N_BINS)
    param_true_pos_bin_mean = []
    n_true_pos_bin_param = []
    for i in range(len(bin_edges) - 1):
        idx_bin_true_pos = np.where(np.logical_and(param_true_pos >= bin_edges[i], param_true_pos < bin_edges[i + 1]))[
            0]
        n_true_pos_bin_param.append(len(idx_bin_true_pos))
        param_true_pos_bin_mean.append(0.5 * (bin_edges[i] + bin_edges[i + 1]))
    return param_true_pos_bin_mean, n_true_pos_bin_param


def plot_recall_function_of(y_test, y_pred, parameter, xlabel):
    mw_true_pos_bin_mean, percentage_true_pos_bin_mw = recall_as_function_of(y_test, y_pred, parameter)
    figure = plt.figure(figsize=(8, 8))
    plt.plot(mw_true_pos_bin_mean, percentage_true_pos_bin_mw)
    # plt.ylabel('% true positives (# true pos in a bin / #true pos)')
    plt.ylabel('True positive ratio (# true positives / positives)')
    plt.xlabel(xlabel)
    return figure


def plot_to_image(figure):
    import tensorflow as tf
    """Converts the matplotlib plot specified by 'figure' to a PNG image and
    returns it. The supplied figure is closed and inaccessible after this call."""
    # Save the plot to a PNG in memory.
    buf = io.BytesIO()
    plt.savefig(buf, format='png')
    # Closing the figure prevents it from being displayed directly inside
    # the notebook.
    plt.close(figure)
    buf.seek(0)
    # Convert PNG buffer to TF image
    image = tf.image.decode_png(buf.getvalue(), channels=4)
    # Add the batch dimension
    image = tf.expand_dims(image, 0)
    return image


def get_cascadia_box(extended=False):
    # cascadia_box = [40 - 1, 51.8 - 0.2, -128.3 - 1, -121 + 0.5]  # min/max_latitude, min/max_longitude
    cascadia_box = [40 - 1, 51.8, -128.3, -121]  # min/max_latitude, min/max_longitude
    if extended:
        cascadia_box = [40 - 1, 51.8, -128.3, -119]
    return cascadia_box


def read_from_slab2(max_depth=60):
    """Reads the slab2 grid and returns depth, strike, dip, and positions of points on the slab
    such that the depth is less than 60 km."""
    longitude_correction = [-360, 0, 0]
    depth_grid = np.loadtxt('geo_data/slab2/cas_slab2_dep_02.24.18.xyz', delimiter=',') + longitude_correction
    dip_grid = np.loadtxt('geo_data/slab2/cas_slab2_dip_02.24.18.xyz', delimiter=',') + longitude_correction
    strike_grid = np.loadtxt('geo_data/slab2/cas_slab2_str_02.24.18.xyz', delimiter=',') + longitude_correction

    ind_depth = np.where(depth_grid[:, 2] > - max_depth)[0]
    region = [np.min(depth_grid[:, 1][ind_depth]), np.max(depth_grid[:, 1][ind_depth]),
              np.min(depth_grid[:, 0][ind_depth]),
              np.max(depth_grid[:, 0][ind_depth])]  # min/max_latitude, min/max_longitude
    # 13322 -> deep
    # 13315 -> shallow
    # 110904 -> nan

    '''cascadia_box = [40 - 1, 51.8 - 0.2, -128.3 - 1, -121 + 0.5]  # min/max_latitude, min/max_longitude
    cascadia_map = Basemap(llcrnrlon=cascadia_box[2], llcrnrlat=cascadia_box[0],
                           urcrnrlon=cascadia_box[3], urcrnrlat=cascadia_box[1],
                           lat_0=cascadia_box[0], lon_0=cascadia_box[2],
                           resolution='i', projection='tmerc')
    # cascadia_map.drawmapboundary(fill_color='aqua')
    # cascadia_map.fillcontinents(color='grey', lake_color='aqua')
    cascadia_map.drawcoastlines()

    cascadia_map.drawparallels(np.arange(cascadia_box[0], cascadia_box[1], (cascadia_box[1] - cascadia_box[0]) / 4),
                               labels=[0, 1, 0, 0])
    cascadia_map.drawmeridians(np.arange(cascadia_box[2], cascadia_box[3], (cascadia_box[3] - cascadia_box[2]) / 4),
                               labels=[0, 0, 0, 1])
    # x, y = cascadia_map(depth_grid[:, 0][ind_depth], depth_grid[:, 1][ind_depth])
    # x, y = cascadia_map(dip_grid[:, 0][ind_depth], dip_grid[:, 1][ind_depth])
    x, y = cascadia_map(strike_grid[:, 0][ind_depth], strike_grid[:, 1][ind_depth])
    # plt.scatter(x, y, c=depth_grid[:, 2][ind_depth])
    # plt.scatter(x, y, c=dip_grid[:, 2][ind_depth])
    plt.scatter(x, y, c=strike_grid[:, 2][ind_depth])
    cmap = plt.colorbar()
    # cmap.set_label('Depth [km]')
    # cmap.set_label('Dip [°]')
    cmap.set_label('Strike [°]')
    plt.tight_layout()
    plt.show()'''
    return depth_grid[ind_depth], strike_grid[ind_depth], dip_grid[ind_depth], region


def cascadia_filtered_stations(n_selected_stations, reference_period=(2007, 2023)):
    """Removes meaningless stations and returns the corresponding indices 'stations_subset_full',
    referring to the whole cascadia network."""
    gnss_data, _, _, _ = _preliminary_operations(reference_period, detrend=False)
    full_station_codes, full_station_coordinates = cascadia_coordinates()
    stations_to_remove = ['WSLB', 'YBHB', 'P687', 'BELI', 'PMAR', 'TGUA', 'OYLR', 'FTS5', 'RPT5', 'RPT6', 'P791',
                          'P674', 'P656', 'TWRI', 'WIFR', 'FRID', 'PNHG', 'COUR', 'SKMA', 'CSHR', 'HGP1', 'CBLV',
                          'PNHR', 'NCS2', 'TSEP', 'BCSC', 'LNG2']
    station_codes, station_coordinates, selected_gnss_data = _remove_stations(stations_to_remove, full_station_codes,
                                                                              full_station_coordinates, gnss_data)
    original_nan_pattern = np.isnan(selected_gnss_data[:, :, 0])
    n_nans_stations = original_nan_pattern.sum(axis=1)
    stations_subset = np.sort(np.argsort(n_nans_stations)[:n_selected_stations])
    station_codes_subset, station_coordinates_subset = np.array(station_codes)[stations_subset], station_coordinates[
                                                                                                 stations_subset, :]
    stations_subset_full = np.nonzero(np.in1d(full_station_codes, station_codes_subset))[0]
    return station_codes_subset, station_coordinates_subset, station_codes, station_coordinates, stations_subset_full


def _train_contour(cat):
    alpha_shape = alphashape.alphashape(cat[:, :2], alpha=1.9)
    hull_pts = alpha_shape.exterior.coords.xy
    x_hull, y_hull = np.array(hull_pts[1]), np.array(hull_pts[0])
    # valid_pts_hull = np.where(x_hull < -121.95)[0]  # remove knot
    # x_hull, y_hull = x_hull[valid_pts_hull], y_hull[valid_pts_hull]
    dist = np.sqrt((x_hull[:-1] - x_hull[1:]) ** 2 + (y_hull[:-1] - y_hull[1:]) ** 2)
    dist_along = np.concatenate(([0], dist.cumsum()))
    spline, u = interpolate.splprep([x_hull, y_hull], u=dist_along, s=0, per=1)
    interp_d_cat = np.linspace(dist_along[0], dist_along[-1], 350)
    interp_x_cat, interp_y_cat = interpolate.splev(interp_d_cat, spline)
    return interp_x_cat, interp_y_cat


def grid(x, y, z, resX=100, resY=100, method='linear'):
    xi = np.linspace(min(x), max(x), resX)
    yi = np.linspace(min(y), max(y), resY)
    X, Y = np.meshgrid(xi, yi)
    non_nan_idx = ~np.isnan(z)
    x, y, z = np.array(x), np.array(y), np.array(z)
    if method == 'cubic':
        interp = CloughTocher2DInterpolator(list(zip(x[non_nan_idx], y[non_nan_idx])), z[non_nan_idx])
        Z = interp(X, Y)
    elif method == 'linear':
        Z = griddata((x, y), z, (X, Y), method=method)
    else:
        Z = np.nan
    return X, Y, Z


def tremor_catalogue(north=False):
    """Return tremors from PNSN + Ide (2012) tremor catalogue.
    The dates are in the JPL-style format."""
    tremors = []
    date_table = ymd_decimal_year_lookup()
    catalogue_names = ['tremor_catalogue', 'tremor_catalogue2']
    for name in catalogue_names:
        with open(f'geo_data/{name}.txt') as f:
            next(f)
            for line in f:
                splitted_line = line.split(',')
                latitude = float(splitted_line[0].replace(' ', ''))
                longitude = float(splitted_line[1].replace(' ', ''))
                depth = float(splitted_line[2].replace(' ', ''))
                time = splitted_line[3][1:].split(' ')[0]
                year, month, day = time.split('-')
                year, month, day = float(year), float(month), float(day)
                decimal_date = date_table[(year, month, day)]
                tremors.append([latitude, longitude, depth, decimal_date])
    with open(f'geo_data/tremor_catalogue_ide.txt') as f:
        for line in f:
            splitted_line = line.split(',')
            latitude = float(splitted_line[2].replace(' ', ''))
            longitude = float(splitted_line[3].replace(' ', ''))
            depth = float(splitted_line[4].replace(' ', ''))
            year, month, day = splitted_line[0].split('-')
            year, month, day = float(year), float(month), float(day)
            decimal_date = date_table[(year, month, day)]
            tremors.append([latitude, longitude, depth, decimal_date])
    tremor_array = np.array(tremors)
    if north:
        valid_tremor_events = np.where(tremor_array[:, 0] >= 47.)[0]
        tremor_array = tremor_array[valid_tremor_events]
    return tremor_array


def sse_catalogue(return_magnitude=False, north=False, south=False):
    """Returns catalogued slow slip events from Michel et al, 2019, having the following format: start_date, end_date.
    Magnitude is returned too, according to the specified keyword parameter."""
    if return_magnitude:
        cols_to_use = (2, 3, 5)
    else:
        cols_to_use = (2, 3)
    filename = 'geo_data/catalogue_sse_michel_cascadia_mw.txt'
    catalog = np.loadtxt(filename, skiprows=1, delimiter=',', usecols=cols_to_use)
    if north:
        valid_michel_events_north = [15, 18, 20, 22, 27, 33, 35, 37]
        catalog = catalog[valid_michel_events_north]
    if south:
        valid_michel_events_south = list(set([i for i in range(len(catalog))]).difference({15, 18, 20, 22, 27, 33, 35, 37}))
        catalog = catalog[valid_michel_events_south]
    return catalog

def get_n_tremors_per_day(time_span, tremor_catalogue):
    cascadia_box = [40 - 1, 51.8 - 0.2, -128.3 - 1, -121 + 0.5]  # min/max_latitude, min/max_longitude
    lat_cut = \
        np.where(np.logical_and(tremor_catalogue[:, 0] > cascadia_box[0], tremor_catalogue[:, 0] < cascadia_box[1]))[0]
    # time_cut = np.where(np.logical_and(tremor_catalogue[:, 3][lat_cut] > time_span[0], tremor_catalogue[:, 3][lat_cut] < time_span[-1]))[0]
    time_cut = np.where(np.logical_and(tremor_catalogue[:, 3][lat_cut] >= time_span[0],
                                       tremor_catalogue[:, 3][lat_cut] <= time_span[-1]))[0]
    tremors = tremor_catalogue[time_cut]
    unique, counts = np.unique(tremors[:, 3], return_counts=True)
    extended_counts = np.zeros(time_span.shape)
    for i in range(unique.shape[0]):
        idx_time = np.argwhere(time_span == unique[i])
        extended_counts[idx_time] = counts[i]
    return extended_counts

def overlap_percentage(xlist, ylist):
    min1 = min(xlist)
    max1 = max(xlist)
    min2 = min(ylist)
    max2 = max(ylist)

    overlap = max(0, min(max1, max2) - max(min1, min2))
    length = max1 - min1 + max2 - min2
    lengthx = max1 - min1
    lengthy = max2 - min2
    return 2 * overlap / length * 100, overlap / lengthx * 100, overlap / lengthy * 100
